<?php
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnusedAliasInspection */

namespace LaravelIdea\Helper {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Kalnoy\Nestedset\Collection;
    use Kalnoy\Nestedset\QueryBuilder;
    
    /**
     * @method \Category shift()
     * @method \Category pop()
     * @method \Category get($key, $default = null)
     * @method \Category pull($key, $default = null)
     * @method \Category first(callable $callback = null, $default = null)
     * @method \Category firstWhere(string $key, $operator = null, $value = null)
     * @method \Category find($key, $default = null)
     * @method \Category[] all()
     * @method \Category last(callable $callback = null, $default = null)
     * @method \Category sole($key = null, $operator = null, $value = null)
     * @method $this toTree($root = false)
     * @method $this toFlatTree(bool $root = false)
     * @method $this linkNodes()
     * @mixin Collection
     */
    class _IH_Category_C extends _BaseCollection {
        /**
         * @param int $size
         * @return \Category[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Category_QB whereId($value)
     * @method _IH_Category_QB wherePosition($value)
     * @method _IH_Category_QB whereImage($value)
     * @method _IH_Category_QB whereStatus($value)
     * @method _IH_Category_QB whereLft($value)
     * @method _IH_Category_QB whereRgt($value)
     * @method _IH_Category_QB whereParentId($value)
     * @method _IH_Category_QB whereCreatedAt($value)
     * @method _IH_Category_QB whereUpdatedAt($value)
     * @method _IH_Category_QB whereDisplayMode($value)
     * @method _IH_Category_QB whereCategoryIconPath($value)
     * @method _IH_Category_QB whereAdditional($value)
     * @method \Category baseSole(array|string $columns = ['*'])
     * @method \Category create(array $attributes = [])
     * @method _IH_Category_C|\Category[] cursor()
     * @method \Category|null|_IH_Category_C|\Category[] find($id, array $columns = ['*'])
     * @method _IH_Category_C|\Category[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method \Category|_IH_Category_C|\Category[] findOrFail($id, array $columns = ['*'])
     * @method \Category|_IH_Category_C|\Category[] findOrNew($id, array $columns = ['*'])
     * @method \Category first(array|string $columns = ['*'])
     * @method \Category firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method \Category firstOrCreate(array $attributes = [], array $values = [])
     * @method \Category firstOrFail(array $columns = ['*'])
     * @method \Category firstOrNew(array $attributes = [], array $values = [])
     * @method \Category firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method \Category forceCreate(array $attributes)
     * @method _IH_Category_C|\Category[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Category_C|\Category[] get(array|string $columns = ['*'])
     * @method \Category getModel()
     * @method \Category[] getModels(array|string $columns = ['*'])
     * @method _IH_Category_C|\Category[] hydrate(array $items)
     * @method \Category make(array $attributes = [])
     * @method \Category newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|\Category[]|_IH_Category_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|\Category[]|_IH_Category_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method \Category sole(array|string $columns = ['*'])
     * @method \Category updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Category_QB withTrashed()
     * @method _IH_Category_QB onlyTrashed()
     * @method _IH_Category_QB withoutTrashed()
     * @method _IH_Category_QB restore()
     * @method _IH_Category_QB orWhereAncestorOf($id, bool $andSelf = false)
     * @method _IH_Category_QB hasParent()
     * @method _IH_Category_QB root(array $columns = ['*'])
     * @method _IH_Category_QB whereDescendantOf($id, string $boolean = 'and', bool $not = false, bool $andSelf = false)
     * @method _IH_Category_QB whereIsLeaf()
     * @method _IH_Category_QB hasChildren()
     * @method _IH_Category_QB whereIsAfter($id, string $boolean = 'and')
     * @method _IH_Category_QB defaultOrder(string $dir = 'asc')
     * @method _IH_Category_QB withDepth(string $as = 'depth')
     * @method _IH_Category_QB whereIsRoot()
     * @method _IH_Category_QB orWhereNodeBetween(array $values)
     * @method _IH_Category_QB orWhereDescendantOf($id)
     * @method _IH_Category_QB whereDescendantOrSelf($id, string $boolean = 'and', bool $not = false)
     * @method _IH_Category_QB whereNodeBetween(array $values, string $boolean = 'and', bool $not = false)
     * @method _IH_Category_QB whereNotDescendantOf($id)
     * @method _IH_Category_QB reversed()
     * @method _IH_Category_QB whereIsBefore($id, string $boolean = 'and')
     * @method _IH_Category_QB whereAncestorOf($id, bool $andSelf = false, string $boolean = 'and')
     * @method _IH_Category_QB withoutRoot()
     * @method _IH_Category_QB whereAncestorOrSelf($id)
     * @method _IH_Category_QB applyNestedSetScope(null|string $table = null)
     * @method _IH_Category_QB orWhereNotDescendantOf($id)
     * @mixin QueryBuilder
     */
    class _IH_Category_QB extends _BaseBuilder {}
    
    /**
     * @method \DuplicateCategory shift()
     * @method \DuplicateCategory pop()
     * @method \DuplicateCategory get($key, $default = null)
     * @method \DuplicateCategory pull($key, $default = null)
     * @method \DuplicateCategory first(callable $callback = null, $default = null)
     * @method \DuplicateCategory firstWhere(string $key, $operator = null, $value = null)
     * @method \DuplicateCategory find($key, $default = null)
     * @method \DuplicateCategory[] all()
     * @method \DuplicateCategory last(callable $callback = null, $default = null)
     * @method \DuplicateCategory sole($key = null, $operator = null, $value = null)
     * @method $this toTree($root = false)
     * @method $this toFlatTree(bool $root = false)
     * @method $this linkNodes()
     * @mixin Collection
     */
    class _IH_DuplicateCategory_C extends _BaseCollection {
        /**
         * @param int $size
         * @return \DuplicateCategory[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_DuplicateCategory_QB whereId($value)
     * @method _IH_DuplicateCategory_QB wherePosition($value)
     * @method _IH_DuplicateCategory_QB whereImage($value)
     * @method _IH_DuplicateCategory_QB whereStatus($value)
     * @method _IH_DuplicateCategory_QB whereLft($value)
     * @method _IH_DuplicateCategory_QB whereRgt($value)
     * @method _IH_DuplicateCategory_QB whereParentId($value)
     * @method _IH_DuplicateCategory_QB whereCreatedAt($value)
     * @method _IH_DuplicateCategory_QB whereUpdatedAt($value)
     * @method _IH_DuplicateCategory_QB whereDisplayMode($value)
     * @method _IH_DuplicateCategory_QB whereCategoryIconPath($value)
     * @method _IH_DuplicateCategory_QB whereAdditional($value)
     * @method \DuplicateCategory baseSole(array|string $columns = ['*'])
     * @method \DuplicateCategory create(array $attributes = [])
     * @method _IH_DuplicateCategory_C|\DuplicateCategory[] cursor()
     * @method \DuplicateCategory|null|_IH_DuplicateCategory_C|\DuplicateCategory[] find($id, array $columns = ['*'])
     * @method _IH_DuplicateCategory_C|\DuplicateCategory[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method \DuplicateCategory|_IH_DuplicateCategory_C|\DuplicateCategory[] findOrFail($id, array $columns = ['*'])
     * @method \DuplicateCategory|_IH_DuplicateCategory_C|\DuplicateCategory[] findOrNew($id, array $columns = ['*'])
     * @method \DuplicateCategory first(array|string $columns = ['*'])
     * @method \DuplicateCategory firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method \DuplicateCategory firstOrCreate(array $attributes = [], array $values = [])
     * @method \DuplicateCategory firstOrFail(array $columns = ['*'])
     * @method \DuplicateCategory firstOrNew(array $attributes = [], array $values = [])
     * @method \DuplicateCategory firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method \DuplicateCategory forceCreate(array $attributes)
     * @method _IH_DuplicateCategory_C|\DuplicateCategory[] fromQuery(string $query, array $bindings = [])
     * @method _IH_DuplicateCategory_C|\DuplicateCategory[] get(array|string $columns = ['*'])
     * @method \DuplicateCategory getModel()
     * @method \DuplicateCategory[] getModels(array|string $columns = ['*'])
     * @method _IH_DuplicateCategory_C|\DuplicateCategory[] hydrate(array $items)
     * @method \DuplicateCategory make(array $attributes = [])
     * @method \DuplicateCategory newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|\DuplicateCategory[]|_IH_DuplicateCategory_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|\DuplicateCategory[]|_IH_DuplicateCategory_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method \DuplicateCategory sole(array|string $columns = ['*'])
     * @method \DuplicateCategory updateOrCreate(array $attributes, array $values = [])
     * @method _IH_DuplicateCategory_QB orWhereAncestorOf($id, bool $andSelf = false)
     * @method _IH_DuplicateCategory_QB hasParent()
     * @method _IH_DuplicateCategory_QB root(array $columns = ['*'])
     * @method _IH_DuplicateCategory_QB whereDescendantOf($id, string $boolean = 'and', bool $not = false, bool $andSelf = false)
     * @method _IH_DuplicateCategory_QB whereIsLeaf()
     * @method _IH_DuplicateCategory_QB hasChildren()
     * @method _IH_DuplicateCategory_QB whereIsAfter($id, string $boolean = 'and')
     * @method _IH_DuplicateCategory_QB defaultOrder(string $dir = 'asc')
     * @method _IH_DuplicateCategory_QB withDepth(string $as = 'depth')
     * @method _IH_DuplicateCategory_QB whereIsRoot()
     * @method _IH_DuplicateCategory_QB orWhereNodeBetween(array $values)
     * @method _IH_DuplicateCategory_QB orWhereDescendantOf($id)
     * @method _IH_DuplicateCategory_QB whereDescendantOrSelf($id, string $boolean = 'and', bool $not = false)
     * @method _IH_DuplicateCategory_QB whereNodeBetween(array $values, string $boolean = 'and', bool $not = false)
     * @method _IH_DuplicateCategory_QB whereNotDescendantOf($id)
     * @method _IH_DuplicateCategory_QB reversed()
     * @method _IH_DuplicateCategory_QB whereIsBefore($id, string $boolean = 'and')
     * @method _IH_DuplicateCategory_QB whereAncestorOf($id, bool $andSelf = false, string $boolean = 'and')
     * @method _IH_DuplicateCategory_QB withoutRoot()
     * @method _IH_DuplicateCategory_QB whereAncestorOrSelf($id)
     * @method _IH_DuplicateCategory_QB applyNestedSetScope(null|string $table = null)
     * @method _IH_DuplicateCategory_QB orWhereNotDescendantOf($id)
     * @mixin QueryBuilder
     */
    class _IH_DuplicateCategory_QB extends _BaseBuilder {}
    
    /**
     * @method \MenuItem shift()
     * @method \MenuItem pop()
     * @method \MenuItem get($key, $default = null)
     * @method \MenuItem pull($key, $default = null)
     * @method \MenuItem first(callable $callback = null, $default = null)
     * @method \MenuItem firstWhere(string $key, $operator = null, $value = null)
     * @method \MenuItem find($key, $default = null)
     * @method \MenuItem[] all()
     * @method \MenuItem last(callable $callback = null, $default = null)
     * @method \MenuItem sole($key = null, $operator = null, $value = null)
     * @method $this toTree($root = false)
     * @method $this toFlatTree(bool $root = false)
     * @method $this linkNodes()
     * @mixin Collection
     */
    class _IH_MenuItem_C extends _BaseCollection {
        /**
         * @param int $size
         * @return \MenuItem[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method \MenuItem baseSole(array|string $columns = ['*'])
     * @method \MenuItem create(array $attributes = [])
     * @method _IH_MenuItem_C|\MenuItem[] cursor()
     * @method \MenuItem|null|_IH_MenuItem_C|\MenuItem[] find($id, array $columns = ['*'])
     * @method _IH_MenuItem_C|\MenuItem[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method \MenuItem|_IH_MenuItem_C|\MenuItem[] findOrFail($id, array $columns = ['*'])
     * @method \MenuItem|_IH_MenuItem_C|\MenuItem[] findOrNew($id, array $columns = ['*'])
     * @method \MenuItem first(array|string $columns = ['*'])
     * @method \MenuItem firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method \MenuItem firstOrCreate(array $attributes = [], array $values = [])
     * @method \MenuItem firstOrFail(array $columns = ['*'])
     * @method \MenuItem firstOrNew(array $attributes = [], array $values = [])
     * @method \MenuItem firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method \MenuItem forceCreate(array $attributes)
     * @method _IH_MenuItem_C|\MenuItem[] fromQuery(string $query, array $bindings = [])
     * @method _IH_MenuItem_C|\MenuItem[] get(array|string $columns = ['*'])
     * @method \MenuItem getModel()
     * @method \MenuItem[] getModels(array|string $columns = ['*'])
     * @method _IH_MenuItem_C|\MenuItem[] hydrate(array $items)
     * @method \MenuItem make(array $attributes = [])
     * @method \MenuItem newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|\MenuItem[]|_IH_MenuItem_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|\MenuItem[]|_IH_MenuItem_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method \MenuItem sole(array|string $columns = ['*'])
     * @method \MenuItem updateOrCreate(array $attributes, array $values = [])
     * @method _IH_MenuItem_QB orWhereAncestorOf($id, bool $andSelf = false)
     * @method _IH_MenuItem_QB hasParent()
     * @method _IH_MenuItem_QB root(array $columns = ['*'])
     * @method _IH_MenuItem_QB whereDescendantOf($id, string $boolean = 'and', bool $not = false, bool $andSelf = false)
     * @method _IH_MenuItem_QB whereIsLeaf()
     * @method _IH_MenuItem_QB hasChildren()
     * @method _IH_MenuItem_QB whereIsAfter($id, string $boolean = 'and')
     * @method _IH_MenuItem_QB defaultOrder(string $dir = 'asc')
     * @method _IH_MenuItem_QB withDepth(string $as = 'depth')
     * @method _IH_MenuItem_QB whereIsRoot()
     * @method _IH_MenuItem_QB orWhereNodeBetween(array $values)
     * @method _IH_MenuItem_QB orWhereDescendantOf($id)
     * @method _IH_MenuItem_QB whereDescendantOrSelf($id, string $boolean = 'and', bool $not = false)
     * @method _IH_MenuItem_QB whereNodeBetween(array $values, string $boolean = 'and', bool $not = false)
     * @method _IH_MenuItem_QB whereNotDescendantOf($id)
     * @method _IH_MenuItem_QB reversed()
     * @method _IH_MenuItem_QB whereIsBefore($id, string $boolean = 'and')
     * @method _IH_MenuItem_QB whereAncestorOf($id, bool $andSelf = false, string $boolean = 'and')
     * @method _IH_MenuItem_QB withoutRoot()
     * @method _IH_MenuItem_QB whereAncestorOrSelf($id)
     * @method _IH_MenuItem_QB applyNestedSetScope(null|string $table = null)
     * @method _IH_MenuItem_QB orWhereNotDescendantOf($id)
     * @mixin QueryBuilder
     */
    class _IH_MenuItem_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\ConnectionInterface;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Query\Expression;
    
    /**
     * @see \Illuminate\Database\Query\Builder::whereIn
     * @method $this whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereNotIn
     * @method $this orWhereNotIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::selectRaw
     * @method $this selectRaw(string $expression, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::insertOrIgnore
     * @method int insertOrIgnore(array $values)
     * @see \Illuminate\Database\Query\Builder::unionAll
     * @method $this unionAll(\Closure|\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::orWhereNull
     * @method $this orWhereNull(string $column)
     * @see \Illuminate\Database\Query\Builder::joinWhere
     * @method $this joinWhere(string $table, \Closure|string $first, string $operator, string $second, string $type = 'inner')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonContains
     * @method $this orWhereJsonContains(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::orderBy
     * @method $this orderBy(\Closure|\Illuminate\Database\Query\Builder|Expression|string $column, string $direction = 'asc')
     * @see \Illuminate\Database\Query\Builder::raw
     * @method Expression raw($value)
     * @see \Illuminate\Database\Concerns\BuildsQueries::each
     * @method $this each(callable $callback, int $count = 1000)
     * @see \Illuminate\Database\Query\Builder::setBindings
     * @method $this setBindings(array $bindings, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonLength
     * @method $this orWhereJsonLength(string $column, $operator, $value = null)
     * @see \Illuminate\Database\Query\Builder::whereRowValues
     * @method $this whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereNotExists
     * @method $this orWhereNotExists(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::orWhereIntegerInRaw
     * @method $this orWhereIntegerInRaw(string $column, array|Arrayable $values)
     * @see \Illuminate\Database\Query\Builder::newQuery
     * @method $this newQuery()
     * @see \Illuminate\Database\Query\Builder::rightJoinSub
     * @method $this rightJoinSub(\Closure|Builder|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::crossJoin
     * @method $this crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::average
     * @method mixed average(string $column)
     * @see \Illuminate\Database\Query\Builder::existsOr
     * @method $this existsOr(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::sum
     * @method int|mixed sum(string $column)
     * @see \Illuminate\Database\Query\Builder::havingRaw
     * @method $this havingRaw(string $sql, array $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Concerns\BuildsQueries::chunkMap
     * @method $this chunkMap(callable $callback, int $count = 1000)
     * @see \Illuminate\Database\Query\Builder::getRawBindings
     * @method $this getRawBindings()
     * @see \Illuminate\Database\Query\Builder::orWhereColumn
     * @method $this orWhereColumn(array|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::min
     * @method mixed min(string $column)
     * @see \Illuminate\Database\Concerns\BuildsQueries::unless
     * @method $this unless($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereNotIn
     * @method $this whereNotIn(string $column, $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereTime
     * @method $this whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::insertUsing
     * @method int insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @see \Illuminate\Database\Concerns\BuildsQueries::lazyById
     * @method $this lazyById($chunkSize = 1000, null|string $column = null, null|string $alias = null)
     * @see \Illuminate\Database\Query\Builder::rightJoinWhere
     * @method $this rightJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::union
     * @method $this union(\Closure|\Illuminate\Database\Query\Builder $query, bool $all = false)
     * @see \Illuminate\Database\Query\Builder::groupBy
     * @method $this groupBy(...$groups)
     * @see \Illuminate\Database\Query\Builder::orWhereDay
     * @method $this orWhereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::joinSub
     * @method $this joinSub(\Closure|Builder|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::selectSub
     * @method $this selectSub(\Closure|Builder|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::dd
     * @method void dd()
     * @see \Illuminate\Database\Query\Builder::whereNull
     * @method $this whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::prepareValueAndOperator
     * @method $this prepareValueAndOperator(string $value, string $operator, bool $useDefault = false)
     * @see \Illuminate\Database\Query\Builder::whereIntegerNotInRaw
     * @method $this whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereRaw
     * @method $this orWhereRaw(string $sql, $bindings = [])
     * @see \Illuminate\Database\Query\Builder::whereJsonContains
     * @method $this whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereBetweenColumns
     * @method $this orWhereBetweenColumns(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::mergeWheres
     * @method $this mergeWheres(array $wheres, array $bindings)
     * @see \Illuminate\Database\Query\Builder::sharedLock
     * @method $this sharedLock()
     * @see \Illuminate\Database\Query\Builder::orderByRaw
     * @method $this orderByRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::doesntExist
     * @method bool doesntExist()
     * @see \Illuminate\Database\Query\Builder::orWhereMonth
     * @method $this orWhereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::whereNotNull
     * @method $this whereNotNull(array|string $columns, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::count
     * @method int count(string $columns = '*')
     * @see \Illuminate\Database\Query\Builder::orWhereNotBetween
     * @method $this orWhereNotBetween(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::fromRaw
     * @method $this fromRaw(string $expression, $bindings = [])
     * @see \Illuminate\Support\Traits\Macroable::mixin
     * @method $this mixin(object $mixin, bool $replace = true)
     * @see \Illuminate\Database\Query\Builder::take
     * @method $this take(int $value)
     * @see \Illuminate\Database\Query\Builder::orWhereNotBetweenColumns
     * @method $this orWhereNotBetweenColumns(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::updateOrInsert
     * @method $this updateOrInsert(array $attributes, array $values = [])
     * @see \Illuminate\Database\Query\Builder::cloneWithout
     * @method $this cloneWithout(array $properties)
     * @see \Illuminate\Database\Query\Builder::whereBetweenColumns
     * @method $this whereBetweenColumns(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::fromSub
     * @method $this fromSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::cleanBindings
     * @method $this cleanBindings(array $bindings)
     * @see \Illuminate\Database\Query\Builder::orWhereDate
     * @method $this orWhereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::avg
     * @method mixed avg(string $column)
     * @see \Illuminate\Database\Query\Builder::addBinding
     * @method $this addBinding($value, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::getGrammar
     * @method $this getGrammar()
     * @see \Illuminate\Database\Query\Builder::lockForUpdate
     * @method $this lockForUpdate()
     * @see \Illuminate\Database\Concerns\BuildsQueries::eachById
     * @method $this eachById(callable $callback, int $count = 1000, null|string $column = null, null|string $alias = null)
     * @see \Illuminate\Database\Query\Builder::cloneWithoutBindings
     * @method $this cloneWithoutBindings(array $except)
     * @see \Illuminate\Database\Query\Builder::orHavingRaw
     * @method $this orHavingRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::forPageBeforeId
     * @method $this forPageBeforeId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::orWhereBetween
     * @method $this orWhereBetween(string $column, array $values)
     * @see \Illuminate\Database\Concerns\ExplainsQueries::explain
     * @method $this explain()
     * @see \Illuminate\Database\Query\Builder::select
     * @method $this select(array|mixed $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::addSelect
     * @method $this addSelect(array|mixed $column)
     * @see \Illuminate\Database\Concerns\BuildsQueries::when
     * @method $this when($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereJsonLength
     * @method $this whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereExists
     * @method $this orWhereExists(\Closure $callback, bool $not = false)
     * @see \Illuminate\Database\Query\Builder::truncate
     * @method $this truncate()
     * @see \Illuminate\Database\Query\Builder::lock
     * @method $this lock(bool|string $value = true)
     * @see \Illuminate\Database\Query\Builder::join
     * @method $this join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::whereMonth
     * @method $this whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::having
     * @method $this having(string $column, null|string $operator = null, null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereNested
     * @method $this whereNested(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereRowValues
     * @method $this orWhereRowValues(array $columns, string $operator, array $values)
     * @see \Illuminate\Database\Query\Builder::useWritePdo
     * @method $this useWritePdo()
     * @see \Illuminate\Database\Query\Builder::orWhereIn
     * @method $this orWhereIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::orderByDesc
     * @method $this orderByDesc(string $column)
     * @see \Illuminate\Database\Query\Builder::orWhereNotNull
     * @method $this orWhereNotNull(string $column)
     * @see \Illuminate\Database\Query\Builder::getProcessor
     * @method $this getProcessor()
     * @see \Illuminate\Database\Concerns\BuildsQueries::lazy
     * @method $this lazy(int $chunkSize = 1000)
     * @see \Illuminate\Database\Query\Builder::skip
     * @method $this skip(int $value)
     * @see \Illuminate\Database\Query\Builder::leftJoinWhere
     * @method $this leftJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::doesntExistOr
     * @method $this doesntExistOr(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::whereNotExists
     * @method $this whereNotExists(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereIntegerInRaw
     * @method $this whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::whereDay
     * @method $this whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::forNestedWhere
     * @method $this forNestedWhere()
     * @see \Illuminate\Database\Query\Builder::max
     * @method mixed max(string $column)
     * @see \Illuminate\Database\Query\Builder::whereExists
     * @method $this whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::inRandomOrder
     * @method $this inRandomOrder(string $seed = '')
     * @see \Illuminate\Database\Query\Builder::havingBetween
     * @method $this havingBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereYear
     * @method $this orWhereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null)
     * @see \Illuminate\Database\Concerns\BuildsQueries::chunkById
     * @method $this chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @see \Illuminate\Database\Query\Builder::whereDate
     * @method $this whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereJsonDoesntContain
     * @method $this whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::forPageAfterId
     * @method $this forPageAfterId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::forPage
     * @method $this forPage(int $page, int $perPage = 15)
     * @see \Illuminate\Database\Query\Builder::exists
     * @method bool exists()
     * @see \Illuminate\Support\Traits\Macroable::macroCall
     * @method $this macroCall(string $method, array $parameters)
     * @see \Illuminate\Database\Concerns\BuildsQueries::first
     * @method $this first(array|string $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereColumn
     * @method $this whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::numericAggregate
     * @method $this numericAggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereNotBetween
     * @method $this whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getConnection
     * @method ConnectionInterface getConnection()
     * @see \Illuminate\Database\Query\Builder::mergeBindings
     * @method $this mergeBindings(\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::orWhereJsonDoesntContain
     * @method $this orWhereJsonDoesntContain(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::leftJoinSub
     * @method $this leftJoinSub(\Closure|Builder|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::crossJoinSub
     * @method $this crossJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::limit
     * @method $this limit(int $value)
     * @see \Illuminate\Database\Query\Builder::from
     * @method $this from(\Closure|\Illuminate\Database\Query\Builder|string $table, null|string $as = null)
     * @see \Illuminate\Database\Query\Builder::whereNotBetweenColumns
     * @method $this whereNotBetweenColumns(string $column, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::insertGetId
     * @method int insertGetId(array $values, null|string $sequence = null)
     * @see \Illuminate\Database\Query\Builder::whereBetween
     * @method $this whereBetween(Expression|string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Concerns\BuildsQueries::tap
     * @method $this tap(callable $callback)
     * @see \Illuminate\Database\Query\Builder::offset
     * @method $this offset(int $value)
     * @see \Illuminate\Database\Query\Builder::addNestedWhereQuery
     * @method $this addNestedWhereQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::rightJoin
     * @method $this rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::leftJoin
     * @method $this leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::insert
     * @method bool insert(array $values)
     * @see \Illuminate\Database\Query\Builder::distinct
     * @method $this distinct()
     * @see \Illuminate\Database\Concerns\BuildsQueries::chunk
     * @method $this chunk(int $count, callable $callback)
     * @see \Illuminate\Database\Query\Builder::reorder
     * @method $this reorder(null|string $column = null, string $direction = 'asc')
     * @see \Illuminate\Database\Query\Builder::whereYear
     * @method $this whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getCountForPagination
     * @method $this getCountForPagination(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::groupByRaw
     * @method $this groupByRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::orWhereIntegerNotInRaw
     * @method $this orWhereIntegerNotInRaw(string $column, array|Arrayable $values)
     * @see \Illuminate\Database\Query\Builder::aggregate
     * @method $this aggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::dump
     * @method void dump()
     * @see \Illuminate\Database\Query\Builder::implode
     * @method $this implode(string $column, string $glue = '')
     * @see \Illuminate\Database\Query\Builder::addWhereExistsQuery
     * @method $this addWhereExistsQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Support\Traits\Macroable::macro
     * @method $this macro(string $name, callable|object $macro)
     * @see \Illuminate\Database\Query\Builder::whereRaw
     * @method $this whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::toSql
     * @method string toSql()
     * @see \Illuminate\Database\Query\Builder::orHaving
     * @method $this orHaving(string $column, null|string $operator = null, null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::getBindings
     * @method array getBindings()
     * @see \Illuminate\Database\Query\Builder::orWhereTime
     * @method $this orWhereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::dynamicWhere
     * @method $this dynamicWhere(string $method, array $parameters)
     */
    class _BaseBuilder extends Builder {}
    
    /**
     * @method \Illuminate\Support\Collection mapSpread(callable $callback)
     * @method \Illuminate\Support\Collection mapWithKeys(callable $callback)
     * @method \Illuminate\Support\Collection zip(array $items)
     * @method \Illuminate\Support\Collection partition(callable|string $key, $operator = null, $value = null)
     * @method \Illuminate\Support\Collection mapInto(string $class)
     * @method \Illuminate\Support\Collection mapToGroups(callable $callback)
     * @method \Illuminate\Support\Collection map(callable $callback)
     * @method \Illuminate\Support\Collection groupBy(array|callable|string $groupBy, bool $preserveKeys = false)
     * @method \Illuminate\Support\Collection pluck(array|string $value, null|string $key = null)
     * @method \Illuminate\Support\Collection pad(int $size, $value)
     * @method \Illuminate\Support\Collection split(int $numberOfGroups)
     * @method \Illuminate\Support\Collection combine($values)
     * @method \Illuminate\Support\Collection countBy(callable|string $countBy = null)
     * @method \Illuminate\Support\Collection mapToDictionary(callable $callback)
     * @method \Illuminate\Support\Collection keys()
     * @method \Illuminate\Support\Collection transform(callable $callback)
     * @method \Illuminate\Support\Collection flatMap(callable $callback)
     * @method \Illuminate\Support\Collection collapse()
     */
    class _BaseCollection extends \Illuminate\Database\Eloquent\Collection {}
}

namespace LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent {

    use Astrotomic\Translatable\Tests\Eloquent\Country;
    use Astrotomic\Translatable\Tests\Eloquent\CountryStrict;
    use Astrotomic\Translatable\Tests\Eloquent\CountryTranslation;
    use Astrotomic\Translatable\Tests\Eloquent\Person;
    use Astrotomic\Translatable\Tests\Eloquent\PersonTranslation;
    use Astrotomic\Translatable\Tests\Eloquent\StrictTranslation;
    use Astrotomic\Translatable\Tests\Eloquent\Vegetable;
    use Astrotomic\Translatable\Tests\Eloquent\VegetableTranslation;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method CountryStrict shift()
     * @method CountryStrict pop()
     * @method CountryStrict get($key, $default = null)
     * @method CountryStrict pull($key, $default = null)
     * @method CountryStrict first(callable $callback = null, $default = null)
     * @method CountryStrict firstWhere(string $key, $operator = null, $value = null)
     * @method CountryStrict find($key, $default = null)
     * @method CountryStrict[] all()
     * @method CountryStrict last(callable $callback = null, $default = null)
     * @method CountryStrict sole($key = null, $operator = null, $value = null)
     */
    class _IH_CountryStrict_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CountryStrict[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CountryStrict_QB whereId($value)
     * @method _IH_CountryStrict_QB whereCode($value)
     * @method _IH_CountryStrict_QB whereName($value)
     * @method CountryStrict baseSole(array|string $columns = ['*'])
     * @method CountryStrict create(array $attributes = [])
     * @method _IH_CountryStrict_C|CountryStrict[] cursor()
     * @method CountryStrict|null|_IH_CountryStrict_C|CountryStrict[] find($id, array $columns = ['*'])
     * @method _IH_CountryStrict_C|CountryStrict[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CountryStrict|_IH_CountryStrict_C|CountryStrict[] findOrFail($id, array $columns = ['*'])
     * @method CountryStrict|_IH_CountryStrict_C|CountryStrict[] findOrNew($id, array $columns = ['*'])
     * @method CountryStrict first(array|string $columns = ['*'])
     * @method CountryStrict firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CountryStrict firstOrCreate(array $attributes = [], array $values = [])
     * @method CountryStrict firstOrFail(array $columns = ['*'])
     * @method CountryStrict firstOrNew(array $attributes = [], array $values = [])
     * @method CountryStrict firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CountryStrict forceCreate(array $attributes)
     * @method _IH_CountryStrict_C|CountryStrict[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CountryStrict_C|CountryStrict[] get(array|string $columns = ['*'])
     * @method CountryStrict getModel()
     * @method CountryStrict[] getModels(array|string $columns = ['*'])
     * @method _IH_CountryStrict_C|CountryStrict[] hydrate(array $items)
     * @method CountryStrict make(array $attributes = [])
     * @method CountryStrict newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CountryStrict[]|_IH_CountryStrict_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CountryStrict[]|_IH_CountryStrict_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CountryStrict sole(array|string $columns = ['*'])
     * @method CountryStrict updateOrCreate(array $attributes, array $values = [])
     * @method _IH_CountryStrict_QB listsTranslations(string $translationField)
     * @method _IH_CountryStrict_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_CountryStrict_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_CountryStrict_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_CountryStrict_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_CountryStrict_QB translated()
     * @method _IH_CountryStrict_QB translatedIn(null|string $locale = null)
     * @method _IH_CountryStrict_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_CountryStrict_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_CountryStrict_QB withTranslation()
     */
    class _IH_CountryStrict_QB extends _BaseBuilder {}
    
    /**
     * @method CountryTranslation shift()
     * @method CountryTranslation pop()
     * @method CountryTranslation get($key, $default = null)
     * @method CountryTranslation pull($key, $default = null)
     * @method CountryTranslation first(callable $callback = null, $default = null)
     * @method CountryTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method CountryTranslation find($key, $default = null)
     * @method CountryTranslation[] all()
     * @method CountryTranslation last(callable $callback = null, $default = null)
     * @method CountryTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_CountryTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CountryTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CountryTranslation_QB whereId($value)
     * @method _IH_CountryTranslation_QB whereLocale($value)
     * @method _IH_CountryTranslation_QB whereName($value)
     * @method _IH_CountryTranslation_QB whereCountryId($value)
     * @method CountryTranslation baseSole(array|string $columns = ['*'])
     * @method CountryTranslation create(array $attributes = [])
     * @method _IH_CountryTranslation_C|CountryTranslation[] cursor()
     * @method CountryTranslation|null|_IH_CountryTranslation_C|CountryTranslation[] find($id, array $columns = ['*'])
     * @method _IH_CountryTranslation_C|CountryTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CountryTranslation|_IH_CountryTranslation_C|CountryTranslation[] findOrFail($id, array $columns = ['*'])
     * @method CountryTranslation|_IH_CountryTranslation_C|CountryTranslation[] findOrNew($id, array $columns = ['*'])
     * @method CountryTranslation first(array|string $columns = ['*'])
     * @method CountryTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CountryTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method CountryTranslation firstOrFail(array $columns = ['*'])
     * @method CountryTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method CountryTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CountryTranslation forceCreate(array $attributes)
     * @method _IH_CountryTranslation_C|CountryTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CountryTranslation_C|CountryTranslation[] get(array|string $columns = ['*'])
     * @method CountryTranslation getModel()
     * @method CountryTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_CountryTranslation_C|CountryTranslation[] hydrate(array $items)
     * @method CountryTranslation make(array $attributes = [])
     * @method CountryTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CountryTranslation[]|_IH_CountryTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CountryTranslation[]|_IH_CountryTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CountryTranslation sole(array|string $columns = ['*'])
     * @method CountryTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CountryTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Country shift()
     * @method Country pop()
     * @method Country get($key, $default = null)
     * @method Country pull($key, $default = null)
     * @method Country first(callable $callback = null, $default = null)
     * @method Country firstWhere(string $key, $operator = null, $value = null)
     * @method Country find($key, $default = null)
     * @method Country[] all()
     * @method Country last(callable $callback = null, $default = null)
     * @method Country sole($key = null, $operator = null, $value = null)
     */
    class _IH_Country_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Country[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Country_QB whereId($value)
     * @method _IH_Country_QB whereCode($value)
     * @method _IH_Country_QB whereName($value)
     * @method Country baseSole(array|string $columns = ['*'])
     * @method Country create(array $attributes = [])
     * @method _IH_Country_C|Country[] cursor()
     * @method Country|null|_IH_Country_C|Country[] find($id, array $columns = ['*'])
     * @method _IH_Country_C|Country[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Country|_IH_Country_C|Country[] findOrFail($id, array $columns = ['*'])
     * @method Country|_IH_Country_C|Country[] findOrNew($id, array $columns = ['*'])
     * @method Country first(array|string $columns = ['*'])
     * @method Country firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Country firstOrCreate(array $attributes = [], array $values = [])
     * @method Country firstOrFail(array $columns = ['*'])
     * @method Country firstOrNew(array $attributes = [], array $values = [])
     * @method Country firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Country forceCreate(array $attributes)
     * @method _IH_Country_C|Country[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Country_C|Country[] get(array|string $columns = ['*'])
     * @method Country getModel()
     * @method Country[] getModels(array|string $columns = ['*'])
     * @method _IH_Country_C|Country[] hydrate(array $items)
     * @method Country make(array $attributes = [])
     * @method Country newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Country[]|_IH_Country_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Country[]|_IH_Country_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Country sole(array|string $columns = ['*'])
     * @method Country updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Country_QB listsTranslations(string $translationField)
     * @method _IH_Country_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Country_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Country_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Country_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Country_QB translated()
     * @method _IH_Country_QB translatedIn(null|string $locale = null)
     * @method _IH_Country_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Country_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Country_QB withTranslation()
     */
    class _IH_Country_QB extends _BaseBuilder {}
    
    /**
     * @method PersonTranslation shift()
     * @method PersonTranslation pop()
     * @method PersonTranslation get($key, $default = null)
     * @method PersonTranslation pull($key, $default = null)
     * @method PersonTranslation first(callable $callback = null, $default = null)
     * @method PersonTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method PersonTranslation find($key, $default = null)
     * @method PersonTranslation[] all()
     * @method PersonTranslation last(callable $callback = null, $default = null)
     * @method PersonTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_PersonTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return PersonTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method PersonTranslation baseSole(array|string $columns = ['*'])
     * @method PersonTranslation create(array $attributes = [])
     * @method _IH_PersonTranslation_C|PersonTranslation[] cursor()
     * @method PersonTranslation|null|_IH_PersonTranslation_C|PersonTranslation[] find($id, array $columns = ['*'])
     * @method _IH_PersonTranslation_C|PersonTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method PersonTranslation|_IH_PersonTranslation_C|PersonTranslation[] findOrFail($id, array $columns = ['*'])
     * @method PersonTranslation|_IH_PersonTranslation_C|PersonTranslation[] findOrNew($id, array $columns = ['*'])
     * @method PersonTranslation first(array|string $columns = ['*'])
     * @method PersonTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method PersonTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method PersonTranslation firstOrFail(array $columns = ['*'])
     * @method PersonTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method PersonTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method PersonTranslation forceCreate(array $attributes)
     * @method _IH_PersonTranslation_C|PersonTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_PersonTranslation_C|PersonTranslation[] get(array|string $columns = ['*'])
     * @method PersonTranslation getModel()
     * @method PersonTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_PersonTranslation_C|PersonTranslation[] hydrate(array $items)
     * @method PersonTranslation make(array $attributes = [])
     * @method PersonTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|PersonTranslation[]|_IH_PersonTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|PersonTranslation[]|_IH_PersonTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method PersonTranslation sole(array|string $columns = ['*'])
     * @method PersonTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_PersonTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Person shift()
     * @method Person pop()
     * @method Person get($key, $default = null)
     * @method Person pull($key, $default = null)
     * @method Person first(callable $callback = null, $default = null)
     * @method Person firstWhere(string $key, $operator = null, $value = null)
     * @method Person find($key, $default = null)
     * @method Person[] all()
     * @method Person last(callable $callback = null, $default = null)
     * @method Person sole($key = null, $operator = null, $value = null)
     */
    class _IH_Person_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Person[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Person baseSole(array|string $columns = ['*'])
     * @method Person create(array $attributes = [])
     * @method _IH_Person_C|Person[] cursor()
     * @method Person|null|_IH_Person_C|Person[] find($id, array $columns = ['*'])
     * @method _IH_Person_C|Person[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Person|_IH_Person_C|Person[] findOrFail($id, array $columns = ['*'])
     * @method Person|_IH_Person_C|Person[] findOrNew($id, array $columns = ['*'])
     * @method Person first(array|string $columns = ['*'])
     * @method Person firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Person firstOrCreate(array $attributes = [], array $values = [])
     * @method Person firstOrFail(array $columns = ['*'])
     * @method Person firstOrNew(array $attributes = [], array $values = [])
     * @method Person firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Person forceCreate(array $attributes)
     * @method _IH_Person_C|Person[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Person_C|Person[] get(array|string $columns = ['*'])
     * @method Person getModel()
     * @method Person[] getModels(array|string $columns = ['*'])
     * @method _IH_Person_C|Person[] hydrate(array $items)
     * @method Person make(array $attributes = [])
     * @method Person newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Person[]|_IH_Person_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Person[]|_IH_Person_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Person sole(array|string $columns = ['*'])
     * @method Person updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Person_QB listsTranslations(string $translationField)
     * @method _IH_Person_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Person_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Person_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Person_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Person_QB translated()
     * @method _IH_Person_QB translatedIn(null|string $locale = null)
     * @method _IH_Person_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Person_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Person_QB withTranslation()
     */
    class _IH_Person_QB extends _BaseBuilder {}
    
    /**
     * @method StrictTranslation shift()
     * @method StrictTranslation pop()
     * @method StrictTranslation get($key, $default = null)
     * @method StrictTranslation pull($key, $default = null)
     * @method StrictTranslation first(callable $callback = null, $default = null)
     * @method StrictTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method StrictTranslation find($key, $default = null)
     * @method StrictTranslation[] all()
     * @method StrictTranslation last(callable $callback = null, $default = null)
     * @method StrictTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_StrictTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return StrictTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_StrictTranslation_QB whereId($value)
     * @method _IH_StrictTranslation_QB whereLocale($value)
     * @method _IH_StrictTranslation_QB whereName($value)
     * @method _IH_StrictTranslation_QB whereCountryId($value)
     * @method StrictTranslation baseSole(array|string $columns = ['*'])
     * @method StrictTranslation create(array $attributes = [])
     * @method _IH_StrictTranslation_C|StrictTranslation[] cursor()
     * @method StrictTranslation|null|_IH_StrictTranslation_C|StrictTranslation[] find($id, array $columns = ['*'])
     * @method _IH_StrictTranslation_C|StrictTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method StrictTranslation|_IH_StrictTranslation_C|StrictTranslation[] findOrFail($id, array $columns = ['*'])
     * @method StrictTranslation|_IH_StrictTranslation_C|StrictTranslation[] findOrNew($id, array $columns = ['*'])
     * @method StrictTranslation first(array|string $columns = ['*'])
     * @method StrictTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method StrictTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method StrictTranslation firstOrFail(array $columns = ['*'])
     * @method StrictTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method StrictTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method StrictTranslation forceCreate(array $attributes)
     * @method _IH_StrictTranslation_C|StrictTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_StrictTranslation_C|StrictTranslation[] get(array|string $columns = ['*'])
     * @method StrictTranslation getModel()
     * @method StrictTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_StrictTranslation_C|StrictTranslation[] hydrate(array $items)
     * @method StrictTranslation make(array $attributes = [])
     * @method StrictTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|StrictTranslation[]|_IH_StrictTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|StrictTranslation[]|_IH_StrictTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method StrictTranslation sole(array|string $columns = ['*'])
     * @method StrictTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_StrictTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method VegetableTranslation shift()
     * @method VegetableTranslation pop()
     * @method VegetableTranslation get($key, $default = null)
     * @method VegetableTranslation pull($key, $default = null)
     * @method VegetableTranslation first(callable $callback = null, $default = null)
     * @method VegetableTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method VegetableTranslation find($key, $default = null)
     * @method VegetableTranslation[] all()
     * @method VegetableTranslation last(callable $callback = null, $default = null)
     * @method VegetableTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_VegetableTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return VegetableTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method VegetableTranslation baseSole(array|string $columns = ['*'])
     * @method VegetableTranslation create(array $attributes = [])
     * @method _IH_VegetableTranslation_C|VegetableTranslation[] cursor()
     * @method VegetableTranslation|null|_IH_VegetableTranslation_C|VegetableTranslation[] find($id, array $columns = ['*'])
     * @method _IH_VegetableTranslation_C|VegetableTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method VegetableTranslation|_IH_VegetableTranslation_C|VegetableTranslation[] findOrFail($id, array $columns = ['*'])
     * @method VegetableTranslation|_IH_VegetableTranslation_C|VegetableTranslation[] findOrNew($id, array $columns = ['*'])
     * @method VegetableTranslation first(array|string $columns = ['*'])
     * @method VegetableTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method VegetableTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method VegetableTranslation firstOrFail(array $columns = ['*'])
     * @method VegetableTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method VegetableTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method VegetableTranslation forceCreate(array $attributes)
     * @method _IH_VegetableTranslation_C|VegetableTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_VegetableTranslation_C|VegetableTranslation[] get(array|string $columns = ['*'])
     * @method VegetableTranslation getModel()
     * @method VegetableTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_VegetableTranslation_C|VegetableTranslation[] hydrate(array $items)
     * @method VegetableTranslation make(array $attributes = [])
     * @method VegetableTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|VegetableTranslation[]|_IH_VegetableTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|VegetableTranslation[]|_IH_VegetableTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method VegetableTranslation sole(array|string $columns = ['*'])
     * @method VegetableTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_VegetableTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Vegetable shift()
     * @method Vegetable pop()
     * @method Vegetable get($key, $default = null)
     * @method Vegetable pull($key, $default = null)
     * @method Vegetable first(callable $callback = null, $default = null)
     * @method Vegetable firstWhere(string $key, $operator = null, $value = null)
     * @method Vegetable find($key, $default = null)
     * @method Vegetable[] all()
     * @method Vegetable last(callable $callback = null, $default = null)
     * @method Vegetable sole($key = null, $operator = null, $value = null)
     */
    class _IH_Vegetable_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Vegetable[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Vegetable baseSole(array|string $columns = ['*'])
     * @method Vegetable create(array $attributes = [])
     * @method _IH_Vegetable_C|Vegetable[] cursor()
     * @method Vegetable|null|_IH_Vegetable_C|Vegetable[] find($id, array $columns = ['*'])
     * @method _IH_Vegetable_C|Vegetable[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Vegetable|_IH_Vegetable_C|Vegetable[] findOrFail($id, array $columns = ['*'])
     * @method Vegetable|_IH_Vegetable_C|Vegetable[] findOrNew($id, array $columns = ['*'])
     * @method Vegetable first(array|string $columns = ['*'])
     * @method Vegetable firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Vegetable firstOrCreate(array $attributes = [], array $values = [])
     * @method Vegetable firstOrFail(array $columns = ['*'])
     * @method Vegetable firstOrNew(array $attributes = [], array $values = [])
     * @method Vegetable firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Vegetable forceCreate(array $attributes)
     * @method _IH_Vegetable_C|Vegetable[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Vegetable_C|Vegetable[] get(array|string $columns = ['*'])
     * @method Vegetable getModel()
     * @method Vegetable[] getModels(array|string $columns = ['*'])
     * @method _IH_Vegetable_C|Vegetable[] hydrate(array $items)
     * @method Vegetable make(array $attributes = [])
     * @method Vegetable newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Vegetable[]|_IH_Vegetable_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Vegetable[]|_IH_Vegetable_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Vegetable sole(array|string $columns = ['*'])
     * @method Vegetable updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Vegetable_QB listsTranslations(string $translationField)
     * @method _IH_Vegetable_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Vegetable_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Vegetable_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Vegetable_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Vegetable_QB translated()
     * @method _IH_Vegetable_QB translatedIn(null|string $locale = null)
     * @method _IH_Vegetable_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Vegetable_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Vegetable_QB withTranslation()
     */
    class _IH_Vegetable_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\ElasticScoutDriver\Tests\App {

    use ElasticScoutDriver\Tests\App\Client;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method Client shift()
     * @method Client pop()
     * @method Client get($key, $default = null)
     * @method Client pull($key, $default = null)
     * @method Client first(callable $callback = null, $default = null)
     * @method Client firstWhere(string $key, $operator = null, $value = null)
     * @method Client find($key, $default = null)
     * @method Client[] all()
     * @method Client last(callable $callback = null, $default = null)
     * @method Client sole($key = null, $operator = null, $value = null)
     */
    class _IH_Client_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Client[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Client baseSole(array|string $columns = ['*'])
     * @method Client create(array $attributes = [])
     * @method _IH_Client_C|Client[] cursor()
     * @method Client|null|_IH_Client_C|Client[] find($id, array $columns = ['*'])
     * @method _IH_Client_C|Client[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Client|_IH_Client_C|Client[] findOrFail($id, array $columns = ['*'])
     * @method Client|_IH_Client_C|Client[] findOrNew($id, array $columns = ['*'])
     * @method Client first(array|string $columns = ['*'])
     * @method Client firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Client firstOrCreate(array $attributes = [], array $values = [])
     * @method Client firstOrFail(array $columns = ['*'])
     * @method Client firstOrNew(array $attributes = [], array $values = [])
     * @method Client firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Client forceCreate(array $attributes)
     * @method _IH_Client_C|Client[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Client_C|Client[] get(array|string $columns = ['*'])
     * @method Client getModel()
     * @method Client[] getModels(array|string $columns = ['*'])
     * @method _IH_Client_C|Client[] hydrate(array $items)
     * @method Client make(array $attributes = [])
     * @method Client newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Client[]|_IH_Client_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Client[]|_IH_Client_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Client sole(array|string $columns = ['*'])
     * @method Client updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Client_QB withTrashed()
     * @method _IH_Client_QB onlyTrashed()
     * @method _IH_Client_QB withoutTrashed()
     * @method _IH_Client_QB restore()
     */
    class _IH_Client_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Illuminate\Notifications {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Notifications\DatabaseNotification;
    use Illuminate\Notifications\DatabaseNotificationCollection;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method DatabaseNotification shift()
     * @method DatabaseNotification pop()
     * @method DatabaseNotification get($key, $default = null)
     * @method DatabaseNotification pull($key, $default = null)
     * @method DatabaseNotification first(callable $callback = null, $default = null)
     * @method DatabaseNotification firstWhere(string $key, $operator = null, $value = null)
     * @method DatabaseNotification find($key, $default = null)
     * @method DatabaseNotification[] all()
     * @method DatabaseNotification last(callable $callback = null, $default = null)
     * @method DatabaseNotification sole($key = null, $operator = null, $value = null)
     * @mixin DatabaseNotificationCollection
     */
    class _IH_DatabaseNotification_C extends _BaseCollection {
        /**
         * @param int $size
         * @return DatabaseNotification[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method DatabaseNotification baseSole(array|string $columns = ['*'])
     * @method DatabaseNotification create(array $attributes = [])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] cursor()
     * @method DatabaseNotification|null|_IH_DatabaseNotification_C|DatabaseNotification[] find($id, array $columns = ['*'])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method DatabaseNotification|_IH_DatabaseNotification_C|DatabaseNotification[] findOrFail($id, array $columns = ['*'])
     * @method DatabaseNotification|_IH_DatabaseNotification_C|DatabaseNotification[] findOrNew($id, array $columns = ['*'])
     * @method DatabaseNotification first(array|string $columns = ['*'])
     * @method DatabaseNotification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method DatabaseNotification firstOrCreate(array $attributes = [], array $values = [])
     * @method DatabaseNotification firstOrFail(array $columns = ['*'])
     * @method DatabaseNotification firstOrNew(array $attributes = [], array $values = [])
     * @method DatabaseNotification firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method DatabaseNotification forceCreate(array $attributes)
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] fromQuery(string $query, array $bindings = [])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] get(array|string $columns = ['*'])
     * @method DatabaseNotification getModel()
     * @method DatabaseNotification[] getModels(array|string $columns = ['*'])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] hydrate(array $items)
     * @method DatabaseNotification make(array $attributes = [])
     * @method DatabaseNotification newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|DatabaseNotification[]|_IH_DatabaseNotification_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|DatabaseNotification[]|_IH_DatabaseNotification_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method DatabaseNotification sole(array|string $columns = ['*'])
     * @method DatabaseNotification updateOrCreate(array $attributes, array $values = [])
     * @method _IH_DatabaseNotification_QB read()
     * @method _IH_DatabaseNotification_QB unread()
     */
    class _IH_DatabaseNotification_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Konekt\Enum\Eloquent\Tests\Models\Address;
    use Konekt\Enum\Eloquent\Tests\Models\Client;
    use Konekt\Enum\Eloquent\Tests\Models\Eloquent;
    use Konekt\Enum\Eloquent\Tests\Models\Order;
    use Konekt\Enum\Eloquent\Tests\Models\OrderV2;
    use Konekt\Enum\Eloquent\Tests\Models\OrderVX;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method Address shift()
     * @method Address pop()
     * @method Address get($key, $default = null)
     * @method Address pull($key, $default = null)
     * @method Address first(callable $callback = null, $default = null)
     * @method Address firstWhere(string $key, $operator = null, $value = null)
     * @method Address find($key, $default = null)
     * @method Address[] all()
     * @method Address last(callable $callback = null, $default = null)
     * @method Address sole($key = null, $operator = null, $value = null)
     */
    class _IH_Address_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Address[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Address_QB whereId($value)
     * @method _IH_Address_QB whereAddressType($value)
     * @method _IH_Address_QB whereCustomerId($value)
     * @method _IH_Address_QB whereCartId($value)
     * @method _IH_Address_QB whereOrderId($value)
     * @method _IH_Address_QB whereFirstName($value)
     * @method _IH_Address_QB whereLastName($value)
     * @method _IH_Address_QB whereGender($value)
     * @method _IH_Address_QB whereCompanyName($value)
     * @method _IH_Address_QB whereAddress1($value)
     * @method _IH_Address_QB whereAddress2($value)
     * @method _IH_Address_QB wherePostcode($value)
     * @method _IH_Address_QB whereCity($value)
     * @method _IH_Address_QB whereState($value)
     * @method _IH_Address_QB whereCountry($value)
     * @method _IH_Address_QB whereEmail($value)
     * @method _IH_Address_QB wherePhone($value)
     * @method _IH_Address_QB whereVatId($value)
     * @method _IH_Address_QB whereDefaultAddress($value)
     * @method _IH_Address_QB whereAdditional($value)
     * @method _IH_Address_QB whereCreatedAt($value)
     * @method _IH_Address_QB whereUpdatedAt($value)
     * @method Address baseSole(array|string $columns = ['*'])
     * @method Address create(array $attributes = [])
     * @method _IH_Address_C|Address[] cursor()
     * @method Address|null|_IH_Address_C|Address[] find($id, array $columns = ['*'])
     * @method _IH_Address_C|Address[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Address|_IH_Address_C|Address[] findOrFail($id, array $columns = ['*'])
     * @method Address|_IH_Address_C|Address[] findOrNew($id, array $columns = ['*'])
     * @method Address first(array|string $columns = ['*'])
     * @method Address firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Address firstOrCreate(array $attributes = [], array $values = [])
     * @method Address firstOrFail(array $columns = ['*'])
     * @method Address firstOrNew(array $attributes = [], array $values = [])
     * @method Address firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Address forceCreate(array $attributes)
     * @method _IH_Address_C|Address[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Address_C|Address[] get(array|string $columns = ['*'])
     * @method Address getModel()
     * @method Address[] getModels(array|string $columns = ['*'])
     * @method _IH_Address_C|Address[] hydrate(array $items)
     * @method Address make(array $attributes = [])
     * @method Address newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Address[]|_IH_Address_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Address[]|_IH_Address_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Address sole(array|string $columns = ['*'])
     * @method Address updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Address_QB extends _BaseBuilder {}
    
    /**
     * @method Client shift()
     * @method Client pop()
     * @method Client get($key, $default = null)
     * @method Client pull($key, $default = null)
     * @method Client first(callable $callback = null, $default = null)
     * @method Client firstWhere(string $key, $operator = null, $value = null)
     * @method Client find($key, $default = null)
     * @method Client[] all()
     * @method Client last(callable $callback = null, $default = null)
     * @method Client sole($key = null, $operator = null, $value = null)
     */
    class _IH_Client_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Client[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Client baseSole(array|string $columns = ['*'])
     * @method Client create(array $attributes = [])
     * @method _IH_Client_C|Client[] cursor()
     * @method Client|null|_IH_Client_C|Client[] find($id, array $columns = ['*'])
     * @method _IH_Client_C|Client[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Client|_IH_Client_C|Client[] findOrFail($id, array $columns = ['*'])
     * @method Client|_IH_Client_C|Client[] findOrNew($id, array $columns = ['*'])
     * @method Client first(array|string $columns = ['*'])
     * @method Client firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Client firstOrCreate(array $attributes = [], array $values = [])
     * @method Client firstOrFail(array $columns = ['*'])
     * @method Client firstOrNew(array $attributes = [], array $values = [])
     * @method Client firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Client forceCreate(array $attributes)
     * @method _IH_Client_C|Client[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Client_C|Client[] get(array|string $columns = ['*'])
     * @method Client getModel()
     * @method Client[] getModels(array|string $columns = ['*'])
     * @method _IH_Client_C|Client[] hydrate(array $items)
     * @method Client make(array $attributes = [])
     * @method Client newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Client[]|_IH_Client_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Client[]|_IH_Client_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Client sole(array|string $columns = ['*'])
     * @method Client updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Client_QB extends _BaseBuilder {}
    
    /**
     * @method Eloquent shift()
     * @method Eloquent pop()
     * @method Eloquent get($key, $default = null)
     * @method Eloquent pull($key, $default = null)
     * @method Eloquent first(callable $callback = null, $default = null)
     * @method Eloquent firstWhere(string $key, $operator = null, $value = null)
     * @method Eloquent find($key, $default = null)
     * @method Eloquent[] all()
     * @method Eloquent last(callable $callback = null, $default = null)
     * @method Eloquent sole($key = null, $operator = null, $value = null)
     */
    class _IH_Eloquent_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Eloquent[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Eloquent baseSole(array|string $columns = ['*'])
     * @method Eloquent create(array $attributes = [])
     * @method _IH_Eloquent_C|Eloquent[] cursor()
     * @method Eloquent|null|_IH_Eloquent_C|Eloquent[] find($id, array $columns = ['*'])
     * @method _IH_Eloquent_C|Eloquent[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Eloquent|_IH_Eloquent_C|Eloquent[] findOrFail($id, array $columns = ['*'])
     * @method Eloquent|_IH_Eloquent_C|Eloquent[] findOrNew($id, array $columns = ['*'])
     * @method Eloquent first(array|string $columns = ['*'])
     * @method Eloquent firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Eloquent firstOrCreate(array $attributes = [], array $values = [])
     * @method Eloquent firstOrFail(array $columns = ['*'])
     * @method Eloquent firstOrNew(array $attributes = [], array $values = [])
     * @method Eloquent firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Eloquent forceCreate(array $attributes)
     * @method _IH_Eloquent_C|Eloquent[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Eloquent_C|Eloquent[] get(array|string $columns = ['*'])
     * @method Eloquent getModel()
     * @method Eloquent[] getModels(array|string $columns = ['*'])
     * @method _IH_Eloquent_C|Eloquent[] hydrate(array $items)
     * @method Eloquent make(array $attributes = [])
     * @method Eloquent newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Eloquent[]|_IH_Eloquent_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Eloquent[]|_IH_Eloquent_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Eloquent sole(array|string $columns = ['*'])
     * @method Eloquent updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Eloquent_QB extends _BaseBuilder {}
    
    /**
     * @method OrderV2 shift()
     * @method OrderV2 pop()
     * @method OrderV2 get($key, $default = null)
     * @method OrderV2 pull($key, $default = null)
     * @method OrderV2 first(callable $callback = null, $default = null)
     * @method OrderV2 firstWhere(string $key, $operator = null, $value = null)
     * @method OrderV2 find($key, $default = null)
     * @method OrderV2[] all()
     * @method OrderV2 last(callable $callback = null, $default = null)
     * @method OrderV2 sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderV2_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderV2[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_OrderV2_QB whereId($value)
     * @method _IH_OrderV2_QB whereIncrementId($value)
     * @method _IH_OrderV2_QB whereStatus($value)
     * @method _IH_OrderV2_QB whereChannelName($value)
     * @method _IH_OrderV2_QB whereIsGuest($value)
     * @method _IH_OrderV2_QB whereCustomerEmail($value)
     * @method _IH_OrderV2_QB whereCustomerFirstName($value)
     * @method _IH_OrderV2_QB whereCustomerLastName($value)
     * @method _IH_OrderV2_QB whereCustomerCompanyName($value)
     * @method _IH_OrderV2_QB whereCustomerVatId($value)
     * @method _IH_OrderV2_QB whereShippingMethod($value)
     * @method _IH_OrderV2_QB whereShippingTitle($value)
     * @method _IH_OrderV2_QB whereShippingDescription($value)
     * @method _IH_OrderV2_QB whereCouponCode($value)
     * @method _IH_OrderV2_QB whereIsGift($value)
     * @method _IH_OrderV2_QB whereTotalItemCount($value)
     * @method _IH_OrderV2_QB whereTotalQtyOrdered($value)
     * @method _IH_OrderV2_QB whereBaseCurrencyCode($value)
     * @method _IH_OrderV2_QB whereChannelCurrencyCode($value)
     * @method _IH_OrderV2_QB whereOrderCurrencyCode($value)
     * @method _IH_OrderV2_QB whereGrandTotal($value)
     * @method _IH_OrderV2_QB whereBaseGrandTotal($value)
     * @method _IH_OrderV2_QB whereGrandTotalInvoiced($value)
     * @method _IH_OrderV2_QB whereBaseGrandTotalInvoiced($value)
     * @method _IH_OrderV2_QB whereGrandTotalRefunded($value)
     * @method _IH_OrderV2_QB whereBaseGrandTotalRefunded($value)
     * @method _IH_OrderV2_QB whereSubTotal($value)
     * @method _IH_OrderV2_QB whereBaseSubTotal($value)
     * @method _IH_OrderV2_QB whereSubTotalInvoiced($value)
     * @method _IH_OrderV2_QB whereBaseSubTotalInvoiced($value)
     * @method _IH_OrderV2_QB whereSubTotalRefunded($value)
     * @method _IH_OrderV2_QB whereBaseSubTotalRefunded($value)
     * @method _IH_OrderV2_QB whereDiscountPercent($value)
     * @method _IH_OrderV2_QB whereDiscountAmount($value)
     * @method _IH_OrderV2_QB whereBaseDiscountAmount($value)
     * @method _IH_OrderV2_QB whereDiscountInvoiced($value)
     * @method _IH_OrderV2_QB whereBaseDiscountInvoiced($value)
     * @method _IH_OrderV2_QB whereDiscountRefunded($value)
     * @method _IH_OrderV2_QB whereBaseDiscountRefunded($value)
     * @method _IH_OrderV2_QB whereTaxAmount($value)
     * @method _IH_OrderV2_QB whereBaseTaxAmount($value)
     * @method _IH_OrderV2_QB whereTaxAmountInvoiced($value)
     * @method _IH_OrderV2_QB whereBaseTaxAmountInvoiced($value)
     * @method _IH_OrderV2_QB whereTaxAmountRefunded($value)
     * @method _IH_OrderV2_QB whereBaseTaxAmountRefunded($value)
     * @method _IH_OrderV2_QB whereShippingAmount($value)
     * @method _IH_OrderV2_QB whereBaseShippingAmount($value)
     * @method _IH_OrderV2_QB whereShippingInvoiced($value)
     * @method _IH_OrderV2_QB whereBaseShippingInvoiced($value)
     * @method _IH_OrderV2_QB whereShippingRefunded($value)
     * @method _IH_OrderV2_QB whereBaseShippingRefunded($value)
     * @method _IH_OrderV2_QB whereCustomerId($value)
     * @method _IH_OrderV2_QB whereCustomerType($value)
     * @method _IH_OrderV2_QB whereChannelId($value)
     * @method _IH_OrderV2_QB whereChannelType($value)
     * @method _IH_OrderV2_QB whereCreatedAt($value)
     * @method _IH_OrderV2_QB whereUpdatedAt($value)
     * @method _IH_OrderV2_QB whereCartId($value)
     * @method _IH_OrderV2_QB whereAppliedCartRuleIds($value)
     * @method _IH_OrderV2_QB whereShippingDiscountAmount($value)
     * @method _IH_OrderV2_QB whereBaseShippingDiscountAmount($value)
     * @method OrderV2 baseSole(array|string $columns = ['*'])
     * @method OrderV2 create(array $attributes = [])
     * @method _IH_OrderV2_C|OrderV2[] cursor()
     * @method OrderV2|null|_IH_OrderV2_C|OrderV2[] find($id, array $columns = ['*'])
     * @method _IH_OrderV2_C|OrderV2[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderV2|_IH_OrderV2_C|OrderV2[] findOrFail($id, array $columns = ['*'])
     * @method OrderV2|_IH_OrderV2_C|OrderV2[] findOrNew($id, array $columns = ['*'])
     * @method OrderV2 first(array|string $columns = ['*'])
     * @method OrderV2 firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderV2 firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderV2 firstOrFail(array $columns = ['*'])
     * @method OrderV2 firstOrNew(array $attributes = [], array $values = [])
     * @method OrderV2 firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderV2 forceCreate(array $attributes)
     * @method _IH_OrderV2_C|OrderV2[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderV2_C|OrderV2[] get(array|string $columns = ['*'])
     * @method OrderV2 getModel()
     * @method OrderV2[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderV2_C|OrderV2[] hydrate(array $items)
     * @method OrderV2 make(array $attributes = [])
     * @method OrderV2 newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderV2[]|_IH_OrderV2_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderV2[]|_IH_OrderV2_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderV2 sole(array|string $columns = ['*'])
     * @method OrderV2 updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderV2_QB extends _BaseBuilder {}
    
    /**
     * @method OrderVX shift()
     * @method OrderVX pop()
     * @method OrderVX get($key, $default = null)
     * @method OrderVX pull($key, $default = null)
     * @method OrderVX first(callable $callback = null, $default = null)
     * @method OrderVX firstWhere(string $key, $operator = null, $value = null)
     * @method OrderVX find($key, $default = null)
     * @method OrderVX[] all()
     * @method OrderVX last(callable $callback = null, $default = null)
     * @method OrderVX sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderVX_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderVX[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method OrderVX baseSole(array|string $columns = ['*'])
     * @method OrderVX create(array $attributes = [])
     * @method _IH_OrderVX_C|OrderVX[] cursor()
     * @method OrderVX|null|_IH_OrderVX_C|OrderVX[] find($id, array $columns = ['*'])
     * @method _IH_OrderVX_C|OrderVX[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderVX|_IH_OrderVX_C|OrderVX[] findOrFail($id, array $columns = ['*'])
     * @method OrderVX|_IH_OrderVX_C|OrderVX[] findOrNew($id, array $columns = ['*'])
     * @method OrderVX first(array|string $columns = ['*'])
     * @method OrderVX firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderVX firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderVX firstOrFail(array $columns = ['*'])
     * @method OrderVX firstOrNew(array $attributes = [], array $values = [])
     * @method OrderVX firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderVX forceCreate(array $attributes)
     * @method _IH_OrderVX_C|OrderVX[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderVX_C|OrderVX[] get(array|string $columns = ['*'])
     * @method OrderVX getModel()
     * @method OrderVX[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderVX_C|OrderVX[] hydrate(array $items)
     * @method OrderVX make(array $attributes = [])
     * @method OrderVX newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderVX[]|_IH_OrderVX_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderVX[]|_IH_OrderVX_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderVX sole(array|string $columns = ['*'])
     * @method OrderVX updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderVX_QB extends _BaseBuilder {}
    
    /**
     * @method Order shift()
     * @method Order pop()
     * @method Order get($key, $default = null)
     * @method Order pull($key, $default = null)
     * @method Order first(callable $callback = null, $default = null)
     * @method Order firstWhere(string $key, $operator = null, $value = null)
     * @method Order find($key, $default = null)
     * @method Order[] all()
     * @method Order last(callable $callback = null, $default = null)
     * @method Order sole($key = null, $operator = null, $value = null)
     */
    class _IH_Order_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Order[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Order_QB whereId($value)
     * @method _IH_Order_QB whereIncrementId($value)
     * @method _IH_Order_QB whereStatus($value)
     * @method _IH_Order_QB whereChannelName($value)
     * @method _IH_Order_QB whereIsGuest($value)
     * @method _IH_Order_QB whereCustomerEmail($value)
     * @method _IH_Order_QB whereCustomerFirstName($value)
     * @method _IH_Order_QB whereCustomerLastName($value)
     * @method _IH_Order_QB whereCustomerCompanyName($value)
     * @method _IH_Order_QB whereCustomerVatId($value)
     * @method _IH_Order_QB whereShippingMethod($value)
     * @method _IH_Order_QB whereShippingTitle($value)
     * @method _IH_Order_QB whereShippingDescription($value)
     * @method _IH_Order_QB whereCouponCode($value)
     * @method _IH_Order_QB whereIsGift($value)
     * @method _IH_Order_QB whereTotalItemCount($value)
     * @method _IH_Order_QB whereTotalQtyOrdered($value)
     * @method _IH_Order_QB whereBaseCurrencyCode($value)
     * @method _IH_Order_QB whereChannelCurrencyCode($value)
     * @method _IH_Order_QB whereOrderCurrencyCode($value)
     * @method _IH_Order_QB whereGrandTotal($value)
     * @method _IH_Order_QB whereBaseGrandTotal($value)
     * @method _IH_Order_QB whereGrandTotalInvoiced($value)
     * @method _IH_Order_QB whereBaseGrandTotalInvoiced($value)
     * @method _IH_Order_QB whereGrandTotalRefunded($value)
     * @method _IH_Order_QB whereBaseGrandTotalRefunded($value)
     * @method _IH_Order_QB whereSubTotal($value)
     * @method _IH_Order_QB whereBaseSubTotal($value)
     * @method _IH_Order_QB whereSubTotalInvoiced($value)
     * @method _IH_Order_QB whereBaseSubTotalInvoiced($value)
     * @method _IH_Order_QB whereSubTotalRefunded($value)
     * @method _IH_Order_QB whereBaseSubTotalRefunded($value)
     * @method _IH_Order_QB whereDiscountPercent($value)
     * @method _IH_Order_QB whereDiscountAmount($value)
     * @method _IH_Order_QB whereBaseDiscountAmount($value)
     * @method _IH_Order_QB whereDiscountInvoiced($value)
     * @method _IH_Order_QB whereBaseDiscountInvoiced($value)
     * @method _IH_Order_QB whereDiscountRefunded($value)
     * @method _IH_Order_QB whereBaseDiscountRefunded($value)
     * @method _IH_Order_QB whereTaxAmount($value)
     * @method _IH_Order_QB whereBaseTaxAmount($value)
     * @method _IH_Order_QB whereTaxAmountInvoiced($value)
     * @method _IH_Order_QB whereBaseTaxAmountInvoiced($value)
     * @method _IH_Order_QB whereTaxAmountRefunded($value)
     * @method _IH_Order_QB whereBaseTaxAmountRefunded($value)
     * @method _IH_Order_QB whereShippingAmount($value)
     * @method _IH_Order_QB whereBaseShippingAmount($value)
     * @method _IH_Order_QB whereShippingInvoiced($value)
     * @method _IH_Order_QB whereBaseShippingInvoiced($value)
     * @method _IH_Order_QB whereShippingRefunded($value)
     * @method _IH_Order_QB whereBaseShippingRefunded($value)
     * @method _IH_Order_QB whereCustomerId($value)
     * @method _IH_Order_QB whereCustomerType($value)
     * @method _IH_Order_QB whereChannelId($value)
     * @method _IH_Order_QB whereChannelType($value)
     * @method _IH_Order_QB whereCreatedAt($value)
     * @method _IH_Order_QB whereUpdatedAt($value)
     * @method _IH_Order_QB whereCartId($value)
     * @method _IH_Order_QB whereAppliedCartRuleIds($value)
     * @method _IH_Order_QB whereShippingDiscountAmount($value)
     * @method _IH_Order_QB whereBaseShippingDiscountAmount($value)
     * @method Order baseSole(array|string $columns = ['*'])
     * @method Order create(array $attributes = [])
     * @method _IH_Order_C|Order[] cursor()
     * @method Order|null|_IH_Order_C|Order[] find($id, array $columns = ['*'])
     * @method _IH_Order_C|Order[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Order|_IH_Order_C|Order[] findOrFail($id, array $columns = ['*'])
     * @method Order|_IH_Order_C|Order[] findOrNew($id, array $columns = ['*'])
     * @method Order first(array|string $columns = ['*'])
     * @method Order firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Order firstOrCreate(array $attributes = [], array $values = [])
     * @method Order firstOrFail(array $columns = ['*'])
     * @method Order firstOrNew(array $attributes = [], array $values = [])
     * @method Order firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Order forceCreate(array $attributes)
     * @method _IH_Order_C|Order[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Order_C|Order[] get(array|string $columns = ['*'])
     * @method Order getModel()
     * @method Order[] getModels(array|string $columns = ['*'])
     * @method _IH_Order_C|Order[] hydrate(array $items)
     * @method Order make(array $attributes = [])
     * @method Order newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Order[]|_IH_Order_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Order[]|_IH_Order_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Order sole(array|string $columns = ['*'])
     * @method Order updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Order_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\Extended {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Konekt\Enum\Eloquent\Tests\Models\Extended\Address;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method Address shift()
     * @method Address pop()
     * @method Address get($key, $default = null)
     * @method Address pull($key, $default = null)
     * @method Address first(callable $callback = null, $default = null)
     * @method Address firstWhere(string $key, $operator = null, $value = null)
     * @method Address find($key, $default = null)
     * @method Address[] all()
     * @method Address last(callable $callback = null, $default = null)
     * @method Address sole($key = null, $operator = null, $value = null)
     */
    class _IH_Address_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Address[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Address_QB whereId($value)
     * @method _IH_Address_QB whereAddressType($value)
     * @method _IH_Address_QB whereCustomerId($value)
     * @method _IH_Address_QB whereCartId($value)
     * @method _IH_Address_QB whereOrderId($value)
     * @method _IH_Address_QB whereFirstName($value)
     * @method _IH_Address_QB whereLastName($value)
     * @method _IH_Address_QB whereGender($value)
     * @method _IH_Address_QB whereCompanyName($value)
     * @method _IH_Address_QB whereAddress1($value)
     * @method _IH_Address_QB whereAddress2($value)
     * @method _IH_Address_QB wherePostcode($value)
     * @method _IH_Address_QB whereCity($value)
     * @method _IH_Address_QB whereState($value)
     * @method _IH_Address_QB whereCountry($value)
     * @method _IH_Address_QB whereEmail($value)
     * @method _IH_Address_QB wherePhone($value)
     * @method _IH_Address_QB whereVatId($value)
     * @method _IH_Address_QB whereDefaultAddress($value)
     * @method _IH_Address_QB whereAdditional($value)
     * @method _IH_Address_QB whereCreatedAt($value)
     * @method _IH_Address_QB whereUpdatedAt($value)
     * @method Address baseSole(array|string $columns = ['*'])
     * @method Address create(array $attributes = [])
     * @method _IH_Address_C|Address[] cursor()
     * @method Address|null|_IH_Address_C|Address[] find($id, array $columns = ['*'])
     * @method _IH_Address_C|Address[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Address|_IH_Address_C|Address[] findOrFail($id, array $columns = ['*'])
     * @method Address|_IH_Address_C|Address[] findOrNew($id, array $columns = ['*'])
     * @method Address first(array|string $columns = ['*'])
     * @method Address firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Address firstOrCreate(array $attributes = [], array $values = [])
     * @method Address firstOrFail(array $columns = ['*'])
     * @method Address firstOrNew(array $attributes = [], array $values = [])
     * @method Address firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Address forceCreate(array $attributes)
     * @method _IH_Address_C|Address[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Address_C|Address[] get(array|string $columns = ['*'])
     * @method Address getModel()
     * @method Address[] getModels(array|string $columns = ['*'])
     * @method _IH_Address_C|Address[] hydrate(array $items)
     * @method Address make(array $attributes = [])
     * @method Address newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Address[]|_IH_Address_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Address[]|_IH_Address_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Address sole(array|string $columns = ['*'])
     * @method Address updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Address_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Attribute\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Attribute\Models\Attribute;
    use Webkul\Attribute\Models\AttributeFamily;
    use Webkul\Attribute\Models\AttributeGroup;
    use Webkul\Attribute\Models\AttributeOption;
    use Webkul\Attribute\Models\AttributeOptionTranslation;
    use Webkul\Attribute\Models\AttributeTranslation;
    
    /**
     * @method AttributeFamily shift()
     * @method AttributeFamily pop()
     * @method AttributeFamily get($key, $default = null)
     * @method AttributeFamily pull($key, $default = null)
     * @method AttributeFamily first(callable $callback = null, $default = null)
     * @method AttributeFamily firstWhere(string $key, $operator = null, $value = null)
     * @method AttributeFamily find($key, $default = null)
     * @method AttributeFamily[] all()
     * @method AttributeFamily last(callable $callback = null, $default = null)
     * @method AttributeFamily sole($key = null, $operator = null, $value = null)
     */
    class _IH_AttributeFamily_C extends _BaseCollection {
        /**
         * @param int $size
         * @return AttributeFamily[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_AttributeFamily_QB whereId($value)
     * @method _IH_AttributeFamily_QB whereCode($value)
     * @method _IH_AttributeFamily_QB whereName($value)
     * @method _IH_AttributeFamily_QB whereStatus($value)
     * @method _IH_AttributeFamily_QB whereIsUserDefined($value)
     * @method AttributeFamily baseSole(array|string $columns = ['*'])
     * @method AttributeFamily create(array $attributes = [])
     * @method _IH_AttributeFamily_C|AttributeFamily[] cursor()
     * @method AttributeFamily|null|_IH_AttributeFamily_C|AttributeFamily[] find($id, array $columns = ['*'])
     * @method _IH_AttributeFamily_C|AttributeFamily[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method AttributeFamily|_IH_AttributeFamily_C|AttributeFamily[] findOrFail($id, array $columns = ['*'])
     * @method AttributeFamily|_IH_AttributeFamily_C|AttributeFamily[] findOrNew($id, array $columns = ['*'])
     * @method AttributeFamily first(array|string $columns = ['*'])
     * @method AttributeFamily firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method AttributeFamily firstOrCreate(array $attributes = [], array $values = [])
     * @method AttributeFamily firstOrFail(array $columns = ['*'])
     * @method AttributeFamily firstOrNew(array $attributes = [], array $values = [])
     * @method AttributeFamily firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method AttributeFamily forceCreate(array $attributes)
     * @method _IH_AttributeFamily_C|AttributeFamily[] fromQuery(string $query, array $bindings = [])
     * @method _IH_AttributeFamily_C|AttributeFamily[] get(array|string $columns = ['*'])
     * @method AttributeFamily getModel()
     * @method AttributeFamily[] getModels(array|string $columns = ['*'])
     * @method _IH_AttributeFamily_C|AttributeFamily[] hydrate(array $items)
     * @method AttributeFamily make(array $attributes = [])
     * @method AttributeFamily newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|AttributeFamily[]|_IH_AttributeFamily_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|AttributeFamily[]|_IH_AttributeFamily_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method AttributeFamily sole(array|string $columns = ['*'])
     * @method AttributeFamily updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_AttributeFamily_QB extends _BaseBuilder {}
    
    /**
     * @method AttributeGroup shift()
     * @method AttributeGroup pop()
     * @method AttributeGroup get($key, $default = null)
     * @method AttributeGroup pull($key, $default = null)
     * @method AttributeGroup first(callable $callback = null, $default = null)
     * @method AttributeGroup firstWhere(string $key, $operator = null, $value = null)
     * @method AttributeGroup find($key, $default = null)
     * @method AttributeGroup[] all()
     * @method AttributeGroup last(callable $callback = null, $default = null)
     * @method AttributeGroup sole($key = null, $operator = null, $value = null)
     */
    class _IH_AttributeGroup_C extends _BaseCollection {
        /**
         * @param int $size
         * @return AttributeGroup[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_AttributeGroup_QB whereId($value)
     * @method _IH_AttributeGroup_QB whereName($value)
     * @method _IH_AttributeGroup_QB wherePosition($value)
     * @method _IH_AttributeGroup_QB whereIsUserDefined($value)
     * @method _IH_AttributeGroup_QB whereAttributeFamilyId($value)
     * @method AttributeGroup baseSole(array|string $columns = ['*'])
     * @method AttributeGroup create(array $attributes = [])
     * @method _IH_AttributeGroup_C|AttributeGroup[] cursor()
     * @method AttributeGroup|null|_IH_AttributeGroup_C|AttributeGroup[] find($id, array $columns = ['*'])
     * @method _IH_AttributeGroup_C|AttributeGroup[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method AttributeGroup|_IH_AttributeGroup_C|AttributeGroup[] findOrFail($id, array $columns = ['*'])
     * @method AttributeGroup|_IH_AttributeGroup_C|AttributeGroup[] findOrNew($id, array $columns = ['*'])
     * @method AttributeGroup first(array|string $columns = ['*'])
     * @method AttributeGroup firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method AttributeGroup firstOrCreate(array $attributes = [], array $values = [])
     * @method AttributeGroup firstOrFail(array $columns = ['*'])
     * @method AttributeGroup firstOrNew(array $attributes = [], array $values = [])
     * @method AttributeGroup firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method AttributeGroup forceCreate(array $attributes)
     * @method _IH_AttributeGroup_C|AttributeGroup[] fromQuery(string $query, array $bindings = [])
     * @method _IH_AttributeGroup_C|AttributeGroup[] get(array|string $columns = ['*'])
     * @method AttributeGroup getModel()
     * @method AttributeGroup[] getModels(array|string $columns = ['*'])
     * @method _IH_AttributeGroup_C|AttributeGroup[] hydrate(array $items)
     * @method AttributeGroup make(array $attributes = [])
     * @method AttributeGroup newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|AttributeGroup[]|_IH_AttributeGroup_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|AttributeGroup[]|_IH_AttributeGroup_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method AttributeGroup sole(array|string $columns = ['*'])
     * @method AttributeGroup updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_AttributeGroup_QB extends _BaseBuilder {}
    
    /**
     * @method AttributeOptionTranslation shift()
     * @method AttributeOptionTranslation pop()
     * @method AttributeOptionTranslation get($key, $default = null)
     * @method AttributeOptionTranslation pull($key, $default = null)
     * @method AttributeOptionTranslation first(callable $callback = null, $default = null)
     * @method AttributeOptionTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method AttributeOptionTranslation find($key, $default = null)
     * @method AttributeOptionTranslation[] all()
     * @method AttributeOptionTranslation last(callable $callback = null, $default = null)
     * @method AttributeOptionTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_AttributeOptionTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return AttributeOptionTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_AttributeOptionTranslation_QB whereId($value)
     * @method _IH_AttributeOptionTranslation_QB whereLocale($value)
     * @method _IH_AttributeOptionTranslation_QB whereLabel($value)
     * @method _IH_AttributeOptionTranslation_QB whereAttributeOptionId($value)
     * @method AttributeOptionTranslation baseSole(array|string $columns = ['*'])
     * @method AttributeOptionTranslation create(array $attributes = [])
     * @method _IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] cursor()
     * @method AttributeOptionTranslation|null|_IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] find($id, array $columns = ['*'])
     * @method _IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method AttributeOptionTranslation|_IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] findOrFail($id, array $columns = ['*'])
     * @method AttributeOptionTranslation|_IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] findOrNew($id, array $columns = ['*'])
     * @method AttributeOptionTranslation first(array|string $columns = ['*'])
     * @method AttributeOptionTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method AttributeOptionTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method AttributeOptionTranslation firstOrFail(array $columns = ['*'])
     * @method AttributeOptionTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method AttributeOptionTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method AttributeOptionTranslation forceCreate(array $attributes)
     * @method _IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] get(array|string $columns = ['*'])
     * @method AttributeOptionTranslation getModel()
     * @method AttributeOptionTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] hydrate(array $items)
     * @method AttributeOptionTranslation make(array $attributes = [])
     * @method AttributeOptionTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|AttributeOptionTranslation[]|_IH_AttributeOptionTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|AttributeOptionTranslation[]|_IH_AttributeOptionTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method AttributeOptionTranslation sole(array|string $columns = ['*'])
     * @method AttributeOptionTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_AttributeOptionTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method AttributeOption shift()
     * @method AttributeOption pop()
     * @method AttributeOption get($key, $default = null)
     * @method AttributeOption pull($key, $default = null)
     * @method AttributeOption first(callable $callback = null, $default = null)
     * @method AttributeOption firstWhere(string $key, $operator = null, $value = null)
     * @method AttributeOption find($key, $default = null)
     * @method AttributeOption[] all()
     * @method AttributeOption last(callable $callback = null, $default = null)
     * @method AttributeOption sole($key = null, $operator = null, $value = null)
     */
    class _IH_AttributeOption_C extends _BaseCollection {
        /**
         * @param int $size
         * @return AttributeOption[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_AttributeOption_QB whereId($value)
     * @method _IH_AttributeOption_QB whereAdminName($value)
     * @method _IH_AttributeOption_QB whereSortOrder($value)
     * @method _IH_AttributeOption_QB whereAttributeId($value)
     * @method _IH_AttributeOption_QB whereSwatchValue($value)
     * @method AttributeOption baseSole(array|string $columns = ['*'])
     * @method AttributeOption create(array $attributes = [])
     * @method _IH_AttributeOption_C|AttributeOption[] cursor()
     * @method AttributeOption|null|_IH_AttributeOption_C|AttributeOption[] find($id, array $columns = ['*'])
     * @method _IH_AttributeOption_C|AttributeOption[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method AttributeOption|_IH_AttributeOption_C|AttributeOption[] findOrFail($id, array $columns = ['*'])
     * @method AttributeOption|_IH_AttributeOption_C|AttributeOption[] findOrNew($id, array $columns = ['*'])
     * @method AttributeOption first(array|string $columns = ['*'])
     * @method AttributeOption firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method AttributeOption firstOrCreate(array $attributes = [], array $values = [])
     * @method AttributeOption firstOrFail(array $columns = ['*'])
     * @method AttributeOption firstOrNew(array $attributes = [], array $values = [])
     * @method AttributeOption firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method AttributeOption forceCreate(array $attributes)
     * @method _IH_AttributeOption_C|AttributeOption[] fromQuery(string $query, array $bindings = [])
     * @method _IH_AttributeOption_C|AttributeOption[] get(array|string $columns = ['*'])
     * @method AttributeOption getModel()
     * @method AttributeOption[] getModels(array|string $columns = ['*'])
     * @method _IH_AttributeOption_C|AttributeOption[] hydrate(array $items)
     * @method AttributeOption make(array $attributes = [])
     * @method AttributeOption newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|AttributeOption[]|_IH_AttributeOption_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|AttributeOption[]|_IH_AttributeOption_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method AttributeOption sole(array|string $columns = ['*'])
     * @method AttributeOption updateOrCreate(array $attributes, array $values = [])
     * @method _IH_AttributeOption_QB listsTranslations(string $translationField)
     * @method _IH_AttributeOption_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_AttributeOption_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_AttributeOption_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_AttributeOption_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_AttributeOption_QB translated()
     * @method _IH_AttributeOption_QB translatedIn(null|string $locale = null)
     * @method _IH_AttributeOption_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_AttributeOption_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_AttributeOption_QB withTranslation()
     */
    class _IH_AttributeOption_QB extends _BaseBuilder {}
    
    /**
     * @method AttributeTranslation shift()
     * @method AttributeTranslation pop()
     * @method AttributeTranslation get($key, $default = null)
     * @method AttributeTranslation pull($key, $default = null)
     * @method AttributeTranslation first(callable $callback = null, $default = null)
     * @method AttributeTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method AttributeTranslation find($key, $default = null)
     * @method AttributeTranslation[] all()
     * @method AttributeTranslation last(callable $callback = null, $default = null)
     * @method AttributeTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_AttributeTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return AttributeTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_AttributeTranslation_QB whereId($value)
     * @method _IH_AttributeTranslation_QB whereLocale($value)
     * @method _IH_AttributeTranslation_QB whereName($value)
     * @method _IH_AttributeTranslation_QB whereAttributeId($value)
     * @method AttributeTranslation baseSole(array|string $columns = ['*'])
     * @method AttributeTranslation create(array $attributes = [])
     * @method _IH_AttributeTranslation_C|AttributeTranslation[] cursor()
     * @method AttributeTranslation|null|_IH_AttributeTranslation_C|AttributeTranslation[] find($id, array $columns = ['*'])
     * @method _IH_AttributeTranslation_C|AttributeTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method AttributeTranslation|_IH_AttributeTranslation_C|AttributeTranslation[] findOrFail($id, array $columns = ['*'])
     * @method AttributeTranslation|_IH_AttributeTranslation_C|AttributeTranslation[] findOrNew($id, array $columns = ['*'])
     * @method AttributeTranslation first(array|string $columns = ['*'])
     * @method AttributeTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method AttributeTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method AttributeTranslation firstOrFail(array $columns = ['*'])
     * @method AttributeTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method AttributeTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method AttributeTranslation forceCreate(array $attributes)
     * @method _IH_AttributeTranslation_C|AttributeTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_AttributeTranslation_C|AttributeTranslation[] get(array|string $columns = ['*'])
     * @method AttributeTranslation getModel()
     * @method AttributeTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_AttributeTranslation_C|AttributeTranslation[] hydrate(array $items)
     * @method AttributeTranslation make(array $attributes = [])
     * @method AttributeTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|AttributeTranslation[]|_IH_AttributeTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|AttributeTranslation[]|_IH_AttributeTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method AttributeTranslation sole(array|string $columns = ['*'])
     * @method AttributeTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_AttributeTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Attribute shift()
     * @method Attribute pop()
     * @method Attribute get($key, $default = null)
     * @method Attribute pull($key, $default = null)
     * @method Attribute first(callable $callback = null, $default = null)
     * @method Attribute firstWhere(string $key, $operator = null, $value = null)
     * @method Attribute find($key, $default = null)
     * @method Attribute[] all()
     * @method Attribute last(callable $callback = null, $default = null)
     * @method Attribute sole($key = null, $operator = null, $value = null)
     */
    class _IH_Attribute_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Attribute[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Attribute_QB whereId($value)
     * @method _IH_Attribute_QB whereCode($value)
     * @method _IH_Attribute_QB whereAdminName($value)
     * @method _IH_Attribute_QB whereType($value)
     * @method _IH_Attribute_QB whereValidation($value)
     * @method _IH_Attribute_QB wherePosition($value)
     * @method _IH_Attribute_QB whereIsRequired($value)
     * @method _IH_Attribute_QB whereIsUnique($value)
     * @method _IH_Attribute_QB whereValuePerLocale($value)
     * @method _IH_Attribute_QB whereValuePerChannel($value)
     * @method _IH_Attribute_QB whereIsFilterable($value)
     * @method _IH_Attribute_QB whereIsConfigurable($value)
     * @method _IH_Attribute_QB whereIsUserDefined($value)
     * @method _IH_Attribute_QB whereIsVisibleOnFront($value)
     * @method _IH_Attribute_QB whereCreatedAt($value)
     * @method _IH_Attribute_QB whereUpdatedAt($value)
     * @method _IH_Attribute_QB whereSwatchType($value)
     * @method _IH_Attribute_QB whereUseInFlat($value)
     * @method _IH_Attribute_QB whereIsComparable($value)
     * @method Attribute baseSole(array|string $columns = ['*'])
     * @method Attribute create(array $attributes = [])
     * @method _IH_Attribute_C|Attribute[] cursor()
     * @method Attribute|null|_IH_Attribute_C|Attribute[] find($id, array $columns = ['*'])
     * @method _IH_Attribute_C|Attribute[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Attribute|_IH_Attribute_C|Attribute[] findOrFail($id, array $columns = ['*'])
     * @method Attribute|_IH_Attribute_C|Attribute[] findOrNew($id, array $columns = ['*'])
     * @method Attribute first(array|string $columns = ['*'])
     * @method Attribute firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Attribute firstOrCreate(array $attributes = [], array $values = [])
     * @method Attribute firstOrFail(array $columns = ['*'])
     * @method Attribute firstOrNew(array $attributes = [], array $values = [])
     * @method Attribute firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Attribute forceCreate(array $attributes)
     * @method _IH_Attribute_C|Attribute[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Attribute_C|Attribute[] get(array|string $columns = ['*'])
     * @method Attribute getModel()
     * @method Attribute[] getModels(array|string $columns = ['*'])
     * @method _IH_Attribute_C|Attribute[] hydrate(array $items)
     * @method Attribute make(array $attributes = [])
     * @method Attribute newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Attribute[]|_IH_Attribute_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Attribute[]|_IH_Attribute_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Attribute sole(array|string $columns = ['*'])
     * @method Attribute updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Attribute_QB filterableAttributes()
     * @method _IH_Attribute_QB listsTranslations(string $translationField)
     * @method _IH_Attribute_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Attribute_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Attribute_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Attribute_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Attribute_QB translated()
     * @method _IH_Attribute_QB translatedIn(null|string $locale = null)
     * @method _IH_Attribute_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Attribute_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Attribute_QB withTranslation()
     */
    class _IH_Attribute_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\BookingProduct\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\BookingProduct\Models\Booking;
    use Webkul\BookingProduct\Models\BookingProduct;
    use Webkul\BookingProduct\Models\BookingProductAppointmentSlot;
    use Webkul\BookingProduct\Models\BookingProductDefaultSlot;
    use Webkul\BookingProduct\Models\BookingProductEventTicket;
    use Webkul\BookingProduct\Models\BookingProductEventTicketTranslation;
    use Webkul\BookingProduct\Models\BookingProductRentalSlot;
    use Webkul\BookingProduct\Models\BookingProductTableSlot;
    
    /**
     * @method BookingProductAppointmentSlot shift()
     * @method BookingProductAppointmentSlot pop()
     * @method BookingProductAppointmentSlot get($key, $default = null)
     * @method BookingProductAppointmentSlot pull($key, $default = null)
     * @method BookingProductAppointmentSlot first(callable $callback = null, $default = null)
     * @method BookingProductAppointmentSlot firstWhere(string $key, $operator = null, $value = null)
     * @method BookingProductAppointmentSlot find($key, $default = null)
     * @method BookingProductAppointmentSlot[] all()
     * @method BookingProductAppointmentSlot last(callable $callback = null, $default = null)
     * @method BookingProductAppointmentSlot sole($key = null, $operator = null, $value = null)
     */
    class _IH_BookingProductAppointmentSlot_C extends _BaseCollection {
        /**
         * @param int $size
         * @return BookingProductAppointmentSlot[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_BookingProductAppointmentSlot_QB whereId($value)
     * @method _IH_BookingProductAppointmentSlot_QB whereDuration($value)
     * @method _IH_BookingProductAppointmentSlot_QB whereBreakTime($value)
     * @method _IH_BookingProductAppointmentSlot_QB whereSameSlotAllDays($value)
     * @method _IH_BookingProductAppointmentSlot_QB whereSlots($value)
     * @method _IH_BookingProductAppointmentSlot_QB whereBookingProductId($value)
     * @method BookingProductAppointmentSlot baseSole(array|string $columns = ['*'])
     * @method BookingProductAppointmentSlot create(array $attributes = [])
     * @method _IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] cursor()
     * @method BookingProductAppointmentSlot|null|_IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] find($id, array $columns = ['*'])
     * @method _IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method BookingProductAppointmentSlot|_IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] findOrFail($id, array $columns = ['*'])
     * @method BookingProductAppointmentSlot|_IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] findOrNew($id, array $columns = ['*'])
     * @method BookingProductAppointmentSlot first(array|string $columns = ['*'])
     * @method BookingProductAppointmentSlot firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method BookingProductAppointmentSlot firstOrCreate(array $attributes = [], array $values = [])
     * @method BookingProductAppointmentSlot firstOrFail(array $columns = ['*'])
     * @method BookingProductAppointmentSlot firstOrNew(array $attributes = [], array $values = [])
     * @method BookingProductAppointmentSlot firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method BookingProductAppointmentSlot forceCreate(array $attributes)
     * @method _IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] fromQuery(string $query, array $bindings = [])
     * @method _IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] get(array|string $columns = ['*'])
     * @method BookingProductAppointmentSlot getModel()
     * @method BookingProductAppointmentSlot[] getModels(array|string $columns = ['*'])
     * @method _IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] hydrate(array $items)
     * @method BookingProductAppointmentSlot make(array $attributes = [])
     * @method BookingProductAppointmentSlot newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|BookingProductAppointmentSlot[]|_IH_BookingProductAppointmentSlot_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|BookingProductAppointmentSlot[]|_IH_BookingProductAppointmentSlot_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method BookingProductAppointmentSlot sole(array|string $columns = ['*'])
     * @method BookingProductAppointmentSlot updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_BookingProductAppointmentSlot_QB extends _BaseBuilder {}
    
    /**
     * @method BookingProductDefaultSlot shift()
     * @method BookingProductDefaultSlot pop()
     * @method BookingProductDefaultSlot get($key, $default = null)
     * @method BookingProductDefaultSlot pull($key, $default = null)
     * @method BookingProductDefaultSlot first(callable $callback = null, $default = null)
     * @method BookingProductDefaultSlot firstWhere(string $key, $operator = null, $value = null)
     * @method BookingProductDefaultSlot find($key, $default = null)
     * @method BookingProductDefaultSlot[] all()
     * @method BookingProductDefaultSlot last(callable $callback = null, $default = null)
     * @method BookingProductDefaultSlot sole($key = null, $operator = null, $value = null)
     */
    class _IH_BookingProductDefaultSlot_C extends _BaseCollection {
        /**
         * @param int $size
         * @return BookingProductDefaultSlot[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_BookingProductDefaultSlot_QB whereId($value)
     * @method _IH_BookingProductDefaultSlot_QB whereBookingType($value)
     * @method _IH_BookingProductDefaultSlot_QB whereDuration($value)
     * @method _IH_BookingProductDefaultSlot_QB whereBreakTime($value)
     * @method _IH_BookingProductDefaultSlot_QB whereSlots($value)
     * @method _IH_BookingProductDefaultSlot_QB whereBookingProductId($value)
     * @method BookingProductDefaultSlot baseSole(array|string $columns = ['*'])
     * @method BookingProductDefaultSlot create(array $attributes = [])
     * @method _IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] cursor()
     * @method BookingProductDefaultSlot|null|_IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] find($id, array $columns = ['*'])
     * @method _IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method BookingProductDefaultSlot|_IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] findOrFail($id, array $columns = ['*'])
     * @method BookingProductDefaultSlot|_IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] findOrNew($id, array $columns = ['*'])
     * @method BookingProductDefaultSlot first(array|string $columns = ['*'])
     * @method BookingProductDefaultSlot firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method BookingProductDefaultSlot firstOrCreate(array $attributes = [], array $values = [])
     * @method BookingProductDefaultSlot firstOrFail(array $columns = ['*'])
     * @method BookingProductDefaultSlot firstOrNew(array $attributes = [], array $values = [])
     * @method BookingProductDefaultSlot firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method BookingProductDefaultSlot forceCreate(array $attributes)
     * @method _IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] fromQuery(string $query, array $bindings = [])
     * @method _IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] get(array|string $columns = ['*'])
     * @method BookingProductDefaultSlot getModel()
     * @method BookingProductDefaultSlot[] getModels(array|string $columns = ['*'])
     * @method _IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] hydrate(array $items)
     * @method BookingProductDefaultSlot make(array $attributes = [])
     * @method BookingProductDefaultSlot newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|BookingProductDefaultSlot[]|_IH_BookingProductDefaultSlot_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|BookingProductDefaultSlot[]|_IH_BookingProductDefaultSlot_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method BookingProductDefaultSlot sole(array|string $columns = ['*'])
     * @method BookingProductDefaultSlot updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_BookingProductDefaultSlot_QB extends _BaseBuilder {}
    
    /**
     * @method BookingProductEventTicketTranslation shift()
     * @method BookingProductEventTicketTranslation pop()
     * @method BookingProductEventTicketTranslation get($key, $default = null)
     * @method BookingProductEventTicketTranslation pull($key, $default = null)
     * @method BookingProductEventTicketTranslation first(callable $callback = null, $default = null)
     * @method BookingProductEventTicketTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method BookingProductEventTicketTranslation find($key, $default = null)
     * @method BookingProductEventTicketTranslation[] all()
     * @method BookingProductEventTicketTranslation last(callable $callback = null, $default = null)
     * @method BookingProductEventTicketTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_BookingProductEventTicketTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return BookingProductEventTicketTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_BookingProductEventTicketTranslation_QB whereId($value)
     * @method _IH_BookingProductEventTicketTranslation_QB whereLocale($value)
     * @method _IH_BookingProductEventTicketTranslation_QB whereName($value)
     * @method _IH_BookingProductEventTicketTranslation_QB whereDescription($value)
     * @method _IH_BookingProductEventTicketTranslation_QB whereBookingProductEventTicketId($value)
     * @method BookingProductEventTicketTranslation baseSole(array|string $columns = ['*'])
     * @method BookingProductEventTicketTranslation create(array $attributes = [])
     * @method _IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] cursor()
     * @method BookingProductEventTicketTranslation|null|_IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] find($id, array $columns = ['*'])
     * @method _IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method BookingProductEventTicketTranslation|_IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] findOrFail($id, array $columns = ['*'])
     * @method BookingProductEventTicketTranslation|_IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] findOrNew($id, array $columns = ['*'])
     * @method BookingProductEventTicketTranslation first(array|string $columns = ['*'])
     * @method BookingProductEventTicketTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method BookingProductEventTicketTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method BookingProductEventTicketTranslation firstOrFail(array $columns = ['*'])
     * @method BookingProductEventTicketTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method BookingProductEventTicketTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method BookingProductEventTicketTranslation forceCreate(array $attributes)
     * @method _IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] get(array|string $columns = ['*'])
     * @method BookingProductEventTicketTranslation getModel()
     * @method BookingProductEventTicketTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] hydrate(array $items)
     * @method BookingProductEventTicketTranslation make(array $attributes = [])
     * @method BookingProductEventTicketTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|BookingProductEventTicketTranslation[]|_IH_BookingProductEventTicketTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|BookingProductEventTicketTranslation[]|_IH_BookingProductEventTicketTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method BookingProductEventTicketTranslation sole(array|string $columns = ['*'])
     * @method BookingProductEventTicketTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_BookingProductEventTicketTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method BookingProductEventTicket shift()
     * @method BookingProductEventTicket pop()
     * @method BookingProductEventTicket get($key, $default = null)
     * @method BookingProductEventTicket pull($key, $default = null)
     * @method BookingProductEventTicket first(callable $callback = null, $default = null)
     * @method BookingProductEventTicket firstWhere(string $key, $operator = null, $value = null)
     * @method BookingProductEventTicket find($key, $default = null)
     * @method BookingProductEventTicket[] all()
     * @method BookingProductEventTicket last(callable $callback = null, $default = null)
     * @method BookingProductEventTicket sole($key = null, $operator = null, $value = null)
     */
    class _IH_BookingProductEventTicket_C extends _BaseCollection {
        /**
         * @param int $size
         * @return BookingProductEventTicket[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_BookingProductEventTicket_QB whereId($value)
     * @method _IH_BookingProductEventTicket_QB wherePrice($value)
     * @method _IH_BookingProductEventTicket_QB whereQty($value)
     * @method _IH_BookingProductEventTicket_QB whereSpecialPrice($value)
     * @method _IH_BookingProductEventTicket_QB whereSpecialPriceFrom($value)
     * @method _IH_BookingProductEventTicket_QB whereSpecialPriceTo($value)
     * @method _IH_BookingProductEventTicket_QB whereBookingProductId($value)
     * @method BookingProductEventTicket baseSole(array|string $columns = ['*'])
     * @method BookingProductEventTicket create(array $attributes = [])
     * @method _IH_BookingProductEventTicket_C|BookingProductEventTicket[] cursor()
     * @method BookingProductEventTicket|null|_IH_BookingProductEventTicket_C|BookingProductEventTicket[] find($id, array $columns = ['*'])
     * @method _IH_BookingProductEventTicket_C|BookingProductEventTicket[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method BookingProductEventTicket|_IH_BookingProductEventTicket_C|BookingProductEventTicket[] findOrFail($id, array $columns = ['*'])
     * @method BookingProductEventTicket|_IH_BookingProductEventTicket_C|BookingProductEventTicket[] findOrNew($id, array $columns = ['*'])
     * @method BookingProductEventTicket first(array|string $columns = ['*'])
     * @method BookingProductEventTicket firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method BookingProductEventTicket firstOrCreate(array $attributes = [], array $values = [])
     * @method BookingProductEventTicket firstOrFail(array $columns = ['*'])
     * @method BookingProductEventTicket firstOrNew(array $attributes = [], array $values = [])
     * @method BookingProductEventTicket firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method BookingProductEventTicket forceCreate(array $attributes)
     * @method _IH_BookingProductEventTicket_C|BookingProductEventTicket[] fromQuery(string $query, array $bindings = [])
     * @method _IH_BookingProductEventTicket_C|BookingProductEventTicket[] get(array|string $columns = ['*'])
     * @method BookingProductEventTicket getModel()
     * @method BookingProductEventTicket[] getModels(array|string $columns = ['*'])
     * @method _IH_BookingProductEventTicket_C|BookingProductEventTicket[] hydrate(array $items)
     * @method BookingProductEventTicket make(array $attributes = [])
     * @method BookingProductEventTicket newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|BookingProductEventTicket[]|_IH_BookingProductEventTicket_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|BookingProductEventTicket[]|_IH_BookingProductEventTicket_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method BookingProductEventTicket sole(array|string $columns = ['*'])
     * @method BookingProductEventTicket updateOrCreate(array $attributes, array $values = [])
     * @method _IH_BookingProductEventTicket_QB listsTranslations(string $translationField)
     * @method _IH_BookingProductEventTicket_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_BookingProductEventTicket_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_BookingProductEventTicket_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_BookingProductEventTicket_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_BookingProductEventTicket_QB translated()
     * @method _IH_BookingProductEventTicket_QB translatedIn(null|string $locale = null)
     * @method _IH_BookingProductEventTicket_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_BookingProductEventTicket_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_BookingProductEventTicket_QB withTranslation()
     */
    class _IH_BookingProductEventTicket_QB extends _BaseBuilder {}
    
    /**
     * @method BookingProductRentalSlot shift()
     * @method BookingProductRentalSlot pop()
     * @method BookingProductRentalSlot get($key, $default = null)
     * @method BookingProductRentalSlot pull($key, $default = null)
     * @method BookingProductRentalSlot first(callable $callback = null, $default = null)
     * @method BookingProductRentalSlot firstWhere(string $key, $operator = null, $value = null)
     * @method BookingProductRentalSlot find($key, $default = null)
     * @method BookingProductRentalSlot[] all()
     * @method BookingProductRentalSlot last(callable $callback = null, $default = null)
     * @method BookingProductRentalSlot sole($key = null, $operator = null, $value = null)
     */
    class _IH_BookingProductRentalSlot_C extends _BaseCollection {
        /**
         * @param int $size
         * @return BookingProductRentalSlot[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_BookingProductRentalSlot_QB whereId($value)
     * @method _IH_BookingProductRentalSlot_QB whereRentingType($value)
     * @method _IH_BookingProductRentalSlot_QB whereDailyPrice($value)
     * @method _IH_BookingProductRentalSlot_QB whereHourlyPrice($value)
     * @method _IH_BookingProductRentalSlot_QB whereSameSlotAllDays($value)
     * @method _IH_BookingProductRentalSlot_QB whereSlots($value)
     * @method _IH_BookingProductRentalSlot_QB whereBookingProductId($value)
     * @method BookingProductRentalSlot baseSole(array|string $columns = ['*'])
     * @method BookingProductRentalSlot create(array $attributes = [])
     * @method _IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] cursor()
     * @method BookingProductRentalSlot|null|_IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] find($id, array $columns = ['*'])
     * @method _IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method BookingProductRentalSlot|_IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] findOrFail($id, array $columns = ['*'])
     * @method BookingProductRentalSlot|_IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] findOrNew($id, array $columns = ['*'])
     * @method BookingProductRentalSlot first(array|string $columns = ['*'])
     * @method BookingProductRentalSlot firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method BookingProductRentalSlot firstOrCreate(array $attributes = [], array $values = [])
     * @method BookingProductRentalSlot firstOrFail(array $columns = ['*'])
     * @method BookingProductRentalSlot firstOrNew(array $attributes = [], array $values = [])
     * @method BookingProductRentalSlot firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method BookingProductRentalSlot forceCreate(array $attributes)
     * @method _IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] fromQuery(string $query, array $bindings = [])
     * @method _IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] get(array|string $columns = ['*'])
     * @method BookingProductRentalSlot getModel()
     * @method BookingProductRentalSlot[] getModels(array|string $columns = ['*'])
     * @method _IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] hydrate(array $items)
     * @method BookingProductRentalSlot make(array $attributes = [])
     * @method BookingProductRentalSlot newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|BookingProductRentalSlot[]|_IH_BookingProductRentalSlot_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|BookingProductRentalSlot[]|_IH_BookingProductRentalSlot_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method BookingProductRentalSlot sole(array|string $columns = ['*'])
     * @method BookingProductRentalSlot updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_BookingProductRentalSlot_QB extends _BaseBuilder {}
    
    /**
     * @method BookingProductTableSlot shift()
     * @method BookingProductTableSlot pop()
     * @method BookingProductTableSlot get($key, $default = null)
     * @method BookingProductTableSlot pull($key, $default = null)
     * @method BookingProductTableSlot first(callable $callback = null, $default = null)
     * @method BookingProductTableSlot firstWhere(string $key, $operator = null, $value = null)
     * @method BookingProductTableSlot find($key, $default = null)
     * @method BookingProductTableSlot[] all()
     * @method BookingProductTableSlot last(callable $callback = null, $default = null)
     * @method BookingProductTableSlot sole($key = null, $operator = null, $value = null)
     */
    class _IH_BookingProductTableSlot_C extends _BaseCollection {
        /**
         * @param int $size
         * @return BookingProductTableSlot[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_BookingProductTableSlot_QB whereId($value)
     * @method _IH_BookingProductTableSlot_QB wherePriceType($value)
     * @method _IH_BookingProductTableSlot_QB whereGuestLimit($value)
     * @method _IH_BookingProductTableSlot_QB whereDuration($value)
     * @method _IH_BookingProductTableSlot_QB whereBreakTime($value)
     * @method _IH_BookingProductTableSlot_QB wherePreventSchedulingBefore($value)
     * @method _IH_BookingProductTableSlot_QB whereSameSlotAllDays($value)
     * @method _IH_BookingProductTableSlot_QB whereSlots($value)
     * @method _IH_BookingProductTableSlot_QB whereBookingProductId($value)
     * @method BookingProductTableSlot baseSole(array|string $columns = ['*'])
     * @method BookingProductTableSlot create(array $attributes = [])
     * @method _IH_BookingProductTableSlot_C|BookingProductTableSlot[] cursor()
     * @method BookingProductTableSlot|null|_IH_BookingProductTableSlot_C|BookingProductTableSlot[] find($id, array $columns = ['*'])
     * @method _IH_BookingProductTableSlot_C|BookingProductTableSlot[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method BookingProductTableSlot|_IH_BookingProductTableSlot_C|BookingProductTableSlot[] findOrFail($id, array $columns = ['*'])
     * @method BookingProductTableSlot|_IH_BookingProductTableSlot_C|BookingProductTableSlot[] findOrNew($id, array $columns = ['*'])
     * @method BookingProductTableSlot first(array|string $columns = ['*'])
     * @method BookingProductTableSlot firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method BookingProductTableSlot firstOrCreate(array $attributes = [], array $values = [])
     * @method BookingProductTableSlot firstOrFail(array $columns = ['*'])
     * @method BookingProductTableSlot firstOrNew(array $attributes = [], array $values = [])
     * @method BookingProductTableSlot firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method BookingProductTableSlot forceCreate(array $attributes)
     * @method _IH_BookingProductTableSlot_C|BookingProductTableSlot[] fromQuery(string $query, array $bindings = [])
     * @method _IH_BookingProductTableSlot_C|BookingProductTableSlot[] get(array|string $columns = ['*'])
     * @method BookingProductTableSlot getModel()
     * @method BookingProductTableSlot[] getModels(array|string $columns = ['*'])
     * @method _IH_BookingProductTableSlot_C|BookingProductTableSlot[] hydrate(array $items)
     * @method BookingProductTableSlot make(array $attributes = [])
     * @method BookingProductTableSlot newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|BookingProductTableSlot[]|_IH_BookingProductTableSlot_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|BookingProductTableSlot[]|_IH_BookingProductTableSlot_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method BookingProductTableSlot sole(array|string $columns = ['*'])
     * @method BookingProductTableSlot updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_BookingProductTableSlot_QB extends _BaseBuilder {}
    
    /**
     * @method BookingProduct shift()
     * @method BookingProduct pop()
     * @method BookingProduct get($key, $default = null)
     * @method BookingProduct pull($key, $default = null)
     * @method BookingProduct first(callable $callback = null, $default = null)
     * @method BookingProduct firstWhere(string $key, $operator = null, $value = null)
     * @method BookingProduct find($key, $default = null)
     * @method BookingProduct[] all()
     * @method BookingProduct last(callable $callback = null, $default = null)
     * @method BookingProduct sole($key = null, $operator = null, $value = null)
     */
    class _IH_BookingProduct_C extends _BaseCollection {
        /**
         * @param int $size
         * @return BookingProduct[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_BookingProduct_QB whereId($value)
     * @method _IH_BookingProduct_QB whereType($value)
     * @method _IH_BookingProduct_QB whereQty($value)
     * @method _IH_BookingProduct_QB whereLocation($value)
     * @method _IH_BookingProduct_QB whereShowLocation($value)
     * @method _IH_BookingProduct_QB whereAvailableEveryWeek($value)
     * @method _IH_BookingProduct_QB whereAvailableFrom($value)
     * @method _IH_BookingProduct_QB whereAvailableTo($value)
     * @method _IH_BookingProduct_QB whereProductId($value)
     * @method _IH_BookingProduct_QB whereCreatedAt($value)
     * @method _IH_BookingProduct_QB whereUpdatedAt($value)
     * @method BookingProduct baseSole(array|string $columns = ['*'])
     * @method BookingProduct create(array $attributes = [])
     * @method _IH_BookingProduct_C|BookingProduct[] cursor()
     * @method BookingProduct|null|_IH_BookingProduct_C|BookingProduct[] find($id, array $columns = ['*'])
     * @method _IH_BookingProduct_C|BookingProduct[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method BookingProduct|_IH_BookingProduct_C|BookingProduct[] findOrFail($id, array $columns = ['*'])
     * @method BookingProduct|_IH_BookingProduct_C|BookingProduct[] findOrNew($id, array $columns = ['*'])
     * @method BookingProduct first(array|string $columns = ['*'])
     * @method BookingProduct firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method BookingProduct firstOrCreate(array $attributes = [], array $values = [])
     * @method BookingProduct firstOrFail(array $columns = ['*'])
     * @method BookingProduct firstOrNew(array $attributes = [], array $values = [])
     * @method BookingProduct firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method BookingProduct forceCreate(array $attributes)
     * @method _IH_BookingProduct_C|BookingProduct[] fromQuery(string $query, array $bindings = [])
     * @method _IH_BookingProduct_C|BookingProduct[] get(array|string $columns = ['*'])
     * @method BookingProduct getModel()
     * @method BookingProduct[] getModels(array|string $columns = ['*'])
     * @method _IH_BookingProduct_C|BookingProduct[] hydrate(array $items)
     * @method BookingProduct make(array $attributes = [])
     * @method BookingProduct newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|BookingProduct[]|_IH_BookingProduct_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|BookingProduct[]|_IH_BookingProduct_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method BookingProduct sole(array|string $columns = ['*'])
     * @method BookingProduct updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_BookingProduct_QB extends _BaseBuilder {}
    
    /**
     * @method Booking shift()
     * @method Booking pop()
     * @method Booking get($key, $default = null)
     * @method Booking pull($key, $default = null)
     * @method Booking first(callable $callback = null, $default = null)
     * @method Booking firstWhere(string $key, $operator = null, $value = null)
     * @method Booking find($key, $default = null)
     * @method Booking[] all()
     * @method Booking last(callable $callback = null, $default = null)
     * @method Booking sole($key = null, $operator = null, $value = null)
     */
    class _IH_Booking_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Booking[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Booking_QB whereId($value)
     * @method _IH_Booking_QB whereQty($value)
     * @method _IH_Booking_QB whereFrom($value)
     * @method _IH_Booking_QB whereTo($value)
     * @method _IH_Booking_QB whereOrderItemId($value)
     * @method _IH_Booking_QB whereBookingProductEventTicketId($value)
     * @method _IH_Booking_QB whereOrderId($value)
     * @method _IH_Booking_QB whereProductId($value)
     * @method Booking baseSole(array|string $columns = ['*'])
     * @method Booking create(array $attributes = [])
     * @method _IH_Booking_C|Booking[] cursor()
     * @method Booking|null|_IH_Booking_C|Booking[] find($id, array $columns = ['*'])
     * @method _IH_Booking_C|Booking[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Booking|_IH_Booking_C|Booking[] findOrFail($id, array $columns = ['*'])
     * @method Booking|_IH_Booking_C|Booking[] findOrNew($id, array $columns = ['*'])
     * @method Booking first(array|string $columns = ['*'])
     * @method Booking firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Booking firstOrCreate(array $attributes = [], array $values = [])
     * @method Booking firstOrFail(array $columns = ['*'])
     * @method Booking firstOrNew(array $attributes = [], array $values = [])
     * @method Booking firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Booking forceCreate(array $attributes)
     * @method _IH_Booking_C|Booking[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Booking_C|Booking[] get(array|string $columns = ['*'])
     * @method Booking getModel()
     * @method Booking[] getModels(array|string $columns = ['*'])
     * @method _IH_Booking_C|Booking[] hydrate(array $items)
     * @method Booking make(array $attributes = [])
     * @method Booking newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Booking[]|_IH_Booking_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Booking[]|_IH_Booking_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Booking sole(array|string $columns = ['*'])
     * @method Booking updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Booking_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\CMS\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\CMS\Models\CmsPage;
    use Webkul\CMS\Models\CmsPageTranslation;
    
    /**
     * @method CmsPageTranslation shift()
     * @method CmsPageTranslation pop()
     * @method CmsPageTranslation get($key, $default = null)
     * @method CmsPageTranslation pull($key, $default = null)
     * @method CmsPageTranslation first(callable $callback = null, $default = null)
     * @method CmsPageTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method CmsPageTranslation find($key, $default = null)
     * @method CmsPageTranslation[] all()
     * @method CmsPageTranslation last(callable $callback = null, $default = null)
     * @method CmsPageTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_CmsPageTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CmsPageTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CmsPageTranslation_QB whereId($value)
     * @method _IH_CmsPageTranslation_QB wherePageTitle($value)
     * @method _IH_CmsPageTranslation_QB whereUrlKey($value)
     * @method _IH_CmsPageTranslation_QB whereHtmlContent($value)
     * @method _IH_CmsPageTranslation_QB whereMetaTitle($value)
     * @method _IH_CmsPageTranslation_QB whereMetaDescription($value)
     * @method _IH_CmsPageTranslation_QB whereMetaKeywords($value)
     * @method _IH_CmsPageTranslation_QB whereLocale($value)
     * @method _IH_CmsPageTranslation_QB whereCmsPageId($value)
     * @method CmsPageTranslation baseSole(array|string $columns = ['*'])
     * @method CmsPageTranslation create(array $attributes = [])
     * @method _IH_CmsPageTranslation_C|CmsPageTranslation[] cursor()
     * @method CmsPageTranslation|null|_IH_CmsPageTranslation_C|CmsPageTranslation[] find($id, array $columns = ['*'])
     * @method _IH_CmsPageTranslation_C|CmsPageTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CmsPageTranslation|_IH_CmsPageTranslation_C|CmsPageTranslation[] findOrFail($id, array $columns = ['*'])
     * @method CmsPageTranslation|_IH_CmsPageTranslation_C|CmsPageTranslation[] findOrNew($id, array $columns = ['*'])
     * @method CmsPageTranslation first(array|string $columns = ['*'])
     * @method CmsPageTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CmsPageTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method CmsPageTranslation firstOrFail(array $columns = ['*'])
     * @method CmsPageTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method CmsPageTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CmsPageTranslation forceCreate(array $attributes)
     * @method _IH_CmsPageTranslation_C|CmsPageTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CmsPageTranslation_C|CmsPageTranslation[] get(array|string $columns = ['*'])
     * @method CmsPageTranslation getModel()
     * @method CmsPageTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_CmsPageTranslation_C|CmsPageTranslation[] hydrate(array $items)
     * @method CmsPageTranslation make(array $attributes = [])
     * @method CmsPageTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CmsPageTranslation[]|_IH_CmsPageTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CmsPageTranslation[]|_IH_CmsPageTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CmsPageTranslation sole(array|string $columns = ['*'])
     * @method CmsPageTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CmsPageTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method CmsPage shift()
     * @method CmsPage pop()
     * @method CmsPage get($key, $default = null)
     * @method CmsPage pull($key, $default = null)
     * @method CmsPage first(callable $callback = null, $default = null)
     * @method CmsPage firstWhere(string $key, $operator = null, $value = null)
     * @method CmsPage find($key, $default = null)
     * @method CmsPage[] all()
     * @method CmsPage last(callable $callback = null, $default = null)
     * @method CmsPage sole($key = null, $operator = null, $value = null)
     */
    class _IH_CmsPage_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CmsPage[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CmsPage_QB whereId($value)
     * @method _IH_CmsPage_QB whereLayout($value)
     * @method _IH_CmsPage_QB whereCreatedAt($value)
     * @method _IH_CmsPage_QB whereUpdatedAt($value)
     * @method CmsPage baseSole(array|string $columns = ['*'])
     * @method CmsPage create(array $attributes = [])
     * @method _IH_CmsPage_C|CmsPage[] cursor()
     * @method CmsPage|null|_IH_CmsPage_C|CmsPage[] find($id, array $columns = ['*'])
     * @method _IH_CmsPage_C|CmsPage[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CmsPage|_IH_CmsPage_C|CmsPage[] findOrFail($id, array $columns = ['*'])
     * @method CmsPage|_IH_CmsPage_C|CmsPage[] findOrNew($id, array $columns = ['*'])
     * @method CmsPage first(array|string $columns = ['*'])
     * @method CmsPage firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CmsPage firstOrCreate(array $attributes = [], array $values = [])
     * @method CmsPage firstOrFail(array $columns = ['*'])
     * @method CmsPage firstOrNew(array $attributes = [], array $values = [])
     * @method CmsPage firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CmsPage forceCreate(array $attributes)
     * @method _IH_CmsPage_C|CmsPage[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CmsPage_C|CmsPage[] get(array|string $columns = ['*'])
     * @method CmsPage getModel()
     * @method CmsPage[] getModels(array|string $columns = ['*'])
     * @method _IH_CmsPage_C|CmsPage[] hydrate(array $items)
     * @method CmsPage make(array $attributes = [])
     * @method CmsPage newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CmsPage[]|_IH_CmsPage_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CmsPage[]|_IH_CmsPage_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CmsPage sole(array|string $columns = ['*'])
     * @method CmsPage updateOrCreate(array $attributes, array $values = [])
     * @method _IH_CmsPage_QB listsTranslations(string $translationField)
     * @method _IH_CmsPage_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_CmsPage_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_CmsPage_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_CmsPage_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_CmsPage_QB translated()
     * @method _IH_CmsPage_QB translatedIn(null|string $locale = null)
     * @method _IH_CmsPage_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_CmsPage_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_CmsPage_QB withTranslation()
     */
    class _IH_CmsPage_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\CartRule\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\CartRule\Models\CartRule;
    use Webkul\CartRule\Models\CartRuleCoupon;
    use Webkul\CartRule\Models\CartRuleCouponUsage;
    use Webkul\CartRule\Models\CartRuleCustomer;
    use Webkul\CartRule\Models\CartRuleTranslation;
    
    /**
     * @method CartRuleCouponUsage shift()
     * @method CartRuleCouponUsage pop()
     * @method CartRuleCouponUsage get($key, $default = null)
     * @method CartRuleCouponUsage pull($key, $default = null)
     * @method CartRuleCouponUsage first(callable $callback = null, $default = null)
     * @method CartRuleCouponUsage firstWhere(string $key, $operator = null, $value = null)
     * @method CartRuleCouponUsage find($key, $default = null)
     * @method CartRuleCouponUsage[] all()
     * @method CartRuleCouponUsage last(callable $callback = null, $default = null)
     * @method CartRuleCouponUsage sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartRuleCouponUsage_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartRuleCouponUsage[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartRuleCouponUsage_QB whereId($value)
     * @method _IH_CartRuleCouponUsage_QB whereTimesUsed($value)
     * @method _IH_CartRuleCouponUsage_QB whereCartRuleCouponId($value)
     * @method _IH_CartRuleCouponUsage_QB whereCustomerId($value)
     * @method CartRuleCouponUsage baseSole(array|string $columns = ['*'])
     * @method CartRuleCouponUsage create(array $attributes = [])
     * @method _IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] cursor()
     * @method CartRuleCouponUsage|null|_IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] find($id, array $columns = ['*'])
     * @method _IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartRuleCouponUsage|_IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] findOrFail($id, array $columns = ['*'])
     * @method CartRuleCouponUsage|_IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] findOrNew($id, array $columns = ['*'])
     * @method CartRuleCouponUsage first(array|string $columns = ['*'])
     * @method CartRuleCouponUsage firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartRuleCouponUsage firstOrCreate(array $attributes = [], array $values = [])
     * @method CartRuleCouponUsage firstOrFail(array $columns = ['*'])
     * @method CartRuleCouponUsage firstOrNew(array $attributes = [], array $values = [])
     * @method CartRuleCouponUsage firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartRuleCouponUsage forceCreate(array $attributes)
     * @method _IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] get(array|string $columns = ['*'])
     * @method CartRuleCouponUsage getModel()
     * @method CartRuleCouponUsage[] getModels(array|string $columns = ['*'])
     * @method _IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] hydrate(array $items)
     * @method CartRuleCouponUsage make(array $attributes = [])
     * @method CartRuleCouponUsage newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartRuleCouponUsage[]|_IH_CartRuleCouponUsage_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartRuleCouponUsage[]|_IH_CartRuleCouponUsage_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartRuleCouponUsage sole(array|string $columns = ['*'])
     * @method CartRuleCouponUsage updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartRuleCouponUsage_QB extends _BaseBuilder {}
    
    /**
     * @method CartRuleCoupon shift()
     * @method CartRuleCoupon pop()
     * @method CartRuleCoupon get($key, $default = null)
     * @method CartRuleCoupon pull($key, $default = null)
     * @method CartRuleCoupon first(callable $callback = null, $default = null)
     * @method CartRuleCoupon firstWhere(string $key, $operator = null, $value = null)
     * @method CartRuleCoupon find($key, $default = null)
     * @method CartRuleCoupon[] all()
     * @method CartRuleCoupon last(callable $callback = null, $default = null)
     * @method CartRuleCoupon sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartRuleCoupon_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartRuleCoupon[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartRuleCoupon_QB whereId($value)
     * @method _IH_CartRuleCoupon_QB whereCode($value)
     * @method _IH_CartRuleCoupon_QB whereUsageLimit($value)
     * @method _IH_CartRuleCoupon_QB whereUsagePerCustomer($value)
     * @method _IH_CartRuleCoupon_QB whereTimesUsed($value)
     * @method _IH_CartRuleCoupon_QB whereType($value)
     * @method _IH_CartRuleCoupon_QB whereIsPrimary($value)
     * @method _IH_CartRuleCoupon_QB whereExpiredAt($value)
     * @method _IH_CartRuleCoupon_QB whereCartRuleId($value)
     * @method _IH_CartRuleCoupon_QB whereCreatedAt($value)
     * @method _IH_CartRuleCoupon_QB whereUpdatedAt($value)
     * @method CartRuleCoupon baseSole(array|string $columns = ['*'])
     * @method CartRuleCoupon create(array $attributes = [])
     * @method _IH_CartRuleCoupon_C|CartRuleCoupon[] cursor()
     * @method CartRuleCoupon|null|_IH_CartRuleCoupon_C|CartRuleCoupon[] find($id, array $columns = ['*'])
     * @method _IH_CartRuleCoupon_C|CartRuleCoupon[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartRuleCoupon|_IH_CartRuleCoupon_C|CartRuleCoupon[] findOrFail($id, array $columns = ['*'])
     * @method CartRuleCoupon|_IH_CartRuleCoupon_C|CartRuleCoupon[] findOrNew($id, array $columns = ['*'])
     * @method CartRuleCoupon first(array|string $columns = ['*'])
     * @method CartRuleCoupon firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartRuleCoupon firstOrCreate(array $attributes = [], array $values = [])
     * @method CartRuleCoupon firstOrFail(array $columns = ['*'])
     * @method CartRuleCoupon firstOrNew(array $attributes = [], array $values = [])
     * @method CartRuleCoupon firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartRuleCoupon forceCreate(array $attributes)
     * @method _IH_CartRuleCoupon_C|CartRuleCoupon[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartRuleCoupon_C|CartRuleCoupon[] get(array|string $columns = ['*'])
     * @method CartRuleCoupon getModel()
     * @method CartRuleCoupon[] getModels(array|string $columns = ['*'])
     * @method _IH_CartRuleCoupon_C|CartRuleCoupon[] hydrate(array $items)
     * @method CartRuleCoupon make(array $attributes = [])
     * @method CartRuleCoupon newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartRuleCoupon[]|_IH_CartRuleCoupon_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartRuleCoupon[]|_IH_CartRuleCoupon_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartRuleCoupon sole(array|string $columns = ['*'])
     * @method CartRuleCoupon updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartRuleCoupon_QB extends _BaseBuilder {}
    
    /**
     * @method CartRuleCustomer shift()
     * @method CartRuleCustomer pop()
     * @method CartRuleCustomer get($key, $default = null)
     * @method CartRuleCustomer pull($key, $default = null)
     * @method CartRuleCustomer first(callable $callback = null, $default = null)
     * @method CartRuleCustomer firstWhere(string $key, $operator = null, $value = null)
     * @method CartRuleCustomer find($key, $default = null)
     * @method CartRuleCustomer[] all()
     * @method CartRuleCustomer last(callable $callback = null, $default = null)
     * @method CartRuleCustomer sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartRuleCustomer_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartRuleCustomer[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartRuleCustomer_QB whereId($value)
     * @method _IH_CartRuleCustomer_QB whereTimesUsed($value)
     * @method _IH_CartRuleCustomer_QB whereCartRuleId($value)
     * @method _IH_CartRuleCustomer_QB whereCustomerId($value)
     * @method CartRuleCustomer baseSole(array|string $columns = ['*'])
     * @method CartRuleCustomer create(array $attributes = [])
     * @method _IH_CartRuleCustomer_C|CartRuleCustomer[] cursor()
     * @method CartRuleCustomer|null|_IH_CartRuleCustomer_C|CartRuleCustomer[] find($id, array $columns = ['*'])
     * @method _IH_CartRuleCustomer_C|CartRuleCustomer[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartRuleCustomer|_IH_CartRuleCustomer_C|CartRuleCustomer[] findOrFail($id, array $columns = ['*'])
     * @method CartRuleCustomer|_IH_CartRuleCustomer_C|CartRuleCustomer[] findOrNew($id, array $columns = ['*'])
     * @method CartRuleCustomer first(array|string $columns = ['*'])
     * @method CartRuleCustomer firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartRuleCustomer firstOrCreate(array $attributes = [], array $values = [])
     * @method CartRuleCustomer firstOrFail(array $columns = ['*'])
     * @method CartRuleCustomer firstOrNew(array $attributes = [], array $values = [])
     * @method CartRuleCustomer firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartRuleCustomer forceCreate(array $attributes)
     * @method _IH_CartRuleCustomer_C|CartRuleCustomer[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartRuleCustomer_C|CartRuleCustomer[] get(array|string $columns = ['*'])
     * @method CartRuleCustomer getModel()
     * @method CartRuleCustomer[] getModels(array|string $columns = ['*'])
     * @method _IH_CartRuleCustomer_C|CartRuleCustomer[] hydrate(array $items)
     * @method CartRuleCustomer make(array $attributes = [])
     * @method CartRuleCustomer newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartRuleCustomer[]|_IH_CartRuleCustomer_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartRuleCustomer[]|_IH_CartRuleCustomer_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartRuleCustomer sole(array|string $columns = ['*'])
     * @method CartRuleCustomer updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartRuleCustomer_QB extends _BaseBuilder {}
    
    /**
     * @method CartRuleTranslation shift()
     * @method CartRuleTranslation pop()
     * @method CartRuleTranslation get($key, $default = null)
     * @method CartRuleTranslation pull($key, $default = null)
     * @method CartRuleTranslation first(callable $callback = null, $default = null)
     * @method CartRuleTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method CartRuleTranslation find($key, $default = null)
     * @method CartRuleTranslation[] all()
     * @method CartRuleTranslation last(callable $callback = null, $default = null)
     * @method CartRuleTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartRuleTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartRuleTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartRuleTranslation_QB whereId($value)
     * @method _IH_CartRuleTranslation_QB whereLocale($value)
     * @method _IH_CartRuleTranslation_QB whereLabel($value)
     * @method _IH_CartRuleTranslation_QB whereCartRuleId($value)
     * @method CartRuleTranslation baseSole(array|string $columns = ['*'])
     * @method CartRuleTranslation create(array $attributes = [])
     * @method _IH_CartRuleTranslation_C|CartRuleTranslation[] cursor()
     * @method CartRuleTranslation|null|_IH_CartRuleTranslation_C|CartRuleTranslation[] find($id, array $columns = ['*'])
     * @method _IH_CartRuleTranslation_C|CartRuleTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartRuleTranslation|_IH_CartRuleTranslation_C|CartRuleTranslation[] findOrFail($id, array $columns = ['*'])
     * @method CartRuleTranslation|_IH_CartRuleTranslation_C|CartRuleTranslation[] findOrNew($id, array $columns = ['*'])
     * @method CartRuleTranslation first(array|string $columns = ['*'])
     * @method CartRuleTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartRuleTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method CartRuleTranslation firstOrFail(array $columns = ['*'])
     * @method CartRuleTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method CartRuleTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartRuleTranslation forceCreate(array $attributes)
     * @method _IH_CartRuleTranslation_C|CartRuleTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartRuleTranslation_C|CartRuleTranslation[] get(array|string $columns = ['*'])
     * @method CartRuleTranslation getModel()
     * @method CartRuleTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_CartRuleTranslation_C|CartRuleTranslation[] hydrate(array $items)
     * @method CartRuleTranslation make(array $attributes = [])
     * @method CartRuleTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartRuleTranslation[]|_IH_CartRuleTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartRuleTranslation[]|_IH_CartRuleTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartRuleTranslation sole(array|string $columns = ['*'])
     * @method CartRuleTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartRuleTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method CartRule shift()
     * @method CartRule pop()
     * @method CartRule get($key, $default = null)
     * @method CartRule pull($key, $default = null)
     * @method CartRule first(callable $callback = null, $default = null)
     * @method CartRule firstWhere(string $key, $operator = null, $value = null)
     * @method CartRule find($key, $default = null)
     * @method CartRule[] all()
     * @method CartRule last(callable $callback = null, $default = null)
     * @method CartRule sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartRule_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartRule[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartRule_QB whereId($value)
     * @method _IH_CartRule_QB whereName($value)
     * @method _IH_CartRule_QB whereDescription($value)
     * @method _IH_CartRule_QB whereStartsFrom($value)
     * @method _IH_CartRule_QB whereEndsTill($value)
     * @method _IH_CartRule_QB whereStatus($value)
     * @method _IH_CartRule_QB whereCouponType($value)
     * @method _IH_CartRule_QB whereUseAutoGeneration($value)
     * @method _IH_CartRule_QB whereUsagePerCustomer($value)
     * @method _IH_CartRule_QB whereUsesPerCoupon($value)
     * @method _IH_CartRule_QB whereTimesUsed($value)
     * @method _IH_CartRule_QB whereConditionType($value)
     * @method _IH_CartRule_QB whereConditions($value)
     * @method _IH_CartRule_QB whereEndOtherRules($value)
     * @method _IH_CartRule_QB whereUsesAttributeConditions($value)
     * @method _IH_CartRule_QB whereActionType($value)
     * @method _IH_CartRule_QB whereDiscountAmount($value)
     * @method _IH_CartRule_QB whereDiscountQuantity($value)
     * @method _IH_CartRule_QB whereDiscountStep($value)
     * @method _IH_CartRule_QB whereApplyToShipping($value)
     * @method _IH_CartRule_QB whereFreeShipping($value)
     * @method _IH_CartRule_QB whereSortOrder($value)
     * @method _IH_CartRule_QB whereCreatedAt($value)
     * @method _IH_CartRule_QB whereUpdatedAt($value)
     * @method CartRule baseSole(array|string $columns = ['*'])
     * @method CartRule create(array $attributes = [])
     * @method _IH_CartRule_C|CartRule[] cursor()
     * @method CartRule|null|_IH_CartRule_C|CartRule[] find($id, array $columns = ['*'])
     * @method _IH_CartRule_C|CartRule[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartRule|_IH_CartRule_C|CartRule[] findOrFail($id, array $columns = ['*'])
     * @method CartRule|_IH_CartRule_C|CartRule[] findOrNew($id, array $columns = ['*'])
     * @method CartRule first(array|string $columns = ['*'])
     * @method CartRule firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartRule firstOrCreate(array $attributes = [], array $values = [])
     * @method CartRule firstOrFail(array $columns = ['*'])
     * @method CartRule firstOrNew(array $attributes = [], array $values = [])
     * @method CartRule firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartRule forceCreate(array $attributes)
     * @method _IH_CartRule_C|CartRule[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartRule_C|CartRule[] get(array|string $columns = ['*'])
     * @method CartRule getModel()
     * @method CartRule[] getModels(array|string $columns = ['*'])
     * @method _IH_CartRule_C|CartRule[] hydrate(array $items)
     * @method CartRule make(array $attributes = [])
     * @method CartRule newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartRule[]|_IH_CartRule_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartRule[]|_IH_CartRule_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartRule sole(array|string $columns = ['*'])
     * @method CartRule updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartRule_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\CatalogRule\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\CatalogRule\Models\CatalogRule;
    use Webkul\CatalogRule\Models\CatalogRuleProduct;
    use Webkul\CatalogRule\Models\CatalogRuleProductPrice;
    
    /**
     * @method CatalogRuleProductPrice shift()
     * @method CatalogRuleProductPrice pop()
     * @method CatalogRuleProductPrice get($key, $default = null)
     * @method CatalogRuleProductPrice pull($key, $default = null)
     * @method CatalogRuleProductPrice first(callable $callback = null, $default = null)
     * @method CatalogRuleProductPrice firstWhere(string $key, $operator = null, $value = null)
     * @method CatalogRuleProductPrice find($key, $default = null)
     * @method CatalogRuleProductPrice[] all()
     * @method CatalogRuleProductPrice last(callable $callback = null, $default = null)
     * @method CatalogRuleProductPrice sole($key = null, $operator = null, $value = null)
     */
    class _IH_CatalogRuleProductPrice_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CatalogRuleProductPrice[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CatalogRuleProductPrice_QB whereId($value)
     * @method _IH_CatalogRuleProductPrice_QB wherePrice($value)
     * @method _IH_CatalogRuleProductPrice_QB whereRuleDate($value)
     * @method _IH_CatalogRuleProductPrice_QB whereStartsFrom($value)
     * @method _IH_CatalogRuleProductPrice_QB whereEndsTill($value)
     * @method _IH_CatalogRuleProductPrice_QB whereProductId($value)
     * @method _IH_CatalogRuleProductPrice_QB whereCustomerGroupId($value)
     * @method _IH_CatalogRuleProductPrice_QB whereCatalogRuleId($value)
     * @method _IH_CatalogRuleProductPrice_QB whereChannelId($value)
     * @method CatalogRuleProductPrice baseSole(array|string $columns = ['*'])
     * @method CatalogRuleProductPrice create(array $attributes = [])
     * @method _IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] cursor()
     * @method CatalogRuleProductPrice|null|_IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] find($id, array $columns = ['*'])
     * @method _IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CatalogRuleProductPrice|_IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] findOrFail($id, array $columns = ['*'])
     * @method CatalogRuleProductPrice|_IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] findOrNew($id, array $columns = ['*'])
     * @method CatalogRuleProductPrice first(array|string $columns = ['*'])
     * @method CatalogRuleProductPrice firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CatalogRuleProductPrice firstOrCreate(array $attributes = [], array $values = [])
     * @method CatalogRuleProductPrice firstOrFail(array $columns = ['*'])
     * @method CatalogRuleProductPrice firstOrNew(array $attributes = [], array $values = [])
     * @method CatalogRuleProductPrice firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CatalogRuleProductPrice forceCreate(array $attributes)
     * @method _IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] get(array|string $columns = ['*'])
     * @method CatalogRuleProductPrice getModel()
     * @method CatalogRuleProductPrice[] getModels(array|string $columns = ['*'])
     * @method _IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] hydrate(array $items)
     * @method CatalogRuleProductPrice make(array $attributes = [])
     * @method CatalogRuleProductPrice newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CatalogRuleProductPrice[]|_IH_CatalogRuleProductPrice_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CatalogRuleProductPrice[]|_IH_CatalogRuleProductPrice_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CatalogRuleProductPrice sole(array|string $columns = ['*'])
     * @method CatalogRuleProductPrice updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CatalogRuleProductPrice_QB extends _BaseBuilder {}
    
    /**
     * @method CatalogRuleProduct shift()
     * @method CatalogRuleProduct pop()
     * @method CatalogRuleProduct get($key, $default = null)
     * @method CatalogRuleProduct pull($key, $default = null)
     * @method CatalogRuleProduct first(callable $callback = null, $default = null)
     * @method CatalogRuleProduct firstWhere(string $key, $operator = null, $value = null)
     * @method CatalogRuleProduct find($key, $default = null)
     * @method CatalogRuleProduct[] all()
     * @method CatalogRuleProduct last(callable $callback = null, $default = null)
     * @method CatalogRuleProduct sole($key = null, $operator = null, $value = null)
     */
    class _IH_CatalogRuleProduct_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CatalogRuleProduct[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CatalogRuleProduct_QB whereId($value)
     * @method _IH_CatalogRuleProduct_QB whereStartsFrom($value)
     * @method _IH_CatalogRuleProduct_QB whereEndsTill($value)
     * @method _IH_CatalogRuleProduct_QB whereEndOtherRules($value)
     * @method _IH_CatalogRuleProduct_QB whereActionType($value)
     * @method _IH_CatalogRuleProduct_QB whereDiscountAmount($value)
     * @method _IH_CatalogRuleProduct_QB whereSortOrder($value)
     * @method _IH_CatalogRuleProduct_QB whereProductId($value)
     * @method _IH_CatalogRuleProduct_QB whereCustomerGroupId($value)
     * @method _IH_CatalogRuleProduct_QB whereCatalogRuleId($value)
     * @method _IH_CatalogRuleProduct_QB whereChannelId($value)
     * @method CatalogRuleProduct baseSole(array|string $columns = ['*'])
     * @method CatalogRuleProduct create(array $attributes = [])
     * @method _IH_CatalogRuleProduct_C|CatalogRuleProduct[] cursor()
     * @method CatalogRuleProduct|null|_IH_CatalogRuleProduct_C|CatalogRuleProduct[] find($id, array $columns = ['*'])
     * @method _IH_CatalogRuleProduct_C|CatalogRuleProduct[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CatalogRuleProduct|_IH_CatalogRuleProduct_C|CatalogRuleProduct[] findOrFail($id, array $columns = ['*'])
     * @method CatalogRuleProduct|_IH_CatalogRuleProduct_C|CatalogRuleProduct[] findOrNew($id, array $columns = ['*'])
     * @method CatalogRuleProduct first(array|string $columns = ['*'])
     * @method CatalogRuleProduct firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CatalogRuleProduct firstOrCreate(array $attributes = [], array $values = [])
     * @method CatalogRuleProduct firstOrFail(array $columns = ['*'])
     * @method CatalogRuleProduct firstOrNew(array $attributes = [], array $values = [])
     * @method CatalogRuleProduct firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CatalogRuleProduct forceCreate(array $attributes)
     * @method _IH_CatalogRuleProduct_C|CatalogRuleProduct[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CatalogRuleProduct_C|CatalogRuleProduct[] get(array|string $columns = ['*'])
     * @method CatalogRuleProduct getModel()
     * @method CatalogRuleProduct[] getModels(array|string $columns = ['*'])
     * @method _IH_CatalogRuleProduct_C|CatalogRuleProduct[] hydrate(array $items)
     * @method CatalogRuleProduct make(array $attributes = [])
     * @method CatalogRuleProduct newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CatalogRuleProduct[]|_IH_CatalogRuleProduct_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CatalogRuleProduct[]|_IH_CatalogRuleProduct_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CatalogRuleProduct sole(array|string $columns = ['*'])
     * @method CatalogRuleProduct updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CatalogRuleProduct_QB extends _BaseBuilder {}
    
    /**
     * @method CatalogRule shift()
     * @method CatalogRule pop()
     * @method CatalogRule get($key, $default = null)
     * @method CatalogRule pull($key, $default = null)
     * @method CatalogRule first(callable $callback = null, $default = null)
     * @method CatalogRule firstWhere(string $key, $operator = null, $value = null)
     * @method CatalogRule find($key, $default = null)
     * @method CatalogRule[] all()
     * @method CatalogRule last(callable $callback = null, $default = null)
     * @method CatalogRule sole($key = null, $operator = null, $value = null)
     */
    class _IH_CatalogRule_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CatalogRule[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CatalogRule_QB whereId($value)
     * @method _IH_CatalogRule_QB whereName($value)
     * @method _IH_CatalogRule_QB whereDescription($value)
     * @method _IH_CatalogRule_QB whereStartsFrom($value)
     * @method _IH_CatalogRule_QB whereEndsTill($value)
     * @method _IH_CatalogRule_QB whereStatus($value)
     * @method _IH_CatalogRule_QB whereConditionType($value)
     * @method _IH_CatalogRule_QB whereConditions($value)
     * @method _IH_CatalogRule_QB whereEndOtherRules($value)
     * @method _IH_CatalogRule_QB whereActionType($value)
     * @method _IH_CatalogRule_QB whereDiscountAmount($value)
     * @method _IH_CatalogRule_QB whereSortOrder($value)
     * @method _IH_CatalogRule_QB whereCreatedAt($value)
     * @method _IH_CatalogRule_QB whereUpdatedAt($value)
     * @method CatalogRule baseSole(array|string $columns = ['*'])
     * @method CatalogRule create(array $attributes = [])
     * @method _IH_CatalogRule_C|CatalogRule[] cursor()
     * @method CatalogRule|null|_IH_CatalogRule_C|CatalogRule[] find($id, array $columns = ['*'])
     * @method _IH_CatalogRule_C|CatalogRule[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CatalogRule|_IH_CatalogRule_C|CatalogRule[] findOrFail($id, array $columns = ['*'])
     * @method CatalogRule|_IH_CatalogRule_C|CatalogRule[] findOrNew($id, array $columns = ['*'])
     * @method CatalogRule first(array|string $columns = ['*'])
     * @method CatalogRule firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CatalogRule firstOrCreate(array $attributes = [], array $values = [])
     * @method CatalogRule firstOrFail(array $columns = ['*'])
     * @method CatalogRule firstOrNew(array $attributes = [], array $values = [])
     * @method CatalogRule firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CatalogRule forceCreate(array $attributes)
     * @method _IH_CatalogRule_C|CatalogRule[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CatalogRule_C|CatalogRule[] get(array|string $columns = ['*'])
     * @method CatalogRule getModel()
     * @method CatalogRule[] getModels(array|string $columns = ['*'])
     * @method _IH_CatalogRule_C|CatalogRule[] hydrate(array $items)
     * @method CatalogRule make(array $attributes = [])
     * @method CatalogRule newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CatalogRule[]|_IH_CatalogRule_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CatalogRule[]|_IH_CatalogRule_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CatalogRule sole(array|string $columns = ['*'])
     * @method CatalogRule updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CatalogRule_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Category\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Kalnoy\Nestedset\Collection;
    use Kalnoy\Nestedset\QueryBuilder;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Category\Models\Category;
    use Webkul\Category\Models\CategoryTranslation;
    
    /**
     * @method CategoryTranslation shift()
     * @method CategoryTranslation pop()
     * @method CategoryTranslation get($key, $default = null)
     * @method CategoryTranslation pull($key, $default = null)
     * @method CategoryTranslation first(callable $callback = null, $default = null)
     * @method CategoryTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method CategoryTranslation find($key, $default = null)
     * @method CategoryTranslation[] all()
     * @method CategoryTranslation last(callable $callback = null, $default = null)
     * @method CategoryTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_CategoryTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CategoryTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CategoryTranslation_QB whereId($value)
     * @method _IH_CategoryTranslation_QB whereName($value)
     * @method _IH_CategoryTranslation_QB whereSlug($value)
     * @method _IH_CategoryTranslation_QB whereDescription($value)
     * @method _IH_CategoryTranslation_QB whereMetaTitle($value)
     * @method _IH_CategoryTranslation_QB whereMetaDescription($value)
     * @method _IH_CategoryTranslation_QB whereMetaKeywords($value)
     * @method _IH_CategoryTranslation_QB whereCategoryId($value)
     * @method _IH_CategoryTranslation_QB whereLocale($value)
     * @method _IH_CategoryTranslation_QB whereLocaleId($value)
     * @method _IH_CategoryTranslation_QB whereUrlPath($value)
     * @method CategoryTranslation baseSole(array|string $columns = ['*'])
     * @method CategoryTranslation create(array $attributes = [])
     * @method _IH_CategoryTranslation_C|CategoryTranslation[] cursor()
     * @method CategoryTranslation|null|_IH_CategoryTranslation_C|CategoryTranslation[] find($id, array $columns = ['*'])
     * @method _IH_CategoryTranslation_C|CategoryTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CategoryTranslation|_IH_CategoryTranslation_C|CategoryTranslation[] findOrFail($id, array $columns = ['*'])
     * @method CategoryTranslation|_IH_CategoryTranslation_C|CategoryTranslation[] findOrNew($id, array $columns = ['*'])
     * @method CategoryTranslation first(array|string $columns = ['*'])
     * @method CategoryTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CategoryTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method CategoryTranslation firstOrFail(array $columns = ['*'])
     * @method CategoryTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method CategoryTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CategoryTranslation forceCreate(array $attributes)
     * @method _IH_CategoryTranslation_C|CategoryTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CategoryTranslation_C|CategoryTranslation[] get(array|string $columns = ['*'])
     * @method CategoryTranslation getModel()
     * @method CategoryTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_CategoryTranslation_C|CategoryTranslation[] hydrate(array $items)
     * @method CategoryTranslation make(array $attributes = [])
     * @method CategoryTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CategoryTranslation[]|_IH_CategoryTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CategoryTranslation[]|_IH_CategoryTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CategoryTranslation sole(array|string $columns = ['*'])
     * @method CategoryTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CategoryTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Category shift()
     * @method Category pop()
     * @method Category get($key, $default = null)
     * @method Category pull($key, $default = null)
     * @method Category first(callable $callback = null, $default = null)
     * @method Category firstWhere(string $key, $operator = null, $value = null)
     * @method Category find($key, $default = null)
     * @method Category[] all()
     * @method Category last(callable $callback = null, $default = null)
     * @method Category sole($key = null, $operator = null, $value = null)
     * @method $this toTree($root = false)
     * @method $this toFlatTree(bool $root = false)
     * @method $this linkNodes()
     * @mixin Collection
     */
    class _IH_Category_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Category[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Category_QB whereId($value)
     * @method _IH_Category_QB wherePosition($value)
     * @method _IH_Category_QB whereImage($value)
     * @method _IH_Category_QB whereStatus($value)
     * @method _IH_Category_QB whereLft($value)
     * @method _IH_Category_QB whereRgt($value)
     * @method _IH_Category_QB whereParentId($value)
     * @method _IH_Category_QB whereCreatedAt($value)
     * @method _IH_Category_QB whereUpdatedAt($value)
     * @method _IH_Category_QB whereDisplayMode($value)
     * @method _IH_Category_QB whereCategoryIconPath($value)
     * @method _IH_Category_QB whereAdditional($value)
     * @method Category baseSole(array|string $columns = ['*'])
     * @method Category create(array $attributes = [])
     * @method _IH_Category_C|Category[] cursor()
     * @method Category|null|_IH_Category_C|Category[] find($id, array $columns = ['*'])
     * @method _IH_Category_C|Category[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Category|_IH_Category_C|Category[] findOrFail($id, array $columns = ['*'])
     * @method Category|_IH_Category_C|Category[] findOrNew($id, array $columns = ['*'])
     * @method Category first(array|string $columns = ['*'])
     * @method Category firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Category firstOrCreate(array $attributes = [], array $values = [])
     * @method Category firstOrFail(array $columns = ['*'])
     * @method Category firstOrNew(array $attributes = [], array $values = [])
     * @method Category firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Category forceCreate(array $attributes)
     * @method _IH_Category_C|Category[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Category_C|Category[] get(array|string $columns = ['*'])
     * @method Category getModel()
     * @method Category[] getModels(array|string $columns = ['*'])
     * @method _IH_Category_C|Category[] hydrate(array $items)
     * @method Category make(array $attributes = [])
     * @method Category newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Category[]|_IH_Category_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Category[]|_IH_Category_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Category sole(array|string $columns = ['*'])
     * @method Category updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Category_QB listsTranslations(string $translationField)
     * @method _IH_Category_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Category_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Category_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Category_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Category_QB translated()
     * @method _IH_Category_QB translatedIn(null|string $locale = null)
     * @method _IH_Category_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Category_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Category_QB withTranslation()
     * @method _IH_Category_QB orWhereAncestorOf($id, bool $andSelf = false)
     * @method _IH_Category_QB hasParent()
     * @method _IH_Category_QB root(array $columns = ['*'])
     * @method _IH_Category_QB whereDescendantOf($id, string $boolean = 'and', bool $not = false, bool $andSelf = false)
     * @method _IH_Category_QB whereIsLeaf()
     * @method _IH_Category_QB hasChildren()
     * @method _IH_Category_QB whereIsAfter($id, string $boolean = 'and')
     * @method _IH_Category_QB defaultOrder(string $dir = 'asc')
     * @method _IH_Category_QB withDepth(string $as = 'depth')
     * @method _IH_Category_QB whereIsRoot()
     * @method _IH_Category_QB orWhereNodeBetween(array $values)
     * @method _IH_Category_QB orWhereDescendantOf($id)
     * @method _IH_Category_QB whereDescendantOrSelf($id, string $boolean = 'and', bool $not = false)
     * @method _IH_Category_QB whereNodeBetween(array $values, string $boolean = 'and', bool $not = false)
     * @method _IH_Category_QB whereNotDescendantOf($id)
     * @method _IH_Category_QB reversed()
     * @method _IH_Category_QB whereIsBefore($id, string $boolean = 'and')
     * @method _IH_Category_QB whereAncestorOf($id, bool $andSelf = false, string $boolean = 'and')
     * @method _IH_Category_QB withoutRoot()
     * @method _IH_Category_QB whereAncestorOrSelf($id)
     * @method _IH_Category_QB applyNestedSetScope(null|string $table = null)
     * @method _IH_Category_QB orWhereNotDescendantOf($id)
     * @mixin QueryBuilder
     */
    class _IH_Category_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Checkout\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Checkout\Models\Cart;
    use Webkul\Checkout\Models\CartAddress;
    use Webkul\Checkout\Models\CartItem;
    use Webkul\Checkout\Models\CartPayment;
    use Webkul\Checkout\Models\CartShippingRate;
    
    /**
     * @method CartAddress shift()
     * @method CartAddress pop()
     * @method CartAddress get($key, $default = null)
     * @method CartAddress pull($key, $default = null)
     * @method CartAddress first(callable $callback = null, $default = null)
     * @method CartAddress firstWhere(string $key, $operator = null, $value = null)
     * @method CartAddress find($key, $default = null)
     * @method CartAddress[] all()
     * @method CartAddress last(callable $callback = null, $default = null)
     * @method CartAddress sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartAddress_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartAddress[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method CartAddress baseSole(array|string $columns = ['*'])
     * @method CartAddress create(array $attributes = [])
     * @method _IH_CartAddress_C|CartAddress[] cursor()
     * @method CartAddress|null|_IH_CartAddress_C|CartAddress[] find($id, array $columns = ['*'])
     * @method _IH_CartAddress_C|CartAddress[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartAddress|_IH_CartAddress_C|CartAddress[] findOrFail($id, array $columns = ['*'])
     * @method CartAddress|_IH_CartAddress_C|CartAddress[] findOrNew($id, array $columns = ['*'])
     * @method CartAddress first(array|string $columns = ['*'])
     * @method CartAddress firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartAddress firstOrCreate(array $attributes = [], array $values = [])
     * @method CartAddress firstOrFail(array $columns = ['*'])
     * @method CartAddress firstOrNew(array $attributes = [], array $values = [])
     * @method CartAddress firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartAddress forceCreate(array $attributes)
     * @method _IH_CartAddress_C|CartAddress[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartAddress_C|CartAddress[] get(array|string $columns = ['*'])
     * @method CartAddress getModel()
     * @method CartAddress[] getModels(array|string $columns = ['*'])
     * @method _IH_CartAddress_C|CartAddress[] hydrate(array $items)
     * @method CartAddress make(array $attributes = [])
     * @method CartAddress newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartAddress[]|_IH_CartAddress_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartAddress[]|_IH_CartAddress_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartAddress sole(array|string $columns = ['*'])
     * @method CartAddress updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartAddress_QB extends _BaseBuilder {}
    
    /**
     * @method CartItem shift()
     * @method CartItem pop()
     * @method CartItem get($key, $default = null)
     * @method CartItem pull($key, $default = null)
     * @method CartItem first(callable $callback = null, $default = null)
     * @method CartItem firstWhere(string $key, $operator = null, $value = null)
     * @method CartItem find($key, $default = null)
     * @method CartItem[] all()
     * @method CartItem last(callable $callback = null, $default = null)
     * @method CartItem sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartItem_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartItem[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartItem_QB whereId($value)
     * @method _IH_CartItem_QB whereQuantity($value)
     * @method _IH_CartItem_QB whereSku($value)
     * @method _IH_CartItem_QB whereType($value)
     * @method _IH_CartItem_QB whereName($value)
     * @method _IH_CartItem_QB whereCouponCode($value)
     * @method _IH_CartItem_QB whereWeight($value)
     * @method _IH_CartItem_QB whereTotalWeight($value)
     * @method _IH_CartItem_QB whereBaseTotalWeight($value)
     * @method _IH_CartItem_QB wherePrice($value)
     * @method _IH_CartItem_QB whereBasePrice($value)
     * @method _IH_CartItem_QB whereTotal($value)
     * @method _IH_CartItem_QB whereBaseTotal($value)
     * @method _IH_CartItem_QB whereTaxPercent($value)
     * @method _IH_CartItem_QB whereTaxAmount($value)
     * @method _IH_CartItem_QB whereBaseTaxAmount($value)
     * @method _IH_CartItem_QB whereDiscountPercent($value)
     * @method _IH_CartItem_QB whereDiscountAmount($value)
     * @method _IH_CartItem_QB whereBaseDiscountAmount($value)
     * @method _IH_CartItem_QB whereAdditional($value)
     * @method _IH_CartItem_QB whereParentId($value)
     * @method _IH_CartItem_QB whereProductId($value)
     * @method _IH_CartItem_QB whereCartId($value)
     * @method _IH_CartItem_QB whereTaxCategoryId($value)
     * @method _IH_CartItem_QB whereCreatedAt($value)
     * @method _IH_CartItem_QB whereUpdatedAt($value)
     * @method _IH_CartItem_QB whereCustomPrice($value)
     * @method _IH_CartItem_QB whereAppliedCartRuleIds($value)
     * @method CartItem baseSole(array|string $columns = ['*'])
     * @method CartItem create(array $attributes = [])
     * @method _IH_CartItem_C|CartItem[] cursor()
     * @method CartItem|null|_IH_CartItem_C|CartItem[] find($id, array $columns = ['*'])
     * @method _IH_CartItem_C|CartItem[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartItem|_IH_CartItem_C|CartItem[] findOrFail($id, array $columns = ['*'])
     * @method CartItem|_IH_CartItem_C|CartItem[] findOrNew($id, array $columns = ['*'])
     * @method CartItem first(array|string $columns = ['*'])
     * @method CartItem firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartItem firstOrCreate(array $attributes = [], array $values = [])
     * @method CartItem firstOrFail(array $columns = ['*'])
     * @method CartItem firstOrNew(array $attributes = [], array $values = [])
     * @method CartItem firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartItem forceCreate(array $attributes)
     * @method _IH_CartItem_C|CartItem[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartItem_C|CartItem[] get(array|string $columns = ['*'])
     * @method CartItem getModel()
     * @method CartItem[] getModels(array|string $columns = ['*'])
     * @method _IH_CartItem_C|CartItem[] hydrate(array $items)
     * @method CartItem make(array $attributes = [])
     * @method CartItem newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartItem[]|_IH_CartItem_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartItem[]|_IH_CartItem_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartItem sole(array|string $columns = ['*'])
     * @method CartItem updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartItem_QB extends _BaseBuilder {}
    
    /**
     * @method CartPayment shift()
     * @method CartPayment pop()
     * @method CartPayment get($key, $default = null)
     * @method CartPayment pull($key, $default = null)
     * @method CartPayment first(callable $callback = null, $default = null)
     * @method CartPayment firstWhere(string $key, $operator = null, $value = null)
     * @method CartPayment find($key, $default = null)
     * @method CartPayment[] all()
     * @method CartPayment last(callable $callback = null, $default = null)
     * @method CartPayment sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartPayment_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartPayment[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartPayment_QB whereId($value)
     * @method _IH_CartPayment_QB whereMethod($value)
     * @method _IH_CartPayment_QB whereMethodTitle($value)
     * @method _IH_CartPayment_QB whereCartId($value)
     * @method _IH_CartPayment_QB whereCreatedAt($value)
     * @method _IH_CartPayment_QB whereUpdatedAt($value)
     * @method CartPayment baseSole(array|string $columns = ['*'])
     * @method CartPayment create(array $attributes = [])
     * @method _IH_CartPayment_C|CartPayment[] cursor()
     * @method CartPayment|null|_IH_CartPayment_C|CartPayment[] find($id, array $columns = ['*'])
     * @method _IH_CartPayment_C|CartPayment[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartPayment|_IH_CartPayment_C|CartPayment[] findOrFail($id, array $columns = ['*'])
     * @method CartPayment|_IH_CartPayment_C|CartPayment[] findOrNew($id, array $columns = ['*'])
     * @method CartPayment first(array|string $columns = ['*'])
     * @method CartPayment firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartPayment firstOrCreate(array $attributes = [], array $values = [])
     * @method CartPayment firstOrFail(array $columns = ['*'])
     * @method CartPayment firstOrNew(array $attributes = [], array $values = [])
     * @method CartPayment firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartPayment forceCreate(array $attributes)
     * @method _IH_CartPayment_C|CartPayment[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartPayment_C|CartPayment[] get(array|string $columns = ['*'])
     * @method CartPayment getModel()
     * @method CartPayment[] getModels(array|string $columns = ['*'])
     * @method _IH_CartPayment_C|CartPayment[] hydrate(array $items)
     * @method CartPayment make(array $attributes = [])
     * @method CartPayment newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartPayment[]|_IH_CartPayment_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartPayment[]|_IH_CartPayment_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartPayment sole(array|string $columns = ['*'])
     * @method CartPayment updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartPayment_QB extends _BaseBuilder {}
    
    /**
     * @method CartShippingRate shift()
     * @method CartShippingRate pop()
     * @method CartShippingRate get($key, $default = null)
     * @method CartShippingRate pull($key, $default = null)
     * @method CartShippingRate first(callable $callback = null, $default = null)
     * @method CartShippingRate firstWhere(string $key, $operator = null, $value = null)
     * @method CartShippingRate find($key, $default = null)
     * @method CartShippingRate[] all()
     * @method CartShippingRate last(callable $callback = null, $default = null)
     * @method CartShippingRate sole($key = null, $operator = null, $value = null)
     */
    class _IH_CartShippingRate_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CartShippingRate[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CartShippingRate_QB whereId($value)
     * @method _IH_CartShippingRate_QB whereCarrier($value)
     * @method _IH_CartShippingRate_QB whereCarrierTitle($value)
     * @method _IH_CartShippingRate_QB whereMethod($value)
     * @method _IH_CartShippingRate_QB whereMethodTitle($value)
     * @method _IH_CartShippingRate_QB whereMethodDescription($value)
     * @method _IH_CartShippingRate_QB wherePrice($value)
     * @method _IH_CartShippingRate_QB whereBasePrice($value)
     * @method _IH_CartShippingRate_QB whereCartAddressId($value)
     * @method _IH_CartShippingRate_QB whereCreatedAt($value)
     * @method _IH_CartShippingRate_QB whereUpdatedAt($value)
     * @method _IH_CartShippingRate_QB whereDiscountAmount($value)
     * @method _IH_CartShippingRate_QB whereBaseDiscountAmount($value)
     * @method _IH_CartShippingRate_QB whereIsCalculateTax($value)
     * @method CartShippingRate baseSole(array|string $columns = ['*'])
     * @method CartShippingRate create(array $attributes = [])
     * @method _IH_CartShippingRate_C|CartShippingRate[] cursor()
     * @method CartShippingRate|null|_IH_CartShippingRate_C|CartShippingRate[] find($id, array $columns = ['*'])
     * @method _IH_CartShippingRate_C|CartShippingRate[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CartShippingRate|_IH_CartShippingRate_C|CartShippingRate[] findOrFail($id, array $columns = ['*'])
     * @method CartShippingRate|_IH_CartShippingRate_C|CartShippingRate[] findOrNew($id, array $columns = ['*'])
     * @method CartShippingRate first(array|string $columns = ['*'])
     * @method CartShippingRate firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CartShippingRate firstOrCreate(array $attributes = [], array $values = [])
     * @method CartShippingRate firstOrFail(array $columns = ['*'])
     * @method CartShippingRate firstOrNew(array $attributes = [], array $values = [])
     * @method CartShippingRate firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CartShippingRate forceCreate(array $attributes)
     * @method _IH_CartShippingRate_C|CartShippingRate[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CartShippingRate_C|CartShippingRate[] get(array|string $columns = ['*'])
     * @method CartShippingRate getModel()
     * @method CartShippingRate[] getModels(array|string $columns = ['*'])
     * @method _IH_CartShippingRate_C|CartShippingRate[] hydrate(array $items)
     * @method CartShippingRate make(array $attributes = [])
     * @method CartShippingRate newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CartShippingRate[]|_IH_CartShippingRate_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CartShippingRate[]|_IH_CartShippingRate_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CartShippingRate sole(array|string $columns = ['*'])
     * @method CartShippingRate updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CartShippingRate_QB extends _BaseBuilder {}
    
    /**
     * @method Cart shift()
     * @method Cart pop()
     * @method Cart get($key, $default = null)
     * @method Cart pull($key, $default = null)
     * @method Cart first(callable $callback = null, $default = null)
     * @method Cart firstWhere(string $key, $operator = null, $value = null)
     * @method Cart find($key, $default = null)
     * @method Cart[] all()
     * @method Cart last(callable $callback = null, $default = null)
     * @method Cart sole($key = null, $operator = null, $value = null)
     */
    class _IH_Cart_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Cart[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Cart_QB whereId($value)
     * @method _IH_Cart_QB whereCustomerEmail($value)
     * @method _IH_Cart_QB whereCustomerFirstName($value)
     * @method _IH_Cart_QB whereCustomerLastName($value)
     * @method _IH_Cart_QB whereShippingMethod($value)
     * @method _IH_Cart_QB whereCouponCode($value)
     * @method _IH_Cart_QB whereIsGift($value)
     * @method _IH_Cart_QB whereItemsCount($value)
     * @method _IH_Cart_QB whereItemsQty($value)
     * @method _IH_Cart_QB whereExchangeRate($value)
     * @method _IH_Cart_QB whereGlobalCurrencyCode($value)
     * @method _IH_Cart_QB whereBaseCurrencyCode($value)
     * @method _IH_Cart_QB whereChannelCurrencyCode($value)
     * @method _IH_Cart_QB whereCartCurrencyCode($value)
     * @method _IH_Cart_QB whereGrandTotal($value)
     * @method _IH_Cart_QB whereBaseGrandTotal($value)
     * @method _IH_Cart_QB whereSubTotal($value)
     * @method _IH_Cart_QB whereBaseSubTotal($value)
     * @method _IH_Cart_QB whereTaxTotal($value)
     * @method _IH_Cart_QB whereBaseTaxTotal($value)
     * @method _IH_Cart_QB whereDiscountAmount($value)
     * @method _IH_Cart_QB whereBaseDiscountAmount($value)
     * @method _IH_Cart_QB whereCheckoutMethod($value)
     * @method _IH_Cart_QB whereIsGuest($value)
     * @method _IH_Cart_QB whereIsActive($value)
     * @method _IH_Cart_QB whereConversionTime($value)
     * @method _IH_Cart_QB whereCustomerId($value)
     * @method _IH_Cart_QB whereChannelId($value)
     * @method _IH_Cart_QB whereCreatedAt($value)
     * @method _IH_Cart_QB whereUpdatedAt($value)
     * @method _IH_Cart_QB whereAppliedCartRuleIds($value)
     * @method Cart baseSole(array|string $columns = ['*'])
     * @method Cart create(array $attributes = [])
     * @method _IH_Cart_C|Cart[] cursor()
     * @method Cart|null|_IH_Cart_C|Cart[] find($id, array $columns = ['*'])
     * @method _IH_Cart_C|Cart[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Cart|_IH_Cart_C|Cart[] findOrFail($id, array $columns = ['*'])
     * @method Cart|_IH_Cart_C|Cart[] findOrNew($id, array $columns = ['*'])
     * @method Cart first(array|string $columns = ['*'])
     * @method Cart firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Cart firstOrCreate(array $attributes = [], array $values = [])
     * @method Cart firstOrFail(array $columns = ['*'])
     * @method Cart firstOrNew(array $attributes = [], array $values = [])
     * @method Cart firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Cart forceCreate(array $attributes)
     * @method _IH_Cart_C|Cart[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Cart_C|Cart[] get(array|string $columns = ['*'])
     * @method Cart getModel()
     * @method Cart[] getModels(array|string $columns = ['*'])
     * @method _IH_Cart_C|Cart[] hydrate(array $items)
     * @method Cart make(array $attributes = [])
     * @method Cart newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Cart[]|_IH_Cart_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Cart[]|_IH_Cart_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Cart sole(array|string $columns = ['*'])
     * @method Cart updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Cart_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Core\Eloquent {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Core\Eloquent\TranslatableModel;
    
    /**
     * @method TranslatableModel shift()
     * @method TranslatableModel pop()
     * @method TranslatableModel get($key, $default = null)
     * @method TranslatableModel pull($key, $default = null)
     * @method TranslatableModel first(callable $callback = null, $default = null)
     * @method TranslatableModel firstWhere(string $key, $operator = null, $value = null)
     * @method TranslatableModel find($key, $default = null)
     * @method TranslatableModel[] all()
     * @method TranslatableModel last(callable $callback = null, $default = null)
     * @method TranslatableModel sole($key = null, $operator = null, $value = null)
     */
    class _IH_TranslatableModel_C extends _BaseCollection {
        /**
         * @param int $size
         * @return TranslatableModel[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method TranslatableModel baseSole(array|string $columns = ['*'])
     * @method TranslatableModel create(array $attributes = [])
     * @method _IH_TranslatableModel_C|TranslatableModel[] cursor()
     * @method TranslatableModel|null|_IH_TranslatableModel_C|TranslatableModel[] find($id, array $columns = ['*'])
     * @method _IH_TranslatableModel_C|TranslatableModel[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method TranslatableModel|_IH_TranslatableModel_C|TranslatableModel[] findOrFail($id, array $columns = ['*'])
     * @method TranslatableModel|_IH_TranslatableModel_C|TranslatableModel[] findOrNew($id, array $columns = ['*'])
     * @method TranslatableModel first(array|string $columns = ['*'])
     * @method TranslatableModel firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method TranslatableModel firstOrCreate(array $attributes = [], array $values = [])
     * @method TranslatableModel firstOrFail(array $columns = ['*'])
     * @method TranslatableModel firstOrNew(array $attributes = [], array $values = [])
     * @method TranslatableModel firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method TranslatableModel forceCreate(array $attributes)
     * @method _IH_TranslatableModel_C|TranslatableModel[] fromQuery(string $query, array $bindings = [])
     * @method _IH_TranslatableModel_C|TranslatableModel[] get(array|string $columns = ['*'])
     * @method TranslatableModel getModel()
     * @method TranslatableModel[] getModels(array|string $columns = ['*'])
     * @method _IH_TranslatableModel_C|TranslatableModel[] hydrate(array $items)
     * @method TranslatableModel make(array $attributes = [])
     * @method TranslatableModel newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|TranslatableModel[]|_IH_TranslatableModel_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|TranslatableModel[]|_IH_TranslatableModel_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method TranslatableModel sole(array|string $columns = ['*'])
     * @method TranslatableModel updateOrCreate(array $attributes, array $values = [])
     * @method _IH_TranslatableModel_QB listsTranslations(string $translationField)
     * @method _IH_TranslatableModel_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_TranslatableModel_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_TranslatableModel_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_TranslatableModel_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_TranslatableModel_QB translated()
     * @method _IH_TranslatableModel_QB translatedIn(null|string $locale = null)
     * @method _IH_TranslatableModel_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_TranslatableModel_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_TranslatableModel_QB withTranslation()
     */
    class _IH_TranslatableModel_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Core\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Core\Models\Channel;
    use Webkul\Core\Models\ChannelTranslation;
    use Webkul\Core\Models\CoreConfig;
    use Webkul\Core\Models\Country;
    use Webkul\Core\Models\CountryState;
    use Webkul\Core\Models\CountryStateTranslation;
    use Webkul\Core\Models\CountryTranslation;
    use Webkul\Core\Models\Currency;
    use Webkul\Core\Models\CurrencyExchangeRate;
    use Webkul\Core\Models\Locale;
    use Webkul\Core\Models\Slider;
    use Webkul\Core\Models\SubscribersList;
    
    /**
     * @method ChannelTranslation shift()
     * @method ChannelTranslation pop()
     * @method ChannelTranslation get($key, $default = null)
     * @method ChannelTranslation pull($key, $default = null)
     * @method ChannelTranslation first(callable $callback = null, $default = null)
     * @method ChannelTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method ChannelTranslation find($key, $default = null)
     * @method ChannelTranslation[] all()
     * @method ChannelTranslation last(callable $callback = null, $default = null)
     * @method ChannelTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_ChannelTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ChannelTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ChannelTranslation_QB whereId($value)
     * @method _IH_ChannelTranslation_QB whereChannelId($value)
     * @method _IH_ChannelTranslation_QB whereLocale($value)
     * @method _IH_ChannelTranslation_QB whereName($value)
     * @method _IH_ChannelTranslation_QB whereDescription($value)
     * @method _IH_ChannelTranslation_QB whereHomePageContent($value)
     * @method _IH_ChannelTranslation_QB whereFooterContent($value)
     * @method _IH_ChannelTranslation_QB whereMaintenanceModeText($value)
     * @method _IH_ChannelTranslation_QB whereHomeSeo($value)
     * @method _IH_ChannelTranslation_QB whereCreatedAt($value)
     * @method _IH_ChannelTranslation_QB whereUpdatedAt($value)
     * @method ChannelTranslation baseSole(array|string $columns = ['*'])
     * @method ChannelTranslation create(array $attributes = [])
     * @method _IH_ChannelTranslation_C|ChannelTranslation[] cursor()
     * @method ChannelTranslation|null|_IH_ChannelTranslation_C|ChannelTranslation[] find($id, array $columns = ['*'])
     * @method _IH_ChannelTranslation_C|ChannelTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ChannelTranslation|_IH_ChannelTranslation_C|ChannelTranslation[] findOrFail($id, array $columns = ['*'])
     * @method ChannelTranslation|_IH_ChannelTranslation_C|ChannelTranslation[] findOrNew($id, array $columns = ['*'])
     * @method ChannelTranslation first(array|string $columns = ['*'])
     * @method ChannelTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ChannelTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method ChannelTranslation firstOrFail(array $columns = ['*'])
     * @method ChannelTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method ChannelTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ChannelTranslation forceCreate(array $attributes)
     * @method _IH_ChannelTranslation_C|ChannelTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ChannelTranslation_C|ChannelTranslation[] get(array|string $columns = ['*'])
     * @method ChannelTranslation getModel()
     * @method ChannelTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_ChannelTranslation_C|ChannelTranslation[] hydrate(array $items)
     * @method ChannelTranslation make(array $attributes = [])
     * @method ChannelTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ChannelTranslation[]|_IH_ChannelTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ChannelTranslation[]|_IH_ChannelTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ChannelTranslation sole(array|string $columns = ['*'])
     * @method ChannelTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ChannelTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Channel shift()
     * @method Channel pop()
     * @method Channel get($key, $default = null)
     * @method Channel pull($key, $default = null)
     * @method Channel first(callable $callback = null, $default = null)
     * @method Channel firstWhere(string $key, $operator = null, $value = null)
     * @method Channel find($key, $default = null)
     * @method Channel[] all()
     * @method Channel last(callable $callback = null, $default = null)
     * @method Channel sole($key = null, $operator = null, $value = null)
     */
    class _IH_Channel_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Channel[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Channel_QB whereId($value)
     * @method _IH_Channel_QB whereCode($value)
     * @method _IH_Channel_QB whereTimezone($value)
     * @method _IH_Channel_QB whereTheme($value)
     * @method _IH_Channel_QB whereHostname($value)
     * @method _IH_Channel_QB whereLogo($value)
     * @method _IH_Channel_QB whereFavicon($value)
     * @method _IH_Channel_QB whereIsMaintenanceOn($value)
     * @method _IH_Channel_QB whereAllowedIps($value)
     * @method _IH_Channel_QB whereDefaultLocaleId($value)
     * @method _IH_Channel_QB whereBaseCurrencyId($value)
     * @method _IH_Channel_QB whereCreatedAt($value)
     * @method _IH_Channel_QB whereUpdatedAt($value)
     * @method _IH_Channel_QB whereRootCategoryId($value)
     * @method Channel baseSole(array|string $columns = ['*'])
     * @method Channel create(array $attributes = [])
     * @method _IH_Channel_C|Channel[] cursor()
     * @method Channel|null|_IH_Channel_C|Channel[] find($id, array $columns = ['*'])
     * @method _IH_Channel_C|Channel[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Channel|_IH_Channel_C|Channel[] findOrFail($id, array $columns = ['*'])
     * @method Channel|_IH_Channel_C|Channel[] findOrNew($id, array $columns = ['*'])
     * @method Channel first(array|string $columns = ['*'])
     * @method Channel firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Channel firstOrCreate(array $attributes = [], array $values = [])
     * @method Channel firstOrFail(array $columns = ['*'])
     * @method Channel firstOrNew(array $attributes = [], array $values = [])
     * @method Channel firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Channel forceCreate(array $attributes)
     * @method _IH_Channel_C|Channel[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Channel_C|Channel[] get(array|string $columns = ['*'])
     * @method Channel getModel()
     * @method Channel[] getModels(array|string $columns = ['*'])
     * @method _IH_Channel_C|Channel[] hydrate(array $items)
     * @method Channel make(array $attributes = [])
     * @method Channel newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Channel[]|_IH_Channel_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Channel[]|_IH_Channel_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Channel sole(array|string $columns = ['*'])
     * @method Channel updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Channel_QB listsTranslations(string $translationField)
     * @method _IH_Channel_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Channel_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Channel_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Channel_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Channel_QB translated()
     * @method _IH_Channel_QB translatedIn(null|string $locale = null)
     * @method _IH_Channel_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Channel_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Channel_QB withTranslation()
     */
    class _IH_Channel_QB extends _BaseBuilder {}
    
    /**
     * @method CoreConfig shift()
     * @method CoreConfig pop()
     * @method CoreConfig get($key, $default = null)
     * @method CoreConfig pull($key, $default = null)
     * @method CoreConfig first(callable $callback = null, $default = null)
     * @method CoreConfig firstWhere(string $key, $operator = null, $value = null)
     * @method CoreConfig find($key, $default = null)
     * @method CoreConfig[] all()
     * @method CoreConfig last(callable $callback = null, $default = null)
     * @method CoreConfig sole($key = null, $operator = null, $value = null)
     */
    class _IH_CoreConfig_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CoreConfig[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CoreConfig_QB whereId($value)
     * @method _IH_CoreConfig_QB whereCode($value)
     * @method _IH_CoreConfig_QB whereValue($value)
     * @method _IH_CoreConfig_QB whereChannelCode($value)
     * @method _IH_CoreConfig_QB whereLocaleCode($value)
     * @method _IH_CoreConfig_QB whereCreatedAt($value)
     * @method _IH_CoreConfig_QB whereUpdatedAt($value)
     * @method CoreConfig baseSole(array|string $columns = ['*'])
     * @method CoreConfig create(array $attributes = [])
     * @method _IH_CoreConfig_C|CoreConfig[] cursor()
     * @method CoreConfig|null|_IH_CoreConfig_C|CoreConfig[] find($id, array $columns = ['*'])
     * @method _IH_CoreConfig_C|CoreConfig[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CoreConfig|_IH_CoreConfig_C|CoreConfig[] findOrFail($id, array $columns = ['*'])
     * @method CoreConfig|_IH_CoreConfig_C|CoreConfig[] findOrNew($id, array $columns = ['*'])
     * @method CoreConfig first(array|string $columns = ['*'])
     * @method CoreConfig firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CoreConfig firstOrCreate(array $attributes = [], array $values = [])
     * @method CoreConfig firstOrFail(array $columns = ['*'])
     * @method CoreConfig firstOrNew(array $attributes = [], array $values = [])
     * @method CoreConfig firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CoreConfig forceCreate(array $attributes)
     * @method _IH_CoreConfig_C|CoreConfig[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CoreConfig_C|CoreConfig[] get(array|string $columns = ['*'])
     * @method CoreConfig getModel()
     * @method CoreConfig[] getModels(array|string $columns = ['*'])
     * @method _IH_CoreConfig_C|CoreConfig[] hydrate(array $items)
     * @method CoreConfig make(array $attributes = [])
     * @method CoreConfig newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CoreConfig[]|_IH_CoreConfig_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CoreConfig[]|_IH_CoreConfig_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CoreConfig sole(array|string $columns = ['*'])
     * @method CoreConfig updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CoreConfig_QB extends _BaseBuilder {}
    
    /**
     * @method CountryStateTranslation shift()
     * @method CountryStateTranslation pop()
     * @method CountryStateTranslation get($key, $default = null)
     * @method CountryStateTranslation pull($key, $default = null)
     * @method CountryStateTranslation first(callable $callback = null, $default = null)
     * @method CountryStateTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method CountryStateTranslation find($key, $default = null)
     * @method CountryStateTranslation[] all()
     * @method CountryStateTranslation last(callable $callback = null, $default = null)
     * @method CountryStateTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_CountryStateTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CountryStateTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CountryStateTranslation_QB whereId($value)
     * @method _IH_CountryStateTranslation_QB whereLocale($value)
     * @method _IH_CountryStateTranslation_QB whereDefaultName($value)
     * @method _IH_CountryStateTranslation_QB whereCountryStateId($value)
     * @method CountryStateTranslation baseSole(array|string $columns = ['*'])
     * @method CountryStateTranslation create(array $attributes = [])
     * @method _IH_CountryStateTranslation_C|CountryStateTranslation[] cursor()
     * @method CountryStateTranslation|null|_IH_CountryStateTranslation_C|CountryStateTranslation[] find($id, array $columns = ['*'])
     * @method _IH_CountryStateTranslation_C|CountryStateTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CountryStateTranslation|_IH_CountryStateTranslation_C|CountryStateTranslation[] findOrFail($id, array $columns = ['*'])
     * @method CountryStateTranslation|_IH_CountryStateTranslation_C|CountryStateTranslation[] findOrNew($id, array $columns = ['*'])
     * @method CountryStateTranslation first(array|string $columns = ['*'])
     * @method CountryStateTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CountryStateTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method CountryStateTranslation firstOrFail(array $columns = ['*'])
     * @method CountryStateTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method CountryStateTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CountryStateTranslation forceCreate(array $attributes)
     * @method _IH_CountryStateTranslation_C|CountryStateTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CountryStateTranslation_C|CountryStateTranslation[] get(array|string $columns = ['*'])
     * @method CountryStateTranslation getModel()
     * @method CountryStateTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_CountryStateTranslation_C|CountryStateTranslation[] hydrate(array $items)
     * @method CountryStateTranslation make(array $attributes = [])
     * @method CountryStateTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CountryStateTranslation[]|_IH_CountryStateTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CountryStateTranslation[]|_IH_CountryStateTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CountryStateTranslation sole(array|string $columns = ['*'])
     * @method CountryStateTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CountryStateTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method CountryState shift()
     * @method CountryState pop()
     * @method CountryState get($key, $default = null)
     * @method CountryState pull($key, $default = null)
     * @method CountryState first(callable $callback = null, $default = null)
     * @method CountryState firstWhere(string $key, $operator = null, $value = null)
     * @method CountryState find($key, $default = null)
     * @method CountryState[] all()
     * @method CountryState last(callable $callback = null, $default = null)
     * @method CountryState sole($key = null, $operator = null, $value = null)
     */
    class _IH_CountryState_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CountryState[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CountryState_QB whereId($value)
     * @method _IH_CountryState_QB whereCountryCode($value)
     * @method _IH_CountryState_QB whereCode($value)
     * @method _IH_CountryState_QB whereDefaultName($value)
     * @method _IH_CountryState_QB whereCountryId($value)
     * @method CountryState baseSole(array|string $columns = ['*'])
     * @method CountryState create(array $attributes = [])
     * @method _IH_CountryState_C|CountryState[] cursor()
     * @method CountryState|null|_IH_CountryState_C|CountryState[] find($id, array $columns = ['*'])
     * @method _IH_CountryState_C|CountryState[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CountryState|_IH_CountryState_C|CountryState[] findOrFail($id, array $columns = ['*'])
     * @method CountryState|_IH_CountryState_C|CountryState[] findOrNew($id, array $columns = ['*'])
     * @method CountryState first(array|string $columns = ['*'])
     * @method CountryState firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CountryState firstOrCreate(array $attributes = [], array $values = [])
     * @method CountryState firstOrFail(array $columns = ['*'])
     * @method CountryState firstOrNew(array $attributes = [], array $values = [])
     * @method CountryState firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CountryState forceCreate(array $attributes)
     * @method _IH_CountryState_C|CountryState[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CountryState_C|CountryState[] get(array|string $columns = ['*'])
     * @method CountryState getModel()
     * @method CountryState[] getModels(array|string $columns = ['*'])
     * @method _IH_CountryState_C|CountryState[] hydrate(array $items)
     * @method CountryState make(array $attributes = [])
     * @method CountryState newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CountryState[]|_IH_CountryState_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CountryState[]|_IH_CountryState_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CountryState sole(array|string $columns = ['*'])
     * @method CountryState updateOrCreate(array $attributes, array $values = [])
     * @method _IH_CountryState_QB listsTranslations(string $translationField)
     * @method _IH_CountryState_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_CountryState_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_CountryState_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_CountryState_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_CountryState_QB translated()
     * @method _IH_CountryState_QB translatedIn(null|string $locale = null)
     * @method _IH_CountryState_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_CountryState_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_CountryState_QB withTranslation()
     */
    class _IH_CountryState_QB extends _BaseBuilder {}
    
    /**
     * @method CountryTranslation shift()
     * @method CountryTranslation pop()
     * @method CountryTranslation get($key, $default = null)
     * @method CountryTranslation pull($key, $default = null)
     * @method CountryTranslation first(callable $callback = null, $default = null)
     * @method CountryTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method CountryTranslation find($key, $default = null)
     * @method CountryTranslation[] all()
     * @method CountryTranslation last(callable $callback = null, $default = null)
     * @method CountryTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_CountryTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CountryTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CountryTranslation_QB whereId($value)
     * @method _IH_CountryTranslation_QB whereLocale($value)
     * @method _IH_CountryTranslation_QB whereName($value)
     * @method _IH_CountryTranslation_QB whereCountryId($value)
     * @method CountryTranslation baseSole(array|string $columns = ['*'])
     * @method CountryTranslation create(array $attributes = [])
     * @method _IH_CountryTranslation_C|CountryTranslation[] cursor()
     * @method CountryTranslation|null|_IH_CountryTranslation_C|CountryTranslation[] find($id, array $columns = ['*'])
     * @method _IH_CountryTranslation_C|CountryTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CountryTranslation|_IH_CountryTranslation_C|CountryTranslation[] findOrFail($id, array $columns = ['*'])
     * @method CountryTranslation|_IH_CountryTranslation_C|CountryTranslation[] findOrNew($id, array $columns = ['*'])
     * @method CountryTranslation first(array|string $columns = ['*'])
     * @method CountryTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CountryTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method CountryTranslation firstOrFail(array $columns = ['*'])
     * @method CountryTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method CountryTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CountryTranslation forceCreate(array $attributes)
     * @method _IH_CountryTranslation_C|CountryTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CountryTranslation_C|CountryTranslation[] get(array|string $columns = ['*'])
     * @method CountryTranslation getModel()
     * @method CountryTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_CountryTranslation_C|CountryTranslation[] hydrate(array $items)
     * @method CountryTranslation make(array $attributes = [])
     * @method CountryTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CountryTranslation[]|_IH_CountryTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CountryTranslation[]|_IH_CountryTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CountryTranslation sole(array|string $columns = ['*'])
     * @method CountryTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CountryTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Country shift()
     * @method Country pop()
     * @method Country get($key, $default = null)
     * @method Country pull($key, $default = null)
     * @method Country first(callable $callback = null, $default = null)
     * @method Country firstWhere(string $key, $operator = null, $value = null)
     * @method Country find($key, $default = null)
     * @method Country[] all()
     * @method Country last(callable $callback = null, $default = null)
     * @method Country sole($key = null, $operator = null, $value = null)
     */
    class _IH_Country_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Country[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Country_QB whereId($value)
     * @method _IH_Country_QB whereCode($value)
     * @method _IH_Country_QB whereName($value)
     * @method Country baseSole(array|string $columns = ['*'])
     * @method Country create(array $attributes = [])
     * @method _IH_Country_C|Country[] cursor()
     * @method Country|null|_IH_Country_C|Country[] find($id, array $columns = ['*'])
     * @method _IH_Country_C|Country[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Country|_IH_Country_C|Country[] findOrFail($id, array $columns = ['*'])
     * @method Country|_IH_Country_C|Country[] findOrNew($id, array $columns = ['*'])
     * @method Country first(array|string $columns = ['*'])
     * @method Country firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Country firstOrCreate(array $attributes = [], array $values = [])
     * @method Country firstOrFail(array $columns = ['*'])
     * @method Country firstOrNew(array $attributes = [], array $values = [])
     * @method Country firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Country forceCreate(array $attributes)
     * @method _IH_Country_C|Country[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Country_C|Country[] get(array|string $columns = ['*'])
     * @method Country getModel()
     * @method Country[] getModels(array|string $columns = ['*'])
     * @method _IH_Country_C|Country[] hydrate(array $items)
     * @method Country make(array $attributes = [])
     * @method Country newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Country[]|_IH_Country_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Country[]|_IH_Country_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Country sole(array|string $columns = ['*'])
     * @method Country updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Country_QB listsTranslations(string $translationField)
     * @method _IH_Country_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Country_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Country_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Country_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Country_QB translated()
     * @method _IH_Country_QB translatedIn(null|string $locale = null)
     * @method _IH_Country_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Country_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Country_QB withTranslation()
     */
    class _IH_Country_QB extends _BaseBuilder {}
    
    /**
     * @method CurrencyExchangeRate shift()
     * @method CurrencyExchangeRate pop()
     * @method CurrencyExchangeRate get($key, $default = null)
     * @method CurrencyExchangeRate pull($key, $default = null)
     * @method CurrencyExchangeRate first(callable $callback = null, $default = null)
     * @method CurrencyExchangeRate firstWhere(string $key, $operator = null, $value = null)
     * @method CurrencyExchangeRate find($key, $default = null)
     * @method CurrencyExchangeRate[] all()
     * @method CurrencyExchangeRate last(callable $callback = null, $default = null)
     * @method CurrencyExchangeRate sole($key = null, $operator = null, $value = null)
     */
    class _IH_CurrencyExchangeRate_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CurrencyExchangeRate[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CurrencyExchangeRate_QB whereId($value)
     * @method _IH_CurrencyExchangeRate_QB whereRate($value)
     * @method _IH_CurrencyExchangeRate_QB whereTargetCurrency($value)
     * @method _IH_CurrencyExchangeRate_QB whereCreatedAt($value)
     * @method _IH_CurrencyExchangeRate_QB whereUpdatedAt($value)
     * @method CurrencyExchangeRate baseSole(array|string $columns = ['*'])
     * @method CurrencyExchangeRate create(array $attributes = [])
     * @method _IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] cursor()
     * @method CurrencyExchangeRate|null|_IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] find($id, array $columns = ['*'])
     * @method _IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CurrencyExchangeRate|_IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] findOrFail($id, array $columns = ['*'])
     * @method CurrencyExchangeRate|_IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] findOrNew($id, array $columns = ['*'])
     * @method CurrencyExchangeRate first(array|string $columns = ['*'])
     * @method CurrencyExchangeRate firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CurrencyExchangeRate firstOrCreate(array $attributes = [], array $values = [])
     * @method CurrencyExchangeRate firstOrFail(array $columns = ['*'])
     * @method CurrencyExchangeRate firstOrNew(array $attributes = [], array $values = [])
     * @method CurrencyExchangeRate firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CurrencyExchangeRate forceCreate(array $attributes)
     * @method _IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] get(array|string $columns = ['*'])
     * @method CurrencyExchangeRate getModel()
     * @method CurrencyExchangeRate[] getModels(array|string $columns = ['*'])
     * @method _IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] hydrate(array $items)
     * @method CurrencyExchangeRate make(array $attributes = [])
     * @method CurrencyExchangeRate newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CurrencyExchangeRate[]|_IH_CurrencyExchangeRate_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CurrencyExchangeRate[]|_IH_CurrencyExchangeRate_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CurrencyExchangeRate sole(array|string $columns = ['*'])
     * @method CurrencyExchangeRate updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CurrencyExchangeRate_QB extends _BaseBuilder {}
    
    /**
     * @method Currency shift()
     * @method Currency pop()
     * @method Currency get($key, $default = null)
     * @method Currency pull($key, $default = null)
     * @method Currency first(callable $callback = null, $default = null)
     * @method Currency firstWhere(string $key, $operator = null, $value = null)
     * @method Currency find($key, $default = null)
     * @method Currency[] all()
     * @method Currency last(callable $callback = null, $default = null)
     * @method Currency sole($key = null, $operator = null, $value = null)
     */
    class _IH_Currency_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Currency[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Currency_QB whereId($value)
     * @method _IH_Currency_QB whereCode($value)
     * @method _IH_Currency_QB whereName($value)
     * @method _IH_Currency_QB whereCreatedAt($value)
     * @method _IH_Currency_QB whereUpdatedAt($value)
     * @method _IH_Currency_QB whereSymbol($value)
     * @method Currency baseSole(array|string $columns = ['*'])
     * @method Currency create(array $attributes = [])
     * @method _IH_Currency_C|Currency[] cursor()
     * @method Currency|null|_IH_Currency_C|Currency[] find($id, array $columns = ['*'])
     * @method _IH_Currency_C|Currency[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Currency|_IH_Currency_C|Currency[] findOrFail($id, array $columns = ['*'])
     * @method Currency|_IH_Currency_C|Currency[] findOrNew($id, array $columns = ['*'])
     * @method Currency first(array|string $columns = ['*'])
     * @method Currency firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Currency firstOrCreate(array $attributes = [], array $values = [])
     * @method Currency firstOrFail(array $columns = ['*'])
     * @method Currency firstOrNew(array $attributes = [], array $values = [])
     * @method Currency firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Currency forceCreate(array $attributes)
     * @method _IH_Currency_C|Currency[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Currency_C|Currency[] get(array|string $columns = ['*'])
     * @method Currency getModel()
     * @method Currency[] getModels(array|string $columns = ['*'])
     * @method _IH_Currency_C|Currency[] hydrate(array $items)
     * @method Currency make(array $attributes = [])
     * @method Currency newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Currency[]|_IH_Currency_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Currency[]|_IH_Currency_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Currency sole(array|string $columns = ['*'])
     * @method Currency updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Currency_QB extends _BaseBuilder {}
    
    /**
     * @method Locale shift()
     * @method Locale pop()
     * @method Locale get($key, $default = null)
     * @method Locale pull($key, $default = null)
     * @method Locale first(callable $callback = null, $default = null)
     * @method Locale firstWhere(string $key, $operator = null, $value = null)
     * @method Locale find($key, $default = null)
     * @method Locale[] all()
     * @method Locale last(callable $callback = null, $default = null)
     * @method Locale sole($key = null, $operator = null, $value = null)
     */
    class _IH_Locale_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Locale[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Locale_QB whereId($value)
     * @method _IH_Locale_QB whereCode($value)
     * @method _IH_Locale_QB whereName($value)
     * @method _IH_Locale_QB whereCreatedAt($value)
     * @method _IH_Locale_QB whereUpdatedAt($value)
     * @method _IH_Locale_QB whereDirection($value)
     * @method _IH_Locale_QB whereLocaleImage($value)
     * @method Locale baseSole(array|string $columns = ['*'])
     * @method Locale create(array $attributes = [])
     * @method _IH_Locale_C|Locale[] cursor()
     * @method Locale|null|_IH_Locale_C|Locale[] find($id, array $columns = ['*'])
     * @method _IH_Locale_C|Locale[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Locale|_IH_Locale_C|Locale[] findOrFail($id, array $columns = ['*'])
     * @method Locale|_IH_Locale_C|Locale[] findOrNew($id, array $columns = ['*'])
     * @method Locale first(array|string $columns = ['*'])
     * @method Locale firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Locale firstOrCreate(array $attributes = [], array $values = [])
     * @method Locale firstOrFail(array $columns = ['*'])
     * @method Locale firstOrNew(array $attributes = [], array $values = [])
     * @method Locale firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Locale forceCreate(array $attributes)
     * @method _IH_Locale_C|Locale[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Locale_C|Locale[] get(array|string $columns = ['*'])
     * @method Locale getModel()
     * @method Locale[] getModels(array|string $columns = ['*'])
     * @method _IH_Locale_C|Locale[] hydrate(array $items)
     * @method Locale make(array $attributes = [])
     * @method Locale newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Locale[]|_IH_Locale_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Locale[]|_IH_Locale_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Locale sole(array|string $columns = ['*'])
     * @method Locale updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Locale_QB extends _BaseBuilder {}
    
    /**
     * @method Slider shift()
     * @method Slider pop()
     * @method Slider get($key, $default = null)
     * @method Slider pull($key, $default = null)
     * @method Slider first(callable $callback = null, $default = null)
     * @method Slider firstWhere(string $key, $operator = null, $value = null)
     * @method Slider find($key, $default = null)
     * @method Slider[] all()
     * @method Slider last(callable $callback = null, $default = null)
     * @method Slider sole($key = null, $operator = null, $value = null)
     */
    class _IH_Slider_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Slider[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Slider_QB whereId($value)
     * @method _IH_Slider_QB whereTitle($value)
     * @method _IH_Slider_QB wherePath($value)
     * @method _IH_Slider_QB whereContent($value)
     * @method _IH_Slider_QB whereChannelId($value)
     * @method _IH_Slider_QB whereCreatedAt($value)
     * @method _IH_Slider_QB whereUpdatedAt($value)
     * @method _IH_Slider_QB whereSliderPath($value)
     * @method _IH_Slider_QB whereLocale($value)
     * @method _IH_Slider_QB whereExpiredAt($value)
     * @method _IH_Slider_QB whereSortOrder($value)
     * @method Slider baseSole(array|string $columns = ['*'])
     * @method Slider create(array $attributes = [])
     * @method _IH_Slider_C|Slider[] cursor()
     * @method Slider|null|_IH_Slider_C|Slider[] find($id, array $columns = ['*'])
     * @method _IH_Slider_C|Slider[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Slider|_IH_Slider_C|Slider[] findOrFail($id, array $columns = ['*'])
     * @method Slider|_IH_Slider_C|Slider[] findOrNew($id, array $columns = ['*'])
     * @method Slider first(array|string $columns = ['*'])
     * @method Slider firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Slider firstOrCreate(array $attributes = [], array $values = [])
     * @method Slider firstOrFail(array $columns = ['*'])
     * @method Slider firstOrNew(array $attributes = [], array $values = [])
     * @method Slider firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Slider forceCreate(array $attributes)
     * @method _IH_Slider_C|Slider[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Slider_C|Slider[] get(array|string $columns = ['*'])
     * @method Slider getModel()
     * @method Slider[] getModels(array|string $columns = ['*'])
     * @method _IH_Slider_C|Slider[] hydrate(array $items)
     * @method Slider make(array $attributes = [])
     * @method Slider newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Slider[]|_IH_Slider_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Slider[]|_IH_Slider_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Slider sole(array|string $columns = ['*'])
     * @method Slider updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Slider_QB extends _BaseBuilder {}
    
    /**
     * @method SubscribersList shift()
     * @method SubscribersList pop()
     * @method SubscribersList get($key, $default = null)
     * @method SubscribersList pull($key, $default = null)
     * @method SubscribersList first(callable $callback = null, $default = null)
     * @method SubscribersList firstWhere(string $key, $operator = null, $value = null)
     * @method SubscribersList find($key, $default = null)
     * @method SubscribersList[] all()
     * @method SubscribersList last(callable $callback = null, $default = null)
     * @method SubscribersList sole($key = null, $operator = null, $value = null)
     */
    class _IH_SubscribersList_C extends _BaseCollection {
        /**
         * @param int $size
         * @return SubscribersList[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_SubscribersList_QB whereId($value)
     * @method _IH_SubscribersList_QB whereEmail($value)
     * @method _IH_SubscribersList_QB whereIsSubscribed($value)
     * @method _IH_SubscribersList_QB whereToken($value)
     * @method _IH_SubscribersList_QB whereChannelId($value)
     * @method _IH_SubscribersList_QB whereCreatedAt($value)
     * @method _IH_SubscribersList_QB whereUpdatedAt($value)
     * @method _IH_SubscribersList_QB whereCustomerId($value)
     * @method SubscribersList baseSole(array|string $columns = ['*'])
     * @method SubscribersList create(array $attributes = [])
     * @method _IH_SubscribersList_C|SubscribersList[] cursor()
     * @method SubscribersList|null|_IH_SubscribersList_C|SubscribersList[] find($id, array $columns = ['*'])
     * @method _IH_SubscribersList_C|SubscribersList[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method SubscribersList|_IH_SubscribersList_C|SubscribersList[] findOrFail($id, array $columns = ['*'])
     * @method SubscribersList|_IH_SubscribersList_C|SubscribersList[] findOrNew($id, array $columns = ['*'])
     * @method SubscribersList first(array|string $columns = ['*'])
     * @method SubscribersList firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method SubscribersList firstOrCreate(array $attributes = [], array $values = [])
     * @method SubscribersList firstOrFail(array $columns = ['*'])
     * @method SubscribersList firstOrNew(array $attributes = [], array $values = [])
     * @method SubscribersList firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method SubscribersList forceCreate(array $attributes)
     * @method _IH_SubscribersList_C|SubscribersList[] fromQuery(string $query, array $bindings = [])
     * @method _IH_SubscribersList_C|SubscribersList[] get(array|string $columns = ['*'])
     * @method SubscribersList getModel()
     * @method SubscribersList[] getModels(array|string $columns = ['*'])
     * @method _IH_SubscribersList_C|SubscribersList[] hydrate(array $items)
     * @method SubscribersList make(array $attributes = [])
     * @method SubscribersList newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|SubscribersList[]|_IH_SubscribersList_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|SubscribersList[]|_IH_SubscribersList_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method SubscribersList sole(array|string $columns = ['*'])
     * @method SubscribersList updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_SubscribersList_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Customer\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Customer\Models\Customer;
    use Webkul\Customer\Models\CustomerAddress;
    use Webkul\Customer\Models\CustomerGroup;
    use Webkul\Customer\Models\Wishlist;
    
    /**
     * @method CustomerAddress shift()
     * @method CustomerAddress pop()
     * @method CustomerAddress get($key, $default = null)
     * @method CustomerAddress pull($key, $default = null)
     * @method CustomerAddress first(callable $callback = null, $default = null)
     * @method CustomerAddress firstWhere(string $key, $operator = null, $value = null)
     * @method CustomerAddress find($key, $default = null)
     * @method CustomerAddress[] all()
     * @method CustomerAddress last(callable $callback = null, $default = null)
     * @method CustomerAddress sole($key = null, $operator = null, $value = null)
     */
    class _IH_CustomerAddress_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CustomerAddress[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method CustomerAddress baseSole(array|string $columns = ['*'])
     * @method CustomerAddress create(array $attributes = [])
     * @method _IH_CustomerAddress_C|CustomerAddress[] cursor()
     * @method CustomerAddress|null|_IH_CustomerAddress_C|CustomerAddress[] find($id, array $columns = ['*'])
     * @method _IH_CustomerAddress_C|CustomerAddress[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CustomerAddress|_IH_CustomerAddress_C|CustomerAddress[] findOrFail($id, array $columns = ['*'])
     * @method CustomerAddress|_IH_CustomerAddress_C|CustomerAddress[] findOrNew($id, array $columns = ['*'])
     * @method CustomerAddress first(array|string $columns = ['*'])
     * @method CustomerAddress firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CustomerAddress firstOrCreate(array $attributes = [], array $values = [])
     * @method CustomerAddress firstOrFail(array $columns = ['*'])
     * @method CustomerAddress firstOrNew(array $attributes = [], array $values = [])
     * @method CustomerAddress firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CustomerAddress forceCreate(array $attributes)
     * @method _IH_CustomerAddress_C|CustomerAddress[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CustomerAddress_C|CustomerAddress[] get(array|string $columns = ['*'])
     * @method CustomerAddress getModel()
     * @method CustomerAddress[] getModels(array|string $columns = ['*'])
     * @method _IH_CustomerAddress_C|CustomerAddress[] hydrate(array $items)
     * @method CustomerAddress make(array $attributes = [])
     * @method CustomerAddress newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CustomerAddress[]|_IH_CustomerAddress_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CustomerAddress[]|_IH_CustomerAddress_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CustomerAddress sole(array|string $columns = ['*'])
     * @method CustomerAddress updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CustomerAddress_QB extends _BaseBuilder {}
    
    /**
     * @method CustomerGroup shift()
     * @method CustomerGroup pop()
     * @method CustomerGroup get($key, $default = null)
     * @method CustomerGroup pull($key, $default = null)
     * @method CustomerGroup first(callable $callback = null, $default = null)
     * @method CustomerGroup firstWhere(string $key, $operator = null, $value = null)
     * @method CustomerGroup find($key, $default = null)
     * @method CustomerGroup[] all()
     * @method CustomerGroup last(callable $callback = null, $default = null)
     * @method CustomerGroup sole($key = null, $operator = null, $value = null)
     */
    class _IH_CustomerGroup_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CustomerGroup[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CustomerGroup_QB whereId($value)
     * @method _IH_CustomerGroup_QB whereName($value)
     * @method _IH_CustomerGroup_QB whereIsUserDefined($value)
     * @method _IH_CustomerGroup_QB whereCreatedAt($value)
     * @method _IH_CustomerGroup_QB whereUpdatedAt($value)
     * @method _IH_CustomerGroup_QB whereCode($value)
     * @method CustomerGroup baseSole(array|string $columns = ['*'])
     * @method CustomerGroup create(array $attributes = [])
     * @method _IH_CustomerGroup_C|CustomerGroup[] cursor()
     * @method CustomerGroup|null|_IH_CustomerGroup_C|CustomerGroup[] find($id, array $columns = ['*'])
     * @method _IH_CustomerGroup_C|CustomerGroup[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CustomerGroup|_IH_CustomerGroup_C|CustomerGroup[] findOrFail($id, array $columns = ['*'])
     * @method CustomerGroup|_IH_CustomerGroup_C|CustomerGroup[] findOrNew($id, array $columns = ['*'])
     * @method CustomerGroup first(array|string $columns = ['*'])
     * @method CustomerGroup firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CustomerGroup firstOrCreate(array $attributes = [], array $values = [])
     * @method CustomerGroup firstOrFail(array $columns = ['*'])
     * @method CustomerGroup firstOrNew(array $attributes = [], array $values = [])
     * @method CustomerGroup firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CustomerGroup forceCreate(array $attributes)
     * @method _IH_CustomerGroup_C|CustomerGroup[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CustomerGroup_C|CustomerGroup[] get(array|string $columns = ['*'])
     * @method CustomerGroup getModel()
     * @method CustomerGroup[] getModels(array|string $columns = ['*'])
     * @method _IH_CustomerGroup_C|CustomerGroup[] hydrate(array $items)
     * @method CustomerGroup make(array $attributes = [])
     * @method CustomerGroup newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CustomerGroup[]|_IH_CustomerGroup_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CustomerGroup[]|_IH_CustomerGroup_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CustomerGroup sole(array|string $columns = ['*'])
     * @method CustomerGroup updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CustomerGroup_QB extends _BaseBuilder {}
    
    /**
     * @method Customer shift()
     * @method Customer pop()
     * @method Customer get($key, $default = null)
     * @method Customer pull($key, $default = null)
     * @method Customer first(callable $callback = null, $default = null)
     * @method Customer firstWhere(string $key, $operator = null, $value = null)
     * @method Customer find($key, $default = null)
     * @method Customer[] all()
     * @method Customer last(callable $callback = null, $default = null)
     * @method Customer sole($key = null, $operator = null, $value = null)
     */
    class _IH_Customer_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Customer[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Customer_QB whereId($value)
     * @method _IH_Customer_QB whereFirstName($value)
     * @method _IH_Customer_QB whereLastName($value)
     * @method _IH_Customer_QB whereGender($value)
     * @method _IH_Customer_QB whereDateOfBirth($value)
     * @method _IH_Customer_QB whereEmail($value)
     * @method _IH_Customer_QB whereImage($value)
     * @method _IH_Customer_QB whereStatus($value)
     * @method _IH_Customer_QB wherePassword($value)
     * @method _IH_Customer_QB whereApiToken($value)
     * @method _IH_Customer_QB whereCustomerGroupId($value)
     * @method _IH_Customer_QB whereSubscribedToNewsLetter($value)
     * @method _IH_Customer_QB whereRememberToken($value)
     * @method _IH_Customer_QB whereCreatedAt($value)
     * @method _IH_Customer_QB whereUpdatedAt($value)
     * @method _IH_Customer_QB whereIsVerified($value)
     * @method _IH_Customer_QB whereToken($value)
     * @method _IH_Customer_QB whereNotes($value)
     * @method _IH_Customer_QB wherePhone($value)
     * @method Customer baseSole(array|string $columns = ['*'])
     * @method Customer create(array $attributes = [])
     * @method _IH_Customer_C|Customer[] cursor()
     * @method Customer|null|_IH_Customer_C|Customer[] find($id, array $columns = ['*'])
     * @method _IH_Customer_C|Customer[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Customer|_IH_Customer_C|Customer[] findOrFail($id, array $columns = ['*'])
     * @method Customer|_IH_Customer_C|Customer[] findOrNew($id, array $columns = ['*'])
     * @method Customer first(array|string $columns = ['*'])
     * @method Customer firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Customer firstOrCreate(array $attributes = [], array $values = [])
     * @method Customer firstOrFail(array $columns = ['*'])
     * @method Customer firstOrNew(array $attributes = [], array $values = [])
     * @method Customer firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Customer forceCreate(array $attributes)
     * @method _IH_Customer_C|Customer[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Customer_C|Customer[] get(array|string $columns = ['*'])
     * @method Customer getModel()
     * @method Customer[] getModels(array|string $columns = ['*'])
     * @method _IH_Customer_C|Customer[] hydrate(array $items)
     * @method Customer make(array $attributes = [])
     * @method Customer newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Customer[]|_IH_Customer_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Customer[]|_IH_Customer_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Customer sole(array|string $columns = ['*'])
     * @method Customer updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Customer_QB extends _BaseBuilder {}
    
    /**
     * @method Wishlist shift()
     * @method Wishlist pop()
     * @method Wishlist get($key, $default = null)
     * @method Wishlist pull($key, $default = null)
     * @method Wishlist first(callable $callback = null, $default = null)
     * @method Wishlist firstWhere(string $key, $operator = null, $value = null)
     * @method Wishlist find($key, $default = null)
     * @method Wishlist[] all()
     * @method Wishlist last(callable $callback = null, $default = null)
     * @method Wishlist sole($key = null, $operator = null, $value = null)
     */
    class _IH_Wishlist_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Wishlist[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Wishlist_QB whereId($value)
     * @method _IH_Wishlist_QB whereChannelId($value)
     * @method _IH_Wishlist_QB whereProductId($value)
     * @method _IH_Wishlist_QB whereCustomerId($value)
     * @method _IH_Wishlist_QB whereItemOptions($value)
     * @method _IH_Wishlist_QB whereMovedToCart($value)
     * @method _IH_Wishlist_QB whereShared($value)
     * @method _IH_Wishlist_QB whereTimeOfMoving($value)
     * @method _IH_Wishlist_QB whereCreatedAt($value)
     * @method _IH_Wishlist_QB whereUpdatedAt($value)
     * @method _IH_Wishlist_QB whereAdditional($value)
     * @method Wishlist baseSole(array|string $columns = ['*'])
     * @method Wishlist create(array $attributes = [])
     * @method _IH_Wishlist_C|Wishlist[] cursor()
     * @method Wishlist|null|_IH_Wishlist_C|Wishlist[] find($id, array $columns = ['*'])
     * @method _IH_Wishlist_C|Wishlist[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Wishlist|_IH_Wishlist_C|Wishlist[] findOrFail($id, array $columns = ['*'])
     * @method Wishlist|_IH_Wishlist_C|Wishlist[] findOrNew($id, array $columns = ['*'])
     * @method Wishlist first(array|string $columns = ['*'])
     * @method Wishlist firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Wishlist firstOrCreate(array $attributes = [], array $values = [])
     * @method Wishlist firstOrFail(array $columns = ['*'])
     * @method Wishlist firstOrNew(array $attributes = [], array $values = [])
     * @method Wishlist firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Wishlist forceCreate(array $attributes)
     * @method _IH_Wishlist_C|Wishlist[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Wishlist_C|Wishlist[] get(array|string $columns = ['*'])
     * @method Wishlist getModel()
     * @method Wishlist[] getModels(array|string $columns = ['*'])
     * @method _IH_Wishlist_C|Wishlist[] hydrate(array $items)
     * @method Wishlist make(array $attributes = [])
     * @method Wishlist newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Wishlist[]|_IH_Wishlist_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Wishlist[]|_IH_Wishlist_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Wishlist sole(array|string $columns = ['*'])
     * @method Wishlist updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Wishlist_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Inventory\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Inventory\Models\InventorySource;
    
    /**
     * @method InventorySource shift()
     * @method InventorySource pop()
     * @method InventorySource get($key, $default = null)
     * @method InventorySource pull($key, $default = null)
     * @method InventorySource first(callable $callback = null, $default = null)
     * @method InventorySource firstWhere(string $key, $operator = null, $value = null)
     * @method InventorySource find($key, $default = null)
     * @method InventorySource[] all()
     * @method InventorySource last(callable $callback = null, $default = null)
     * @method InventorySource sole($key = null, $operator = null, $value = null)
     */
    class _IH_InventorySource_C extends _BaseCollection {
        /**
         * @param int $size
         * @return InventorySource[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_InventorySource_QB whereId($value)
     * @method _IH_InventorySource_QB whereCode($value)
     * @method _IH_InventorySource_QB whereName($value)
     * @method _IH_InventorySource_QB whereDescription($value)
     * @method _IH_InventorySource_QB whereContactName($value)
     * @method _IH_InventorySource_QB whereContactEmail($value)
     * @method _IH_InventorySource_QB whereContactNumber($value)
     * @method _IH_InventorySource_QB whereContactFax($value)
     * @method _IH_InventorySource_QB whereCountry($value)
     * @method _IH_InventorySource_QB whereState($value)
     * @method _IH_InventorySource_QB whereCity($value)
     * @method _IH_InventorySource_QB whereStreet($value)
     * @method _IH_InventorySource_QB wherePostcode($value)
     * @method _IH_InventorySource_QB wherePriority($value)
     * @method _IH_InventorySource_QB whereLatitude($value)
     * @method _IH_InventorySource_QB whereLongitude($value)
     * @method _IH_InventorySource_QB whereStatus($value)
     * @method _IH_InventorySource_QB whereCreatedAt($value)
     * @method _IH_InventorySource_QB whereUpdatedAt($value)
     * @method InventorySource baseSole(array|string $columns = ['*'])
     * @method InventorySource create(array $attributes = [])
     * @method _IH_InventorySource_C|InventorySource[] cursor()
     * @method InventorySource|null|_IH_InventorySource_C|InventorySource[] find($id, array $columns = ['*'])
     * @method _IH_InventorySource_C|InventorySource[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method InventorySource|_IH_InventorySource_C|InventorySource[] findOrFail($id, array $columns = ['*'])
     * @method InventorySource|_IH_InventorySource_C|InventorySource[] findOrNew($id, array $columns = ['*'])
     * @method InventorySource first(array|string $columns = ['*'])
     * @method InventorySource firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method InventorySource firstOrCreate(array $attributes = [], array $values = [])
     * @method InventorySource firstOrFail(array $columns = ['*'])
     * @method InventorySource firstOrNew(array $attributes = [], array $values = [])
     * @method InventorySource firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method InventorySource forceCreate(array $attributes)
     * @method _IH_InventorySource_C|InventorySource[] fromQuery(string $query, array $bindings = [])
     * @method _IH_InventorySource_C|InventorySource[] get(array|string $columns = ['*'])
     * @method InventorySource getModel()
     * @method InventorySource[] getModels(array|string $columns = ['*'])
     * @method _IH_InventorySource_C|InventorySource[] hydrate(array $items)
     * @method InventorySource make(array $attributes = [])
     * @method InventorySource newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|InventorySource[]|_IH_InventorySource_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|InventorySource[]|_IH_InventorySource_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method InventorySource sole(array|string $columns = ['*'])
     * @method InventorySource updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_InventorySource_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Marketing\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Marketing\Models\Campaign;
    use Webkul\Marketing\Models\Event;
    use Webkul\Marketing\Models\Template;
    
    /**
     * @method Campaign shift()
     * @method Campaign pop()
     * @method Campaign get($key, $default = null)
     * @method Campaign pull($key, $default = null)
     * @method Campaign first(callable $callback = null, $default = null)
     * @method Campaign firstWhere(string $key, $operator = null, $value = null)
     * @method Campaign find($key, $default = null)
     * @method Campaign[] all()
     * @method Campaign last(callable $callback = null, $default = null)
     * @method Campaign sole($key = null, $operator = null, $value = null)
     */
    class _IH_Campaign_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Campaign[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Campaign_QB whereId($value)
     * @method _IH_Campaign_QB whereName($value)
     * @method _IH_Campaign_QB whereSubject($value)
     * @method _IH_Campaign_QB whereStatus($value)
     * @method _IH_Campaign_QB whereType($value)
     * @method _IH_Campaign_QB whereMailTo($value)
     * @method _IH_Campaign_QB whereSpooling($value)
     * @method _IH_Campaign_QB whereChannelId($value)
     * @method _IH_Campaign_QB whereCustomerGroupId($value)
     * @method _IH_Campaign_QB whereMarketingTemplateId($value)
     * @method _IH_Campaign_QB whereMarketingEventId($value)
     * @method _IH_Campaign_QB whereCreatedAt($value)
     * @method _IH_Campaign_QB whereUpdatedAt($value)
     * @method Campaign baseSole(array|string $columns = ['*'])
     * @method Campaign create(array $attributes = [])
     * @method _IH_Campaign_C|Campaign[] cursor()
     * @method Campaign|null|_IH_Campaign_C|Campaign[] find($id, array $columns = ['*'])
     * @method _IH_Campaign_C|Campaign[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Campaign|_IH_Campaign_C|Campaign[] findOrFail($id, array $columns = ['*'])
     * @method Campaign|_IH_Campaign_C|Campaign[] findOrNew($id, array $columns = ['*'])
     * @method Campaign first(array|string $columns = ['*'])
     * @method Campaign firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Campaign firstOrCreate(array $attributes = [], array $values = [])
     * @method Campaign firstOrFail(array $columns = ['*'])
     * @method Campaign firstOrNew(array $attributes = [], array $values = [])
     * @method Campaign firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Campaign forceCreate(array $attributes)
     * @method _IH_Campaign_C|Campaign[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Campaign_C|Campaign[] get(array|string $columns = ['*'])
     * @method Campaign getModel()
     * @method Campaign[] getModels(array|string $columns = ['*'])
     * @method _IH_Campaign_C|Campaign[] hydrate(array $items)
     * @method Campaign make(array $attributes = [])
     * @method Campaign newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Campaign[]|_IH_Campaign_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Campaign[]|_IH_Campaign_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Campaign sole(array|string $columns = ['*'])
     * @method Campaign updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Campaign_QB extends _BaseBuilder {}
    
    /**
     * @method Event shift()
     * @method Event pop()
     * @method Event get($key, $default = null)
     * @method Event pull($key, $default = null)
     * @method Event first(callable $callback = null, $default = null)
     * @method Event firstWhere(string $key, $operator = null, $value = null)
     * @method Event find($key, $default = null)
     * @method Event[] all()
     * @method Event last(callable $callback = null, $default = null)
     * @method Event sole($key = null, $operator = null, $value = null)
     */
    class _IH_Event_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Event[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Event_QB whereId($value)
     * @method _IH_Event_QB whereName($value)
     * @method _IH_Event_QB whereDescription($value)
     * @method _IH_Event_QB whereDate($value)
     * @method _IH_Event_QB whereCreatedAt($value)
     * @method _IH_Event_QB whereUpdatedAt($value)
     * @method Event baseSole(array|string $columns = ['*'])
     * @method Event create(array $attributes = [])
     * @method _IH_Event_C|Event[] cursor()
     * @method Event|null|_IH_Event_C|Event[] find($id, array $columns = ['*'])
     * @method _IH_Event_C|Event[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Event|_IH_Event_C|Event[] findOrFail($id, array $columns = ['*'])
     * @method Event|_IH_Event_C|Event[] findOrNew($id, array $columns = ['*'])
     * @method Event first(array|string $columns = ['*'])
     * @method Event firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Event firstOrCreate(array $attributes = [], array $values = [])
     * @method Event firstOrFail(array $columns = ['*'])
     * @method Event firstOrNew(array $attributes = [], array $values = [])
     * @method Event firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Event forceCreate(array $attributes)
     * @method _IH_Event_C|Event[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Event_C|Event[] get(array|string $columns = ['*'])
     * @method Event getModel()
     * @method Event[] getModels(array|string $columns = ['*'])
     * @method _IH_Event_C|Event[] hydrate(array $items)
     * @method Event make(array $attributes = [])
     * @method Event newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Event[]|_IH_Event_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Event[]|_IH_Event_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Event sole(array|string $columns = ['*'])
     * @method Event updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Event_QB extends _BaseBuilder {}
    
    /**
     * @method Template shift()
     * @method Template pop()
     * @method Template get($key, $default = null)
     * @method Template pull($key, $default = null)
     * @method Template first(callable $callback = null, $default = null)
     * @method Template firstWhere(string $key, $operator = null, $value = null)
     * @method Template find($key, $default = null)
     * @method Template[] all()
     * @method Template last(callable $callback = null, $default = null)
     * @method Template sole($key = null, $operator = null, $value = null)
     */
    class _IH_Template_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Template[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Template_QB whereId($value)
     * @method _IH_Template_QB whereName($value)
     * @method _IH_Template_QB whereStatus($value)
     * @method _IH_Template_QB whereContent($value)
     * @method _IH_Template_QB whereCreatedAt($value)
     * @method _IH_Template_QB whereUpdatedAt($value)
     * @method Template baseSole(array|string $columns = ['*'])
     * @method Template create(array $attributes = [])
     * @method _IH_Template_C|Template[] cursor()
     * @method Template|null|_IH_Template_C|Template[] find($id, array $columns = ['*'])
     * @method _IH_Template_C|Template[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Template|_IH_Template_C|Template[] findOrFail($id, array $columns = ['*'])
     * @method Template|_IH_Template_C|Template[] findOrNew($id, array $columns = ['*'])
     * @method Template first(array|string $columns = ['*'])
     * @method Template firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Template firstOrCreate(array $attributes = [], array $values = [])
     * @method Template firstOrFail(array $columns = ['*'])
     * @method Template firstOrNew(array $attributes = [], array $values = [])
     * @method Template firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Template forceCreate(array $attributes)
     * @method _IH_Template_C|Template[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Template_C|Template[] get(array|string $columns = ['*'])
     * @method Template getModel()
     * @method Template[] getModels(array|string $columns = ['*'])
     * @method _IH_Template_C|Template[] hydrate(array $items)
     * @method Template make(array $attributes = [])
     * @method Template newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Template[]|_IH_Template_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Template[]|_IH_Template_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Template sole(array|string $columns = ['*'])
     * @method Template updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Template_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Product\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Product\Database\Eloquent\Builder;
    use Webkul\Product\Models\Product;
    use Webkul\Product\Models\ProductAttributeValue;
    use Webkul\Product\Models\ProductBundleOption;
    use Webkul\Product\Models\ProductBundleOptionProduct;
    use Webkul\Product\Models\ProductBundleOptionTranslation;
    use Webkul\Product\Models\ProductCustomerGroupPrice;
    use Webkul\Product\Models\ProductDownloadableLink;
    use Webkul\Product\Models\ProductDownloadableLinkTranslation;
    use Webkul\Product\Models\ProductDownloadableSample;
    use Webkul\Product\Models\ProductDownloadableSampleTranslation;
    use Webkul\Product\Models\ProductFlat;
    use Webkul\Product\Models\ProductGroupedProduct;
    use Webkul\Product\Models\ProductImage;
    use Webkul\Product\Models\ProductInventory;
    use Webkul\Product\Models\ProductOrderedInventory;
    use Webkul\Product\Models\ProductReview;
    use Webkul\Product\Models\ProductReviewImage;
    use Webkul\Product\Models\ProductSalableInventory;
    use Webkul\Product\Models\ProductVideo;
    
    /**
     * @method ProductAttributeValue shift()
     * @method ProductAttributeValue pop()
     * @method ProductAttributeValue get($key, $default = null)
     * @method ProductAttributeValue pull($key, $default = null)
     * @method ProductAttributeValue first(callable $callback = null, $default = null)
     * @method ProductAttributeValue firstWhere(string $key, $operator = null, $value = null)
     * @method ProductAttributeValue find($key, $default = null)
     * @method ProductAttributeValue[] all()
     * @method ProductAttributeValue last(callable $callback = null, $default = null)
     * @method ProductAttributeValue sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductAttributeValue_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductAttributeValue[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductAttributeValue_QB whereId($value)
     * @method _IH_ProductAttributeValue_QB whereLocale($value)
     * @method _IH_ProductAttributeValue_QB whereChannel($value)
     * @method _IH_ProductAttributeValue_QB whereTextValue($value)
     * @method _IH_ProductAttributeValue_QB whereBooleanValue($value)
     * @method _IH_ProductAttributeValue_QB whereIntegerValue($value)
     * @method _IH_ProductAttributeValue_QB whereFloatValue($value)
     * @method _IH_ProductAttributeValue_QB whereDatetimeValue($value)
     * @method _IH_ProductAttributeValue_QB whereDateValue($value)
     * @method _IH_ProductAttributeValue_QB whereJsonValue($value)
     * @method _IH_ProductAttributeValue_QB whereProductId($value)
     * @method _IH_ProductAttributeValue_QB whereAttributeId($value)
     * @method ProductAttributeValue baseSole(array|string $columns = ['*'])
     * @method ProductAttributeValue create(array $attributes = [])
     * @method _IH_ProductAttributeValue_C|ProductAttributeValue[] cursor()
     * @method ProductAttributeValue|null|_IH_ProductAttributeValue_C|ProductAttributeValue[] find($id, array $columns = ['*'])
     * @method _IH_ProductAttributeValue_C|ProductAttributeValue[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductAttributeValue|_IH_ProductAttributeValue_C|ProductAttributeValue[] findOrFail($id, array $columns = ['*'])
     * @method ProductAttributeValue|_IH_ProductAttributeValue_C|ProductAttributeValue[] findOrNew($id, array $columns = ['*'])
     * @method ProductAttributeValue first(array|string $columns = ['*'])
     * @method ProductAttributeValue firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductAttributeValue firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductAttributeValue firstOrFail(array $columns = ['*'])
     * @method ProductAttributeValue firstOrNew(array $attributes = [], array $values = [])
     * @method ProductAttributeValue firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductAttributeValue forceCreate(array $attributes)
     * @method _IH_ProductAttributeValue_C|ProductAttributeValue[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductAttributeValue_C|ProductAttributeValue[] get(array|string $columns = ['*'])
     * @method ProductAttributeValue getModel()
     * @method ProductAttributeValue[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductAttributeValue_C|ProductAttributeValue[] hydrate(array $items)
     * @method ProductAttributeValue make(array $attributes = [])
     * @method ProductAttributeValue newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductAttributeValue[]|_IH_ProductAttributeValue_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductAttributeValue[]|_IH_ProductAttributeValue_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductAttributeValue sole(array|string $columns = ['*'])
     * @method ProductAttributeValue updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductAttributeValue_QB extends _BaseBuilder {}
    
    /**
     * @method ProductBundleOptionProduct shift()
     * @method ProductBundleOptionProduct pop()
     * @method ProductBundleOptionProduct get($key, $default = null)
     * @method ProductBundleOptionProduct pull($key, $default = null)
     * @method ProductBundleOptionProduct first(callable $callback = null, $default = null)
     * @method ProductBundleOptionProduct firstWhere(string $key, $operator = null, $value = null)
     * @method ProductBundleOptionProduct find($key, $default = null)
     * @method ProductBundleOptionProduct[] all()
     * @method ProductBundleOptionProduct last(callable $callback = null, $default = null)
     * @method ProductBundleOptionProduct sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductBundleOptionProduct_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductBundleOptionProduct[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductBundleOptionProduct_QB whereId($value)
     * @method _IH_ProductBundleOptionProduct_QB whereQty($value)
     * @method _IH_ProductBundleOptionProduct_QB whereIsUserDefined($value)
     * @method _IH_ProductBundleOptionProduct_QB whereIsDefault($value)
     * @method _IH_ProductBundleOptionProduct_QB whereSortOrder($value)
     * @method _IH_ProductBundleOptionProduct_QB whereProductBundleOptionId($value)
     * @method _IH_ProductBundleOptionProduct_QB whereProductId($value)
     * @method ProductBundleOptionProduct baseSole(array|string $columns = ['*'])
     * @method ProductBundleOptionProduct create(array $attributes = [])
     * @method _IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] cursor()
     * @method ProductBundleOptionProduct|null|_IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] find($id, array $columns = ['*'])
     * @method _IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductBundleOptionProduct|_IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] findOrFail($id, array $columns = ['*'])
     * @method ProductBundleOptionProduct|_IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] findOrNew($id, array $columns = ['*'])
     * @method ProductBundleOptionProduct first(array|string $columns = ['*'])
     * @method ProductBundleOptionProduct firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductBundleOptionProduct firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductBundleOptionProduct firstOrFail(array $columns = ['*'])
     * @method ProductBundleOptionProduct firstOrNew(array $attributes = [], array $values = [])
     * @method ProductBundleOptionProduct firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductBundleOptionProduct forceCreate(array $attributes)
     * @method _IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] get(array|string $columns = ['*'])
     * @method ProductBundleOptionProduct getModel()
     * @method ProductBundleOptionProduct[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] hydrate(array $items)
     * @method ProductBundleOptionProduct make(array $attributes = [])
     * @method ProductBundleOptionProduct newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductBundleOptionProduct[]|_IH_ProductBundleOptionProduct_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductBundleOptionProduct[]|_IH_ProductBundleOptionProduct_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductBundleOptionProduct sole(array|string $columns = ['*'])
     * @method ProductBundleOptionProduct updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductBundleOptionProduct_QB extends _BaseBuilder {}
    
    /**
     * @method ProductBundleOptionTranslation shift()
     * @method ProductBundleOptionTranslation pop()
     * @method ProductBundleOptionTranslation get($key, $default = null)
     * @method ProductBundleOptionTranslation pull($key, $default = null)
     * @method ProductBundleOptionTranslation first(callable $callback = null, $default = null)
     * @method ProductBundleOptionTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method ProductBundleOptionTranslation find($key, $default = null)
     * @method ProductBundleOptionTranslation[] all()
     * @method ProductBundleOptionTranslation last(callable $callback = null, $default = null)
     * @method ProductBundleOptionTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductBundleOptionTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductBundleOptionTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductBundleOptionTranslation_QB whereId($value)
     * @method _IH_ProductBundleOptionTranslation_QB whereLocale($value)
     * @method _IH_ProductBundleOptionTranslation_QB whereLabel($value)
     * @method _IH_ProductBundleOptionTranslation_QB whereProductBundleOptionId($value)
     * @method ProductBundleOptionTranslation baseSole(array|string $columns = ['*'])
     * @method ProductBundleOptionTranslation create(array $attributes = [])
     * @method _IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] cursor()
     * @method ProductBundleOptionTranslation|null|_IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] find($id, array $columns = ['*'])
     * @method _IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductBundleOptionTranslation|_IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] findOrFail($id, array $columns = ['*'])
     * @method ProductBundleOptionTranslation|_IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] findOrNew($id, array $columns = ['*'])
     * @method ProductBundleOptionTranslation first(array|string $columns = ['*'])
     * @method ProductBundleOptionTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductBundleOptionTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductBundleOptionTranslation firstOrFail(array $columns = ['*'])
     * @method ProductBundleOptionTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method ProductBundleOptionTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductBundleOptionTranslation forceCreate(array $attributes)
     * @method _IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] get(array|string $columns = ['*'])
     * @method ProductBundleOptionTranslation getModel()
     * @method ProductBundleOptionTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] hydrate(array $items)
     * @method ProductBundleOptionTranslation make(array $attributes = [])
     * @method ProductBundleOptionTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductBundleOptionTranslation[]|_IH_ProductBundleOptionTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductBundleOptionTranslation[]|_IH_ProductBundleOptionTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductBundleOptionTranslation sole(array|string $columns = ['*'])
     * @method ProductBundleOptionTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductBundleOptionTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method ProductBundleOption shift()
     * @method ProductBundleOption pop()
     * @method ProductBundleOption get($key, $default = null)
     * @method ProductBundleOption pull($key, $default = null)
     * @method ProductBundleOption first(callable $callback = null, $default = null)
     * @method ProductBundleOption firstWhere(string $key, $operator = null, $value = null)
     * @method ProductBundleOption find($key, $default = null)
     * @method ProductBundleOption[] all()
     * @method ProductBundleOption last(callable $callback = null, $default = null)
     * @method ProductBundleOption sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductBundleOption_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductBundleOption[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductBundleOption_QB whereId($value)
     * @method _IH_ProductBundleOption_QB whereType($value)
     * @method _IH_ProductBundleOption_QB whereIsRequired($value)
     * @method _IH_ProductBundleOption_QB whereSortOrder($value)
     * @method _IH_ProductBundleOption_QB whereProductId($value)
     * @method ProductBundleOption baseSole(array|string $columns = ['*'])
     * @method ProductBundleOption create(array $attributes = [])
     * @method _IH_ProductBundleOption_C|ProductBundleOption[] cursor()
     * @method ProductBundleOption|null|_IH_ProductBundleOption_C|ProductBundleOption[] find($id, array $columns = ['*'])
     * @method _IH_ProductBundleOption_C|ProductBundleOption[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductBundleOption|_IH_ProductBundleOption_C|ProductBundleOption[] findOrFail($id, array $columns = ['*'])
     * @method ProductBundleOption|_IH_ProductBundleOption_C|ProductBundleOption[] findOrNew($id, array $columns = ['*'])
     * @method ProductBundleOption first(array|string $columns = ['*'])
     * @method ProductBundleOption firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductBundleOption firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductBundleOption firstOrFail(array $columns = ['*'])
     * @method ProductBundleOption firstOrNew(array $attributes = [], array $values = [])
     * @method ProductBundleOption firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductBundleOption forceCreate(array $attributes)
     * @method _IH_ProductBundleOption_C|ProductBundleOption[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductBundleOption_C|ProductBundleOption[] get(array|string $columns = ['*'])
     * @method ProductBundleOption getModel()
     * @method ProductBundleOption[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductBundleOption_C|ProductBundleOption[] hydrate(array $items)
     * @method ProductBundleOption make(array $attributes = [])
     * @method ProductBundleOption newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductBundleOption[]|_IH_ProductBundleOption_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductBundleOption[]|_IH_ProductBundleOption_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductBundleOption sole(array|string $columns = ['*'])
     * @method ProductBundleOption updateOrCreate(array $attributes, array $values = [])
     * @method _IH_ProductBundleOption_QB listsTranslations(string $translationField)
     * @method _IH_ProductBundleOption_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_ProductBundleOption_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductBundleOption_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductBundleOption_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_ProductBundleOption_QB translated()
     * @method _IH_ProductBundleOption_QB translatedIn(null|string $locale = null)
     * @method _IH_ProductBundleOption_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_ProductBundleOption_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductBundleOption_QB withTranslation()
     */
    class _IH_ProductBundleOption_QB extends _BaseBuilder {}
    
    /**
     * @method ProductCustomerGroupPrice shift()
     * @method ProductCustomerGroupPrice pop()
     * @method ProductCustomerGroupPrice get($key, $default = null)
     * @method ProductCustomerGroupPrice pull($key, $default = null)
     * @method ProductCustomerGroupPrice first(callable $callback = null, $default = null)
     * @method ProductCustomerGroupPrice firstWhere(string $key, $operator = null, $value = null)
     * @method ProductCustomerGroupPrice find($key, $default = null)
     * @method ProductCustomerGroupPrice[] all()
     * @method ProductCustomerGroupPrice last(callable $callback = null, $default = null)
     * @method ProductCustomerGroupPrice sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductCustomerGroupPrice_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductCustomerGroupPrice[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductCustomerGroupPrice_QB whereId($value)
     * @method _IH_ProductCustomerGroupPrice_QB whereQty($value)
     * @method _IH_ProductCustomerGroupPrice_QB whereValueType($value)
     * @method _IH_ProductCustomerGroupPrice_QB whereValue($value)
     * @method _IH_ProductCustomerGroupPrice_QB whereProductId($value)
     * @method _IH_ProductCustomerGroupPrice_QB whereCustomerGroupId($value)
     * @method _IH_ProductCustomerGroupPrice_QB whereCreatedAt($value)
     * @method _IH_ProductCustomerGroupPrice_QB whereUpdatedAt($value)
     * @method ProductCustomerGroupPrice baseSole(array|string $columns = ['*'])
     * @method ProductCustomerGroupPrice create(array $attributes = [])
     * @method _IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] cursor()
     * @method ProductCustomerGroupPrice|null|_IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] find($id, array $columns = ['*'])
     * @method _IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductCustomerGroupPrice|_IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] findOrFail($id, array $columns = ['*'])
     * @method ProductCustomerGroupPrice|_IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] findOrNew($id, array $columns = ['*'])
     * @method ProductCustomerGroupPrice first(array|string $columns = ['*'])
     * @method ProductCustomerGroupPrice firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductCustomerGroupPrice firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductCustomerGroupPrice firstOrFail(array $columns = ['*'])
     * @method ProductCustomerGroupPrice firstOrNew(array $attributes = [], array $values = [])
     * @method ProductCustomerGroupPrice firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductCustomerGroupPrice forceCreate(array $attributes)
     * @method _IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] get(array|string $columns = ['*'])
     * @method ProductCustomerGroupPrice getModel()
     * @method ProductCustomerGroupPrice[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] hydrate(array $items)
     * @method ProductCustomerGroupPrice make(array $attributes = [])
     * @method ProductCustomerGroupPrice newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductCustomerGroupPrice[]|_IH_ProductCustomerGroupPrice_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductCustomerGroupPrice[]|_IH_ProductCustomerGroupPrice_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductCustomerGroupPrice sole(array|string $columns = ['*'])
     * @method ProductCustomerGroupPrice updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductCustomerGroupPrice_QB extends _BaseBuilder {}
    
    /**
     * @method ProductDownloadableLinkTranslation shift()
     * @method ProductDownloadableLinkTranslation pop()
     * @method ProductDownloadableLinkTranslation get($key, $default = null)
     * @method ProductDownloadableLinkTranslation pull($key, $default = null)
     * @method ProductDownloadableLinkTranslation first(callable $callback = null, $default = null)
     * @method ProductDownloadableLinkTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method ProductDownloadableLinkTranslation find($key, $default = null)
     * @method ProductDownloadableLinkTranslation[] all()
     * @method ProductDownloadableLinkTranslation last(callable $callback = null, $default = null)
     * @method ProductDownloadableLinkTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductDownloadableLinkTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductDownloadableLinkTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductDownloadableLinkTranslation_QB whereId($value)
     * @method _IH_ProductDownloadableLinkTranslation_QB whereLocale($value)
     * @method _IH_ProductDownloadableLinkTranslation_QB whereTitle($value)
     * @method _IH_ProductDownloadableLinkTranslation_QB whereProductDownloadableLinkId($value)
     * @method ProductDownloadableLinkTranslation baseSole(array|string $columns = ['*'])
     * @method ProductDownloadableLinkTranslation create(array $attributes = [])
     * @method _IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] cursor()
     * @method ProductDownloadableLinkTranslation|null|_IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] find($id, array $columns = ['*'])
     * @method _IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductDownloadableLinkTranslation|_IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] findOrFail($id, array $columns = ['*'])
     * @method ProductDownloadableLinkTranslation|_IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] findOrNew($id, array $columns = ['*'])
     * @method ProductDownloadableLinkTranslation first(array|string $columns = ['*'])
     * @method ProductDownloadableLinkTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductDownloadableLinkTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductDownloadableLinkTranslation firstOrFail(array $columns = ['*'])
     * @method ProductDownloadableLinkTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method ProductDownloadableLinkTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductDownloadableLinkTranslation forceCreate(array $attributes)
     * @method _IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] get(array|string $columns = ['*'])
     * @method ProductDownloadableLinkTranslation getModel()
     * @method ProductDownloadableLinkTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] hydrate(array $items)
     * @method ProductDownloadableLinkTranslation make(array $attributes = [])
     * @method ProductDownloadableLinkTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductDownloadableLinkTranslation[]|_IH_ProductDownloadableLinkTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductDownloadableLinkTranslation[]|_IH_ProductDownloadableLinkTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductDownloadableLinkTranslation sole(array|string $columns = ['*'])
     * @method ProductDownloadableLinkTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductDownloadableLinkTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method ProductDownloadableLink shift()
     * @method ProductDownloadableLink pop()
     * @method ProductDownloadableLink get($key, $default = null)
     * @method ProductDownloadableLink pull($key, $default = null)
     * @method ProductDownloadableLink first(callable $callback = null, $default = null)
     * @method ProductDownloadableLink firstWhere(string $key, $operator = null, $value = null)
     * @method ProductDownloadableLink find($key, $default = null)
     * @method ProductDownloadableLink[] all()
     * @method ProductDownloadableLink last(callable $callback = null, $default = null)
     * @method ProductDownloadableLink sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductDownloadableLink_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductDownloadableLink[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductDownloadableLink_QB whereId($value)
     * @method _IH_ProductDownloadableLink_QB whereUrl($value)
     * @method _IH_ProductDownloadableLink_QB whereFile($value)
     * @method _IH_ProductDownloadableLink_QB whereFileName($value)
     * @method _IH_ProductDownloadableLink_QB whereType($value)
     * @method _IH_ProductDownloadableLink_QB wherePrice($value)
     * @method _IH_ProductDownloadableLink_QB whereSampleUrl($value)
     * @method _IH_ProductDownloadableLink_QB whereSampleFile($value)
     * @method _IH_ProductDownloadableLink_QB whereSampleFileName($value)
     * @method _IH_ProductDownloadableLink_QB whereSampleType($value)
     * @method _IH_ProductDownloadableLink_QB whereDownloads($value)
     * @method _IH_ProductDownloadableLink_QB whereSortOrder($value)
     * @method _IH_ProductDownloadableLink_QB whereProductId($value)
     * @method _IH_ProductDownloadableLink_QB whereCreatedAt($value)
     * @method _IH_ProductDownloadableLink_QB whereUpdatedAt($value)
     * @method ProductDownloadableLink baseSole(array|string $columns = ['*'])
     * @method ProductDownloadableLink create(array $attributes = [])
     * @method _IH_ProductDownloadableLink_C|ProductDownloadableLink[] cursor()
     * @method ProductDownloadableLink|null|_IH_ProductDownloadableLink_C|ProductDownloadableLink[] find($id, array $columns = ['*'])
     * @method _IH_ProductDownloadableLink_C|ProductDownloadableLink[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductDownloadableLink|_IH_ProductDownloadableLink_C|ProductDownloadableLink[] findOrFail($id, array $columns = ['*'])
     * @method ProductDownloadableLink|_IH_ProductDownloadableLink_C|ProductDownloadableLink[] findOrNew($id, array $columns = ['*'])
     * @method ProductDownloadableLink first(array|string $columns = ['*'])
     * @method ProductDownloadableLink firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductDownloadableLink firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductDownloadableLink firstOrFail(array $columns = ['*'])
     * @method ProductDownloadableLink firstOrNew(array $attributes = [], array $values = [])
     * @method ProductDownloadableLink firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductDownloadableLink forceCreate(array $attributes)
     * @method _IH_ProductDownloadableLink_C|ProductDownloadableLink[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductDownloadableLink_C|ProductDownloadableLink[] get(array|string $columns = ['*'])
     * @method ProductDownloadableLink getModel()
     * @method ProductDownloadableLink[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductDownloadableLink_C|ProductDownloadableLink[] hydrate(array $items)
     * @method ProductDownloadableLink make(array $attributes = [])
     * @method ProductDownloadableLink newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductDownloadableLink[]|_IH_ProductDownloadableLink_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductDownloadableLink[]|_IH_ProductDownloadableLink_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductDownloadableLink sole(array|string $columns = ['*'])
     * @method ProductDownloadableLink updateOrCreate(array $attributes, array $values = [])
     * @method _IH_ProductDownloadableLink_QB listsTranslations(string $translationField)
     * @method _IH_ProductDownloadableLink_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_ProductDownloadableLink_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductDownloadableLink_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductDownloadableLink_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_ProductDownloadableLink_QB translated()
     * @method _IH_ProductDownloadableLink_QB translatedIn(null|string $locale = null)
     * @method _IH_ProductDownloadableLink_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_ProductDownloadableLink_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductDownloadableLink_QB withTranslation()
     */
    class _IH_ProductDownloadableLink_QB extends _BaseBuilder {}
    
    /**
     * @method ProductDownloadableSampleTranslation shift()
     * @method ProductDownloadableSampleTranslation pop()
     * @method ProductDownloadableSampleTranslation get($key, $default = null)
     * @method ProductDownloadableSampleTranslation pull($key, $default = null)
     * @method ProductDownloadableSampleTranslation first(callable $callback = null, $default = null)
     * @method ProductDownloadableSampleTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method ProductDownloadableSampleTranslation find($key, $default = null)
     * @method ProductDownloadableSampleTranslation[] all()
     * @method ProductDownloadableSampleTranslation last(callable $callback = null, $default = null)
     * @method ProductDownloadableSampleTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductDownloadableSampleTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductDownloadableSampleTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductDownloadableSampleTranslation_QB whereId($value)
     * @method _IH_ProductDownloadableSampleTranslation_QB whereLocale($value)
     * @method _IH_ProductDownloadableSampleTranslation_QB whereTitle($value)
     * @method _IH_ProductDownloadableSampleTranslation_QB whereProductDownloadableSampleId($value)
     * @method ProductDownloadableSampleTranslation baseSole(array|string $columns = ['*'])
     * @method ProductDownloadableSampleTranslation create(array $attributes = [])
     * @method _IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] cursor()
     * @method ProductDownloadableSampleTranslation|null|_IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] find($id, array $columns = ['*'])
     * @method _IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductDownloadableSampleTranslation|_IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] findOrFail($id, array $columns = ['*'])
     * @method ProductDownloadableSampleTranslation|_IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] findOrNew($id, array $columns = ['*'])
     * @method ProductDownloadableSampleTranslation first(array|string $columns = ['*'])
     * @method ProductDownloadableSampleTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductDownloadableSampleTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductDownloadableSampleTranslation firstOrFail(array $columns = ['*'])
     * @method ProductDownloadableSampleTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method ProductDownloadableSampleTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductDownloadableSampleTranslation forceCreate(array $attributes)
     * @method _IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] get(array|string $columns = ['*'])
     * @method ProductDownloadableSampleTranslation getModel()
     * @method ProductDownloadableSampleTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] hydrate(array $items)
     * @method ProductDownloadableSampleTranslation make(array $attributes = [])
     * @method ProductDownloadableSampleTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductDownloadableSampleTranslation[]|_IH_ProductDownloadableSampleTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductDownloadableSampleTranslation[]|_IH_ProductDownloadableSampleTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductDownloadableSampleTranslation sole(array|string $columns = ['*'])
     * @method ProductDownloadableSampleTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductDownloadableSampleTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method ProductDownloadableSample shift()
     * @method ProductDownloadableSample pop()
     * @method ProductDownloadableSample get($key, $default = null)
     * @method ProductDownloadableSample pull($key, $default = null)
     * @method ProductDownloadableSample first(callable $callback = null, $default = null)
     * @method ProductDownloadableSample firstWhere(string $key, $operator = null, $value = null)
     * @method ProductDownloadableSample find($key, $default = null)
     * @method ProductDownloadableSample[] all()
     * @method ProductDownloadableSample last(callable $callback = null, $default = null)
     * @method ProductDownloadableSample sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductDownloadableSample_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductDownloadableSample[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductDownloadableSample_QB whereId($value)
     * @method _IH_ProductDownloadableSample_QB whereUrl($value)
     * @method _IH_ProductDownloadableSample_QB whereFile($value)
     * @method _IH_ProductDownloadableSample_QB whereFileName($value)
     * @method _IH_ProductDownloadableSample_QB whereType($value)
     * @method _IH_ProductDownloadableSample_QB whereSortOrder($value)
     * @method _IH_ProductDownloadableSample_QB whereProductId($value)
     * @method _IH_ProductDownloadableSample_QB whereCreatedAt($value)
     * @method _IH_ProductDownloadableSample_QB whereUpdatedAt($value)
     * @method ProductDownloadableSample baseSole(array|string $columns = ['*'])
     * @method ProductDownloadableSample create(array $attributes = [])
     * @method _IH_ProductDownloadableSample_C|ProductDownloadableSample[] cursor()
     * @method ProductDownloadableSample|null|_IH_ProductDownloadableSample_C|ProductDownloadableSample[] find($id, array $columns = ['*'])
     * @method _IH_ProductDownloadableSample_C|ProductDownloadableSample[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductDownloadableSample|_IH_ProductDownloadableSample_C|ProductDownloadableSample[] findOrFail($id, array $columns = ['*'])
     * @method ProductDownloadableSample|_IH_ProductDownloadableSample_C|ProductDownloadableSample[] findOrNew($id, array $columns = ['*'])
     * @method ProductDownloadableSample first(array|string $columns = ['*'])
     * @method ProductDownloadableSample firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductDownloadableSample firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductDownloadableSample firstOrFail(array $columns = ['*'])
     * @method ProductDownloadableSample firstOrNew(array $attributes = [], array $values = [])
     * @method ProductDownloadableSample firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductDownloadableSample forceCreate(array $attributes)
     * @method _IH_ProductDownloadableSample_C|ProductDownloadableSample[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductDownloadableSample_C|ProductDownloadableSample[] get(array|string $columns = ['*'])
     * @method ProductDownloadableSample getModel()
     * @method ProductDownloadableSample[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductDownloadableSample_C|ProductDownloadableSample[] hydrate(array $items)
     * @method ProductDownloadableSample make(array $attributes = [])
     * @method ProductDownloadableSample newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductDownloadableSample[]|_IH_ProductDownloadableSample_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductDownloadableSample[]|_IH_ProductDownloadableSample_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductDownloadableSample sole(array|string $columns = ['*'])
     * @method ProductDownloadableSample updateOrCreate(array $attributes, array $values = [])
     * @method _IH_ProductDownloadableSample_QB listsTranslations(string $translationField)
     * @method _IH_ProductDownloadableSample_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_ProductDownloadableSample_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductDownloadableSample_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductDownloadableSample_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_ProductDownloadableSample_QB translated()
     * @method _IH_ProductDownloadableSample_QB translatedIn(null|string $locale = null)
     * @method _IH_ProductDownloadableSample_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_ProductDownloadableSample_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_ProductDownloadableSample_QB withTranslation()
     */
    class _IH_ProductDownloadableSample_QB extends _BaseBuilder {}
    
    /**
     * @method ProductFlat shift()
     * @method ProductFlat pop()
     * @method ProductFlat get($key, $default = null)
     * @method ProductFlat pull($key, $default = null)
     * @method ProductFlat first(callable $callback = null, $default = null)
     * @method ProductFlat firstWhere(string $key, $operator = null, $value = null)
     * @method ProductFlat find($key, $default = null)
     * @method ProductFlat[] all()
     * @method ProductFlat last(callable $callback = null, $default = null)
     * @method ProductFlat sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductFlat_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductFlat[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductFlat_QB whereId($value)
     * @method _IH_ProductFlat_QB whereSku($value)
     * @method _IH_ProductFlat_QB whereProductNumber($value)
     * @method _IH_ProductFlat_QB whereName($value)
     * @method _IH_ProductFlat_QB whereDescription($value)
     * @method _IH_ProductFlat_QB whereUrlKey($value)
     * @method _IH_ProductFlat_QB whereNew($value)
     * @method _IH_ProductFlat_QB whereFeatured($value)
     * @method _IH_ProductFlat_QB whereStatus($value)
     * @method _IH_ProductFlat_QB whereThumbnail($value)
     * @method _IH_ProductFlat_QB wherePrice($value)
     * @method _IH_ProductFlat_QB whereCost($value)
     * @method _IH_ProductFlat_QB whereSpecialPrice($value)
     * @method _IH_ProductFlat_QB whereSpecialPriceFrom($value)
     * @method _IH_ProductFlat_QB whereSpecialPriceTo($value)
     * @method _IH_ProductFlat_QB whereWeight($value)
     * @method _IH_ProductFlat_QB whereColor($value)
     * @method _IH_ProductFlat_QB whereColorLabel($value)
     * @method _IH_ProductFlat_QB whereSize($value)
     * @method _IH_ProductFlat_QB whereSizeLabel($value)
     * @method _IH_ProductFlat_QB whereCreatedAt($value)
     * @method _IH_ProductFlat_QB whereLocale($value)
     * @method _IH_ProductFlat_QB whereChannel($value)
     * @method _IH_ProductFlat_QB whereProductId($value)
     * @method _IH_ProductFlat_QB whereUpdatedAt($value)
     * @method _IH_ProductFlat_QB whereParentId($value)
     * @method _IH_ProductFlat_QB whereVisibleIndividually($value)
     * @method _IH_ProductFlat_QB whereMinPrice($value)
     * @method _IH_ProductFlat_QB whereMaxPrice($value)
     * @method _IH_ProductFlat_QB whereShortDescription($value)
     * @method _IH_ProductFlat_QB whereMetaTitle($value)
     * @method _IH_ProductFlat_QB whereMetaKeywords($value)
     * @method _IH_ProductFlat_QB whereMetaDescription($value)
     * @method _IH_ProductFlat_QB whereWidth($value)
     * @method _IH_ProductFlat_QB whereHeight($value)
     * @method _IH_ProductFlat_QB whereDepth($value)
     * @method ProductFlat baseSole(array|string $columns = ['*'])
     * @method ProductFlat create(array $attributes = [])
     * @method _IH_ProductFlat_C|ProductFlat[] cursor()
     * @method ProductFlat|null|_IH_ProductFlat_C|ProductFlat[] find($id, array $columns = ['*'])
     * @method _IH_ProductFlat_C|ProductFlat[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductFlat|_IH_ProductFlat_C|ProductFlat[] findOrFail($id, array $columns = ['*'])
     * @method ProductFlat|_IH_ProductFlat_C|ProductFlat[] findOrNew($id, array $columns = ['*'])
     * @method ProductFlat first(array|string $columns = ['*'])
     * @method ProductFlat firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductFlat firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductFlat firstOrFail(array $columns = ['*'])
     * @method ProductFlat firstOrNew(array $attributes = [], array $values = [])
     * @method ProductFlat firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductFlat forceCreate(array $attributes)
     * @method _IH_ProductFlat_C|ProductFlat[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductFlat_C|ProductFlat[] get(array|string $columns = ['*'])
     * @method ProductFlat getModel()
     * @method ProductFlat[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductFlat_C|ProductFlat[] hydrate(array $items)
     * @method ProductFlat make(array $attributes = [])
     * @method ProductFlat newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductFlat[]|_IH_ProductFlat_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductFlat[]|_IH_ProductFlat_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductFlat sole(array|string $columns = ['*'])
     * @method ProductFlat updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductFlat_QB extends _BaseBuilder {}
    
    /**
     * @method ProductGroupedProduct shift()
     * @method ProductGroupedProduct pop()
     * @method ProductGroupedProduct get($key, $default = null)
     * @method ProductGroupedProduct pull($key, $default = null)
     * @method ProductGroupedProduct first(callable $callback = null, $default = null)
     * @method ProductGroupedProduct firstWhere(string $key, $operator = null, $value = null)
     * @method ProductGroupedProduct find($key, $default = null)
     * @method ProductGroupedProduct[] all()
     * @method ProductGroupedProduct last(callable $callback = null, $default = null)
     * @method ProductGroupedProduct sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductGroupedProduct_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductGroupedProduct[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductGroupedProduct_QB whereId($value)
     * @method _IH_ProductGroupedProduct_QB whereQty($value)
     * @method _IH_ProductGroupedProduct_QB whereSortOrder($value)
     * @method _IH_ProductGroupedProduct_QB whereProductId($value)
     * @method _IH_ProductGroupedProduct_QB whereAssociatedProductId($value)
     * @method ProductGroupedProduct baseSole(array|string $columns = ['*'])
     * @method ProductGroupedProduct create(array $attributes = [])
     * @method _IH_ProductGroupedProduct_C|ProductGroupedProduct[] cursor()
     * @method ProductGroupedProduct|null|_IH_ProductGroupedProduct_C|ProductGroupedProduct[] find($id, array $columns = ['*'])
     * @method _IH_ProductGroupedProduct_C|ProductGroupedProduct[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductGroupedProduct|_IH_ProductGroupedProduct_C|ProductGroupedProduct[] findOrFail($id, array $columns = ['*'])
     * @method ProductGroupedProduct|_IH_ProductGroupedProduct_C|ProductGroupedProduct[] findOrNew($id, array $columns = ['*'])
     * @method ProductGroupedProduct first(array|string $columns = ['*'])
     * @method ProductGroupedProduct firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductGroupedProduct firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductGroupedProduct firstOrFail(array $columns = ['*'])
     * @method ProductGroupedProduct firstOrNew(array $attributes = [], array $values = [])
     * @method ProductGroupedProduct firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductGroupedProduct forceCreate(array $attributes)
     * @method _IH_ProductGroupedProduct_C|ProductGroupedProduct[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductGroupedProduct_C|ProductGroupedProduct[] get(array|string $columns = ['*'])
     * @method ProductGroupedProduct getModel()
     * @method ProductGroupedProduct[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductGroupedProduct_C|ProductGroupedProduct[] hydrate(array $items)
     * @method ProductGroupedProduct make(array $attributes = [])
     * @method ProductGroupedProduct newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductGroupedProduct[]|_IH_ProductGroupedProduct_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductGroupedProduct[]|_IH_ProductGroupedProduct_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductGroupedProduct sole(array|string $columns = ['*'])
     * @method ProductGroupedProduct updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductGroupedProduct_QB extends _BaseBuilder {}
    
    /**
     * @method ProductImage shift()
     * @method ProductImage pop()
     * @method ProductImage get($key, $default = null)
     * @method ProductImage pull($key, $default = null)
     * @method ProductImage first(callable $callback = null, $default = null)
     * @method ProductImage firstWhere(string $key, $operator = null, $value = null)
     * @method ProductImage find($key, $default = null)
     * @method ProductImage[] all()
     * @method ProductImage last(callable $callback = null, $default = null)
     * @method ProductImage sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductImage_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductImage[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductImage_QB whereId($value)
     * @method _IH_ProductImage_QB whereType($value)
     * @method _IH_ProductImage_QB wherePath($value)
     * @method _IH_ProductImage_QB whereProductId($value)
     * @method ProductImage baseSole(array|string $columns = ['*'])
     * @method ProductImage create(array $attributes = [])
     * @method _IH_ProductImage_C|ProductImage[] cursor()
     * @method ProductImage|null|_IH_ProductImage_C|ProductImage[] find($id, array $columns = ['*'])
     * @method _IH_ProductImage_C|ProductImage[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductImage|_IH_ProductImage_C|ProductImage[] findOrFail($id, array $columns = ['*'])
     * @method ProductImage|_IH_ProductImage_C|ProductImage[] findOrNew($id, array $columns = ['*'])
     * @method ProductImage first(array|string $columns = ['*'])
     * @method ProductImage firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductImage firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductImage firstOrFail(array $columns = ['*'])
     * @method ProductImage firstOrNew(array $attributes = [], array $values = [])
     * @method ProductImage firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductImage forceCreate(array $attributes)
     * @method _IH_ProductImage_C|ProductImage[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductImage_C|ProductImage[] get(array|string $columns = ['*'])
     * @method ProductImage getModel()
     * @method ProductImage[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductImage_C|ProductImage[] hydrate(array $items)
     * @method ProductImage make(array $attributes = [])
     * @method ProductImage newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductImage[]|_IH_ProductImage_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductImage[]|_IH_ProductImage_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductImage sole(array|string $columns = ['*'])
     * @method ProductImage updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductImage_QB extends _BaseBuilder {}
    
    /**
     * @method ProductInventory shift()
     * @method ProductInventory pop()
     * @method ProductInventory get($key, $default = null)
     * @method ProductInventory pull($key, $default = null)
     * @method ProductInventory first(callable $callback = null, $default = null)
     * @method ProductInventory firstWhere(string $key, $operator = null, $value = null)
     * @method ProductInventory find($key, $default = null)
     * @method ProductInventory[] all()
     * @method ProductInventory last(callable $callback = null, $default = null)
     * @method ProductInventory sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductInventory_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductInventory[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductInventory_QB whereId($value)
     * @method _IH_ProductInventory_QB whereQty($value)
     * @method _IH_ProductInventory_QB whereProductId($value)
     * @method _IH_ProductInventory_QB whereInventorySourceId($value)
     * @method _IH_ProductInventory_QB whereVendorId($value)
     * @method ProductInventory baseSole(array|string $columns = ['*'])
     * @method ProductInventory create(array $attributes = [])
     * @method _IH_ProductInventory_C|ProductInventory[] cursor()
     * @method ProductInventory|null|_IH_ProductInventory_C|ProductInventory[] find($id, array $columns = ['*'])
     * @method _IH_ProductInventory_C|ProductInventory[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductInventory|_IH_ProductInventory_C|ProductInventory[] findOrFail($id, array $columns = ['*'])
     * @method ProductInventory|_IH_ProductInventory_C|ProductInventory[] findOrNew($id, array $columns = ['*'])
     * @method ProductInventory first(array|string $columns = ['*'])
     * @method ProductInventory firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductInventory firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductInventory firstOrFail(array $columns = ['*'])
     * @method ProductInventory firstOrNew(array $attributes = [], array $values = [])
     * @method ProductInventory firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductInventory forceCreate(array $attributes)
     * @method _IH_ProductInventory_C|ProductInventory[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductInventory_C|ProductInventory[] get(array|string $columns = ['*'])
     * @method ProductInventory getModel()
     * @method ProductInventory[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductInventory_C|ProductInventory[] hydrate(array $items)
     * @method ProductInventory make(array $attributes = [])
     * @method ProductInventory newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductInventory[]|_IH_ProductInventory_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductInventory[]|_IH_ProductInventory_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductInventory sole(array|string $columns = ['*'])
     * @method ProductInventory updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductInventory_QB extends _BaseBuilder {}
    
    /**
     * @method ProductOrderedInventory shift()
     * @method ProductOrderedInventory pop()
     * @method ProductOrderedInventory get($key, $default = null)
     * @method ProductOrderedInventory pull($key, $default = null)
     * @method ProductOrderedInventory first(callable $callback = null, $default = null)
     * @method ProductOrderedInventory firstWhere(string $key, $operator = null, $value = null)
     * @method ProductOrderedInventory find($key, $default = null)
     * @method ProductOrderedInventory[] all()
     * @method ProductOrderedInventory last(callable $callback = null, $default = null)
     * @method ProductOrderedInventory sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductOrderedInventory_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductOrderedInventory[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductOrderedInventory_QB whereId($value)
     * @method _IH_ProductOrderedInventory_QB whereQty($value)
     * @method _IH_ProductOrderedInventory_QB whereProductId($value)
     * @method _IH_ProductOrderedInventory_QB whereChannelId($value)
     * @method ProductOrderedInventory baseSole(array|string $columns = ['*'])
     * @method ProductOrderedInventory create(array $attributes = [])
     * @method _IH_ProductOrderedInventory_C|ProductOrderedInventory[] cursor()
     * @method ProductOrderedInventory|null|_IH_ProductOrderedInventory_C|ProductOrderedInventory[] find($id, array $columns = ['*'])
     * @method _IH_ProductOrderedInventory_C|ProductOrderedInventory[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductOrderedInventory|_IH_ProductOrderedInventory_C|ProductOrderedInventory[] findOrFail($id, array $columns = ['*'])
     * @method ProductOrderedInventory|_IH_ProductOrderedInventory_C|ProductOrderedInventory[] findOrNew($id, array $columns = ['*'])
     * @method ProductOrderedInventory first(array|string $columns = ['*'])
     * @method ProductOrderedInventory firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductOrderedInventory firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductOrderedInventory firstOrFail(array $columns = ['*'])
     * @method ProductOrderedInventory firstOrNew(array $attributes = [], array $values = [])
     * @method ProductOrderedInventory firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductOrderedInventory forceCreate(array $attributes)
     * @method _IH_ProductOrderedInventory_C|ProductOrderedInventory[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductOrderedInventory_C|ProductOrderedInventory[] get(array|string $columns = ['*'])
     * @method ProductOrderedInventory getModel()
     * @method ProductOrderedInventory[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductOrderedInventory_C|ProductOrderedInventory[] hydrate(array $items)
     * @method ProductOrderedInventory make(array $attributes = [])
     * @method ProductOrderedInventory newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductOrderedInventory[]|_IH_ProductOrderedInventory_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductOrderedInventory[]|_IH_ProductOrderedInventory_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductOrderedInventory sole(array|string $columns = ['*'])
     * @method ProductOrderedInventory updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductOrderedInventory_QB extends _BaseBuilder {}
    
    /**
     * @method ProductReviewImage shift()
     * @method ProductReviewImage pop()
     * @method ProductReviewImage get($key, $default = null)
     * @method ProductReviewImage pull($key, $default = null)
     * @method ProductReviewImage first(callable $callback = null, $default = null)
     * @method ProductReviewImage firstWhere(string $key, $operator = null, $value = null)
     * @method ProductReviewImage find($key, $default = null)
     * @method ProductReviewImage[] all()
     * @method ProductReviewImage last(callable $callback = null, $default = null)
     * @method ProductReviewImage sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductReviewImage_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductReviewImage[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductReviewImage_QB whereId($value)
     * @method _IH_ProductReviewImage_QB whereType($value)
     * @method _IH_ProductReviewImage_QB wherePath($value)
     * @method _IH_ProductReviewImage_QB whereReviewId($value)
     * @method ProductReviewImage baseSole(array|string $columns = ['*'])
     * @method ProductReviewImage create(array $attributes = [])
     * @method _IH_ProductReviewImage_C|ProductReviewImage[] cursor()
     * @method ProductReviewImage|null|_IH_ProductReviewImage_C|ProductReviewImage[] find($id, array $columns = ['*'])
     * @method _IH_ProductReviewImage_C|ProductReviewImage[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductReviewImage|_IH_ProductReviewImage_C|ProductReviewImage[] findOrFail($id, array $columns = ['*'])
     * @method ProductReviewImage|_IH_ProductReviewImage_C|ProductReviewImage[] findOrNew($id, array $columns = ['*'])
     * @method ProductReviewImage first(array|string $columns = ['*'])
     * @method ProductReviewImage firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductReviewImage firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductReviewImage firstOrFail(array $columns = ['*'])
     * @method ProductReviewImage firstOrNew(array $attributes = [], array $values = [])
     * @method ProductReviewImage firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductReviewImage forceCreate(array $attributes)
     * @method _IH_ProductReviewImage_C|ProductReviewImage[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductReviewImage_C|ProductReviewImage[] get(array|string $columns = ['*'])
     * @method ProductReviewImage getModel()
     * @method ProductReviewImage[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductReviewImage_C|ProductReviewImage[] hydrate(array $items)
     * @method ProductReviewImage make(array $attributes = [])
     * @method ProductReviewImage newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductReviewImage[]|_IH_ProductReviewImage_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductReviewImage[]|_IH_ProductReviewImage_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductReviewImage sole(array|string $columns = ['*'])
     * @method ProductReviewImage updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductReviewImage_QB extends _BaseBuilder {}
    
    /**
     * @method ProductReview shift()
     * @method ProductReview pop()
     * @method ProductReview get($key, $default = null)
     * @method ProductReview pull($key, $default = null)
     * @method ProductReview first(callable $callback = null, $default = null)
     * @method ProductReview firstWhere(string $key, $operator = null, $value = null)
     * @method ProductReview find($key, $default = null)
     * @method ProductReview[] all()
     * @method ProductReview last(callable $callback = null, $default = null)
     * @method ProductReview sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductReview_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductReview[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductReview_QB whereId($value)
     * @method _IH_ProductReview_QB whereTitle($value)
     * @method _IH_ProductReview_QB whereRating($value)
     * @method _IH_ProductReview_QB whereComment($value)
     * @method _IH_ProductReview_QB whereStatus($value)
     * @method _IH_ProductReview_QB whereCreatedAt($value)
     * @method _IH_ProductReview_QB whereUpdatedAt($value)
     * @method _IH_ProductReview_QB whereProductId($value)
     * @method _IH_ProductReview_QB whereCustomerId($value)
     * @method _IH_ProductReview_QB whereName($value)
     * @method ProductReview baseSole(array|string $columns = ['*'])
     * @method ProductReview create(array $attributes = [])
     * @method _IH_ProductReview_C|ProductReview[] cursor()
     * @method ProductReview|null|_IH_ProductReview_C|ProductReview[] find($id, array $columns = ['*'])
     * @method _IH_ProductReview_C|ProductReview[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductReview|_IH_ProductReview_C|ProductReview[] findOrFail($id, array $columns = ['*'])
     * @method ProductReview|_IH_ProductReview_C|ProductReview[] findOrNew($id, array $columns = ['*'])
     * @method ProductReview first(array|string $columns = ['*'])
     * @method ProductReview firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductReview firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductReview firstOrFail(array $columns = ['*'])
     * @method ProductReview firstOrNew(array $attributes = [], array $values = [])
     * @method ProductReview firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductReview forceCreate(array $attributes)
     * @method _IH_ProductReview_C|ProductReview[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductReview_C|ProductReview[] get(array|string $columns = ['*'])
     * @method ProductReview getModel()
     * @method ProductReview[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductReview_C|ProductReview[] hydrate(array $items)
     * @method ProductReview make(array $attributes = [])
     * @method ProductReview newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductReview[]|_IH_ProductReview_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductReview[]|_IH_ProductReview_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductReview sole(array|string $columns = ['*'])
     * @method ProductReview updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductReview_QB extends _BaseBuilder {}
    
    /**
     * @method ProductSalableInventory shift()
     * @method ProductSalableInventory pop()
     * @method ProductSalableInventory get($key, $default = null)
     * @method ProductSalableInventory pull($key, $default = null)
     * @method ProductSalableInventory first(callable $callback = null, $default = null)
     * @method ProductSalableInventory firstWhere(string $key, $operator = null, $value = null)
     * @method ProductSalableInventory find($key, $default = null)
     * @method ProductSalableInventory[] all()
     * @method ProductSalableInventory last(callable $callback = null, $default = null)
     * @method ProductSalableInventory sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductSalableInventory_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductSalableInventory[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method ProductSalableInventory baseSole(array|string $columns = ['*'])
     * @method ProductSalableInventory create(array $attributes = [])
     * @method _IH_ProductSalableInventory_C|ProductSalableInventory[] cursor()
     * @method ProductSalableInventory|null|_IH_ProductSalableInventory_C|ProductSalableInventory[] find($id, array $columns = ['*'])
     * @method _IH_ProductSalableInventory_C|ProductSalableInventory[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductSalableInventory|_IH_ProductSalableInventory_C|ProductSalableInventory[] findOrFail($id, array $columns = ['*'])
     * @method ProductSalableInventory|_IH_ProductSalableInventory_C|ProductSalableInventory[] findOrNew($id, array $columns = ['*'])
     * @method ProductSalableInventory first(array|string $columns = ['*'])
     * @method ProductSalableInventory firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductSalableInventory firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductSalableInventory firstOrFail(array $columns = ['*'])
     * @method ProductSalableInventory firstOrNew(array $attributes = [], array $values = [])
     * @method ProductSalableInventory firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductSalableInventory forceCreate(array $attributes)
     * @method _IH_ProductSalableInventory_C|ProductSalableInventory[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductSalableInventory_C|ProductSalableInventory[] get(array|string $columns = ['*'])
     * @method ProductSalableInventory getModel()
     * @method ProductSalableInventory[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductSalableInventory_C|ProductSalableInventory[] hydrate(array $items)
     * @method ProductSalableInventory make(array $attributes = [])
     * @method ProductSalableInventory newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductSalableInventory[]|_IH_ProductSalableInventory_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductSalableInventory[]|_IH_ProductSalableInventory_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductSalableInventory sole(array|string $columns = ['*'])
     * @method ProductSalableInventory updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductSalableInventory_QB extends _BaseBuilder {}
    
    /**
     * @method ProductVideo shift()
     * @method ProductVideo pop()
     * @method ProductVideo get($key, $default = null)
     * @method ProductVideo pull($key, $default = null)
     * @method ProductVideo first(callable $callback = null, $default = null)
     * @method ProductVideo firstWhere(string $key, $operator = null, $value = null)
     * @method ProductVideo find($key, $default = null)
     * @method ProductVideo[] all()
     * @method ProductVideo last(callable $callback = null, $default = null)
     * @method ProductVideo sole($key = null, $operator = null, $value = null)
     */
    class _IH_ProductVideo_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ProductVideo[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ProductVideo_QB whereId($value)
     * @method _IH_ProductVideo_QB whereType($value)
     * @method _IH_ProductVideo_QB wherePath($value)
     * @method _IH_ProductVideo_QB whereProductId($value)
     * @method ProductVideo baseSole(array|string $columns = ['*'])
     * @method ProductVideo create(array $attributes = [])
     * @method _IH_ProductVideo_C|ProductVideo[] cursor()
     * @method ProductVideo|null|_IH_ProductVideo_C|ProductVideo[] find($id, array $columns = ['*'])
     * @method _IH_ProductVideo_C|ProductVideo[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ProductVideo|_IH_ProductVideo_C|ProductVideo[] findOrFail($id, array $columns = ['*'])
     * @method ProductVideo|_IH_ProductVideo_C|ProductVideo[] findOrNew($id, array $columns = ['*'])
     * @method ProductVideo first(array|string $columns = ['*'])
     * @method ProductVideo firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ProductVideo firstOrCreate(array $attributes = [], array $values = [])
     * @method ProductVideo firstOrFail(array $columns = ['*'])
     * @method ProductVideo firstOrNew(array $attributes = [], array $values = [])
     * @method ProductVideo firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ProductVideo forceCreate(array $attributes)
     * @method _IH_ProductVideo_C|ProductVideo[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ProductVideo_C|ProductVideo[] get(array|string $columns = ['*'])
     * @method ProductVideo getModel()
     * @method ProductVideo[] getModels(array|string $columns = ['*'])
     * @method _IH_ProductVideo_C|ProductVideo[] hydrate(array $items)
     * @method ProductVideo make(array $attributes = [])
     * @method ProductVideo newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ProductVideo[]|_IH_ProductVideo_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ProductVideo[]|_IH_ProductVideo_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ProductVideo sole(array|string $columns = ['*'])
     * @method ProductVideo updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ProductVideo_QB extends _BaseBuilder {}
    
    /**
     * @method Product shift()
     * @method Product pop()
     * @method Product get($key, $default = null)
     * @method Product pull($key, $default = null)
     * @method Product first(callable $callback = null, $default = null)
     * @method Product firstWhere(string $key, $operator = null, $value = null)
     * @method Product find($key, $default = null)
     * @method Product[] all()
     * @method Product last(callable $callback = null, $default = null)
     * @method Product sole($key = null, $operator = null, $value = null)
     */
    class _IH_Product_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Product[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Product_QB whereId($value)
     * @method _IH_Product_QB whereSku($value)
     * @method _IH_Product_QB whereType($value)
     * @method _IH_Product_QB whereCreatedAt($value)
     * @method _IH_Product_QB whereUpdatedAt($value)
     * @method _IH_Product_QB whereParentId($value)
     * @method _IH_Product_QB whereAttributeFamilyId($value)
     * @method _IH_Product_QB whereAdditional($value)
     * @method Product baseSole(array|string $columns = ['*'])
     * @method Product create(array $attributes = [])
     * @method _IH_Product_C|Product[] cursor()
     * @method Product|null|_IH_Product_C|Product[] find($id, array $columns = ['*'])
     * @method _IH_Product_C|Product[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Product|_IH_Product_C|Product[] findOrFail($id, array $columns = ['*'])
     * @method Product|_IH_Product_C|Product[] findOrNew($id, array $columns = ['*'])
     * @method Product first(array|string $columns = ['*'])
     * @method Product firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Product firstOrCreate(array $attributes = [], array $values = [])
     * @method Product firstOrFail(array $columns = ['*'])
     * @method Product firstOrNew(array $attributes = [], array $values = [])
     * @method Product firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Product forceCreate(array $attributes)
     * @method _IH_Product_C|Product[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Product_C|Product[] get(array|string $columns = ['*'])
     * @method Product getModel()
     * @method Product[] getModels(array|string $columns = ['*'])
     * @method _IH_Product_C|Product[] hydrate(array $items)
     * @method Product make(array $attributes = [])
     * @method Product newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Product[]|_IH_Product_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Product[]|_IH_Product_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Product sole(array|string $columns = ['*'])
     * @method Product updateOrCreate(array $attributes, array $values = [])
     * @mixin Builder
     */
    class _IH_Product_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Sales\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Sales\Models\DownloadableLinkPurchased;
    use Webkul\Sales\Models\Invoice;
    use Webkul\Sales\Models\InvoiceItem;
    use Webkul\Sales\Models\Order;
    use Webkul\Sales\Models\OrderAddress;
    use Webkul\Sales\Models\OrderComment;
    use Webkul\Sales\Models\OrderItem;
    use Webkul\Sales\Models\OrderPayment;
    use Webkul\Sales\Models\OrderTransaction;
    use Webkul\Sales\Models\Refund;
    use Webkul\Sales\Models\RefundItem;
    use Webkul\Sales\Models\Shipment;
    use Webkul\Sales\Models\ShipmentItem;
    
    /**
     * @method DownloadableLinkPurchased shift()
     * @method DownloadableLinkPurchased pop()
     * @method DownloadableLinkPurchased get($key, $default = null)
     * @method DownloadableLinkPurchased pull($key, $default = null)
     * @method DownloadableLinkPurchased first(callable $callback = null, $default = null)
     * @method DownloadableLinkPurchased firstWhere(string $key, $operator = null, $value = null)
     * @method DownloadableLinkPurchased find($key, $default = null)
     * @method DownloadableLinkPurchased[] all()
     * @method DownloadableLinkPurchased last(callable $callback = null, $default = null)
     * @method DownloadableLinkPurchased sole($key = null, $operator = null, $value = null)
     */
    class _IH_DownloadableLinkPurchased_C extends _BaseCollection {
        /**
         * @param int $size
         * @return DownloadableLinkPurchased[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_DownloadableLinkPurchased_QB whereId($value)
     * @method _IH_DownloadableLinkPurchased_QB whereProductName($value)
     * @method _IH_DownloadableLinkPurchased_QB whereName($value)
     * @method _IH_DownloadableLinkPurchased_QB whereUrl($value)
     * @method _IH_DownloadableLinkPurchased_QB whereFile($value)
     * @method _IH_DownloadableLinkPurchased_QB whereFileName($value)
     * @method _IH_DownloadableLinkPurchased_QB whereType($value)
     * @method _IH_DownloadableLinkPurchased_QB whereDownloadBought($value)
     * @method _IH_DownloadableLinkPurchased_QB whereDownloadUsed($value)
     * @method _IH_DownloadableLinkPurchased_QB whereStatus($value)
     * @method _IH_DownloadableLinkPurchased_QB whereCustomerId($value)
     * @method _IH_DownloadableLinkPurchased_QB whereOrderId($value)
     * @method _IH_DownloadableLinkPurchased_QB whereOrderItemId($value)
     * @method _IH_DownloadableLinkPurchased_QB whereCreatedAt($value)
     * @method _IH_DownloadableLinkPurchased_QB whereUpdatedAt($value)
     * @method _IH_DownloadableLinkPurchased_QB whereDownloadCanceled($value)
     * @method DownloadableLinkPurchased baseSole(array|string $columns = ['*'])
     * @method DownloadableLinkPurchased create(array $attributes = [])
     * @method _IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] cursor()
     * @method DownloadableLinkPurchased|null|_IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] find($id, array $columns = ['*'])
     * @method _IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method DownloadableLinkPurchased|_IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] findOrFail($id, array $columns = ['*'])
     * @method DownloadableLinkPurchased|_IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] findOrNew($id, array $columns = ['*'])
     * @method DownloadableLinkPurchased first(array|string $columns = ['*'])
     * @method DownloadableLinkPurchased firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method DownloadableLinkPurchased firstOrCreate(array $attributes = [], array $values = [])
     * @method DownloadableLinkPurchased firstOrFail(array $columns = ['*'])
     * @method DownloadableLinkPurchased firstOrNew(array $attributes = [], array $values = [])
     * @method DownloadableLinkPurchased firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method DownloadableLinkPurchased forceCreate(array $attributes)
     * @method _IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] fromQuery(string $query, array $bindings = [])
     * @method _IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] get(array|string $columns = ['*'])
     * @method DownloadableLinkPurchased getModel()
     * @method DownloadableLinkPurchased[] getModels(array|string $columns = ['*'])
     * @method _IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] hydrate(array $items)
     * @method DownloadableLinkPurchased make(array $attributes = [])
     * @method DownloadableLinkPurchased newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|DownloadableLinkPurchased[]|_IH_DownloadableLinkPurchased_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|DownloadableLinkPurchased[]|_IH_DownloadableLinkPurchased_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method DownloadableLinkPurchased sole(array|string $columns = ['*'])
     * @method DownloadableLinkPurchased updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_DownloadableLinkPurchased_QB extends _BaseBuilder {}
    
    /**
     * @method InvoiceItem shift()
     * @method InvoiceItem pop()
     * @method InvoiceItem get($key, $default = null)
     * @method InvoiceItem pull($key, $default = null)
     * @method InvoiceItem first(callable $callback = null, $default = null)
     * @method InvoiceItem firstWhere(string $key, $operator = null, $value = null)
     * @method InvoiceItem find($key, $default = null)
     * @method InvoiceItem[] all()
     * @method InvoiceItem last(callable $callback = null, $default = null)
     * @method InvoiceItem sole($key = null, $operator = null, $value = null)
     */
    class _IH_InvoiceItem_C extends _BaseCollection {
        /**
         * @param int $size
         * @return InvoiceItem[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_InvoiceItem_QB whereId($value)
     * @method _IH_InvoiceItem_QB whereName($value)
     * @method _IH_InvoiceItem_QB whereDescription($value)
     * @method _IH_InvoiceItem_QB whereSku($value)
     * @method _IH_InvoiceItem_QB whereQty($value)
     * @method _IH_InvoiceItem_QB wherePrice($value)
     * @method _IH_InvoiceItem_QB whereBasePrice($value)
     * @method _IH_InvoiceItem_QB whereTotal($value)
     * @method _IH_InvoiceItem_QB whereBaseTotal($value)
     * @method _IH_InvoiceItem_QB whereTaxAmount($value)
     * @method _IH_InvoiceItem_QB whereBaseTaxAmount($value)
     * @method _IH_InvoiceItem_QB whereProductId($value)
     * @method _IH_InvoiceItem_QB whereProductType($value)
     * @method _IH_InvoiceItem_QB whereOrderItemId($value)
     * @method _IH_InvoiceItem_QB whereInvoiceId($value)
     * @method _IH_InvoiceItem_QB whereParentId($value)
     * @method _IH_InvoiceItem_QB whereAdditional($value)
     * @method _IH_InvoiceItem_QB whereCreatedAt($value)
     * @method _IH_InvoiceItem_QB whereUpdatedAt($value)
     * @method _IH_InvoiceItem_QB whereDiscountPercent($value)
     * @method _IH_InvoiceItem_QB whereDiscountAmount($value)
     * @method _IH_InvoiceItem_QB whereBaseDiscountAmount($value)
     * @method InvoiceItem baseSole(array|string $columns = ['*'])
     * @method InvoiceItem create(array $attributes = [])
     * @method _IH_InvoiceItem_C|InvoiceItem[] cursor()
     * @method InvoiceItem|null|_IH_InvoiceItem_C|InvoiceItem[] find($id, array $columns = ['*'])
     * @method _IH_InvoiceItem_C|InvoiceItem[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method InvoiceItem|_IH_InvoiceItem_C|InvoiceItem[] findOrFail($id, array $columns = ['*'])
     * @method InvoiceItem|_IH_InvoiceItem_C|InvoiceItem[] findOrNew($id, array $columns = ['*'])
     * @method InvoiceItem first(array|string $columns = ['*'])
     * @method InvoiceItem firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method InvoiceItem firstOrCreate(array $attributes = [], array $values = [])
     * @method InvoiceItem firstOrFail(array $columns = ['*'])
     * @method InvoiceItem firstOrNew(array $attributes = [], array $values = [])
     * @method InvoiceItem firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method InvoiceItem forceCreate(array $attributes)
     * @method _IH_InvoiceItem_C|InvoiceItem[] fromQuery(string $query, array $bindings = [])
     * @method _IH_InvoiceItem_C|InvoiceItem[] get(array|string $columns = ['*'])
     * @method InvoiceItem getModel()
     * @method InvoiceItem[] getModels(array|string $columns = ['*'])
     * @method _IH_InvoiceItem_C|InvoiceItem[] hydrate(array $items)
     * @method InvoiceItem make(array $attributes = [])
     * @method InvoiceItem newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|InvoiceItem[]|_IH_InvoiceItem_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|InvoiceItem[]|_IH_InvoiceItem_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method InvoiceItem sole(array|string $columns = ['*'])
     * @method InvoiceItem updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_InvoiceItem_QB extends _BaseBuilder {}
    
    /**
     * @method Invoice shift()
     * @method Invoice pop()
     * @method Invoice get($key, $default = null)
     * @method Invoice pull($key, $default = null)
     * @method Invoice first(callable $callback = null, $default = null)
     * @method Invoice firstWhere(string $key, $operator = null, $value = null)
     * @method Invoice find($key, $default = null)
     * @method Invoice[] all()
     * @method Invoice last(callable $callback = null, $default = null)
     * @method Invoice sole($key = null, $operator = null, $value = null)
     */
    class _IH_Invoice_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Invoice[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Invoice_QB whereId($value)
     * @method _IH_Invoice_QB whereIncrementId($value)
     * @method _IH_Invoice_QB whereState($value)
     * @method _IH_Invoice_QB whereEmailSent($value)
     * @method _IH_Invoice_QB whereTotalQty($value)
     * @method _IH_Invoice_QB whereBaseCurrencyCode($value)
     * @method _IH_Invoice_QB whereChannelCurrencyCode($value)
     * @method _IH_Invoice_QB whereOrderCurrencyCode($value)
     * @method _IH_Invoice_QB whereSubTotal($value)
     * @method _IH_Invoice_QB whereBaseSubTotal($value)
     * @method _IH_Invoice_QB whereGrandTotal($value)
     * @method _IH_Invoice_QB whereBaseGrandTotal($value)
     * @method _IH_Invoice_QB whereShippingAmount($value)
     * @method _IH_Invoice_QB whereBaseShippingAmount($value)
     * @method _IH_Invoice_QB whereTaxAmount($value)
     * @method _IH_Invoice_QB whereBaseTaxAmount($value)
     * @method _IH_Invoice_QB whereDiscountAmount($value)
     * @method _IH_Invoice_QB whereBaseDiscountAmount($value)
     * @method _IH_Invoice_QB whereOrderId($value)
     * @method _IH_Invoice_QB whereOrderAddressId($value)
     * @method _IH_Invoice_QB whereCreatedAt($value)
     * @method _IH_Invoice_QB whereUpdatedAt($value)
     * @method _IH_Invoice_QB whereTransactionId($value)
     * @method _IH_Invoice_QB whereReminders($value)
     * @method _IH_Invoice_QB whereNextReminderAt($value)
     * @method Invoice baseSole(array|string $columns = ['*'])
     * @method Invoice create(array $attributes = [])
     * @method _IH_Invoice_C|Invoice[] cursor()
     * @method Invoice|null|_IH_Invoice_C|Invoice[] find($id, array $columns = ['*'])
     * @method _IH_Invoice_C|Invoice[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Invoice|_IH_Invoice_C|Invoice[] findOrFail($id, array $columns = ['*'])
     * @method Invoice|_IH_Invoice_C|Invoice[] findOrNew($id, array $columns = ['*'])
     * @method Invoice first(array|string $columns = ['*'])
     * @method Invoice firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Invoice firstOrCreate(array $attributes = [], array $values = [])
     * @method Invoice firstOrFail(array $columns = ['*'])
     * @method Invoice firstOrNew(array $attributes = [], array $values = [])
     * @method Invoice firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Invoice forceCreate(array $attributes)
     * @method _IH_Invoice_C|Invoice[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Invoice_C|Invoice[] get(array|string $columns = ['*'])
     * @method Invoice getModel()
     * @method Invoice[] getModels(array|string $columns = ['*'])
     * @method _IH_Invoice_C|Invoice[] hydrate(array $items)
     * @method Invoice make(array $attributes = [])
     * @method Invoice newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Invoice[]|_IH_Invoice_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Invoice[]|_IH_Invoice_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Invoice sole(array|string $columns = ['*'])
     * @method Invoice updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Invoice_QB inOverdueAndRemindersLimit()
     */
    class _IH_Invoice_QB extends _BaseBuilder {}
    
    /**
     * @method OrderAddress shift()
     * @method OrderAddress pop()
     * @method OrderAddress get($key, $default = null)
     * @method OrderAddress pull($key, $default = null)
     * @method OrderAddress first(callable $callback = null, $default = null)
     * @method OrderAddress firstWhere(string $key, $operator = null, $value = null)
     * @method OrderAddress find($key, $default = null)
     * @method OrderAddress[] all()
     * @method OrderAddress last(callable $callback = null, $default = null)
     * @method OrderAddress sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderAddress_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderAddress[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method OrderAddress baseSole(array|string $columns = ['*'])
     * @method OrderAddress create(array $attributes = [])
     * @method _IH_OrderAddress_C|OrderAddress[] cursor()
     * @method OrderAddress|null|_IH_OrderAddress_C|OrderAddress[] find($id, array $columns = ['*'])
     * @method _IH_OrderAddress_C|OrderAddress[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderAddress|_IH_OrderAddress_C|OrderAddress[] findOrFail($id, array $columns = ['*'])
     * @method OrderAddress|_IH_OrderAddress_C|OrderAddress[] findOrNew($id, array $columns = ['*'])
     * @method OrderAddress first(array|string $columns = ['*'])
     * @method OrderAddress firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderAddress firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderAddress firstOrFail(array $columns = ['*'])
     * @method OrderAddress firstOrNew(array $attributes = [], array $values = [])
     * @method OrderAddress firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderAddress forceCreate(array $attributes)
     * @method _IH_OrderAddress_C|OrderAddress[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderAddress_C|OrderAddress[] get(array|string $columns = ['*'])
     * @method OrderAddress getModel()
     * @method OrderAddress[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderAddress_C|OrderAddress[] hydrate(array $items)
     * @method OrderAddress make(array $attributes = [])
     * @method OrderAddress newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderAddress[]|_IH_OrderAddress_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderAddress[]|_IH_OrderAddress_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderAddress sole(array|string $columns = ['*'])
     * @method OrderAddress updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderAddress_QB extends _BaseBuilder {}
    
    /**
     * @method OrderComment shift()
     * @method OrderComment pop()
     * @method OrderComment get($key, $default = null)
     * @method OrderComment pull($key, $default = null)
     * @method OrderComment first(callable $callback = null, $default = null)
     * @method OrderComment firstWhere(string $key, $operator = null, $value = null)
     * @method OrderComment find($key, $default = null)
     * @method OrderComment[] all()
     * @method OrderComment last(callable $callback = null, $default = null)
     * @method OrderComment sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderComment_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderComment[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_OrderComment_QB whereId($value)
     * @method _IH_OrderComment_QB whereComment($value)
     * @method _IH_OrderComment_QB whereCustomerNotified($value)
     * @method _IH_OrderComment_QB whereOrderId($value)
     * @method _IH_OrderComment_QB whereCreatedAt($value)
     * @method _IH_OrderComment_QB whereUpdatedAt($value)
     * @method OrderComment baseSole(array|string $columns = ['*'])
     * @method OrderComment create(array $attributes = [])
     * @method _IH_OrderComment_C|OrderComment[] cursor()
     * @method OrderComment|null|_IH_OrderComment_C|OrderComment[] find($id, array $columns = ['*'])
     * @method _IH_OrderComment_C|OrderComment[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderComment|_IH_OrderComment_C|OrderComment[] findOrFail($id, array $columns = ['*'])
     * @method OrderComment|_IH_OrderComment_C|OrderComment[] findOrNew($id, array $columns = ['*'])
     * @method OrderComment first(array|string $columns = ['*'])
     * @method OrderComment firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderComment firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderComment firstOrFail(array $columns = ['*'])
     * @method OrderComment firstOrNew(array $attributes = [], array $values = [])
     * @method OrderComment firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderComment forceCreate(array $attributes)
     * @method _IH_OrderComment_C|OrderComment[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderComment_C|OrderComment[] get(array|string $columns = ['*'])
     * @method OrderComment getModel()
     * @method OrderComment[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderComment_C|OrderComment[] hydrate(array $items)
     * @method OrderComment make(array $attributes = [])
     * @method OrderComment newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderComment[]|_IH_OrderComment_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderComment[]|_IH_OrderComment_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderComment sole(array|string $columns = ['*'])
     * @method OrderComment updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderComment_QB extends _BaseBuilder {}
    
    /**
     * @method OrderItem shift()
     * @method OrderItem pop()
     * @method OrderItem get($key, $default = null)
     * @method OrderItem pull($key, $default = null)
     * @method OrderItem first(callable $callback = null, $default = null)
     * @method OrderItem firstWhere(string $key, $operator = null, $value = null)
     * @method OrderItem find($key, $default = null)
     * @method OrderItem[] all()
     * @method OrderItem last(callable $callback = null, $default = null)
     * @method OrderItem sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderItem_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderItem[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_OrderItem_QB whereId($value)
     * @method _IH_OrderItem_QB whereSku($value)
     * @method _IH_OrderItem_QB whereType($value)
     * @method _IH_OrderItem_QB whereName($value)
     * @method _IH_OrderItem_QB whereCouponCode($value)
     * @method _IH_OrderItem_QB whereWeight($value)
     * @method _IH_OrderItem_QB whereTotalWeight($value)
     * @method _IH_OrderItem_QB whereQtyOrdered($value)
     * @method _IH_OrderItem_QB whereQtyShipped($value)
     * @method _IH_OrderItem_QB whereQtyInvoiced($value)
     * @method _IH_OrderItem_QB whereQtyCanceled($value)
     * @method _IH_OrderItem_QB whereQtyRefunded($value)
     * @method _IH_OrderItem_QB wherePrice($value)
     * @method _IH_OrderItem_QB whereBasePrice($value)
     * @method _IH_OrderItem_QB whereTotal($value)
     * @method _IH_OrderItem_QB whereBaseTotal($value)
     * @method _IH_OrderItem_QB whereTotalInvoiced($value)
     * @method _IH_OrderItem_QB whereBaseTotalInvoiced($value)
     * @method _IH_OrderItem_QB whereAmountRefunded($value)
     * @method _IH_OrderItem_QB whereBaseAmountRefunded($value)
     * @method _IH_OrderItem_QB whereDiscountPercent($value)
     * @method _IH_OrderItem_QB whereDiscountAmount($value)
     * @method _IH_OrderItem_QB whereBaseDiscountAmount($value)
     * @method _IH_OrderItem_QB whereDiscountInvoiced($value)
     * @method _IH_OrderItem_QB whereBaseDiscountInvoiced($value)
     * @method _IH_OrderItem_QB whereDiscountRefunded($value)
     * @method _IH_OrderItem_QB whereBaseDiscountRefunded($value)
     * @method _IH_OrderItem_QB whereTaxPercent($value)
     * @method _IH_OrderItem_QB whereTaxAmount($value)
     * @method _IH_OrderItem_QB whereBaseTaxAmount($value)
     * @method _IH_OrderItem_QB whereTaxAmountInvoiced($value)
     * @method _IH_OrderItem_QB whereBaseTaxAmountInvoiced($value)
     * @method _IH_OrderItem_QB whereTaxAmountRefunded($value)
     * @method _IH_OrderItem_QB whereBaseTaxAmountRefunded($value)
     * @method _IH_OrderItem_QB whereProductId($value)
     * @method _IH_OrderItem_QB whereProductType($value)
     * @method _IH_OrderItem_QB whereOrderId($value)
     * @method _IH_OrderItem_QB whereParentId($value)
     * @method _IH_OrderItem_QB whereAdditional($value)
     * @method _IH_OrderItem_QB whereCreatedAt($value)
     * @method _IH_OrderItem_QB whereUpdatedAt($value)
     * @method OrderItem baseSole(array|string $columns = ['*'])
     * @method OrderItem create(array $attributes = [])
     * @method _IH_OrderItem_C|OrderItem[] cursor()
     * @method OrderItem|null|_IH_OrderItem_C|OrderItem[] find($id, array $columns = ['*'])
     * @method _IH_OrderItem_C|OrderItem[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderItem|_IH_OrderItem_C|OrderItem[] findOrFail($id, array $columns = ['*'])
     * @method OrderItem|_IH_OrderItem_C|OrderItem[] findOrNew($id, array $columns = ['*'])
     * @method OrderItem first(array|string $columns = ['*'])
     * @method OrderItem firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderItem firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderItem firstOrFail(array $columns = ['*'])
     * @method OrderItem firstOrNew(array $attributes = [], array $values = [])
     * @method OrderItem firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderItem forceCreate(array $attributes)
     * @method _IH_OrderItem_C|OrderItem[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderItem_C|OrderItem[] get(array|string $columns = ['*'])
     * @method OrderItem getModel()
     * @method OrderItem[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderItem_C|OrderItem[] hydrate(array $items)
     * @method OrderItem make(array $attributes = [])
     * @method OrderItem newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderItem[]|_IH_OrderItem_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderItem[]|_IH_OrderItem_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderItem sole(array|string $columns = ['*'])
     * @method OrderItem updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderItem_QB extends _BaseBuilder {}
    
    /**
     * @method OrderPayment shift()
     * @method OrderPayment pop()
     * @method OrderPayment get($key, $default = null)
     * @method OrderPayment pull($key, $default = null)
     * @method OrderPayment first(callable $callback = null, $default = null)
     * @method OrderPayment firstWhere(string $key, $operator = null, $value = null)
     * @method OrderPayment find($key, $default = null)
     * @method OrderPayment[] all()
     * @method OrderPayment last(callable $callback = null, $default = null)
     * @method OrderPayment sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderPayment_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderPayment[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_OrderPayment_QB whereId($value)
     * @method _IH_OrderPayment_QB whereMethod($value)
     * @method _IH_OrderPayment_QB whereMethodTitle($value)
     * @method _IH_OrderPayment_QB whereOrderId($value)
     * @method _IH_OrderPayment_QB whereAdditional($value)
     * @method _IH_OrderPayment_QB whereCreatedAt($value)
     * @method _IH_OrderPayment_QB whereUpdatedAt($value)
     * @method OrderPayment baseSole(array|string $columns = ['*'])
     * @method OrderPayment create(array $attributes = [])
     * @method _IH_OrderPayment_C|OrderPayment[] cursor()
     * @method OrderPayment|null|_IH_OrderPayment_C|OrderPayment[] find($id, array $columns = ['*'])
     * @method _IH_OrderPayment_C|OrderPayment[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderPayment|_IH_OrderPayment_C|OrderPayment[] findOrFail($id, array $columns = ['*'])
     * @method OrderPayment|_IH_OrderPayment_C|OrderPayment[] findOrNew($id, array $columns = ['*'])
     * @method OrderPayment first(array|string $columns = ['*'])
     * @method OrderPayment firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderPayment firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderPayment firstOrFail(array $columns = ['*'])
     * @method OrderPayment firstOrNew(array $attributes = [], array $values = [])
     * @method OrderPayment firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderPayment forceCreate(array $attributes)
     * @method _IH_OrderPayment_C|OrderPayment[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderPayment_C|OrderPayment[] get(array|string $columns = ['*'])
     * @method OrderPayment getModel()
     * @method OrderPayment[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderPayment_C|OrderPayment[] hydrate(array $items)
     * @method OrderPayment make(array $attributes = [])
     * @method OrderPayment newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderPayment[]|_IH_OrderPayment_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderPayment[]|_IH_OrderPayment_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderPayment sole(array|string $columns = ['*'])
     * @method OrderPayment updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderPayment_QB extends _BaseBuilder {}
    
    /**
     * @method OrderTransaction shift()
     * @method OrderTransaction pop()
     * @method OrderTransaction get($key, $default = null)
     * @method OrderTransaction pull($key, $default = null)
     * @method OrderTransaction first(callable $callback = null, $default = null)
     * @method OrderTransaction firstWhere(string $key, $operator = null, $value = null)
     * @method OrderTransaction find($key, $default = null)
     * @method OrderTransaction[] all()
     * @method OrderTransaction last(callable $callback = null, $default = null)
     * @method OrderTransaction sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderTransaction_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderTransaction[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_OrderTransaction_QB whereId($value)
     * @method _IH_OrderTransaction_QB whereTransactionId($value)
     * @method _IH_OrderTransaction_QB whereStatus($value)
     * @method _IH_OrderTransaction_QB whereType($value)
     * @method _IH_OrderTransaction_QB wherePaymentMethod($value)
     * @method _IH_OrderTransaction_QB whereData($value)
     * @method _IH_OrderTransaction_QB whereInvoiceId($value)
     * @method _IH_OrderTransaction_QB whereOrderId($value)
     * @method _IH_OrderTransaction_QB whereCreatedAt($value)
     * @method _IH_OrderTransaction_QB whereUpdatedAt($value)
     * @method _IH_OrderTransaction_QB whereAmount($value)
     * @method OrderTransaction baseSole(array|string $columns = ['*'])
     * @method OrderTransaction create(array $attributes = [])
     * @method _IH_OrderTransaction_C|OrderTransaction[] cursor()
     * @method OrderTransaction|null|_IH_OrderTransaction_C|OrderTransaction[] find($id, array $columns = ['*'])
     * @method _IH_OrderTransaction_C|OrderTransaction[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderTransaction|_IH_OrderTransaction_C|OrderTransaction[] findOrFail($id, array $columns = ['*'])
     * @method OrderTransaction|_IH_OrderTransaction_C|OrderTransaction[] findOrNew($id, array $columns = ['*'])
     * @method OrderTransaction first(array|string $columns = ['*'])
     * @method OrderTransaction firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderTransaction firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderTransaction firstOrFail(array $columns = ['*'])
     * @method OrderTransaction firstOrNew(array $attributes = [], array $values = [])
     * @method OrderTransaction firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderTransaction forceCreate(array $attributes)
     * @method _IH_OrderTransaction_C|OrderTransaction[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderTransaction_C|OrderTransaction[] get(array|string $columns = ['*'])
     * @method OrderTransaction getModel()
     * @method OrderTransaction[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderTransaction_C|OrderTransaction[] hydrate(array $items)
     * @method OrderTransaction make(array $attributes = [])
     * @method OrderTransaction newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderTransaction[]|_IH_OrderTransaction_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderTransaction[]|_IH_OrderTransaction_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderTransaction sole(array|string $columns = ['*'])
     * @method OrderTransaction updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderTransaction_QB extends _BaseBuilder {}
    
    /**
     * @method Order shift()
     * @method Order pop()
     * @method Order get($key, $default = null)
     * @method Order pull($key, $default = null)
     * @method Order first(callable $callback = null, $default = null)
     * @method Order firstWhere(string $key, $operator = null, $value = null)
     * @method Order find($key, $default = null)
     * @method Order[] all()
     * @method Order last(callable $callback = null, $default = null)
     * @method Order sole($key = null, $operator = null, $value = null)
     */
    class _IH_Order_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Order[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Order_QB whereId($value)
     * @method _IH_Order_QB whereIncrementId($value)
     * @method _IH_Order_QB whereStatus($value)
     * @method _IH_Order_QB whereChannelName($value)
     * @method _IH_Order_QB whereIsGuest($value)
     * @method _IH_Order_QB whereCustomerEmail($value)
     * @method _IH_Order_QB whereCustomerFirstName($value)
     * @method _IH_Order_QB whereCustomerLastName($value)
     * @method _IH_Order_QB whereCustomerCompanyName($value)
     * @method _IH_Order_QB whereCustomerVatId($value)
     * @method _IH_Order_QB whereShippingMethod($value)
     * @method _IH_Order_QB whereShippingTitle($value)
     * @method _IH_Order_QB whereShippingDescription($value)
     * @method _IH_Order_QB whereCouponCode($value)
     * @method _IH_Order_QB whereIsGift($value)
     * @method _IH_Order_QB whereTotalItemCount($value)
     * @method _IH_Order_QB whereTotalQtyOrdered($value)
     * @method _IH_Order_QB whereBaseCurrencyCode($value)
     * @method _IH_Order_QB whereChannelCurrencyCode($value)
     * @method _IH_Order_QB whereOrderCurrencyCode($value)
     * @method _IH_Order_QB whereGrandTotal($value)
     * @method _IH_Order_QB whereBaseGrandTotal($value)
     * @method _IH_Order_QB whereGrandTotalInvoiced($value)
     * @method _IH_Order_QB whereBaseGrandTotalInvoiced($value)
     * @method _IH_Order_QB whereGrandTotalRefunded($value)
     * @method _IH_Order_QB whereBaseGrandTotalRefunded($value)
     * @method _IH_Order_QB whereSubTotal($value)
     * @method _IH_Order_QB whereBaseSubTotal($value)
     * @method _IH_Order_QB whereSubTotalInvoiced($value)
     * @method _IH_Order_QB whereBaseSubTotalInvoiced($value)
     * @method _IH_Order_QB whereSubTotalRefunded($value)
     * @method _IH_Order_QB whereBaseSubTotalRefunded($value)
     * @method _IH_Order_QB whereDiscountPercent($value)
     * @method _IH_Order_QB whereDiscountAmount($value)
     * @method _IH_Order_QB whereBaseDiscountAmount($value)
     * @method _IH_Order_QB whereDiscountInvoiced($value)
     * @method _IH_Order_QB whereBaseDiscountInvoiced($value)
     * @method _IH_Order_QB whereDiscountRefunded($value)
     * @method _IH_Order_QB whereBaseDiscountRefunded($value)
     * @method _IH_Order_QB whereTaxAmount($value)
     * @method _IH_Order_QB whereBaseTaxAmount($value)
     * @method _IH_Order_QB whereTaxAmountInvoiced($value)
     * @method _IH_Order_QB whereBaseTaxAmountInvoiced($value)
     * @method _IH_Order_QB whereTaxAmountRefunded($value)
     * @method _IH_Order_QB whereBaseTaxAmountRefunded($value)
     * @method _IH_Order_QB whereShippingAmount($value)
     * @method _IH_Order_QB whereBaseShippingAmount($value)
     * @method _IH_Order_QB whereShippingInvoiced($value)
     * @method _IH_Order_QB whereBaseShippingInvoiced($value)
     * @method _IH_Order_QB whereShippingRefunded($value)
     * @method _IH_Order_QB whereBaseShippingRefunded($value)
     * @method _IH_Order_QB whereCustomerId($value)
     * @method _IH_Order_QB whereCustomerType($value)
     * @method _IH_Order_QB whereChannelId($value)
     * @method _IH_Order_QB whereChannelType($value)
     * @method _IH_Order_QB whereCreatedAt($value)
     * @method _IH_Order_QB whereUpdatedAt($value)
     * @method _IH_Order_QB whereCartId($value)
     * @method _IH_Order_QB whereAppliedCartRuleIds($value)
     * @method _IH_Order_QB whereShippingDiscountAmount($value)
     * @method _IH_Order_QB whereBaseShippingDiscountAmount($value)
     * @method Order baseSole(array|string $columns = ['*'])
     * @method Order create(array $attributes = [])
     * @method _IH_Order_C|Order[] cursor()
     * @method Order|null|_IH_Order_C|Order[] find($id, array $columns = ['*'])
     * @method _IH_Order_C|Order[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Order|_IH_Order_C|Order[] findOrFail($id, array $columns = ['*'])
     * @method Order|_IH_Order_C|Order[] findOrNew($id, array $columns = ['*'])
     * @method Order first(array|string $columns = ['*'])
     * @method Order firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Order firstOrCreate(array $attributes = [], array $values = [])
     * @method Order firstOrFail(array $columns = ['*'])
     * @method Order firstOrNew(array $attributes = [], array $values = [])
     * @method Order firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Order forceCreate(array $attributes)
     * @method _IH_Order_C|Order[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Order_C|Order[] get(array|string $columns = ['*'])
     * @method Order getModel()
     * @method Order[] getModels(array|string $columns = ['*'])
     * @method _IH_Order_C|Order[] hydrate(array $items)
     * @method Order make(array $attributes = [])
     * @method Order newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Order[]|_IH_Order_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Order[]|_IH_Order_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Order sole(array|string $columns = ['*'])
     * @method Order updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Order_QB extends _BaseBuilder {}
    
    /**
     * @method RefundItem shift()
     * @method RefundItem pop()
     * @method RefundItem get($key, $default = null)
     * @method RefundItem pull($key, $default = null)
     * @method RefundItem first(callable $callback = null, $default = null)
     * @method RefundItem firstWhere(string $key, $operator = null, $value = null)
     * @method RefundItem find($key, $default = null)
     * @method RefundItem[] all()
     * @method RefundItem last(callable $callback = null, $default = null)
     * @method RefundItem sole($key = null, $operator = null, $value = null)
     */
    class _IH_RefundItem_C extends _BaseCollection {
        /**
         * @param int $size
         * @return RefundItem[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_RefundItem_QB whereId($value)
     * @method _IH_RefundItem_QB whereName($value)
     * @method _IH_RefundItem_QB whereDescription($value)
     * @method _IH_RefundItem_QB whereSku($value)
     * @method _IH_RefundItem_QB whereQty($value)
     * @method _IH_RefundItem_QB wherePrice($value)
     * @method _IH_RefundItem_QB whereBasePrice($value)
     * @method _IH_RefundItem_QB whereTotal($value)
     * @method _IH_RefundItem_QB whereBaseTotal($value)
     * @method _IH_RefundItem_QB whereTaxAmount($value)
     * @method _IH_RefundItem_QB whereBaseTaxAmount($value)
     * @method _IH_RefundItem_QB whereDiscountPercent($value)
     * @method _IH_RefundItem_QB whereDiscountAmount($value)
     * @method _IH_RefundItem_QB whereBaseDiscountAmount($value)
     * @method _IH_RefundItem_QB whereProductId($value)
     * @method _IH_RefundItem_QB whereProductType($value)
     * @method _IH_RefundItem_QB whereOrderItemId($value)
     * @method _IH_RefundItem_QB whereRefundId($value)
     * @method _IH_RefundItem_QB whereParentId($value)
     * @method _IH_RefundItem_QB whereAdditional($value)
     * @method _IH_RefundItem_QB whereCreatedAt($value)
     * @method _IH_RefundItem_QB whereUpdatedAt($value)
     * @method RefundItem baseSole(array|string $columns = ['*'])
     * @method RefundItem create(array $attributes = [])
     * @method _IH_RefundItem_C|RefundItem[] cursor()
     * @method RefundItem|null|_IH_RefundItem_C|RefundItem[] find($id, array $columns = ['*'])
     * @method _IH_RefundItem_C|RefundItem[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method RefundItem|_IH_RefundItem_C|RefundItem[] findOrFail($id, array $columns = ['*'])
     * @method RefundItem|_IH_RefundItem_C|RefundItem[] findOrNew($id, array $columns = ['*'])
     * @method RefundItem first(array|string $columns = ['*'])
     * @method RefundItem firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method RefundItem firstOrCreate(array $attributes = [], array $values = [])
     * @method RefundItem firstOrFail(array $columns = ['*'])
     * @method RefundItem firstOrNew(array $attributes = [], array $values = [])
     * @method RefundItem firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method RefundItem forceCreate(array $attributes)
     * @method _IH_RefundItem_C|RefundItem[] fromQuery(string $query, array $bindings = [])
     * @method _IH_RefundItem_C|RefundItem[] get(array|string $columns = ['*'])
     * @method RefundItem getModel()
     * @method RefundItem[] getModels(array|string $columns = ['*'])
     * @method _IH_RefundItem_C|RefundItem[] hydrate(array $items)
     * @method RefundItem make(array $attributes = [])
     * @method RefundItem newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|RefundItem[]|_IH_RefundItem_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|RefundItem[]|_IH_RefundItem_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method RefundItem sole(array|string $columns = ['*'])
     * @method RefundItem updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_RefundItem_QB extends _BaseBuilder {}
    
    /**
     * @method Refund shift()
     * @method Refund pop()
     * @method Refund get($key, $default = null)
     * @method Refund pull($key, $default = null)
     * @method Refund first(callable $callback = null, $default = null)
     * @method Refund firstWhere(string $key, $operator = null, $value = null)
     * @method Refund find($key, $default = null)
     * @method Refund[] all()
     * @method Refund last(callable $callback = null, $default = null)
     * @method Refund sole($key = null, $operator = null, $value = null)
     */
    class _IH_Refund_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Refund[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Refund_QB whereId($value)
     * @method _IH_Refund_QB whereIncrementId($value)
     * @method _IH_Refund_QB whereState($value)
     * @method _IH_Refund_QB whereEmailSent($value)
     * @method _IH_Refund_QB whereTotalQty($value)
     * @method _IH_Refund_QB whereBaseCurrencyCode($value)
     * @method _IH_Refund_QB whereChannelCurrencyCode($value)
     * @method _IH_Refund_QB whereOrderCurrencyCode($value)
     * @method _IH_Refund_QB whereAdjustmentRefund($value)
     * @method _IH_Refund_QB whereBaseAdjustmentRefund($value)
     * @method _IH_Refund_QB whereAdjustmentFee($value)
     * @method _IH_Refund_QB whereBaseAdjustmentFee($value)
     * @method _IH_Refund_QB whereSubTotal($value)
     * @method _IH_Refund_QB whereBaseSubTotal($value)
     * @method _IH_Refund_QB whereGrandTotal($value)
     * @method _IH_Refund_QB whereBaseGrandTotal($value)
     * @method _IH_Refund_QB whereShippingAmount($value)
     * @method _IH_Refund_QB whereBaseShippingAmount($value)
     * @method _IH_Refund_QB whereTaxAmount($value)
     * @method _IH_Refund_QB whereBaseTaxAmount($value)
     * @method _IH_Refund_QB whereDiscountPercent($value)
     * @method _IH_Refund_QB whereDiscountAmount($value)
     * @method _IH_Refund_QB whereBaseDiscountAmount($value)
     * @method _IH_Refund_QB whereOrderId($value)
     * @method _IH_Refund_QB whereCreatedAt($value)
     * @method _IH_Refund_QB whereUpdatedAt($value)
     * @method Refund baseSole(array|string $columns = ['*'])
     * @method Refund create(array $attributes = [])
     * @method _IH_Refund_C|Refund[] cursor()
     * @method Refund|null|_IH_Refund_C|Refund[] find($id, array $columns = ['*'])
     * @method _IH_Refund_C|Refund[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Refund|_IH_Refund_C|Refund[] findOrFail($id, array $columns = ['*'])
     * @method Refund|_IH_Refund_C|Refund[] findOrNew($id, array $columns = ['*'])
     * @method Refund first(array|string $columns = ['*'])
     * @method Refund firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Refund firstOrCreate(array $attributes = [], array $values = [])
     * @method Refund firstOrFail(array $columns = ['*'])
     * @method Refund firstOrNew(array $attributes = [], array $values = [])
     * @method Refund firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Refund forceCreate(array $attributes)
     * @method _IH_Refund_C|Refund[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Refund_C|Refund[] get(array|string $columns = ['*'])
     * @method Refund getModel()
     * @method Refund[] getModels(array|string $columns = ['*'])
     * @method _IH_Refund_C|Refund[] hydrate(array $items)
     * @method Refund make(array $attributes = [])
     * @method Refund newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Refund[]|_IH_Refund_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Refund[]|_IH_Refund_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Refund sole(array|string $columns = ['*'])
     * @method Refund updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Refund_QB extends _BaseBuilder {}
    
    /**
     * @method ShipmentItem shift()
     * @method ShipmentItem pop()
     * @method ShipmentItem get($key, $default = null)
     * @method ShipmentItem pull($key, $default = null)
     * @method ShipmentItem first(callable $callback = null, $default = null)
     * @method ShipmentItem firstWhere(string $key, $operator = null, $value = null)
     * @method ShipmentItem find($key, $default = null)
     * @method ShipmentItem[] all()
     * @method ShipmentItem last(callable $callback = null, $default = null)
     * @method ShipmentItem sole($key = null, $operator = null, $value = null)
     */
    class _IH_ShipmentItem_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ShipmentItem[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ShipmentItem_QB whereId($value)
     * @method _IH_ShipmentItem_QB whereName($value)
     * @method _IH_ShipmentItem_QB whereDescription($value)
     * @method _IH_ShipmentItem_QB whereSku($value)
     * @method _IH_ShipmentItem_QB whereQty($value)
     * @method _IH_ShipmentItem_QB whereWeight($value)
     * @method _IH_ShipmentItem_QB wherePrice($value)
     * @method _IH_ShipmentItem_QB whereBasePrice($value)
     * @method _IH_ShipmentItem_QB whereTotal($value)
     * @method _IH_ShipmentItem_QB whereBaseTotal($value)
     * @method _IH_ShipmentItem_QB whereProductId($value)
     * @method _IH_ShipmentItem_QB whereProductType($value)
     * @method _IH_ShipmentItem_QB whereOrderItemId($value)
     * @method _IH_ShipmentItem_QB whereShipmentId($value)
     * @method _IH_ShipmentItem_QB whereAdditional($value)
     * @method _IH_ShipmentItem_QB whereCreatedAt($value)
     * @method _IH_ShipmentItem_QB whereUpdatedAt($value)
     * @method ShipmentItem baseSole(array|string $columns = ['*'])
     * @method ShipmentItem create(array $attributes = [])
     * @method _IH_ShipmentItem_C|ShipmentItem[] cursor()
     * @method ShipmentItem|null|_IH_ShipmentItem_C|ShipmentItem[] find($id, array $columns = ['*'])
     * @method _IH_ShipmentItem_C|ShipmentItem[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ShipmentItem|_IH_ShipmentItem_C|ShipmentItem[] findOrFail($id, array $columns = ['*'])
     * @method ShipmentItem|_IH_ShipmentItem_C|ShipmentItem[] findOrNew($id, array $columns = ['*'])
     * @method ShipmentItem first(array|string $columns = ['*'])
     * @method ShipmentItem firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ShipmentItem firstOrCreate(array $attributes = [], array $values = [])
     * @method ShipmentItem firstOrFail(array $columns = ['*'])
     * @method ShipmentItem firstOrNew(array $attributes = [], array $values = [])
     * @method ShipmentItem firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ShipmentItem forceCreate(array $attributes)
     * @method _IH_ShipmentItem_C|ShipmentItem[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ShipmentItem_C|ShipmentItem[] get(array|string $columns = ['*'])
     * @method ShipmentItem getModel()
     * @method ShipmentItem[] getModels(array|string $columns = ['*'])
     * @method _IH_ShipmentItem_C|ShipmentItem[] hydrate(array $items)
     * @method ShipmentItem make(array $attributes = [])
     * @method ShipmentItem newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ShipmentItem[]|_IH_ShipmentItem_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ShipmentItem[]|_IH_ShipmentItem_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ShipmentItem sole(array|string $columns = ['*'])
     * @method ShipmentItem updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ShipmentItem_QB extends _BaseBuilder {}
    
    /**
     * @method Shipment shift()
     * @method Shipment pop()
     * @method Shipment get($key, $default = null)
     * @method Shipment pull($key, $default = null)
     * @method Shipment first(callable $callback = null, $default = null)
     * @method Shipment firstWhere(string $key, $operator = null, $value = null)
     * @method Shipment find($key, $default = null)
     * @method Shipment[] all()
     * @method Shipment last(callable $callback = null, $default = null)
     * @method Shipment sole($key = null, $operator = null, $value = null)
     */
    class _IH_Shipment_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Shipment[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Shipment_QB whereId($value)
     * @method _IH_Shipment_QB whereStatus($value)
     * @method _IH_Shipment_QB whereTotalQty($value)
     * @method _IH_Shipment_QB whereTotalWeight($value)
     * @method _IH_Shipment_QB whereCarrierCode($value)
     * @method _IH_Shipment_QB whereCarrierTitle($value)
     * @method _IH_Shipment_QB whereTrackNumber($value)
     * @method _IH_Shipment_QB whereEmailSent($value)
     * @method _IH_Shipment_QB whereCustomerId($value)
     * @method _IH_Shipment_QB whereCustomerType($value)
     * @method _IH_Shipment_QB whereOrderId($value)
     * @method _IH_Shipment_QB whereOrderAddressId($value)
     * @method _IH_Shipment_QB whereCreatedAt($value)
     * @method _IH_Shipment_QB whereUpdatedAt($value)
     * @method _IH_Shipment_QB whereInventorySourceId($value)
     * @method _IH_Shipment_QB whereInventorySourceName($value)
     * @method Shipment baseSole(array|string $columns = ['*'])
     * @method Shipment create(array $attributes = [])
     * @method _IH_Shipment_C|Shipment[] cursor()
     * @method Shipment|null|_IH_Shipment_C|Shipment[] find($id, array $columns = ['*'])
     * @method _IH_Shipment_C|Shipment[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Shipment|_IH_Shipment_C|Shipment[] findOrFail($id, array $columns = ['*'])
     * @method Shipment|_IH_Shipment_C|Shipment[] findOrNew($id, array $columns = ['*'])
     * @method Shipment first(array|string $columns = ['*'])
     * @method Shipment firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Shipment firstOrCreate(array $attributes = [], array $values = [])
     * @method Shipment firstOrFail(array $columns = ['*'])
     * @method Shipment firstOrNew(array $attributes = [], array $values = [])
     * @method Shipment firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Shipment forceCreate(array $attributes)
     * @method _IH_Shipment_C|Shipment[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Shipment_C|Shipment[] get(array|string $columns = ['*'])
     * @method Shipment getModel()
     * @method Shipment[] getModels(array|string $columns = ['*'])
     * @method _IH_Shipment_C|Shipment[] hydrate(array $items)
     * @method Shipment make(array $attributes = [])
     * @method Shipment newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Shipment[]|_IH_Shipment_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Shipment[]|_IH_Shipment_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Shipment sole(array|string $columns = ['*'])
     * @method Shipment updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Shipment_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\SocialLogin\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\SocialLogin\Models\CustomerSocialAccount;
    
    /**
     * @method CustomerSocialAccount shift()
     * @method CustomerSocialAccount pop()
     * @method CustomerSocialAccount get($key, $default = null)
     * @method CustomerSocialAccount pull($key, $default = null)
     * @method CustomerSocialAccount first(callable $callback = null, $default = null)
     * @method CustomerSocialAccount firstWhere(string $key, $operator = null, $value = null)
     * @method CustomerSocialAccount find($key, $default = null)
     * @method CustomerSocialAccount[] all()
     * @method CustomerSocialAccount last(callable $callback = null, $default = null)
     * @method CustomerSocialAccount sole($key = null, $operator = null, $value = null)
     */
    class _IH_CustomerSocialAccount_C extends _BaseCollection {
        /**
         * @param int $size
         * @return CustomerSocialAccount[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_CustomerSocialAccount_QB whereId($value)
     * @method _IH_CustomerSocialAccount_QB whereProviderName($value)
     * @method _IH_CustomerSocialAccount_QB whereProviderId($value)
     * @method _IH_CustomerSocialAccount_QB whereCustomerId($value)
     * @method _IH_CustomerSocialAccount_QB whereCreatedAt($value)
     * @method _IH_CustomerSocialAccount_QB whereUpdatedAt($value)
     * @method CustomerSocialAccount baseSole(array|string $columns = ['*'])
     * @method CustomerSocialAccount create(array $attributes = [])
     * @method _IH_CustomerSocialAccount_C|CustomerSocialAccount[] cursor()
     * @method CustomerSocialAccount|null|_IH_CustomerSocialAccount_C|CustomerSocialAccount[] find($id, array $columns = ['*'])
     * @method _IH_CustomerSocialAccount_C|CustomerSocialAccount[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method CustomerSocialAccount|_IH_CustomerSocialAccount_C|CustomerSocialAccount[] findOrFail($id, array $columns = ['*'])
     * @method CustomerSocialAccount|_IH_CustomerSocialAccount_C|CustomerSocialAccount[] findOrNew($id, array $columns = ['*'])
     * @method CustomerSocialAccount first(array|string $columns = ['*'])
     * @method CustomerSocialAccount firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method CustomerSocialAccount firstOrCreate(array $attributes = [], array $values = [])
     * @method CustomerSocialAccount firstOrFail(array $columns = ['*'])
     * @method CustomerSocialAccount firstOrNew(array $attributes = [], array $values = [])
     * @method CustomerSocialAccount firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method CustomerSocialAccount forceCreate(array $attributes)
     * @method _IH_CustomerSocialAccount_C|CustomerSocialAccount[] fromQuery(string $query, array $bindings = [])
     * @method _IH_CustomerSocialAccount_C|CustomerSocialAccount[] get(array|string $columns = ['*'])
     * @method CustomerSocialAccount getModel()
     * @method CustomerSocialAccount[] getModels(array|string $columns = ['*'])
     * @method _IH_CustomerSocialAccount_C|CustomerSocialAccount[] hydrate(array $items)
     * @method CustomerSocialAccount make(array $attributes = [])
     * @method CustomerSocialAccount newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|CustomerSocialAccount[]|_IH_CustomerSocialAccount_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|CustomerSocialAccount[]|_IH_CustomerSocialAccount_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method CustomerSocialAccount sole(array|string $columns = ['*'])
     * @method CustomerSocialAccount updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_CustomerSocialAccount_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Tax\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Tax\Models\TaxCategory;
    use Webkul\Tax\Models\TaxMap;
    use Webkul\Tax\Models\TaxRate;
    
    /**
     * @method TaxCategory shift()
     * @method TaxCategory pop()
     * @method TaxCategory get($key, $default = null)
     * @method TaxCategory pull($key, $default = null)
     * @method TaxCategory first(callable $callback = null, $default = null)
     * @method TaxCategory firstWhere(string $key, $operator = null, $value = null)
     * @method TaxCategory find($key, $default = null)
     * @method TaxCategory[] all()
     * @method TaxCategory last(callable $callback = null, $default = null)
     * @method TaxCategory sole($key = null, $operator = null, $value = null)
     */
    class _IH_TaxCategory_C extends _BaseCollection {
        /**
         * @param int $size
         * @return TaxCategory[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_TaxCategory_QB whereId($value)
     * @method _IH_TaxCategory_QB whereCode($value)
     * @method _IH_TaxCategory_QB whereName($value)
     * @method _IH_TaxCategory_QB whereDescription($value)
     * @method _IH_TaxCategory_QB whereCreatedAt($value)
     * @method _IH_TaxCategory_QB whereUpdatedAt($value)
     * @method TaxCategory baseSole(array|string $columns = ['*'])
     * @method TaxCategory create(array $attributes = [])
     * @method _IH_TaxCategory_C|TaxCategory[] cursor()
     * @method TaxCategory|null|_IH_TaxCategory_C|TaxCategory[] find($id, array $columns = ['*'])
     * @method _IH_TaxCategory_C|TaxCategory[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method TaxCategory|_IH_TaxCategory_C|TaxCategory[] findOrFail($id, array $columns = ['*'])
     * @method TaxCategory|_IH_TaxCategory_C|TaxCategory[] findOrNew($id, array $columns = ['*'])
     * @method TaxCategory first(array|string $columns = ['*'])
     * @method TaxCategory firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method TaxCategory firstOrCreate(array $attributes = [], array $values = [])
     * @method TaxCategory firstOrFail(array $columns = ['*'])
     * @method TaxCategory firstOrNew(array $attributes = [], array $values = [])
     * @method TaxCategory firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method TaxCategory forceCreate(array $attributes)
     * @method _IH_TaxCategory_C|TaxCategory[] fromQuery(string $query, array $bindings = [])
     * @method _IH_TaxCategory_C|TaxCategory[] get(array|string $columns = ['*'])
     * @method TaxCategory getModel()
     * @method TaxCategory[] getModels(array|string $columns = ['*'])
     * @method _IH_TaxCategory_C|TaxCategory[] hydrate(array $items)
     * @method TaxCategory make(array $attributes = [])
     * @method TaxCategory newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|TaxCategory[]|_IH_TaxCategory_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|TaxCategory[]|_IH_TaxCategory_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method TaxCategory sole(array|string $columns = ['*'])
     * @method TaxCategory updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_TaxCategory_QB extends _BaseBuilder {}
    
    /**
     * @method TaxMap shift()
     * @method TaxMap pop()
     * @method TaxMap get($key, $default = null)
     * @method TaxMap pull($key, $default = null)
     * @method TaxMap first(callable $callback = null, $default = null)
     * @method TaxMap firstWhere(string $key, $operator = null, $value = null)
     * @method TaxMap find($key, $default = null)
     * @method TaxMap[] all()
     * @method TaxMap last(callable $callback = null, $default = null)
     * @method TaxMap sole($key = null, $operator = null, $value = null)
     */
    class _IH_TaxMap_C extends _BaseCollection {
        /**
         * @param int $size
         * @return TaxMap[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_TaxMap_QB whereId($value)
     * @method _IH_TaxMap_QB whereTaxCategoryId($value)
     * @method _IH_TaxMap_QB whereTaxRateId($value)
     * @method _IH_TaxMap_QB whereCreatedAt($value)
     * @method _IH_TaxMap_QB whereUpdatedAt($value)
     * @method TaxMap baseSole(array|string $columns = ['*'])
     * @method TaxMap create(array $attributes = [])
     * @method _IH_TaxMap_C|TaxMap[] cursor()
     * @method TaxMap|null|_IH_TaxMap_C|TaxMap[] find($id, array $columns = ['*'])
     * @method _IH_TaxMap_C|TaxMap[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method TaxMap|_IH_TaxMap_C|TaxMap[] findOrFail($id, array $columns = ['*'])
     * @method TaxMap|_IH_TaxMap_C|TaxMap[] findOrNew($id, array $columns = ['*'])
     * @method TaxMap first(array|string $columns = ['*'])
     * @method TaxMap firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method TaxMap firstOrCreate(array $attributes = [], array $values = [])
     * @method TaxMap firstOrFail(array $columns = ['*'])
     * @method TaxMap firstOrNew(array $attributes = [], array $values = [])
     * @method TaxMap firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method TaxMap forceCreate(array $attributes)
     * @method _IH_TaxMap_C|TaxMap[] fromQuery(string $query, array $bindings = [])
     * @method _IH_TaxMap_C|TaxMap[] get(array|string $columns = ['*'])
     * @method TaxMap getModel()
     * @method TaxMap[] getModels(array|string $columns = ['*'])
     * @method _IH_TaxMap_C|TaxMap[] hydrate(array $items)
     * @method TaxMap make(array $attributes = [])
     * @method TaxMap newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|TaxMap[]|_IH_TaxMap_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|TaxMap[]|_IH_TaxMap_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method TaxMap sole(array|string $columns = ['*'])
     * @method TaxMap updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_TaxMap_QB extends _BaseBuilder {}
    
    /**
     * @method TaxRate shift()
     * @method TaxRate pop()
     * @method TaxRate get($key, $default = null)
     * @method TaxRate pull($key, $default = null)
     * @method TaxRate first(callable $callback = null, $default = null)
     * @method TaxRate firstWhere(string $key, $operator = null, $value = null)
     * @method TaxRate find($key, $default = null)
     * @method TaxRate[] all()
     * @method TaxRate last(callable $callback = null, $default = null)
     * @method TaxRate sole($key = null, $operator = null, $value = null)
     */
    class _IH_TaxRate_C extends _BaseCollection {
        /**
         * @param int $size
         * @return TaxRate[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_TaxRate_QB whereId($value)
     * @method _IH_TaxRate_QB whereIdentifier($value)
     * @method _IH_TaxRate_QB whereIsZip($value)
     * @method _IH_TaxRate_QB whereZipCode($value)
     * @method _IH_TaxRate_QB whereZipFrom($value)
     * @method _IH_TaxRate_QB whereZipTo($value)
     * @method _IH_TaxRate_QB whereState($value)
     * @method _IH_TaxRate_QB whereCountry($value)
     * @method _IH_TaxRate_QB whereTaxRate($value)
     * @method _IH_TaxRate_QB whereCreatedAt($value)
     * @method _IH_TaxRate_QB whereUpdatedAt($value)
     * @method TaxRate baseSole(array|string $columns = ['*'])
     * @method TaxRate create(array $attributes = [])
     * @method _IH_TaxRate_C|TaxRate[] cursor()
     * @method TaxRate|null|_IH_TaxRate_C|TaxRate[] find($id, array $columns = ['*'])
     * @method _IH_TaxRate_C|TaxRate[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method TaxRate|_IH_TaxRate_C|TaxRate[] findOrFail($id, array $columns = ['*'])
     * @method TaxRate|_IH_TaxRate_C|TaxRate[] findOrNew($id, array $columns = ['*'])
     * @method TaxRate first(array|string $columns = ['*'])
     * @method TaxRate firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method TaxRate firstOrCreate(array $attributes = [], array $values = [])
     * @method TaxRate firstOrFail(array $columns = ['*'])
     * @method TaxRate firstOrNew(array $attributes = [], array $values = [])
     * @method TaxRate firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method TaxRate forceCreate(array $attributes)
     * @method _IH_TaxRate_C|TaxRate[] fromQuery(string $query, array $bindings = [])
     * @method _IH_TaxRate_C|TaxRate[] get(array|string $columns = ['*'])
     * @method TaxRate getModel()
     * @method TaxRate[] getModels(array|string $columns = ['*'])
     * @method _IH_TaxRate_C|TaxRate[] hydrate(array $items)
     * @method TaxRate make(array $attributes = [])
     * @method TaxRate newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|TaxRate[]|_IH_TaxRate_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|TaxRate[]|_IH_TaxRate_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method TaxRate sole(array|string $columns = ['*'])
     * @method TaxRate updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_TaxRate_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\User\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\User\Models\Admin;
    use Webkul\User\Models\Role;
    
    /**
     * @method Admin shift()
     * @method Admin pop()
     * @method Admin get($key, $default = null)
     * @method Admin pull($key, $default = null)
     * @method Admin first(callable $callback = null, $default = null)
     * @method Admin firstWhere(string $key, $operator = null, $value = null)
     * @method Admin find($key, $default = null)
     * @method Admin[] all()
     * @method Admin last(callable $callback = null, $default = null)
     * @method Admin sole($key = null, $operator = null, $value = null)
     */
    class _IH_Admin_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Admin[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Admin_QB whereId($value)
     * @method _IH_Admin_QB whereName($value)
     * @method _IH_Admin_QB whereEmail($value)
     * @method _IH_Admin_QB wherePassword($value)
     * @method _IH_Admin_QB whereApiToken($value)
     * @method _IH_Admin_QB whereStatus($value)
     * @method _IH_Admin_QB whereRoleId($value)
     * @method _IH_Admin_QB whereRememberToken($value)
     * @method _IH_Admin_QB whereCreatedAt($value)
     * @method _IH_Admin_QB whereUpdatedAt($value)
     * @method Admin baseSole(array|string $columns = ['*'])
     * @method Admin create(array $attributes = [])
     * @method _IH_Admin_C|Admin[] cursor()
     * @method Admin|null|_IH_Admin_C|Admin[] find($id, array $columns = ['*'])
     * @method _IH_Admin_C|Admin[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Admin|_IH_Admin_C|Admin[] findOrFail($id, array $columns = ['*'])
     * @method Admin|_IH_Admin_C|Admin[] findOrNew($id, array $columns = ['*'])
     * @method Admin first(array|string $columns = ['*'])
     * @method Admin firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Admin firstOrCreate(array $attributes = [], array $values = [])
     * @method Admin firstOrFail(array $columns = ['*'])
     * @method Admin firstOrNew(array $attributes = [], array $values = [])
     * @method Admin firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Admin forceCreate(array $attributes)
     * @method _IH_Admin_C|Admin[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Admin_C|Admin[] get(array|string $columns = ['*'])
     * @method Admin getModel()
     * @method Admin[] getModels(array|string $columns = ['*'])
     * @method _IH_Admin_C|Admin[] hydrate(array $items)
     * @method Admin make(array $attributes = [])
     * @method Admin newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Admin[]|_IH_Admin_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Admin[]|_IH_Admin_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Admin sole(array|string $columns = ['*'])
     * @method Admin updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Admin_QB extends _BaseBuilder {}
    
    /**
     * @method Role shift()
     * @method Role pop()
     * @method Role get($key, $default = null)
     * @method Role pull($key, $default = null)
     * @method Role first(callable $callback = null, $default = null)
     * @method Role firstWhere(string $key, $operator = null, $value = null)
     * @method Role find($key, $default = null)
     * @method Role[] all()
     * @method Role last(callable $callback = null, $default = null)
     * @method Role sole($key = null, $operator = null, $value = null)
     */
    class _IH_Role_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Role[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Role_QB whereId($value)
     * @method _IH_Role_QB whereName($value)
     * @method _IH_Role_QB whereDescription($value)
     * @method _IH_Role_QB wherePermissionType($value)
     * @method _IH_Role_QB wherePermissions($value)
     * @method _IH_Role_QB whereCreatedAt($value)
     * @method _IH_Role_QB whereUpdatedAt($value)
     * @method Role baseSole(array|string $columns = ['*'])
     * @method Role create(array $attributes = [])
     * @method _IH_Role_C|Role[] cursor()
     * @method Role|null|_IH_Role_C|Role[] find($id, array $columns = ['*'])
     * @method _IH_Role_C|Role[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Role|_IH_Role_C|Role[] findOrFail($id, array $columns = ['*'])
     * @method Role|_IH_Role_C|Role[] findOrNew($id, array $columns = ['*'])
     * @method Role first(array|string $columns = ['*'])
     * @method Role firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Role firstOrCreate(array $attributes = [], array $values = [])
     * @method Role firstOrFail(array $columns = ['*'])
     * @method Role firstOrNew(array $attributes = [], array $values = [])
     * @method Role firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Role forceCreate(array $attributes)
     * @method _IH_Role_C|Role[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Role_C|Role[] get(array|string $columns = ['*'])
     * @method Role getModel()
     * @method Role[] getModels(array|string $columns = ['*'])
     * @method _IH_Role_C|Role[] hydrate(array $items)
     * @method Role make(array $attributes = [])
     * @method Role newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Role[]|_IH_Role_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Role[]|_IH_Role_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Role sole(array|string $columns = ['*'])
     * @method Role updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Role_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Webkul\Velocity\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Webkul\Velocity\Models\Category;
    use Webkul\Velocity\Models\Content;
    use Webkul\Velocity\Models\ContentTranslation;
    use Webkul\Velocity\Models\OrderBrand;
    use Webkul\Velocity\Models\VelocityCustomerCompareProduct;
    use Webkul\Velocity\Models\VelocityMetadata;
    
    /**
     * @method Category shift()
     * @method Category pop()
     * @method Category get($key, $default = null)
     * @method Category pull($key, $default = null)
     * @method Category first(callable $callback = null, $default = null)
     * @method Category firstWhere(string $key, $operator = null, $value = null)
     * @method Category find($key, $default = null)
     * @method Category[] all()
     * @method Category last(callable $callback = null, $default = null)
     * @method Category sole($key = null, $operator = null, $value = null)
     */
    class _IH_Category_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Category[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Category baseSole(array|string $columns = ['*'])
     * @method Category create(array $attributes = [])
     * @method _IH_Category_C|Category[] cursor()
     * @method Category|null|_IH_Category_C|Category[] find($id, array $columns = ['*'])
     * @method _IH_Category_C|Category[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Category|_IH_Category_C|Category[] findOrFail($id, array $columns = ['*'])
     * @method Category|_IH_Category_C|Category[] findOrNew($id, array $columns = ['*'])
     * @method Category first(array|string $columns = ['*'])
     * @method Category firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Category firstOrCreate(array $attributes = [], array $values = [])
     * @method Category firstOrFail(array $columns = ['*'])
     * @method Category firstOrNew(array $attributes = [], array $values = [])
     * @method Category firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Category forceCreate(array $attributes)
     * @method _IH_Category_C|Category[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Category_C|Category[] get(array|string $columns = ['*'])
     * @method Category getModel()
     * @method Category[] getModels(array|string $columns = ['*'])
     * @method _IH_Category_C|Category[] hydrate(array $items)
     * @method Category make(array $attributes = [])
     * @method Category newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Category[]|_IH_Category_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Category[]|_IH_Category_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Category sole(array|string $columns = ['*'])
     * @method Category updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Category_QB extends _BaseBuilder {}
    
    /**
     * @method ContentTranslation shift()
     * @method ContentTranslation pop()
     * @method ContentTranslation get($key, $default = null)
     * @method ContentTranslation pull($key, $default = null)
     * @method ContentTranslation first(callable $callback = null, $default = null)
     * @method ContentTranslation firstWhere(string $key, $operator = null, $value = null)
     * @method ContentTranslation find($key, $default = null)
     * @method ContentTranslation[] all()
     * @method ContentTranslation last(callable $callback = null, $default = null)
     * @method ContentTranslation sole($key = null, $operator = null, $value = null)
     */
    class _IH_ContentTranslation_C extends _BaseCollection {
        /**
         * @param int $size
         * @return ContentTranslation[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_ContentTranslation_QB whereId($value)
     * @method _IH_ContentTranslation_QB whereContentId($value)
     * @method _IH_ContentTranslation_QB whereTitle($value)
     * @method _IH_ContentTranslation_QB whereCustomTitle($value)
     * @method _IH_ContentTranslation_QB whereCustomHeading($value)
     * @method _IH_ContentTranslation_QB wherePageLink($value)
     * @method _IH_ContentTranslation_QB whereLinkTarget($value)
     * @method _IH_ContentTranslation_QB whereCatalogType($value)
     * @method _IH_ContentTranslation_QB whereProducts($value)
     * @method _IH_ContentTranslation_QB whereDescription($value)
     * @method _IH_ContentTranslation_QB whereLocale($value)
     * @method _IH_ContentTranslation_QB whereCreatedAt($value)
     * @method _IH_ContentTranslation_QB whereUpdatedAt($value)
     * @method ContentTranslation baseSole(array|string $columns = ['*'])
     * @method ContentTranslation create(array $attributes = [])
     * @method _IH_ContentTranslation_C|ContentTranslation[] cursor()
     * @method ContentTranslation|null|_IH_ContentTranslation_C|ContentTranslation[] find($id, array $columns = ['*'])
     * @method _IH_ContentTranslation_C|ContentTranslation[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method ContentTranslation|_IH_ContentTranslation_C|ContentTranslation[] findOrFail($id, array $columns = ['*'])
     * @method ContentTranslation|_IH_ContentTranslation_C|ContentTranslation[] findOrNew($id, array $columns = ['*'])
     * @method ContentTranslation first(array|string $columns = ['*'])
     * @method ContentTranslation firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method ContentTranslation firstOrCreate(array $attributes = [], array $values = [])
     * @method ContentTranslation firstOrFail(array $columns = ['*'])
     * @method ContentTranslation firstOrNew(array $attributes = [], array $values = [])
     * @method ContentTranslation firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method ContentTranslation forceCreate(array $attributes)
     * @method _IH_ContentTranslation_C|ContentTranslation[] fromQuery(string $query, array $bindings = [])
     * @method _IH_ContentTranslation_C|ContentTranslation[] get(array|string $columns = ['*'])
     * @method ContentTranslation getModel()
     * @method ContentTranslation[] getModels(array|string $columns = ['*'])
     * @method _IH_ContentTranslation_C|ContentTranslation[] hydrate(array $items)
     * @method ContentTranslation make(array $attributes = [])
     * @method ContentTranslation newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|ContentTranslation[]|_IH_ContentTranslation_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|ContentTranslation[]|_IH_ContentTranslation_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method ContentTranslation sole(array|string $columns = ['*'])
     * @method ContentTranslation updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_ContentTranslation_QB extends _BaseBuilder {}
    
    /**
     * @method Content shift()
     * @method Content pop()
     * @method Content get($key, $default = null)
     * @method Content pull($key, $default = null)
     * @method Content first(callable $callback = null, $default = null)
     * @method Content firstWhere(string $key, $operator = null, $value = null)
     * @method Content find($key, $default = null)
     * @method Content[] all()
     * @method Content last(callable $callback = null, $default = null)
     * @method Content sole($key = null, $operator = null, $value = null)
     */
    class _IH_Content_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Content[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Content_QB whereId($value)
     * @method _IH_Content_QB whereContentType($value)
     * @method _IH_Content_QB wherePosition($value)
     * @method _IH_Content_QB whereStatus($value)
     * @method _IH_Content_QB whereCreatedAt($value)
     * @method _IH_Content_QB whereUpdatedAt($value)
     * @method Content baseSole(array|string $columns = ['*'])
     * @method Content create(array $attributes = [])
     * @method _IH_Content_C|Content[] cursor()
     * @method Content|null|_IH_Content_C|Content[] find($id, array $columns = ['*'])
     * @method _IH_Content_C|Content[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Content|_IH_Content_C|Content[] findOrFail($id, array $columns = ['*'])
     * @method Content|_IH_Content_C|Content[] findOrNew($id, array $columns = ['*'])
     * @method Content first(array|string $columns = ['*'])
     * @method Content firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Content firstOrCreate(array $attributes = [], array $values = [])
     * @method Content firstOrFail(array $columns = ['*'])
     * @method Content firstOrNew(array $attributes = [], array $values = [])
     * @method Content firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Content forceCreate(array $attributes)
     * @method _IH_Content_C|Content[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Content_C|Content[] get(array|string $columns = ['*'])
     * @method Content getModel()
     * @method Content[] getModels(array|string $columns = ['*'])
     * @method _IH_Content_C|Content[] hydrate(array $items)
     * @method Content make(array $attributes = [])
     * @method Content newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Content[]|_IH_Content_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Content[]|_IH_Content_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Content sole(array|string $columns = ['*'])
     * @method Content updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Content_QB listsTranslations(string $translationField)
     * @method _IH_Content_QB notTranslatedIn(null|string $locale = null)
     * @method _IH_Content_QB orWhereTranslation(string $translationField, $value, null|string $locale = null)
     * @method _IH_Content_QB orWhereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Content_QB orderByTranslation(string $translationField, string $sortMethod = 'asc')
     * @method _IH_Content_QB translated()
     * @method _IH_Content_QB translatedIn(null|string $locale = null)
     * @method _IH_Content_QB whereTranslation(string $translationField, $value, null|string $locale = null, string $method = 'whereHas', string $operator = '=')
     * @method _IH_Content_QB whereTranslationLike(string $translationField, $value, null|string $locale = null)
     * @method _IH_Content_QB withTranslation()
     */
    class _IH_Content_QB extends _BaseBuilder {}
    
    /**
     * @method OrderBrand shift()
     * @method OrderBrand pop()
     * @method OrderBrand get($key, $default = null)
     * @method OrderBrand pull($key, $default = null)
     * @method OrderBrand first(callable $callback = null, $default = null)
     * @method OrderBrand firstWhere(string $key, $operator = null, $value = null)
     * @method OrderBrand find($key, $default = null)
     * @method OrderBrand[] all()
     * @method OrderBrand last(callable $callback = null, $default = null)
     * @method OrderBrand sole($key = null, $operator = null, $value = null)
     */
    class _IH_OrderBrand_C extends _BaseCollection {
        /**
         * @param int $size
         * @return OrderBrand[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_OrderBrand_QB whereId($value)
     * @method _IH_OrderBrand_QB whereOrderId($value)
     * @method _IH_OrderBrand_QB whereOrderItemId($value)
     * @method _IH_OrderBrand_QB whereProductId($value)
     * @method _IH_OrderBrand_QB whereBrand($value)
     * @method _IH_OrderBrand_QB whereCreatedAt($value)
     * @method _IH_OrderBrand_QB whereUpdatedAt($value)
     * @method OrderBrand baseSole(array|string $columns = ['*'])
     * @method OrderBrand create(array $attributes = [])
     * @method _IH_OrderBrand_C|OrderBrand[] cursor()
     * @method OrderBrand|null|_IH_OrderBrand_C|OrderBrand[] find($id, array $columns = ['*'])
     * @method _IH_OrderBrand_C|OrderBrand[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method OrderBrand|_IH_OrderBrand_C|OrderBrand[] findOrFail($id, array $columns = ['*'])
     * @method OrderBrand|_IH_OrderBrand_C|OrderBrand[] findOrNew($id, array $columns = ['*'])
     * @method OrderBrand first(array|string $columns = ['*'])
     * @method OrderBrand firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method OrderBrand firstOrCreate(array $attributes = [], array $values = [])
     * @method OrderBrand firstOrFail(array $columns = ['*'])
     * @method OrderBrand firstOrNew(array $attributes = [], array $values = [])
     * @method OrderBrand firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method OrderBrand forceCreate(array $attributes)
     * @method _IH_OrderBrand_C|OrderBrand[] fromQuery(string $query, array $bindings = [])
     * @method _IH_OrderBrand_C|OrderBrand[] get(array|string $columns = ['*'])
     * @method OrderBrand getModel()
     * @method OrderBrand[] getModels(array|string $columns = ['*'])
     * @method _IH_OrderBrand_C|OrderBrand[] hydrate(array $items)
     * @method OrderBrand make(array $attributes = [])
     * @method OrderBrand newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|OrderBrand[]|_IH_OrderBrand_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|OrderBrand[]|_IH_OrderBrand_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method OrderBrand sole(array|string $columns = ['*'])
     * @method OrderBrand updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_OrderBrand_QB extends _BaseBuilder {}
    
    /**
     * @method VelocityCustomerCompareProduct shift()
     * @method VelocityCustomerCompareProduct pop()
     * @method VelocityCustomerCompareProduct get($key, $default = null)
     * @method VelocityCustomerCompareProduct pull($key, $default = null)
     * @method VelocityCustomerCompareProduct first(callable $callback = null, $default = null)
     * @method VelocityCustomerCompareProduct firstWhere(string $key, $operator = null, $value = null)
     * @method VelocityCustomerCompareProduct find($key, $default = null)
     * @method VelocityCustomerCompareProduct[] all()
     * @method VelocityCustomerCompareProduct last(callable $callback = null, $default = null)
     * @method VelocityCustomerCompareProduct sole($key = null, $operator = null, $value = null)
     */
    class _IH_VelocityCustomerCompareProduct_C extends _BaseCollection {
        /**
         * @param int $size
         * @return VelocityCustomerCompareProduct[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_VelocityCustomerCompareProduct_QB whereId($value)
     * @method _IH_VelocityCustomerCompareProduct_QB whereProductFlatId($value)
     * @method _IH_VelocityCustomerCompareProduct_QB whereCustomerId($value)
     * @method _IH_VelocityCustomerCompareProduct_QB whereCreatedAt($value)
     * @method _IH_VelocityCustomerCompareProduct_QB whereUpdatedAt($value)
     * @method VelocityCustomerCompareProduct baseSole(array|string $columns = ['*'])
     * @method VelocityCustomerCompareProduct create(array $attributes = [])
     * @method _IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] cursor()
     * @method VelocityCustomerCompareProduct|null|_IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] find($id, array $columns = ['*'])
     * @method _IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method VelocityCustomerCompareProduct|_IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] findOrFail($id, array $columns = ['*'])
     * @method VelocityCustomerCompareProduct|_IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] findOrNew($id, array $columns = ['*'])
     * @method VelocityCustomerCompareProduct first(array|string $columns = ['*'])
     * @method VelocityCustomerCompareProduct firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method VelocityCustomerCompareProduct firstOrCreate(array $attributes = [], array $values = [])
     * @method VelocityCustomerCompareProduct firstOrFail(array $columns = ['*'])
     * @method VelocityCustomerCompareProduct firstOrNew(array $attributes = [], array $values = [])
     * @method VelocityCustomerCompareProduct firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method VelocityCustomerCompareProduct forceCreate(array $attributes)
     * @method _IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] fromQuery(string $query, array $bindings = [])
     * @method _IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] get(array|string $columns = ['*'])
     * @method VelocityCustomerCompareProduct getModel()
     * @method VelocityCustomerCompareProduct[] getModels(array|string $columns = ['*'])
     * @method _IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] hydrate(array $items)
     * @method VelocityCustomerCompareProduct make(array $attributes = [])
     * @method VelocityCustomerCompareProduct newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|VelocityCustomerCompareProduct[]|_IH_VelocityCustomerCompareProduct_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|VelocityCustomerCompareProduct[]|_IH_VelocityCustomerCompareProduct_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method VelocityCustomerCompareProduct sole(array|string $columns = ['*'])
     * @method VelocityCustomerCompareProduct updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_VelocityCustomerCompareProduct_QB extends _BaseBuilder {}
    
    /**
     * @method VelocityMetadata shift()
     * @method VelocityMetadata pop()
     * @method VelocityMetadata get($key, $default = null)
     * @method VelocityMetadata pull($key, $default = null)
     * @method VelocityMetadata first(callable $callback = null, $default = null)
     * @method VelocityMetadata firstWhere(string $key, $operator = null, $value = null)
     * @method VelocityMetadata find($key, $default = null)
     * @method VelocityMetadata[] all()
     * @method VelocityMetadata last(callable $callback = null, $default = null)
     * @method VelocityMetadata sole($key = null, $operator = null, $value = null)
     */
    class _IH_VelocityMetadata_C extends _BaseCollection {
        /**
         * @param int $size
         * @return VelocityMetadata[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_VelocityMetadata_QB whereId($value)
     * @method _IH_VelocityMetadata_QB whereHomePageContent($value)
     * @method _IH_VelocityMetadata_QB whereFooterLeftContent($value)
     * @method _IH_VelocityMetadata_QB whereFooterMiddleContent($value)
     * @method _IH_VelocityMetadata_QB whereSlider($value)
     * @method _IH_VelocityMetadata_QB whereAdvertisement($value)
     * @method _IH_VelocityMetadata_QB whereSidebarCategoryCount($value)
     * @method _IH_VelocityMetadata_QB whereFeaturedProductCount($value)
     * @method _IH_VelocityMetadata_QB whereNewProductsCount($value)
     * @method _IH_VelocityMetadata_QB whereSubscriptionBarContent($value)
     * @method _IH_VelocityMetadata_QB whereCreatedAt($value)
     * @method _IH_VelocityMetadata_QB whereUpdatedAt($value)
     * @method _IH_VelocityMetadata_QB whereProductViewImages($value)
     * @method _IH_VelocityMetadata_QB whereProductPolicy($value)
     * @method _IH_VelocityMetadata_QB whereLocale($value)
     * @method _IH_VelocityMetadata_QB whereChannel($value)
     * @method _IH_VelocityMetadata_QB whereHeaderContentCount($value)
     * @method VelocityMetadata baseSole(array|string $columns = ['*'])
     * @method VelocityMetadata create(array $attributes = [])
     * @method _IH_VelocityMetadata_C|VelocityMetadata[] cursor()
     * @method VelocityMetadata|null|_IH_VelocityMetadata_C|VelocityMetadata[] find($id, array $columns = ['*'])
     * @method _IH_VelocityMetadata_C|VelocityMetadata[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method VelocityMetadata|_IH_VelocityMetadata_C|VelocityMetadata[] findOrFail($id, array $columns = ['*'])
     * @method VelocityMetadata|_IH_VelocityMetadata_C|VelocityMetadata[] findOrNew($id, array $columns = ['*'])
     * @method VelocityMetadata first(array|string $columns = ['*'])
     * @method VelocityMetadata firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method VelocityMetadata firstOrCreate(array $attributes = [], array $values = [])
     * @method VelocityMetadata firstOrFail(array $columns = ['*'])
     * @method VelocityMetadata firstOrNew(array $attributes = [], array $values = [])
     * @method VelocityMetadata firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method VelocityMetadata forceCreate(array $attributes)
     * @method _IH_VelocityMetadata_C|VelocityMetadata[] fromQuery(string $query, array $bindings = [])
     * @method _IH_VelocityMetadata_C|VelocityMetadata[] get(array|string $columns = ['*'])
     * @method VelocityMetadata getModel()
     * @method VelocityMetadata[] getModels(array|string $columns = ['*'])
     * @method _IH_VelocityMetadata_C|VelocityMetadata[] hydrate(array $items)
     * @method VelocityMetadata make(array $attributes = [])
     * @method VelocityMetadata newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|VelocityMetadata[]|_IH_VelocityMetadata_C paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|VelocityMetadata[]|_IH_VelocityMetadata_C simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method VelocityMetadata sole(array|string $columns = ['*'])
     * @method VelocityMetadata updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_VelocityMetadata_QB extends _BaseBuilder {}
}