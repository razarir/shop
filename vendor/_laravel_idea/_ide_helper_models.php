<?php
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnusedAliasInspection */

namespace  {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\_IH_Category_C;
    use LaravelIdea\Helper\_IH_Category_QB;
    use LaravelIdea\Helper\_IH_DuplicateCategory_C;
    use LaravelIdea\Helper\_IH_DuplicateCategory_QB;
    use LaravelIdea\Helper\_IH_MenuItem_C;
    use LaravelIdea\Helper\_IH_MenuItem_QB;
    
    /**
     * @property int $id
     * @property int $position
     * @property string|null $image
     * @property bool $status
     * @property int $_lft
     * @property int $_rgt
     * @property int|null $parent_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $display_mode
     * @property string|null $category_icon_path
     * @property string|null $additional
     * @method static _IH_Category_QB onWriteConnection()
     * @method _IH_Category_QB newQuery()
     * @method static _IH_Category_QB on()
     * @method static _IH_Category_QB query()
     * @method static _IH_Category_QB with()
     * @method _IH_Category_QB newModelQuery()
     * @method static _IH_Category_C|\Category[] all()
     * @mixin _IH_Category_QB
     */
    class Category extends Model {}
    
    /**
     * @property int $id
     * @property int $position
     * @property string|null $image
     * @property bool $status
     * @property int $_lft
     * @property int $_rgt
     * @property int|null $parent_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $display_mode
     * @property string|null $category_icon_path
     * @property string|null $additional
     * @method static _IH_DuplicateCategory_QB onWriteConnection()
     * @method _IH_DuplicateCategory_QB newQuery()
     * @method static _IH_DuplicateCategory_QB on()
     * @method static _IH_DuplicateCategory_QB query()
     * @method static _IH_DuplicateCategory_QB with()
     * @method _IH_DuplicateCategory_QB newModelQuery()
     * @method static _IH_DuplicateCategory_C|\DuplicateCategory[] all()
     * @mixin _IH_DuplicateCategory_QB
     */
    class DuplicateCategory extends Model {}
    
    /**
     * @method static _IH_MenuItem_QB onWriteConnection()
     * @method _IH_MenuItem_QB newQuery()
     * @method static _IH_MenuItem_QB on()
     * @method static _IH_MenuItem_QB query()
     * @method static _IH_MenuItem_QB with()
     * @method _IH_MenuItem_QB newModelQuery()
     * @method static _IH_MenuItem_C|\MenuItem[] all()
     * @mixin _IH_MenuItem_QB
     */
    class MenuItem extends Model {}
}

namespace Astrotomic\Translatable\Tests\Eloquent {

    use Illuminate\Database\Eloquent\Model;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_CountryStrict_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_CountryStrict_QB;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_CountryTranslation_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_CountryTranslation_QB;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_Country_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_Country_QB;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_PersonTranslation_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_PersonTranslation_QB;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_Person_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_Person_QB;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_StrictTranslation_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_StrictTranslation_QB;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_VegetableTranslation_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_VegetableTranslation_QB;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_Vegetable_C;
    use LaravelIdea\Helper\Astrotomic\Translatable\Tests\Eloquent\_IH_Vegetable_QB;
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @method static _IH_Country_QB onWriteConnection()
     * @method _IH_Country_QB newQuery()
     * @method static _IH_Country_QB on()
     * @method static _IH_Country_QB query()
     * @method static _IH_Country_QB with()
     * @method _IH_Country_QB newModelQuery()
     * @method static _IH_Country_C|Country[] all()
     * @mixin _IH_Country_QB
     */
    class Country extends Model {}
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @method static _IH_CountryStrict_QB onWriteConnection()
     * @method _IH_CountryStrict_QB newQuery()
     * @method static _IH_CountryStrict_QB on()
     * @method static _IH_CountryStrict_QB query()
     * @method static _IH_CountryStrict_QB with()
     * @method _IH_CountryStrict_QB newModelQuery()
     * @method static _IH_CountryStrict_C|CountryStrict[] all()
     * @mixin _IH_CountryStrict_QB
     */
    class CountryStrict extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $name
     * @property int $country_id
     * @method static _IH_CountryTranslation_QB onWriteConnection()
     * @method _IH_CountryTranslation_QB newQuery()
     * @method static _IH_CountryTranslation_QB on()
     * @method static _IH_CountryTranslation_QB query()
     * @method static _IH_CountryTranslation_QB with()
     * @method _IH_CountryTranslation_QB newModelQuery()
     * @method static _IH_CountryTranslation_C|CountryTranslation[] all()
     * @mixin _IH_CountryTranslation_QB
     */
    class CountryTranslation extends Model {}
    
    /**
     * @method static _IH_Person_QB onWriteConnection()
     * @method _IH_Person_QB newQuery()
     * @method static _IH_Person_QB on()
     * @method static _IH_Person_QB query()
     * @method static _IH_Person_QB with()
     * @method _IH_Person_QB newModelQuery()
     * @method static _IH_Person_C|Person[] all()
     * @mixin _IH_Person_QB
     */
    class Person extends Model {}
    
    /**
     * @method static _IH_PersonTranslation_QB onWriteConnection()
     * @method _IH_PersonTranslation_QB newQuery()
     * @method static _IH_PersonTranslation_QB on()
     * @method static _IH_PersonTranslation_QB query()
     * @method static _IH_PersonTranslation_QB with()
     * @method _IH_PersonTranslation_QB newModelQuery()
     * @method static _IH_PersonTranslation_C|PersonTranslation[] all()
     * @mixin _IH_PersonTranslation_QB
     */
    class PersonTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $name
     * @property int $country_id
     * @method static _IH_StrictTranslation_QB onWriteConnection()
     * @method _IH_StrictTranslation_QB newQuery()
     * @method static _IH_StrictTranslation_QB on()
     * @method static _IH_StrictTranslation_QB query()
     * @method static _IH_StrictTranslation_QB with()
     * @method _IH_StrictTranslation_QB newModelQuery()
     * @method static _IH_StrictTranslation_C|StrictTranslation[] all()
     * @mixin _IH_StrictTranslation_QB
     */
    class StrictTranslation extends Model {}
    
    /**
     * @method static _IH_Vegetable_QB onWriteConnection()
     * @method _IH_Vegetable_QB newQuery()
     * @method static _IH_Vegetable_QB on()
     * @method static _IH_Vegetable_QB query()
     * @method static _IH_Vegetable_QB with()
     * @method _IH_Vegetable_QB newModelQuery()
     * @method static _IH_Vegetable_C|Vegetable[] all()
     * @mixin _IH_Vegetable_QB
     */
    class Vegetable extends Model {}
    
    /**
     * @method static _IH_VegetableTranslation_QB onWriteConnection()
     * @method _IH_VegetableTranslation_QB newQuery()
     * @method static _IH_VegetableTranslation_QB on()
     * @method static _IH_VegetableTranslation_QB query()
     * @method static _IH_VegetableTranslation_QB with()
     * @method _IH_VegetableTranslation_QB newModelQuery()
     * @method static _IH_VegetableTranslation_C|VegetableTranslation[] all()
     * @mixin _IH_VegetableTranslation_QB
     */
    class VegetableTranslation extends Model {}
}

namespace ElasticScoutDriver\Tests\App {

    use Illuminate\Database\Eloquent\Model;
    use LaravelIdea\Helper\ElasticScoutDriver\Tests\App\_IH_Client_C;
    use LaravelIdea\Helper\ElasticScoutDriver\Tests\App\_IH_Client_QB;
    
    /**
     * @method static _IH_Client_QB onWriteConnection()
     * @method _IH_Client_QB newQuery()
     * @method static _IH_Client_QB on()
     * @method static _IH_Client_QB query()
     * @method static _IH_Client_QB with()
     * @method _IH_Client_QB newModelQuery()
     * @method static _IH_Client_C|Client[] all()
     * @mixin _IH_Client_QB
     */
    class Client extends Model {}
}

namespace Illuminate\Notifications {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\MorphTo;
    use LaravelIdea\Helper\Illuminate\Notifications\_IH_DatabaseNotification_C;
    use LaravelIdea\Helper\Illuminate\Notifications\_IH_DatabaseNotification_QB;
    
    /**
     * @property Model $notifiable
     * @method MorphTo notifiable()
     * @method static _IH_DatabaseNotification_QB onWriteConnection()
     * @method _IH_DatabaseNotification_QB newQuery()
     * @method static _IH_DatabaseNotification_QB on()
     * @method static _IH_DatabaseNotification_QB query()
     * @method static _IH_DatabaseNotification_QB with()
     * @method _IH_DatabaseNotification_QB newModelQuery()
     * @method static _IH_DatabaseNotification_C|DatabaseNotification[] all()
     * @mixin _IH_DatabaseNotification_QB
     */
    class DatabaseNotification extends Model {}
}

namespace Konekt\Enum\Eloquent\Tests\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Address_C;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Address_QB;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Client_C;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Client_QB;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Eloquent_C;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Eloquent_QB;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_OrderV2_C;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_OrderV2_QB;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_OrderVX_C;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_OrderVX_QB;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Order_C;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\_IH_Order_QB;
    
    /**
     * @property int $id
     * @property string $address_type
     * @property int|null $customer_id
     * @property int|null $cart_id
     * @property int|null $order_id
     * @property string $first_name
     * @property string $last_name
     * @property string|null $gender
     * @property string|null $company_name
     * @property string $address1
     * @property string|null $address2
     * @property string $postcode
     * @property string $city
     * @property string $state
     * @property string $country
     * @property string|null $email
     * @property string|null $phone
     * @property string|null $vat_id
     * @property bool $default_address
     * @property string|null $additional
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_Address_QB onWriteConnection()
     * @method _IH_Address_QB newQuery()
     * @method static _IH_Address_QB on()
     * @method static _IH_Address_QB query()
     * @method static _IH_Address_QB with()
     * @method _IH_Address_QB newModelQuery()
     * @method static _IH_Address_C|Address[] all()
     * @mixin _IH_Address_QB
     */
    class Address extends Model {}
    
    /**
     * @method static _IH_Client_QB onWriteConnection()
     * @method _IH_Client_QB newQuery()
     * @method static _IH_Client_QB on()
     * @method static _IH_Client_QB query()
     * @method static _IH_Client_QB with()
     * @method _IH_Client_QB newModelQuery()
     * @method static _IH_Client_C|Client[] all()
     * @mixin _IH_Client_QB
     */
    class Client extends Model {}
    
    /**
     * @method static _IH_Eloquent_QB onWriteConnection()
     * @method _IH_Eloquent_QB newQuery()
     * @method static _IH_Eloquent_QB on()
     * @method static _IH_Eloquent_QB query()
     * @method static _IH_Eloquent_QB with()
     * @method _IH_Eloquent_QB newModelQuery()
     * @method static _IH_Eloquent_C|Eloquent[] all()
     * @mixin _IH_Eloquent_QB
     */
    class Eloquent extends Model {}
    
    /**
     * @property int $id
     * @property string $increment_id
     * @property string|null $status
     * @property string|null $channel_name
     * @property bool|null $is_guest
     * @property string|null $customer_email
     * @property string|null $customer_first_name
     * @property string|null $customer_last_name
     * @property string|null $customer_company_name
     * @property string|null $customer_vat_id
     * @property string|null $shipping_method
     * @property string|null $shipping_title
     * @property string|null $shipping_description
     * @property string|null $coupon_code
     * @property bool $is_gift
     * @property int|null $total_item_count
     * @property int|null $total_qty_ordered
     * @property string|null $base_currency_code
     * @property string|null $channel_currency_code
     * @property string|null $order_currency_code
     * @property float|null $grand_total
     * @property float|null $base_grand_total
     * @property float|null $grand_total_invoiced
     * @property float|null $base_grand_total_invoiced
     * @property float|null $grand_total_refunded
     * @property float|null $base_grand_total_refunded
     * @property float|null $sub_total
     * @property float|null $base_sub_total
     * @property float|null $sub_total_invoiced
     * @property float|null $base_sub_total_invoiced
     * @property float|null $sub_total_refunded
     * @property float|null $base_sub_total_refunded
     * @property float|null $discount_percent
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property float|null $discount_invoiced
     * @property float|null $base_discount_invoiced
     * @property float|null $discount_refunded
     * @property float|null $base_discount_refunded
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float|null $tax_amount_invoiced
     * @property float|null $base_tax_amount_invoiced
     * @property float|null $tax_amount_refunded
     * @property float|null $base_tax_amount_refunded
     * @property float|null $shipping_amount
     * @property float|null $base_shipping_amount
     * @property float|null $shipping_invoiced
     * @property float|null $base_shipping_invoiced
     * @property float|null $shipping_refunded
     * @property float|null $base_shipping_refunded
     * @property int|null $customer_id
     * @property string|null $customer_type
     * @property int|null $channel_id
     * @property string|null $channel_type
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int|null $cart_id
     * @property string|null $applied_cart_rule_ids
     * @property float|null $shipping_discount_amount
     * @property float|null $base_shipping_discount_amount
     * @property Client $client
     * @method BelongsTo|_IH_Client_QB client()
     * @method static _IH_Order_QB onWriteConnection()
     * @method _IH_Order_QB newQuery()
     * @method static _IH_Order_QB on()
     * @method static _IH_Order_QB query()
     * @method static _IH_Order_QB with()
     * @method _IH_Order_QB newModelQuery()
     * @method static _IH_Order_C|Order[] all()
     * @mixin _IH_Order_QB
     */
    class Order extends Model {}
    
    /**
     * @property int $id
     * @property string $increment_id
     * @property string|null $status
     * @property string|null $channel_name
     * @property bool|null $is_guest
     * @property string|null $customer_email
     * @property string|null $customer_first_name
     * @property string|null $customer_last_name
     * @property string|null $customer_company_name
     * @property string|null $customer_vat_id
     * @property string|null $shipping_method
     * @property string|null $shipping_title
     * @property string|null $shipping_description
     * @property string|null $coupon_code
     * @property bool $is_gift
     * @property int|null $total_item_count
     * @property int|null $total_qty_ordered
     * @property string|null $base_currency_code
     * @property string|null $channel_currency_code
     * @property string|null $order_currency_code
     * @property float|null $grand_total
     * @property float|null $base_grand_total
     * @property float|null $grand_total_invoiced
     * @property float|null $base_grand_total_invoiced
     * @property float|null $grand_total_refunded
     * @property float|null $base_grand_total_refunded
     * @property float|null $sub_total
     * @property float|null $base_sub_total
     * @property float|null $sub_total_invoiced
     * @property float|null $base_sub_total_invoiced
     * @property float|null $sub_total_refunded
     * @property float|null $base_sub_total_refunded
     * @property float|null $discount_percent
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property float|null $discount_invoiced
     * @property float|null $base_discount_invoiced
     * @property float|null $discount_refunded
     * @property float|null $base_discount_refunded
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float|null $tax_amount_invoiced
     * @property float|null $base_tax_amount_invoiced
     * @property float|null $tax_amount_refunded
     * @property float|null $base_tax_amount_refunded
     * @property float|null $shipping_amount
     * @property float|null $base_shipping_amount
     * @property float|null $shipping_invoiced
     * @property float|null $base_shipping_invoiced
     * @property float|null $shipping_refunded
     * @property float|null $base_shipping_refunded
     * @property int|null $customer_id
     * @property string|null $customer_type
     * @property int|null $channel_id
     * @property string|null $channel_type
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int|null $cart_id
     * @property string|null $applied_cart_rule_ids
     * @property float|null $shipping_discount_amount
     * @property float|null $base_shipping_discount_amount
     * @property Client $client
     * @method BelongsTo|_IH_Client_QB client()
     * @method static _IH_OrderV2_QB onWriteConnection()
     * @method _IH_OrderV2_QB newQuery()
     * @method static _IH_OrderV2_QB on()
     * @method static _IH_OrderV2_QB query()
     * @method static _IH_OrderV2_QB with()
     * @method _IH_OrderV2_QB newModelQuery()
     * @method static _IH_OrderV2_C|OrderV2[] all()
     * @mixin _IH_OrderV2_QB
     */
    class OrderV2 extends Model {}
    
    /**
     * @property Client $client
     * @method BelongsTo|_IH_Client_QB client()
     * @method static _IH_OrderVX_QB onWriteConnection()
     * @method _IH_OrderVX_QB newQuery()
     * @method static _IH_OrderVX_QB on()
     * @method static _IH_OrderVX_QB query()
     * @method static _IH_OrderVX_QB with()
     * @method _IH_OrderVX_QB newModelQuery()
     * @method static _IH_OrderVX_C|OrderVX[] all()
     * @mixin _IH_OrderVX_QB
     */
    class OrderVX extends Model {}
}

namespace Konekt\Enum\Eloquent\Tests\Models\Extended {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\Extended\_IH_Address_C;
    use LaravelIdea\Helper\Konekt\Enum\Eloquent\Tests\Models\Extended\_IH_Address_QB;
    
    /**
     * @property int $id
     * @property string $address_type
     * @property int|null $customer_id
     * @property int|null $cart_id
     * @property int|null $order_id
     * @property string $first_name
     * @property string $last_name
     * @property string|null $gender
     * @property string|null $company_name
     * @property string $address1
     * @property string|null $address2
     * @property string $postcode
     * @property string $city
     * @property string $state
     * @property string $country
     * @property string|null $email
     * @property string|null $phone
     * @property string|null $vat_id
     * @property bool $default_address
     * @property string|null $additional
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_Address_QB onWriteConnection()
     * @method _IH_Address_QB newQuery()
     * @method static _IH_Address_QB on()
     * @method static _IH_Address_QB query()
     * @method static _IH_Address_QB with()
     * @method _IH_Address_QB newModelQuery()
     * @method static _IH_Address_C|Address[] all()
     * @mixin _IH_Address_QB
     */
    class Address extends Model {}
}

namespace Webkul\Attribute\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeFamily_C;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeFamily_QB;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeGroup_C;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeGroup_QB;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeOptionTranslation_C;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeOptionTranslation_QB;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeOption_C;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeOption_QB;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeTranslation_C;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_AttributeTranslation_QB;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_Attribute_C;
    use LaravelIdea\Helper\Webkul\Attribute\Models\_IH_Attribute_QB;
    use Webkul\Attribute\Database\Factories\AttributeFactory;
    use Webkul\Attribute\Database\Factories\AttributeFamilyFactory;
    use Webkul\Attribute\Database\Factories\AttributeOptionFactory;
    
    /**
     * @property int $id
     * @property string $code
     * @property string $admin_name
     * @property string $type
     * @property string|null $validation
     * @property int|null $position
     * @property bool $is_required
     * @property bool $is_unique
     * @property bool $value_per_locale
     * @property bool $value_per_channel
     * @property bool $is_filterable
     * @property bool $is_configurable
     * @property bool $is_user_defined
     * @property bool $is_visible_on_front
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $swatch_type
     * @property bool $use_in_flat
     * @property bool $is_comparable
     * @method static _IH_Attribute_QB onWriteConnection()
     * @method _IH_Attribute_QB newQuery()
     * @method static _IH_Attribute_QB on()
     * @method static _IH_Attribute_QB query()
     * @method static _IH_Attribute_QB with()
     * @method _IH_Attribute_QB newModelQuery()
     * @method static _IH_Attribute_C|Attribute[] all()
     * @mixin _IH_Attribute_QB
     * @method static AttributeFactory factory(...$parameters)
     */
    class Attribute extends Model {}
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @property bool $status
     * @property bool $is_user_defined
     * @property-read $configurable_attributes
     * @property-read $custom_attributes
     * @method static _IH_AttributeFamily_QB onWriteConnection()
     * @method _IH_AttributeFamily_QB newQuery()
     * @method static _IH_AttributeFamily_QB on()
     * @method static _IH_AttributeFamily_QB query()
     * @method static _IH_AttributeFamily_QB with()
     * @method _IH_AttributeFamily_QB newModelQuery()
     * @method static _IH_AttributeFamily_C|AttributeFamily[] all()
     * @mixin _IH_AttributeFamily_QB
     * @method static AttributeFamilyFactory factory(...$parameters)
     */
    class AttributeFamily extends Model {}
    
    /**
     * @property int $id
     * @property string $name
     * @property int $position
     * @property bool $is_user_defined
     * @property int $attribute_family_id
     * @method static _IH_AttributeGroup_QB onWriteConnection()
     * @method _IH_AttributeGroup_QB newQuery()
     * @method static _IH_AttributeGroup_QB on()
     * @method static _IH_AttributeGroup_QB query()
     * @method static _IH_AttributeGroup_QB with()
     * @method _IH_AttributeGroup_QB newModelQuery()
     * @method static _IH_AttributeGroup_C|AttributeGroup[] all()
     * @mixin _IH_AttributeGroup_QB
     */
    class AttributeGroup extends Model {}
    
    /**
     * @property int $id
     * @property string|null $admin_name
     * @property int|null $sort_order
     * @property int $attribute_id
     * @property string|null $swatch_value
     * @property-read null $swatch_value_url
     * @method static _IH_AttributeOption_QB onWriteConnection()
     * @method _IH_AttributeOption_QB newQuery()
     * @method static _IH_AttributeOption_QB on()
     * @method static _IH_AttributeOption_QB query()
     * @method static _IH_AttributeOption_QB with()
     * @method _IH_AttributeOption_QB newModelQuery()
     * @method static _IH_AttributeOption_C|AttributeOption[] all()
     * @mixin _IH_AttributeOption_QB
     * @method static AttributeOptionFactory factory(...$parameters)
     */
    class AttributeOption extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $label
     * @property int $attribute_option_id
     * @method static _IH_AttributeOptionTranslation_QB onWriteConnection()
     * @method _IH_AttributeOptionTranslation_QB newQuery()
     * @method static _IH_AttributeOptionTranslation_QB on()
     * @method static _IH_AttributeOptionTranslation_QB query()
     * @method static _IH_AttributeOptionTranslation_QB with()
     * @method _IH_AttributeOptionTranslation_QB newModelQuery()
     * @method static _IH_AttributeOptionTranslation_C|AttributeOptionTranslation[] all()
     * @mixin _IH_AttributeOptionTranslation_QB
     */
    class AttributeOptionTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $name
     * @property int $attribute_id
     * @method static _IH_AttributeTranslation_QB onWriteConnection()
     * @method _IH_AttributeTranslation_QB newQuery()
     * @method static _IH_AttributeTranslation_QB on()
     * @method static _IH_AttributeTranslation_QB query()
     * @method static _IH_AttributeTranslation_QB with()
     * @method _IH_AttributeTranslation_QB newModelQuery()
     * @method static _IH_AttributeTranslation_C|AttributeTranslation[] all()
     * @mixin _IH_AttributeTranslation_QB
     */
    class AttributeTranslation extends Model {}
}

namespace Webkul\BookingProduct\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductAppointmentSlot_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductAppointmentSlot_QB;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductDefaultSlot_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductDefaultSlot_QB;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductEventTicketTranslation_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductEventTicketTranslation_QB;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductEventTicket_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductEventTicket_QB;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductRentalSlot_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductRentalSlot_QB;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductTableSlot_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProductTableSlot_QB;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProduct_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_BookingProduct_QB;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_Booking_C;
    use LaravelIdea\Helper\Webkul\BookingProduct\Models\_IH_Booking_QB;
    use Webkul\BookingProduct\Database\Factories\BookingProductEventTicketFactory;
    use Webkul\BookingProduct\Database\Factories\BookingProductFactory;
    
    /**
     * @property int $id
     * @property int|null $qty
     * @property int|null $from
     * @property int|null $to
     * @property int|null $order_item_id
     * @property int|null $booking_product_event_ticket_id
     * @property int|null $order_id
     * @property int|null $product_id
     * @method static _IH_Booking_QB onWriteConnection()
     * @method _IH_Booking_QB newQuery()
     * @method static _IH_Booking_QB on()
     * @method static _IH_Booking_QB query()
     * @method static _IH_Booking_QB with()
     * @method _IH_Booking_QB newModelQuery()
     * @method static _IH_Booking_C|Booking[] all()
     * @mixin _IH_Booking_QB
     */
    class Booking extends Model {}
    
    /**
     * @property int $id
     * @property string $type
     * @property int|null $qty
     * @property string|null $location
     * @property bool $show_location
     * @property bool|null $available_every_week
     * @property Carbon|null $available_from
     * @property Carbon|null $available_to
     * @property int $product_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_BookingProduct_QB onWriteConnection()
     * @method _IH_BookingProduct_QB newQuery()
     * @method static _IH_BookingProduct_QB on()
     * @method static _IH_BookingProduct_QB query()
     * @method static _IH_BookingProduct_QB with()
     * @method _IH_BookingProduct_QB newModelQuery()
     * @method static _IH_BookingProduct_C|BookingProduct[] all()
     * @mixin _IH_BookingProduct_QB
     * @method static BookingProductFactory factory(...$parameters)
     */
    class BookingProduct extends Model {}
    
    /**
     * @property int $id
     * @property int|null $duration
     * @property int|null $break_time
     * @property bool|null $same_slot_all_days
     * @property array|null $slots
     * @property int $booking_product_id
     * @method static _IH_BookingProductAppointmentSlot_QB onWriteConnection()
     * @method _IH_BookingProductAppointmentSlot_QB newQuery()
     * @method static _IH_BookingProductAppointmentSlot_QB on()
     * @method static _IH_BookingProductAppointmentSlot_QB query()
     * @method static _IH_BookingProductAppointmentSlot_QB with()
     * @method _IH_BookingProductAppointmentSlot_QB newModelQuery()
     * @method static _IH_BookingProductAppointmentSlot_C|BookingProductAppointmentSlot[] all()
     * @mixin _IH_BookingProductAppointmentSlot_QB
     */
    class BookingProductAppointmentSlot extends Model {}
    
    /**
     * @property int $id
     * @property string $booking_type
     * @property int|null $duration
     * @property int|null $break_time
     * @property array|null $slots
     * @property int $booking_product_id
     * @method static _IH_BookingProductDefaultSlot_QB onWriteConnection()
     * @method _IH_BookingProductDefaultSlot_QB newQuery()
     * @method static _IH_BookingProductDefaultSlot_QB on()
     * @method static _IH_BookingProductDefaultSlot_QB query()
     * @method static _IH_BookingProductDefaultSlot_QB with()
     * @method _IH_BookingProductDefaultSlot_QB newModelQuery()
     * @method static _IH_BookingProductDefaultSlot_C|BookingProductDefaultSlot[] all()
     * @mixin _IH_BookingProductDefaultSlot_QB
     */
    class BookingProductDefaultSlot extends Model {}
    
    /**
     * @property int $id
     * @property float|null $price
     * @property int|null $qty
     * @property float|null $special_price
     * @property Carbon|null $special_price_from
     * @property Carbon|null $special_price_to
     * @property int $booking_product_id
     * @method static _IH_BookingProductEventTicket_QB onWriteConnection()
     * @method _IH_BookingProductEventTicket_QB newQuery()
     * @method static _IH_BookingProductEventTicket_QB on()
     * @method static _IH_BookingProductEventTicket_QB query()
     * @method static _IH_BookingProductEventTicket_QB with()
     * @method _IH_BookingProductEventTicket_QB newModelQuery()
     * @method static _IH_BookingProductEventTicket_C|BookingProductEventTicket[] all()
     * @mixin _IH_BookingProductEventTicket_QB
     * @method static BookingProductEventTicketFactory factory(...$parameters)
     */
    class BookingProductEventTicket extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $name
     * @property string|null $description
     * @property int $booking_product_event_ticket_id
     * @method static _IH_BookingProductEventTicketTranslation_QB onWriteConnection()
     * @method _IH_BookingProductEventTicketTranslation_QB newQuery()
     * @method static _IH_BookingProductEventTicketTranslation_QB on()
     * @method static _IH_BookingProductEventTicketTranslation_QB query()
     * @method static _IH_BookingProductEventTicketTranslation_QB with()
     * @method _IH_BookingProductEventTicketTranslation_QB newModelQuery()
     * @method static _IH_BookingProductEventTicketTranslation_C|BookingProductEventTicketTranslation[] all()
     * @mixin _IH_BookingProductEventTicketTranslation_QB
     */
    class BookingProductEventTicketTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string $renting_type
     * @property float|null $daily_price
     * @property float|null $hourly_price
     * @property bool|null $same_slot_all_days
     * @property array|null $slots
     * @property int $booking_product_id
     * @method static _IH_BookingProductRentalSlot_QB onWriteConnection()
     * @method _IH_BookingProductRentalSlot_QB newQuery()
     * @method static _IH_BookingProductRentalSlot_QB on()
     * @method static _IH_BookingProductRentalSlot_QB query()
     * @method static _IH_BookingProductRentalSlot_QB with()
     * @method _IH_BookingProductRentalSlot_QB newModelQuery()
     * @method static _IH_BookingProductRentalSlot_C|BookingProductRentalSlot[] all()
     * @mixin _IH_BookingProductRentalSlot_QB
     */
    class BookingProductRentalSlot extends Model {}
    
    /**
     * @property int $id
     * @property string $price_type
     * @property int $guest_limit
     * @property int $duration
     * @property int $break_time
     * @property int $prevent_scheduling_before
     * @property bool|null $same_slot_all_days
     * @property array|null $slots
     * @property int $booking_product_id
     * @method static _IH_BookingProductTableSlot_QB onWriteConnection()
     * @method _IH_BookingProductTableSlot_QB newQuery()
     * @method static _IH_BookingProductTableSlot_QB on()
     * @method static _IH_BookingProductTableSlot_QB query()
     * @method static _IH_BookingProductTableSlot_QB with()
     * @method _IH_BookingProductTableSlot_QB newModelQuery()
     * @method static _IH_BookingProductTableSlot_C|BookingProductTableSlot[] all()
     * @mixin _IH_BookingProductTableSlot_QB
     */
    class BookingProductTableSlot extends Model {}
}

namespace Webkul\CMS\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\CMS\Models\_IH_CmsPageTranslation_C;
    use LaravelIdea\Helper\Webkul\CMS\Models\_IH_CmsPageTranslation_QB;
    use LaravelIdea\Helper\Webkul\CMS\Models\_IH_CmsPage_C;
    use LaravelIdea\Helper\Webkul\CMS\Models\_IH_CmsPage_QB;
    
    /**
     * @property int $id
     * @property string|null $layout
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_CmsPage_QB onWriteConnection()
     * @method _IH_CmsPage_QB newQuery()
     * @method static _IH_CmsPage_QB on()
     * @method static _IH_CmsPage_QB query()
     * @method static _IH_CmsPage_QB with()
     * @method _IH_CmsPage_QB newModelQuery()
     * @method static _IH_CmsPage_C|CmsPage[] all()
     * @mixin _IH_CmsPage_QB
     */
    class CmsPage extends Model {}
    
    /**
     * @property int $id
     * @property string $page_title
     * @property string $url_key
     * @property string|null $html_content
     * @property string|null $meta_title
     * @property string|null $meta_description
     * @property string|null $meta_keywords
     * @property string $locale
     * @property int $cms_page_id
     * @method static _IH_CmsPageTranslation_QB onWriteConnection()
     * @method _IH_CmsPageTranslation_QB newQuery()
     * @method static _IH_CmsPageTranslation_QB on()
     * @method static _IH_CmsPageTranslation_QB query()
     * @method static _IH_CmsPageTranslation_QB with()
     * @method _IH_CmsPageTranslation_QB newModelQuery()
     * @method static _IH_CmsPageTranslation_C|CmsPageTranslation[] all()
     * @mixin _IH_CmsPageTranslation_QB
     */
    class CmsPageTranslation extends Model {}
}

namespace Webkul\CartRule\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleCouponUsage_C;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleCouponUsage_QB;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleCoupon_C;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleCoupon_QB;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleCustomer_C;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleCustomer_QB;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleTranslation_C;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRuleTranslation_QB;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRule_C;
    use LaravelIdea\Helper\Webkul\CartRule\Models\_IH_CartRule_QB;
    use Webkul\Core\Database\Factories\CartRuleCouponFactory;
    use Webkul\Core\Database\Factories\CartRuleFactory;
    
    /**
     * @property int $id
     * @property string|null $name
     * @property string|null $description
     * @property Carbon|null $starts_from
     * @property Carbon|null $ends_till
     * @property bool $status
     * @property int $coupon_type
     * @property bool $use_auto_generation
     * @property int $usage_per_customer
     * @property int $uses_per_coupon
     * @property int $times_used
     * @property bool $condition_type
     * @property array|null $conditions
     * @property bool $end_other_rules
     * @property bool $uses_attribute_conditions
     * @property string|null $action_type
     * @property float $discount_amount
     * @property int $discount_quantity
     * @property string $discount_step
     * @property bool $apply_to_shipping
     * @property bool $free_shipping
     * @property int $sort_order
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read string|void $coupon_code
     * @method static _IH_CartRule_QB onWriteConnection()
     * @method _IH_CartRule_QB newQuery()
     * @method static _IH_CartRule_QB on()
     * @method static _IH_CartRule_QB query()
     * @method static _IH_CartRule_QB with()
     * @method _IH_CartRule_QB newModelQuery()
     * @method static _IH_CartRule_C|CartRule[] all()
     * @mixin _IH_CartRule_QB
     * @method static CartRuleFactory factory(...$parameters)
     */
    class CartRule extends Model {}
    
    /**
     * @property int $id
     * @property string|null $code
     * @property int $usage_limit
     * @property int $usage_per_customer
     * @property int $times_used
     * @property int $type
     * @property bool $is_primary
     * @property Carbon|null $expired_at
     * @property int $cart_rule_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_CartRuleCoupon_QB onWriteConnection()
     * @method _IH_CartRuleCoupon_QB newQuery()
     * @method static _IH_CartRuleCoupon_QB on()
     * @method static _IH_CartRuleCoupon_QB query()
     * @method static _IH_CartRuleCoupon_QB with()
     * @method _IH_CartRuleCoupon_QB newModelQuery()
     * @method static _IH_CartRuleCoupon_C|CartRuleCoupon[] all()
     * @mixin _IH_CartRuleCoupon_QB
     * @method static CartRuleCouponFactory factory(...$parameters)
     */
    class CartRuleCoupon extends Model {}
    
    /**
     * @property int $id
     * @property int $times_used
     * @property int $cart_rule_coupon_id
     * @property int $customer_id
     * @method static _IH_CartRuleCouponUsage_QB onWriteConnection()
     * @method _IH_CartRuleCouponUsage_QB newQuery()
     * @method static _IH_CartRuleCouponUsage_QB on()
     * @method static _IH_CartRuleCouponUsage_QB query()
     * @method static _IH_CartRuleCouponUsage_QB with()
     * @method _IH_CartRuleCouponUsage_QB newModelQuery()
     * @method static _IH_CartRuleCouponUsage_C|CartRuleCouponUsage[] all()
     * @mixin _IH_CartRuleCouponUsage_QB
     */
    class CartRuleCouponUsage extends Model {}
    
    /**
     * @property int $id
     * @property int $times_used
     * @property int $cart_rule_id
     * @property int $customer_id
     * @method static _IH_CartRuleCustomer_QB onWriteConnection()
     * @method _IH_CartRuleCustomer_QB newQuery()
     * @method static _IH_CartRuleCustomer_QB on()
     * @method static _IH_CartRuleCustomer_QB query()
     * @method static _IH_CartRuleCustomer_QB with()
     * @method _IH_CartRuleCustomer_QB newModelQuery()
     * @method static _IH_CartRuleCustomer_C|CartRuleCustomer[] all()
     * @mixin _IH_CartRuleCustomer_QB
     */
    class CartRuleCustomer extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $label
     * @property int $cart_rule_id
     * @method static _IH_CartRuleTranslation_QB onWriteConnection()
     * @method _IH_CartRuleTranslation_QB newQuery()
     * @method static _IH_CartRuleTranslation_QB on()
     * @method static _IH_CartRuleTranslation_QB query()
     * @method static _IH_CartRuleTranslation_QB with()
     * @method _IH_CartRuleTranslation_QB newModelQuery()
     * @method static _IH_CartRuleTranslation_C|CartRuleTranslation[] all()
     * @mixin _IH_CartRuleTranslation_QB
     */
    class CartRuleTranslation extends Model {}
}

namespace Webkul\CatalogRule\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\CatalogRule\Models\_IH_CatalogRuleProductPrice_C;
    use LaravelIdea\Helper\Webkul\CatalogRule\Models\_IH_CatalogRuleProductPrice_QB;
    use LaravelIdea\Helper\Webkul\CatalogRule\Models\_IH_CatalogRuleProduct_C;
    use LaravelIdea\Helper\Webkul\CatalogRule\Models\_IH_CatalogRuleProduct_QB;
    use LaravelIdea\Helper\Webkul\CatalogRule\Models\_IH_CatalogRule_C;
    use LaravelIdea\Helper\Webkul\CatalogRule\Models\_IH_CatalogRule_QB;
    
    /**
     * @property int $id
     * @property string|null $name
     * @property string|null $description
     * @property Carbon|null $starts_from
     * @property Carbon|null $ends_till
     * @property bool $status
     * @property bool $condition_type
     * @property array|null $conditions
     * @property bool $end_other_rules
     * @property string|null $action_type
     * @property float $discount_amount
     * @property int $sort_order
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_CatalogRule_QB onWriteConnection()
     * @method _IH_CatalogRule_QB newQuery()
     * @method static _IH_CatalogRule_QB on()
     * @method static _IH_CatalogRule_QB query()
     * @method static _IH_CatalogRule_QB with()
     * @method _IH_CatalogRule_QB newModelQuery()
     * @method static _IH_CatalogRule_C|CatalogRule[] all()
     * @mixin _IH_CatalogRule_QB
     */
    class CatalogRule extends Model {}
    
    /**
     * @property int $id
     * @property Carbon|null $starts_from
     * @property Carbon|null $ends_till
     * @property bool $end_other_rules
     * @property string|null $action_type
     * @property float $discount_amount
     * @property int $sort_order
     * @property int $product_id
     * @property int $customer_group_id
     * @property int $catalog_rule_id
     * @property int $channel_id
     * @method static _IH_CatalogRuleProduct_QB onWriteConnection()
     * @method _IH_CatalogRuleProduct_QB newQuery()
     * @method static _IH_CatalogRuleProduct_QB on()
     * @method static _IH_CatalogRuleProduct_QB query()
     * @method static _IH_CatalogRuleProduct_QB with()
     * @method _IH_CatalogRuleProduct_QB newModelQuery()
     * @method static _IH_CatalogRuleProduct_C|CatalogRuleProduct[] all()
     * @mixin _IH_CatalogRuleProduct_QB
     */
    class CatalogRuleProduct extends Model {}
    
    /**
     * @property int $id
     * @property float $price
     * @property Carbon $rule_date
     * @property Carbon|null $starts_from
     * @property Carbon|null $ends_till
     * @property int $product_id
     * @property int $customer_group_id
     * @property int $catalog_rule_id
     * @property int $channel_id
     * @method static _IH_CatalogRuleProductPrice_QB onWriteConnection()
     * @method _IH_CatalogRuleProductPrice_QB newQuery()
     * @method static _IH_CatalogRuleProductPrice_QB on()
     * @method static _IH_CatalogRuleProductPrice_QB query()
     * @method static _IH_CatalogRuleProductPrice_QB with()
     * @method _IH_CatalogRuleProductPrice_QB newModelQuery()
     * @method static _IH_CatalogRuleProductPrice_C|CatalogRuleProductPrice[] all()
     * @mixin _IH_CatalogRuleProductPrice_QB
     */
    class CatalogRuleProductPrice extends Model {}
}

namespace Webkul\Category\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Category\Models\_IH_CategoryTranslation_C;
    use LaravelIdea\Helper\Webkul\Category\Models\_IH_CategoryTranslation_QB;
    use LaravelIdea\Helper\Webkul\Category\Models\_IH_Category_C;
    use LaravelIdea\Helper\Webkul\Category\Models\_IH_Category_QB;
    use Webkul\Category\Database\Factories\CategoryFactory;
    
    /**
     * @property int $id
     * @property int $position
     * @property string|null $image
     * @property bool $status
     * @property int $_lft
     * @property int $_rgt
     * @property int|null $parent_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $display_mode
     * @property string|null $category_icon_path
     * @property string|null $additional
     * @property-read void $image_url
     * @method static _IH_Category_QB onWriteConnection()
     * @method _IH_Category_QB newQuery()
     * @method static _IH_Category_QB on()
     * @method static _IH_Category_QB query()
     * @method static _IH_Category_QB with()
     * @method _IH_Category_QB newModelQuery()
     * @method static _IH_Category_C|Category[] all()
     * @mixin _IH_Category_QB
     * @method static CategoryFactory factory(...$parameters)
     */
    class Category extends Model {}
    
    /**
     * @property int $id
     * @property string $name
     * @property string $slug
     * @property string|null $description
     * @property string|null $meta_title
     * @property string|null $meta_description
     * @property string|null $meta_keywords
     * @property int $category_id
     * @property string $locale
     * @property int|null $locale_id
     * @property string $url_path
     * @method static _IH_CategoryTranslation_QB onWriteConnection()
     * @method _IH_CategoryTranslation_QB newQuery()
     * @method static _IH_CategoryTranslation_QB on()
     * @method static _IH_CategoryTranslation_QB query()
     * @method static _IH_CategoryTranslation_QB with()
     * @method _IH_CategoryTranslation_QB newModelQuery()
     * @method static _IH_CategoryTranslation_C|CategoryTranslation[] all()
     * @mixin _IH_CategoryTranslation_QB
     */
    class CategoryTranslation extends Model {}
}

namespace Webkul\Checkout\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\HasMany;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartAddress_C;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartAddress_QB;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartItem_C;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartItem_QB;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartPayment_C;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartPayment_QB;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartShippingRate_C;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_CartShippingRate_QB;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_Cart_C;
    use LaravelIdea\Helper\Webkul\Checkout\Models\_IH_Cart_QB;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_Customer_QB;
    use Webkul\Checkout\Database\Factories\CartAddressFactory;
    use Webkul\Checkout\Database\Factories\CartFactory;
    use Webkul\Checkout\Database\Factories\CartItemFactory;
    use Webkul\Checkout\Database\Factories\CartPaymentFactory;
    use Webkul\Customer\Models\Customer;
    
    /**
     * @property int $id
     * @property string|null $customer_email
     * @property string|null $customer_first_name
     * @property string|null $customer_last_name
     * @property string|null $shipping_method
     * @property string|null $coupon_code
     * @property bool $is_gift
     * @property int|null $items_count
     * @property float|null $items_qty
     * @property float|null $exchange_rate
     * @property string|null $global_currency_code
     * @property string|null $base_currency_code
     * @property string|null $channel_currency_code
     * @property string|null $cart_currency_code
     * @property float|null $grand_total
     * @property float|null $base_grand_total
     * @property float|null $sub_total
     * @property float|null $base_sub_total
     * @property float|null $tax_total
     * @property float|null $base_tax_total
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property string|null $checkout_method
     * @property bool|null $is_guest
     * @property bool|null $is_active
     * @property Carbon|null $conversion_time
     * @property int|null $customer_id
     * @property int $channel_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $applied_cart_rule_ids
     * @property-read $billing_address
     * @property-read $selected_shipping_rate
     * @property-read $shipping_address
     * @method static _IH_Cart_QB onWriteConnection()
     * @method _IH_Cart_QB newQuery()
     * @method static _IH_Cart_QB on()
     * @method static _IH_Cart_QB query()
     * @method static _IH_Cart_QB with()
     * @method _IH_Cart_QB newModelQuery()
     * @method static _IH_Cart_C|Cart[] all()
     * @mixin _IH_Cart_QB
     * @method static CartFactory factory(...$parameters)
     */
    class Cart extends Model {}
    
    /**
     * @property-read string $name
     * @property Cart $cart
     * @method BelongsTo|_IH_Cart_QB cart()
     * @property Customer $customer
     * @method BelongsTo|_IH_Customer_QB customer()
     * @method static _IH_CartAddress_QB onWriteConnection()
     * @method _IH_CartAddress_QB newQuery()
     * @method static _IH_CartAddress_QB on()
     * @method static _IH_CartAddress_QB query()
     * @method static _IH_CartAddress_QB with()
     * @method _IH_CartAddress_QB newModelQuery()
     * @method static _IH_CartAddress_C|CartAddress[] all()
     * @mixin _IH_CartAddress_QB
     * @method static CartAddressFactory factory(...$parameters)
     */
    class CartAddress extends Model {}
    
    /**
     * @property int $id
     * @property int $quantity
     * @property string|null $sku
     * @property string|null $type
     * @property string|null $name
     * @property string|null $coupon_code
     * @property float $weight
     * @property float $total_weight
     * @property float $base_total_weight
     * @property float $price
     * @property float $base_price
     * @property float $total
     * @property float $base_total
     * @property float|null $tax_percent
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float $discount_percent
     * @property float $discount_amount
     * @property float $base_discount_amount
     * @property array|null $additional
     * @property int|null $parent_id
     * @property int $product_id
     * @property int $cart_id
     * @property int|null $tax_category_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property float|null $custom_price
     * @property string|null $applied_cart_rule_ids
     * @property-read $product_flat
     * @property CartItem $child
     * @method BelongsTo|_IH_CartItem_QB child()
     * @property _IH_CartItem_C|CartItem[] $children
     * @property-read int $children_count
     * @method HasMany|_IH_CartItem_QB children()
     * @property CartItem|null $parent
     * @method BelongsTo|_IH_CartItem_QB parent()
     * @method static _IH_CartItem_QB onWriteConnection()
     * @method _IH_CartItem_QB newQuery()
     * @method static _IH_CartItem_QB on()
     * @method static _IH_CartItem_QB query()
     * @method static _IH_CartItem_QB with()
     * @method _IH_CartItem_QB newModelQuery()
     * @method static _IH_CartItem_C|CartItem[] all()
     * @mixin _IH_CartItem_QB
     * @method static CartItemFactory factory(...$parameters)
     */
    class CartItem extends Model {}
    
    /**
     * @property int $id
     * @property string $method
     * @property string|null $method_title
     * @property int|null $cart_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_CartPayment_QB onWriteConnection()
     * @method _IH_CartPayment_QB newQuery()
     * @method static _IH_CartPayment_QB on()
     * @method static _IH_CartPayment_QB query()
     * @method static _IH_CartPayment_QB with()
     * @method _IH_CartPayment_QB newModelQuery()
     * @method static _IH_CartPayment_C|CartPayment[] all()
     * @mixin _IH_CartPayment_QB
     * @method static CartPaymentFactory factory(...$parameters)
     */
    class CartPayment extends Model {}
    
    /**
     * @property int $id
     * @property string $carrier
     * @property string $carrier_title
     * @property string $method
     * @property string $method_title
     * @property string|null $method_description
     * @property float|null $price
     * @property float|null $base_price
     * @property int|null $cart_address_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property float $discount_amount
     * @property float $base_discount_amount
     * @property bool $is_calculate_tax
     * @method static _IH_CartShippingRate_QB onWriteConnection()
     * @method _IH_CartShippingRate_QB newQuery()
     * @method static _IH_CartShippingRate_QB on()
     * @method static _IH_CartShippingRate_QB query()
     * @method static _IH_CartShippingRate_QB with()
     * @method _IH_CartShippingRate_QB newModelQuery()
     * @method static _IH_CartShippingRate_C|CartShippingRate[] all()
     * @mixin _IH_CartShippingRate_QB
     */
    class CartShippingRate extends Model {}
}

namespace Webkul\Core\Eloquent {

    use Illuminate\Database\Eloquent\Model;
    use LaravelIdea\Helper\Webkul\Core\Eloquent\_IH_TranslatableModel_C;
    use LaravelIdea\Helper\Webkul\Core\Eloquent\_IH_TranslatableModel_QB;
    
    /**
     * @method static _IH_TranslatableModel_QB onWriteConnection()
     * @method _IH_TranslatableModel_QB newQuery()
     * @method static _IH_TranslatableModel_QB on()
     * @method static _IH_TranslatableModel_QB query()
     * @method static _IH_TranslatableModel_QB with()
     * @method _IH_TranslatableModel_QB newModelQuery()
     * @method static _IH_TranslatableModel_C|TranslatableModel[] all()
     * @mixin _IH_TranslatableModel_QB
     */
    class TranslatableModel extends Model {}
}

namespace Webkul\Core\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_ChannelTranslation_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_ChannelTranslation_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Channel_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Channel_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CoreConfig_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CoreConfig_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CountryStateTranslation_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CountryStateTranslation_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CountryState_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CountryState_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CountryTranslation_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CountryTranslation_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Country_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Country_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CurrencyExchangeRate_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_CurrencyExchangeRate_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Currency_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Currency_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Locale_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Locale_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Slider_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_Slider_QB;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_SubscribersList_C;
    use LaravelIdea\Helper\Webkul\Core\Models\_IH_SubscribersList_QB;
    use Webkul\Core\Database\Factories\ChannelFactory;
    use Webkul\Core\Database\Factories\CurrencyFactory;
    use Webkul\Core\Database\Factories\LocaleFactory;
    use Webkul\Core\Database\Factories\SubscriberListFactory;
    
    /**
     * @property int $id
     * @property string $code
     * @property string|null $timezone
     * @property string|null $theme
     * @property string|null $hostname
     * @property string|null $logo
     * @property string|null $favicon
     * @property bool $is_maintenance_on
     * @property string|null $allowed_ips
     * @property int $default_locale_id
     * @property int $base_currency_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int|null $root_category_id
     * @property-read void $favicon_url
     * @property-read void $logo_url
     * @method static _IH_Channel_QB onWriteConnection()
     * @method _IH_Channel_QB newQuery()
     * @method static _IH_Channel_QB on()
     * @method static _IH_Channel_QB query()
     * @method static _IH_Channel_QB with()
     * @method _IH_Channel_QB newModelQuery()
     * @method static _IH_Channel_C|Channel[] all()
     * @mixin _IH_Channel_QB
     * @method static ChannelFactory factory(...$parameters)
     */
    class Channel extends Model {}
    
    /**
     * @property int $id
     * @property int $channel_id
     * @property string $locale
     * @property string $name
     * @property string|null $description
     * @property string|null $home_page_content
     * @property string|null $footer_content
     * @property string|null $maintenance_mode_text
     * @property string|null $home_seo
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_ChannelTranslation_QB onWriteConnection()
     * @method _IH_ChannelTranslation_QB newQuery()
     * @method static _IH_ChannelTranslation_QB on()
     * @method static _IH_ChannelTranslation_QB query()
     * @method static _IH_ChannelTranslation_QB with()
     * @method _IH_ChannelTranslation_QB newModelQuery()
     * @method static _IH_ChannelTranslation_C|ChannelTranslation[] all()
     * @mixin _IH_ChannelTranslation_QB
     */
    class ChannelTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string $code
     * @property string $value
     * @property string|null $channel_code
     * @property string|null $locale_code
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_CoreConfig_QB onWriteConnection()
     * @method _IH_CoreConfig_QB newQuery()
     * @method static _IH_CoreConfig_QB on()
     * @method static _IH_CoreConfig_QB query()
     * @method static _IH_CoreConfig_QB with()
     * @method _IH_CoreConfig_QB newModelQuery()
     * @method static _IH_CoreConfig_C|CoreConfig[] all()
     * @mixin _IH_CoreConfig_QB
     */
    class CoreConfig extends Model {}
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @method static _IH_Country_QB onWriteConnection()
     * @method _IH_Country_QB newQuery()
     * @method static _IH_Country_QB on()
     * @method static _IH_Country_QB query()
     * @method static _IH_Country_QB with()
     * @method _IH_Country_QB newModelQuery()
     * @method static _IH_Country_C|Country[] all()
     * @mixin _IH_Country_QB
     */
    class Country extends Model {}
    
    /**
     * @property int $id
     * @property string|null $country_code
     * @property string|null $code
     * @property string|null $default_name
     * @property int|null $country_id
     * @method static _IH_CountryState_QB onWriteConnection()
     * @method _IH_CountryState_QB newQuery()
     * @method static _IH_CountryState_QB on()
     * @method static _IH_CountryState_QB query()
     * @method static _IH_CountryState_QB with()
     * @method _IH_CountryState_QB newModelQuery()
     * @method static _IH_CountryState_C|CountryState[] all()
     * @mixin _IH_CountryState_QB
     */
    class CountryState extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $default_name
     * @property int $country_state_id
     * @method static _IH_CountryStateTranslation_QB onWriteConnection()
     * @method _IH_CountryStateTranslation_QB newQuery()
     * @method static _IH_CountryStateTranslation_QB on()
     * @method static _IH_CountryStateTranslation_QB query()
     * @method static _IH_CountryStateTranslation_QB with()
     * @method _IH_CountryStateTranslation_QB newModelQuery()
     * @method static _IH_CountryStateTranslation_C|CountryStateTranslation[] all()
     * @mixin _IH_CountryStateTranslation_QB
     */
    class CountryStateTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $name
     * @property int $country_id
     * @method static _IH_CountryTranslation_QB onWriteConnection()
     * @method _IH_CountryTranslation_QB newQuery()
     * @method static _IH_CountryTranslation_QB on()
     * @method static _IH_CountryTranslation_QB query()
     * @method static _IH_CountryTranslation_QB with()
     * @method _IH_CountryTranslation_QB newModelQuery()
     * @method static _IH_CountryTranslation_C|CountryTranslation[] all()
     * @mixin _IH_CountryTranslation_QB
     */
    class CountryTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $symbol
     * @method static _IH_Currency_QB onWriteConnection()
     * @method _IH_Currency_QB newQuery()
     * @method static _IH_Currency_QB on()
     * @method static _IH_Currency_QB query()
     * @method static _IH_Currency_QB with()
     * @method _IH_Currency_QB newModelQuery()
     * @method static _IH_Currency_C|Currency[] all()
     * @mixin _IH_Currency_QB
     * @method static CurrencyFactory factory(...$parameters)
     */
    class Currency extends Model {}
    
    /**
     * @property int $id
     * @property float $rate
     * @property int $target_currency
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_CurrencyExchangeRate_QB onWriteConnection()
     * @method _IH_CurrencyExchangeRate_QB newQuery()
     * @method static _IH_CurrencyExchangeRate_QB on()
     * @method static _IH_CurrencyExchangeRate_QB query()
     * @method static _IH_CurrencyExchangeRate_QB with()
     * @method _IH_CurrencyExchangeRate_QB newModelQuery()
     * @method static _IH_CurrencyExchangeRate_C|CurrencyExchangeRate[] all()
     * @mixin _IH_CurrencyExchangeRate_QB
     */
    class CurrencyExchangeRate extends Model {}
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string $direction
     * @property string|null $locale_image
     * @method static _IH_Locale_QB onWriteConnection()
     * @method _IH_Locale_QB newQuery()
     * @method static _IH_Locale_QB on()
     * @method static _IH_Locale_QB query()
     * @method static _IH_Locale_QB with()
     * @method _IH_Locale_QB newModelQuery()
     * @method static _IH_Locale_C|Locale[] all()
     * @mixin _IH_Locale_QB
     * @method static LocaleFactory factory(...$parameters)
     */
    class Locale extends Model {}
    
    /**
     * @property int $id
     * @property string $title
     * @property string $path
     * @property string|null $content
     * @property int $channel_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $slider_path
     * @property string $locale
     * @property Carbon|null $expired_at
     * @property int $sort_order
     * @property-read string $image_url
     * @method static _IH_Slider_QB onWriteConnection()
     * @method _IH_Slider_QB newQuery()
     * @method static _IH_Slider_QB on()
     * @method static _IH_Slider_QB query()
     * @method static _IH_Slider_QB with()
     * @method _IH_Slider_QB newModelQuery()
     * @method static _IH_Slider_C|Slider[] all()
     * @mixin _IH_Slider_QB
     */
    class Slider extends Model {}
    
    /**
     * @property int $id
     * @property string $email
     * @property bool $is_subscribed
     * @property string|null $token
     * @property int $channel_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int|null $customer_id
     * @method static _IH_SubscribersList_QB onWriteConnection()
     * @method _IH_SubscribersList_QB newQuery()
     * @method static _IH_SubscribersList_QB on()
     * @method static _IH_SubscribersList_QB query()
     * @method static _IH_SubscribersList_QB with()
     * @method _IH_SubscribersList_QB newModelQuery()
     * @method static _IH_SubscribersList_C|SubscribersList[] all()
     * @mixin _IH_SubscribersList_QB
     * @method static SubscriberListFactory factory(...$parameters)
     */
    class SubscribersList extends Model {}
}

namespace Webkul\Customer\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\MorphToMany;
    use Illuminate\Notifications\DatabaseNotification;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Illuminate\Notifications\_IH_DatabaseNotification_C;
    use LaravelIdea\Helper\Illuminate\Notifications\_IH_DatabaseNotification_QB;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_CustomerAddress_C;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_CustomerAddress_QB;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_CustomerGroup_C;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_CustomerGroup_QB;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_Customer_C;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_Customer_QB;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_Wishlist_C;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_Wishlist_QB;
    use Webkul\Customer\Database\Factories\CustomerAddressFactory;
    use Webkul\Customer\Database\Factories\CustomerFactory;
    use Webkul\Customer\Database\Factories\CustomerGroupFactory;
    
    /**
     * @property int $id
     * @property string $first_name
     * @property string $last_name
     * @property string|null $gender
     * @property Carbon|null $date_of_birth
     * @property string|null $email
     * @property string|null $image
     * @property int $status
     * @property string|null $password
     * @property string|null $api_token
     * @property int|null $customer_group_id
     * @property bool $subscribed_to_news_letter
     * @property string|null $remember_token
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property bool $is_verified
     * @property string|null $token
     * @property string|null $notes
     * @property string|null $phone
     * @property-read void $image_url
     * @property-read string $name
     * @property _IH_DatabaseNotification_C|DatabaseNotification[] $notifications
     * @property-read int $notifications_count
     * @method MorphToMany|_IH_DatabaseNotification_QB notifications()
     * @method static _IH_Customer_QB onWriteConnection()
     * @method _IH_Customer_QB newQuery()
     * @method static _IH_Customer_QB on()
     * @method static _IH_Customer_QB query()
     * @method static _IH_Customer_QB with()
     * @method _IH_Customer_QB newModelQuery()
     * @method static _IH_Customer_C|Customer[] all()
     * @mixin _IH_Customer_QB
     * @method static CustomerFactory factory(...$parameters)
     */
    class Customer extends Model {}
    
    /**
     * @property-read string $name
     * @property Customer $customer
     * @method BelongsTo|_IH_Customer_QB customer()
     * @method static _IH_CustomerAddress_QB onWriteConnection()
     * @method _IH_CustomerAddress_QB newQuery()
     * @method static _IH_CustomerAddress_QB on()
     * @method static _IH_CustomerAddress_QB query()
     * @method static _IH_CustomerAddress_QB with()
     * @method _IH_CustomerAddress_QB newModelQuery()
     * @method static _IH_CustomerAddress_C|CustomerAddress[] all()
     * @mixin _IH_CustomerAddress_QB
     * @method static CustomerAddressFactory factory(...$parameters)
     */
    class CustomerAddress extends Model {}
    
    /**
     * @property int $id
     * @property string $name
     * @property bool $is_user_defined
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string $code
     * @method static _IH_CustomerGroup_QB onWriteConnection()
     * @method _IH_CustomerGroup_QB newQuery()
     * @method static _IH_CustomerGroup_QB on()
     * @method static _IH_CustomerGroup_QB query()
     * @method static _IH_CustomerGroup_QB with()
     * @method _IH_CustomerGroup_QB newModelQuery()
     * @method static _IH_CustomerGroup_C|CustomerGroup[] all()
     * @mixin _IH_CustomerGroup_QB
     * @method static CustomerGroupFactory factory(...$parameters)
     */
    class CustomerGroup extends Model {}
    
    /**
     * @property int $id
     * @property int $channel_id
     * @property int $product_id
     * @property int $customer_id
     * @property string|null $item_options
     * @property Carbon|null $moved_to_cart
     * @property bool|null $shared
     * @property Carbon|null $time_of_moving
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property array|null $additional
     * @method static _IH_Wishlist_QB onWriteConnection()
     * @method _IH_Wishlist_QB newQuery()
     * @method static _IH_Wishlist_QB on()
     * @method static _IH_Wishlist_QB query()
     * @method static _IH_Wishlist_QB with()
     * @method _IH_Wishlist_QB newModelQuery()
     * @method static _IH_Wishlist_C|Wishlist[] all()
     * @mixin _IH_Wishlist_QB
     */
    class Wishlist extends Model {}
}

namespace Webkul\Inventory\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Inventory\Models\_IH_InventorySource_C;
    use LaravelIdea\Helper\Webkul\Inventory\Models\_IH_InventorySource_QB;
    use Webkul\Inventory\Database\Factories\InventorySourceFactory;
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @property string|null $description
     * @property string $contact_name
     * @property string $contact_email
     * @property string $contact_number
     * @property string|null $contact_fax
     * @property string $country
     * @property string $state
     * @property string $city
     * @property string $street
     * @property string $postcode
     * @property int $priority
     * @property float|null $latitude
     * @property float|null $longitude
     * @property bool $status
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_InventorySource_QB onWriteConnection()
     * @method _IH_InventorySource_QB newQuery()
     * @method static _IH_InventorySource_QB on()
     * @method static _IH_InventorySource_QB query()
     * @method static _IH_InventorySource_QB with()
     * @method _IH_InventorySource_QB newModelQuery()
     * @method static _IH_InventorySource_C|InventorySource[] all()
     * @mixin _IH_InventorySource_QB
     * @method static InventorySourceFactory factory(...$parameters)
     */
    class InventorySource extends Model {}
}

namespace Webkul\Marketing\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Marketing\Models\_IH_Campaign_C;
    use LaravelIdea\Helper\Webkul\Marketing\Models\_IH_Campaign_QB;
    use LaravelIdea\Helper\Webkul\Marketing\Models\_IH_Event_C;
    use LaravelIdea\Helper\Webkul\Marketing\Models\_IH_Event_QB;
    use LaravelIdea\Helper\Webkul\Marketing\Models\_IH_Template_C;
    use LaravelIdea\Helper\Webkul\Marketing\Models\_IH_Template_QB;
    
    /**
     * @property int $id
     * @property string $name
     * @property string $subject
     * @property bool $status
     * @property string $type
     * @property string $mail_to
     * @property string|null $spooling
     * @property int|null $channel_id
     * @property int|null $customer_group_id
     * @property int|null $marketing_template_id
     * @property int|null $marketing_event_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_Campaign_QB onWriteConnection()
     * @method _IH_Campaign_QB newQuery()
     * @method static _IH_Campaign_QB on()
     * @method static _IH_Campaign_QB query()
     * @method static _IH_Campaign_QB with()
     * @method _IH_Campaign_QB newModelQuery()
     * @method static _IH_Campaign_C|Campaign[] all()
     * @mixin _IH_Campaign_QB
     */
    class Campaign extends Model {}
    
    /**
     * @property int $id
     * @property string $name
     * @property string|null $description
     * @property Carbon|null $date
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_Event_QB onWriteConnection()
     * @method _IH_Event_QB newQuery()
     * @method static _IH_Event_QB on()
     * @method static _IH_Event_QB query()
     * @method static _IH_Event_QB with()
     * @method _IH_Event_QB newModelQuery()
     * @method static _IH_Event_C|Event[] all()
     * @mixin _IH_Event_QB
     */
    class Event extends Model {}
    
    /**
     * @property int $id
     * @property string $name
     * @property string $status
     * @property string $content
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_Template_QB onWriteConnection()
     * @method _IH_Template_QB newQuery()
     * @method static _IH_Template_QB on()
     * @method static _IH_Template_QB query()
     * @method static _IH_Template_QB with()
     * @method _IH_Template_QB newModelQuery()
     * @method static _IH_Template_C|Template[] all()
     * @mixin _IH_Template_QB
     */
    class Template extends Model {}
}

namespace Webkul\Product\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\BelongsToMany;
    use Illuminate\Database\Eloquent\Relations\HasMany;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductAttributeValue_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductAttributeValue_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductBundleOptionProduct_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductBundleOptionProduct_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductBundleOptionTranslation_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductBundleOptionTranslation_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductBundleOption_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductBundleOption_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductCustomerGroupPrice_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductCustomerGroupPrice_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableLinkTranslation_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableLinkTranslation_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableLink_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableLink_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableSampleTranslation_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableSampleTranslation_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableSample_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductDownloadableSample_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductFlat_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductFlat_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductGroupedProduct_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductGroupedProduct_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductImage_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductImage_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductInventory_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductInventory_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductOrderedInventory_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductOrderedInventory_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductReviewImage_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductReviewImage_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductReview_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductReview_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductSalableInventory_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductSalableInventory_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductVideo_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_ProductVideo_QB;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_Product_C;
    use LaravelIdea\Helper\Webkul\Product\Models\_IH_Product_QB;
    use Webkul\Product\Database\Factories\ProductAttributeValueFactory;
    use Webkul\Product\Database\Factories\ProductDownloadableLinkFactory;
    use Webkul\Product\Database\Factories\ProductDownloadableLinkTranslationFactory;
    use Webkul\Product\Database\Factories\ProductFactory;
    use Webkul\Product\Database\Factories\ProductInventoryFactory;
    use Webkul\Product\Database\Factories\ProductReviewFactory;
    
    /**
     * @property int $id
     * @property string $sku
     * @property string $type
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int|null $parent_id
     * @property int|null $attribute_family_id
     * @property array|null $additional
     * @property-read null $base_image_url
     * @property-read $product
     * @property-read $product_id
     * @property _IH_Product_C|Product[] $cross_sells
     * @property-read int $cross_sells_count
     * @method BelongsToMany|_IH_Product_QB cross_sells()
     * @property Product|null $parent
     * @method BelongsTo|_IH_Product_QB parent()
     * @property _IH_Product_C|Product[] $related_products
     * @property-read int $related_products_count
     * @method BelongsToMany|_IH_Product_QB related_products()
     * @property _IH_Product_C|Product[] $up_sells
     * @property-read int $up_sells_count
     * @method BelongsToMany|_IH_Product_QB up_sells()
     * @property _IH_Product_C|Product[] $variants
     * @property-read int $variants_count
     * @method HasMany|_IH_Product_QB variants()
     * @method static _IH_Product_QB onWriteConnection()
     * @method _IH_Product_QB newQuery()
     * @method static _IH_Product_QB on()
     * @method static _IH_Product_QB query()
     * @method static _IH_Product_QB with()
     * @method _IH_Product_QB newModelQuery()
     * @method static _IH_Product_C|Product[] all()
     * @mixin _IH_Product_QB
     * @method static ProductFactory factory(...$parameters)
     */
    class Product extends Model {}
    
    /**
     * @property int $id
     * @property string|null $locale
     * @property string|null $channel
     * @property string|null $text_value
     * @property bool|null $boolean_value
     * @property int|null $integer_value
     * @property float|null $float_value
     * @property Carbon|null $datetime_value
     * @property Carbon|null $date_value
     * @property string|null $json_value
     * @property int $product_id
     * @property int $attribute_id
     * @method static _IH_ProductAttributeValue_QB onWriteConnection()
     * @method _IH_ProductAttributeValue_QB newQuery()
     * @method static _IH_ProductAttributeValue_QB on()
     * @method static _IH_ProductAttributeValue_QB query()
     * @method static _IH_ProductAttributeValue_QB with()
     * @method _IH_ProductAttributeValue_QB newModelQuery()
     * @method static _IH_ProductAttributeValue_C|ProductAttributeValue[] all()
     * @mixin _IH_ProductAttributeValue_QB
     * @method static ProductAttributeValueFactory factory(...$parameters)
     */
    class ProductAttributeValue extends Model {}
    
    /**
     * @property int $id
     * @property string $type
     * @property bool $is_required
     * @property int $sort_order
     * @property int $product_id
     * @method static _IH_ProductBundleOption_QB onWriteConnection()
     * @method _IH_ProductBundleOption_QB newQuery()
     * @method static _IH_ProductBundleOption_QB on()
     * @method static _IH_ProductBundleOption_QB query()
     * @method static _IH_ProductBundleOption_QB with()
     * @method _IH_ProductBundleOption_QB newModelQuery()
     * @method static _IH_ProductBundleOption_C|ProductBundleOption[] all()
     * @mixin _IH_ProductBundleOption_QB
     */
    class ProductBundleOption extends Model {}
    
    /**
     * @property int $id
     * @property int $qty
     * @property bool $is_user_defined
     * @property bool $is_default
     * @property int $sort_order
     * @property int $product_bundle_option_id
     * @property int $product_id
     * @method static _IH_ProductBundleOptionProduct_QB onWriteConnection()
     * @method _IH_ProductBundleOptionProduct_QB newQuery()
     * @method static _IH_ProductBundleOptionProduct_QB on()
     * @method static _IH_ProductBundleOptionProduct_QB query()
     * @method static _IH_ProductBundleOptionProduct_QB with()
     * @method _IH_ProductBundleOptionProduct_QB newModelQuery()
     * @method static _IH_ProductBundleOptionProduct_C|ProductBundleOptionProduct[] all()
     * @mixin _IH_ProductBundleOptionProduct_QB
     */
    class ProductBundleOptionProduct extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $label
     * @property int $product_bundle_option_id
     * @method static _IH_ProductBundleOptionTranslation_QB onWriteConnection()
     * @method _IH_ProductBundleOptionTranslation_QB newQuery()
     * @method static _IH_ProductBundleOptionTranslation_QB on()
     * @method static _IH_ProductBundleOptionTranslation_QB query()
     * @method static _IH_ProductBundleOptionTranslation_QB with()
     * @method _IH_ProductBundleOptionTranslation_QB newModelQuery()
     * @method static _IH_ProductBundleOptionTranslation_C|ProductBundleOptionTranslation[] all()
     * @mixin _IH_ProductBundleOptionTranslation_QB
     */
    class ProductBundleOptionTranslation extends Model {}
    
    /**
     * @property int $id
     * @property int $qty
     * @property string $value_type
     * @property float $value
     * @property int $product_id
     * @property int|null $customer_group_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_ProductCustomerGroupPrice_QB onWriteConnection()
     * @method _IH_ProductCustomerGroupPrice_QB newQuery()
     * @method static _IH_ProductCustomerGroupPrice_QB on()
     * @method static _IH_ProductCustomerGroupPrice_QB query()
     * @method static _IH_ProductCustomerGroupPrice_QB with()
     * @method _IH_ProductCustomerGroupPrice_QB newModelQuery()
     * @method static _IH_ProductCustomerGroupPrice_C|ProductCustomerGroupPrice[] all()
     * @mixin _IH_ProductCustomerGroupPrice_QB
     */
    class ProductCustomerGroupPrice extends Model {}
    
    /**
     * @property int $id
     * @property string|null $url
     * @property string|null $file
     * @property string|null $file_name
     * @property string $type
     * @property float $price
     * @property string|null $sample_url
     * @property string|null $sample_file
     * @property string|null $sample_file_name
     * @property string|null $sample_type
     * @property int $downloads
     * @property int|null $sort_order
     * @property int $product_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read string $file_url
     * @property-read string $sample_file_url
     * @method static _IH_ProductDownloadableLink_QB onWriteConnection()
     * @method _IH_ProductDownloadableLink_QB newQuery()
     * @method static _IH_ProductDownloadableLink_QB on()
     * @method static _IH_ProductDownloadableLink_QB query()
     * @method static _IH_ProductDownloadableLink_QB with()
     * @method _IH_ProductDownloadableLink_QB newModelQuery()
     * @method static _IH_ProductDownloadableLink_C|ProductDownloadableLink[] all()
     * @mixin _IH_ProductDownloadableLink_QB
     * @method static ProductDownloadableLinkFactory factory(...$parameters)
     */
    class ProductDownloadableLink extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $title
     * @property int $product_downloadable_link_id
     * @method static _IH_ProductDownloadableLinkTranslation_QB onWriteConnection()
     * @method _IH_ProductDownloadableLinkTranslation_QB newQuery()
     * @method static _IH_ProductDownloadableLinkTranslation_QB on()
     * @method static _IH_ProductDownloadableLinkTranslation_QB query()
     * @method static _IH_ProductDownloadableLinkTranslation_QB with()
     * @method _IH_ProductDownloadableLinkTranslation_QB newModelQuery()
     * @method static _IH_ProductDownloadableLinkTranslation_C|ProductDownloadableLinkTranslation[] all()
     * @mixin _IH_ProductDownloadableLinkTranslation_QB
     * @method static ProductDownloadableLinkTranslationFactory factory(...$parameters)
     */
    class ProductDownloadableLinkTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string|null $url
     * @property string|null $file
     * @property string|null $file_name
     * @property string $type
     * @property int|null $sort_order
     * @property int $product_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read $file_url
     * @method static _IH_ProductDownloadableSample_QB onWriteConnection()
     * @method _IH_ProductDownloadableSample_QB newQuery()
     * @method static _IH_ProductDownloadableSample_QB on()
     * @method static _IH_ProductDownloadableSample_QB query()
     * @method static _IH_ProductDownloadableSample_QB with()
     * @method _IH_ProductDownloadableSample_QB newModelQuery()
     * @method static _IH_ProductDownloadableSample_C|ProductDownloadableSample[] all()
     * @mixin _IH_ProductDownloadableSample_QB
     */
    class ProductDownloadableSample extends Model {}
    
    /**
     * @property int $id
     * @property string $locale
     * @property string|null $title
     * @property int $product_downloadable_sample_id
     * @method static _IH_ProductDownloadableSampleTranslation_QB onWriteConnection()
     * @method _IH_ProductDownloadableSampleTranslation_QB newQuery()
     * @method static _IH_ProductDownloadableSampleTranslation_QB on()
     * @method static _IH_ProductDownloadableSampleTranslation_QB query()
     * @method static _IH_ProductDownloadableSampleTranslation_QB with()
     * @method _IH_ProductDownloadableSampleTranslation_QB newModelQuery()
     * @method static _IH_ProductDownloadableSampleTranslation_C|ProductDownloadableSampleTranslation[] all()
     * @mixin _IH_ProductDownloadableSampleTranslation_QB
     */
    class ProductDownloadableSampleTranslation extends Model {}
    
    /**
     * @property int $id
     * @property string $sku
     * @property string|null $product_number
     * @property string|null $name
     * @property string|null $description
     * @property string|null $url_key
     * @property bool|null $new
     * @property bool|null $featured
     * @property bool|null $status
     * @property string|null $thumbnail
     * @property float|null $price
     * @property float|null $cost
     * @property float|null $special_price
     * @property Carbon|null $special_price_from
     * @property Carbon|null $special_price_to
     * @property float|null $weight
     * @property int|null $color
     * @property string|null $color_label
     * @property int|null $size
     * @property string|null $size_label
     * @property Carbon|null $created_at
     * @property string|null $locale
     * @property string|null $channel
     * @property int $product_id
     * @property Carbon|null $updated_at
     * @property int|null $parent_id
     * @property bool|null $visible_individually
     * @property float|null $min_price
     * @property float|null $max_price
     * @property string|null $short_description
     * @property string|null $meta_title
     * @property string|null $meta_keywords
     * @property string|null $meta_description
     * @property float|null $width
     * @property float|null $height
     * @property float|null $depth
     * @property-read $attribute_family
     * @property-read $images
     * @property-read $reviews
     * @property-read $type
     * @property ProductFlat|null $parent
     * @method BelongsTo|_IH_ProductFlat_QB parent()
     * @property _IH_ProductFlat_C|ProductFlat[] $variants
     * @property-read int $variants_count
     * @method HasMany|_IH_ProductFlat_QB variants()
     * @method static _IH_ProductFlat_QB onWriteConnection()
     * @method _IH_ProductFlat_QB newQuery()
     * @method static _IH_ProductFlat_QB on()
     * @method static _IH_ProductFlat_QB query()
     * @method static _IH_ProductFlat_QB with()
     * @method _IH_ProductFlat_QB newModelQuery()
     * @method static _IH_ProductFlat_C|ProductFlat[] all()
     * @mixin _IH_ProductFlat_QB
     */
    class ProductFlat extends Model {}
    
    /**
     * @property int $id
     * @property int $qty
     * @property int $sort_order
     * @property int $product_id
     * @property int $associated_product_id
     * @method static _IH_ProductGroupedProduct_QB onWriteConnection()
     * @method _IH_ProductGroupedProduct_QB newQuery()
     * @method static _IH_ProductGroupedProduct_QB on()
     * @method static _IH_ProductGroupedProduct_QB query()
     * @method static _IH_ProductGroupedProduct_QB with()
     * @method _IH_ProductGroupedProduct_QB newModelQuery()
     * @method static _IH_ProductGroupedProduct_C|ProductGroupedProduct[] all()
     * @mixin _IH_ProductGroupedProduct_QB
     */
    class ProductGroupedProduct extends Model {}
    
    /**
     * @property int $id
     * @property string|null $type
     * @property string $path
     * @property int $product_id
     * @property-read $url
     * @method static _IH_ProductImage_QB onWriteConnection()
     * @method _IH_ProductImage_QB newQuery()
     * @method static _IH_ProductImage_QB on()
     * @method static _IH_ProductImage_QB query()
     * @method static _IH_ProductImage_QB with()
     * @method _IH_ProductImage_QB newModelQuery()
     * @method static _IH_ProductImage_C|ProductImage[] all()
     * @mixin _IH_ProductImage_QB
     */
    class ProductImage extends Model {}
    
    /**
     * @property int $id
     * @property int $qty
     * @property int $product_id
     * @property int $inventory_source_id
     * @property int $vendor_id
     * @method static _IH_ProductInventory_QB onWriteConnection()
     * @method _IH_ProductInventory_QB newQuery()
     * @method static _IH_ProductInventory_QB on()
     * @method static _IH_ProductInventory_QB query()
     * @method static _IH_ProductInventory_QB with()
     * @method _IH_ProductInventory_QB newModelQuery()
     * @method static _IH_ProductInventory_C|ProductInventory[] all()
     * @mixin _IH_ProductInventory_QB
     * @method static ProductInventoryFactory factory(...$parameters)
     */
    class ProductInventory extends Model {}
    
    /**
     * @property int $id
     * @property int $qty
     * @property int $product_id
     * @property int $channel_id
     * @method static _IH_ProductOrderedInventory_QB onWriteConnection()
     * @method _IH_ProductOrderedInventory_QB newQuery()
     * @method static _IH_ProductOrderedInventory_QB on()
     * @method static _IH_ProductOrderedInventory_QB query()
     * @method static _IH_ProductOrderedInventory_QB with()
     * @method _IH_ProductOrderedInventory_QB newModelQuery()
     * @method static _IH_ProductOrderedInventory_C|ProductOrderedInventory[] all()
     * @mixin _IH_ProductOrderedInventory_QB
     */
    class ProductOrderedInventory extends Model {}
    
    /**
     * @property int $id
     * @property string $title
     * @property int $rating
     * @property string|null $comment
     * @property string $status
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int $product_id
     * @property int|null $customer_id
     * @property string $name
     * @method static _IH_ProductReview_QB onWriteConnection()
     * @method _IH_ProductReview_QB newQuery()
     * @method static _IH_ProductReview_QB on()
     * @method static _IH_ProductReview_QB query()
     * @method static _IH_ProductReview_QB with()
     * @method _IH_ProductReview_QB newModelQuery()
     * @method static _IH_ProductReview_C|ProductReview[] all()
     * @mixin _IH_ProductReview_QB
     * @method static ProductReviewFactory factory(...$parameters)
     */
    class ProductReview extends Model {}
    
    /**
     * @property int $id
     * @property string|null $type
     * @property string $path
     * @property int $review_id
     * @property-read $url
     * @method static _IH_ProductReviewImage_QB onWriteConnection()
     * @method _IH_ProductReviewImage_QB newQuery()
     * @method static _IH_ProductReviewImage_QB on()
     * @method static _IH_ProductReviewImage_QB query()
     * @method static _IH_ProductReviewImage_QB with()
     * @method _IH_ProductReviewImage_QB newModelQuery()
     * @method static _IH_ProductReviewImage_C|ProductReviewImage[] all()
     * @mixin _IH_ProductReviewImage_QB
     */
    class ProductReviewImage extends Model {}
    
    /**
     * @method static _IH_ProductSalableInventory_QB onWriteConnection()
     * @method _IH_ProductSalableInventory_QB newQuery()
     * @method static _IH_ProductSalableInventory_QB on()
     * @method static _IH_ProductSalableInventory_QB query()
     * @method static _IH_ProductSalableInventory_QB with()
     * @method _IH_ProductSalableInventory_QB newModelQuery()
     * @method static _IH_ProductSalableInventory_C|ProductSalableInventory[] all()
     * @mixin _IH_ProductSalableInventory_QB
     */
    class ProductSalableInventory extends Model {}
    
    /**
     * @property int $id
     * @property string|null $type
     * @property string $path
     * @property int $product_id
     * @property-read $url
     * @method static _IH_ProductVideo_QB onWriteConnection()
     * @method _IH_ProductVideo_QB newQuery()
     * @method static _IH_ProductVideo_QB on()
     * @method static _IH_ProductVideo_QB query()
     * @method static _IH_ProductVideo_QB with()
     * @method _IH_ProductVideo_QB newModelQuery()
     * @method static _IH_ProductVideo_C|ProductVideo[] all()
     * @mixin _IH_ProductVideo_QB
     */
    class ProductVideo extends Model {}
}

namespace Webkul\Sales\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\HasMany;
    use Illuminate\Database\Eloquent\Relations\MorphTo;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Customer\Models\_IH_Customer_QB;
    use LaravelIdea\Helper\Webkul\Inventory\Models\_IH_InventorySource_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_DownloadableLinkPurchased_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_DownloadableLinkPurchased_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_InvoiceItem_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_InvoiceItem_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Invoice_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Invoice_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderAddress_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderAddress_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderComment_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderComment_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderItem_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderItem_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderPayment_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderPayment_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderTransaction_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_OrderTransaction_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Order_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Order_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_RefundItem_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_RefundItem_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Refund_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Refund_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_ShipmentItem_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_ShipmentItem_QB;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Shipment_C;
    use LaravelIdea\Helper\Webkul\Sales\Models\_IH_Shipment_QB;
    use Webkul\Customer\Models\Customer;
    use Webkul\Inventory\Models\InventorySource;
    use Webkul\Sales\Database\Factories\InvoiceFactory;
    use Webkul\Sales\Database\Factories\InvoiceItemFactory;
    use Webkul\Sales\Database\Factories\OrderAddressFactory;
    use Webkul\Sales\Database\Factories\OrderFactory;
    use Webkul\Sales\Database\Factories\OrderItemFactory;
    use Webkul\Sales\Database\Factories\OrderPaymentFactory;
    use Webkul\Sales\Database\Factories\RefundFactory;
    use Webkul\Sales\Database\Factories\ShipmentFactory;
    
    /**
     * @property int $id
     * @property string|null $product_name
     * @property string|null $name
     * @property string|null $url
     * @property string|null $file
     * @property string|null $file_name
     * @property string $type
     * @property int $download_bought
     * @property int $download_used
     * @property string|null $status
     * @property int $customer_id
     * @property int $order_id
     * @property int $order_item_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int $download_canceled
     * @method static _IH_DownloadableLinkPurchased_QB onWriteConnection()
     * @method _IH_DownloadableLinkPurchased_QB newQuery()
     * @method static _IH_DownloadableLinkPurchased_QB on()
     * @method static _IH_DownloadableLinkPurchased_QB query()
     * @method static _IH_DownloadableLinkPurchased_QB with()
     * @method _IH_DownloadableLinkPurchased_QB newModelQuery()
     * @method static _IH_DownloadableLinkPurchased_C|DownloadableLinkPurchased[] all()
     * @mixin _IH_DownloadableLinkPurchased_QB
     */
    class DownloadableLinkPurchased extends Model {}
    
    /**
     * @property int $id
     * @property string|null $increment_id
     * @property string|null $state
     * @property bool $email_sent
     * @property int|null $total_qty
     * @property string|null $base_currency_code
     * @property string|null $channel_currency_code
     * @property string|null $order_currency_code
     * @property float|null $sub_total
     * @property float|null $base_sub_total
     * @property float|null $grand_total
     * @property float|null $base_grand_total
     * @property float|null $shipping_amount
     * @property float|null $base_shipping_amount
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property int|null $order_id
     * @property int|null $order_address_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $transaction_id
     * @property int $reminders
     * @property Carbon|null $next_reminder_at
     * @property-read mixed|string $status_label
     * @property Model $channel
     * @method MorphTo channel()
     * @property Model $customer
     * @method MorphTo customer()
     * @method static _IH_Invoice_QB onWriteConnection()
     * @method _IH_Invoice_QB newQuery()
     * @method static _IH_Invoice_QB on()
     * @method static _IH_Invoice_QB query()
     * @method static _IH_Invoice_QB with()
     * @method _IH_Invoice_QB newModelQuery()
     * @method static _IH_Invoice_C|Invoice[] all()
     * @mixin _IH_Invoice_QB
     * @method static InvoiceFactory factory(...$parameters)
     */
    class Invoice extends Model {}
    
    /**
     * @property int $id
     * @property string|null $name
     * @property string|null $description
     * @property string|null $sku
     * @property int|null $qty
     * @property float $price
     * @property float $base_price
     * @property float $total
     * @property float $base_total
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property int|null $product_id
     * @property string|null $product_type
     * @property int|null $order_item_id
     * @property int|null $invoice_id
     * @property int|null $parent_id
     * @property array|null $additional
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property float|null $discount_percent
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property-read $type
     * @property _IH_InvoiceItem_C|InvoiceItem[] $children
     * @property-read int $children_count
     * @method HasMany|_IH_InvoiceItem_QB children()
     * @property Model $product
     * @method MorphTo product()
     * @method static _IH_InvoiceItem_QB onWriteConnection()
     * @method _IH_InvoiceItem_QB newQuery()
     * @method static _IH_InvoiceItem_QB on()
     * @method static _IH_InvoiceItem_QB query()
     * @method static _IH_InvoiceItem_QB with()
     * @method _IH_InvoiceItem_QB newModelQuery()
     * @method static _IH_InvoiceItem_C|InvoiceItem[] all()
     * @mixin _IH_InvoiceItem_QB
     * @method static InvoiceItemFactory factory(...$parameters)
     */
    class InvoiceItem extends Model {}
    
    /**
     * @property int $id
     * @property string $increment_id
     * @property string|null $status
     * @property string|null $channel_name
     * @property bool|null $is_guest
     * @property string|null $customer_email
     * @property string|null $customer_first_name
     * @property string|null $customer_last_name
     * @property string|null $customer_company_name
     * @property string|null $customer_vat_id
     * @property string|null $shipping_method
     * @property string|null $shipping_title
     * @property string|null $shipping_description
     * @property string|null $coupon_code
     * @property bool $is_gift
     * @property int|null $total_item_count
     * @property int|null $total_qty_ordered
     * @property string|null $base_currency_code
     * @property string|null $channel_currency_code
     * @property string|null $order_currency_code
     * @property float|null $grand_total
     * @property float|null $base_grand_total
     * @property float|null $grand_total_invoiced
     * @property float|null $base_grand_total_invoiced
     * @property float|null $grand_total_refunded
     * @property float|null $base_grand_total_refunded
     * @property float|null $sub_total
     * @property float|null $base_sub_total
     * @property float|null $sub_total_invoiced
     * @property float|null $base_sub_total_invoiced
     * @property float|null $sub_total_refunded
     * @property float|null $base_sub_total_refunded
     * @property float|null $discount_percent
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property float|null $discount_invoiced
     * @property float|null $base_discount_invoiced
     * @property float|null $discount_refunded
     * @property float|null $base_discount_refunded
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float|null $tax_amount_invoiced
     * @property float|null $base_tax_amount_invoiced
     * @property float|null $tax_amount_refunded
     * @property float|null $base_tax_amount_refunded
     * @property float|null $shipping_amount
     * @property float|null $base_shipping_amount
     * @property float|null $shipping_invoiced
     * @property float|null $base_shipping_invoiced
     * @property float|null $shipping_refunded
     * @property float|null $base_shipping_refunded
     * @property int|null $customer_id
     * @property string|null $customer_type
     * @property int|null $channel_id
     * @property string|null $channel_type
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int|null $cart_id
     * @property string|null $applied_cart_rule_ids
     * @property float|null $shipping_discount_amount
     * @property float|null $base_shipping_discount_amount
     * @property-read $base_total_due
     * @property-read $billing_address
     * @property-read string $customer_full_name
     * @property-read $shipping_address
     * @property-read string $status_label
     * @property-read $total_due
     * @property Model $channel
     * @method MorphTo channel()
     * @property Model $customer
     * @method MorphTo customer()
     * @method static _IH_Order_QB onWriteConnection()
     * @method _IH_Order_QB newQuery()
     * @method static _IH_Order_QB on()
     * @method static _IH_Order_QB query()
     * @method static _IH_Order_QB with()
     * @method _IH_Order_QB newModelQuery()
     * @method static _IH_Order_C|Order[] all()
     * @mixin _IH_Order_QB
     * @method static OrderFactory factory(...$parameters)
     */
    class Order extends Model {}
    
    /**
     * @property-read string $name
     * @property Customer $customer
     * @method BelongsTo|_IH_Customer_QB customer()
     * @property Order $order
     * @method BelongsTo|_IH_Order_QB order()
     * @method static _IH_OrderAddress_QB onWriteConnection()
     * @method _IH_OrderAddress_QB newQuery()
     * @method static _IH_OrderAddress_QB on()
     * @method static _IH_OrderAddress_QB query()
     * @method static _IH_OrderAddress_QB with()
     * @method _IH_OrderAddress_QB newModelQuery()
     * @method static _IH_OrderAddress_C|OrderAddress[] all()
     * @mixin _IH_OrderAddress_QB
     * @method static OrderAddressFactory factory(...$parameters)
     */
    class OrderAddress extends Model {}
    
    /**
     * @property int $id
     * @property string $comment
     * @property bool $customer_notified
     * @property int|null $order_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_OrderComment_QB onWriteConnection()
     * @method _IH_OrderComment_QB newQuery()
     * @method static _IH_OrderComment_QB on()
     * @method static _IH_OrderComment_QB query()
     * @method static _IH_OrderComment_QB with()
     * @method _IH_OrderComment_QB newModelQuery()
     * @method static _IH_OrderComment_C|OrderComment[] all()
     * @mixin _IH_OrderComment_QB
     */
    class OrderComment extends Model {}
    
    /**
     * @property int $id
     * @property string|null $sku
     * @property string|null $type
     * @property string|null $name
     * @property string|null $coupon_code
     * @property float|null $weight
     * @property float|null $total_weight
     * @property int|null $qty_ordered
     * @property int|null $qty_shipped
     * @property int|null $qty_invoiced
     * @property int|null $qty_canceled
     * @property int|null $qty_refunded
     * @property float $price
     * @property float $base_price
     * @property float $total
     * @property float $base_total
     * @property float $total_invoiced
     * @property float $base_total_invoiced
     * @property float $amount_refunded
     * @property float $base_amount_refunded
     * @property float|null $discount_percent
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property float|null $discount_invoiced
     * @property float|null $base_discount_invoiced
     * @property float|null $discount_refunded
     * @property float|null $base_discount_refunded
     * @property float|null $tax_percent
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float|null $tax_amount_invoiced
     * @property float|null $base_tax_amount_invoiced
     * @property float|null $tax_amount_refunded
     * @property float|null $base_tax_amount_refunded
     * @property int|null $product_id
     * @property string|null $product_type
     * @property int|null $order_id
     * @property int|null $parent_id
     * @property array|null $additional
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read $qty_to_cancel
     * @property-read $qty_to_invoice
     * @property-read $qty_to_refund
     * @property-read int $qty_to_ship
     * @property _IH_OrderItem_C|OrderItem[] $children
     * @property-read int $children_count
     * @method HasMany|_IH_OrderItem_QB children()
     * @property OrderItem|null $parent
     * @method BelongsTo|_IH_OrderItem_QB parent()
     * @property Model $product
     * @method MorphTo product()
     * @method static _IH_OrderItem_QB onWriteConnection()
     * @method _IH_OrderItem_QB newQuery()
     * @method static _IH_OrderItem_QB on()
     * @method static _IH_OrderItem_QB query()
     * @method static _IH_OrderItem_QB with()
     * @method _IH_OrderItem_QB newModelQuery()
     * @method static _IH_OrderItem_C|OrderItem[] all()
     * @mixin _IH_OrderItem_QB
     * @method static OrderItemFactory factory(...$parameters)
     */
    class OrderItem extends Model {}
    
    /**
     * @property int $id
     * @property string $method
     * @property string|null $method_title
     * @property int|null $order_id
     * @property array|null $additional
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_OrderPayment_QB onWriteConnection()
     * @method _IH_OrderPayment_QB newQuery()
     * @method static _IH_OrderPayment_QB on()
     * @method static _IH_OrderPayment_QB query()
     * @method static _IH_OrderPayment_QB with()
     * @method _IH_OrderPayment_QB newModelQuery()
     * @method static _IH_OrderPayment_C|OrderPayment[] all()
     * @mixin _IH_OrderPayment_QB
     * @method static OrderPaymentFactory factory(...$parameters)
     */
    class OrderPayment extends Model {}
    
    /**
     * @property int $id
     * @property string $transaction_id
     * @property string|null $status
     * @property string|null $type
     * @property string|null $payment_method
     * @property string|null $data
     * @property int $invoice_id
     * @property int $order_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property float|null $amount
     * @method static _IH_OrderTransaction_QB onWriteConnection()
     * @method _IH_OrderTransaction_QB newQuery()
     * @method static _IH_OrderTransaction_QB on()
     * @method static _IH_OrderTransaction_QB query()
     * @method static _IH_OrderTransaction_QB with()
     * @method _IH_OrderTransaction_QB newModelQuery()
     * @method static _IH_OrderTransaction_C|OrderTransaction[] all()
     * @mixin _IH_OrderTransaction_QB
     */
    class OrderTransaction extends Model {}
    
    /**
     * @property int $id
     * @property string|null $increment_id
     * @property string|null $state
     * @property bool $email_sent
     * @property int|null $total_qty
     * @property string|null $base_currency_code
     * @property string|null $channel_currency_code
     * @property string|null $order_currency_code
     * @property float|null $adjustment_refund
     * @property float|null $base_adjustment_refund
     * @property float|null $adjustment_fee
     * @property float|null $base_adjustment_fee
     * @property float|null $sub_total
     * @property float|null $base_sub_total
     * @property float|null $grand_total
     * @property float|null $base_grand_total
     * @property float|null $shipping_amount
     * @property float|null $base_shipping_amount
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float|null $discount_percent
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property int|null $order_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read mixed|string $status_label
     * @property Model $channel
     * @method MorphTo channel()
     * @property Model $customer
     * @method MorphTo customer()
     * @method static _IH_Refund_QB onWriteConnection()
     * @method _IH_Refund_QB newQuery()
     * @method static _IH_Refund_QB on()
     * @method static _IH_Refund_QB query()
     * @method static _IH_Refund_QB with()
     * @method _IH_Refund_QB newModelQuery()
     * @method static _IH_Refund_C|Refund[] all()
     * @mixin _IH_Refund_QB
     * @method static RefundFactory factory(...$parameters)
     */
    class Refund extends Model {}
    
    /**
     * @property int $id
     * @property string|null $name
     * @property string|null $description
     * @property string|null $sku
     * @property int|null $qty
     * @property float $price
     * @property float $base_price
     * @property float $total
     * @property float $base_total
     * @property float|null $tax_amount
     * @property float|null $base_tax_amount
     * @property float|null $discount_percent
     * @property float|null $discount_amount
     * @property float|null $base_discount_amount
     * @property int|null $product_id
     * @property string|null $product_type
     * @property int|null $order_item_id
     * @property int|null $refund_id
     * @property int|null $parent_id
     * @property array|null $additional
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property Model $product
     * @method MorphTo product()
     * @method static _IH_RefundItem_QB onWriteConnection()
     * @method _IH_RefundItem_QB newQuery()
     * @method static _IH_RefundItem_QB on()
     * @method static _IH_RefundItem_QB query()
     * @method static _IH_RefundItem_QB with()
     * @method _IH_RefundItem_QB newModelQuery()
     * @method static _IH_RefundItem_C|RefundItem[] all()
     * @mixin _IH_RefundItem_QB
     */
    class RefundItem extends Model {}
    
    /**
     * @property int $id
     * @property string|null $status
     * @property int|null $total_qty
     * @property int|null $total_weight
     * @property string|null $carrier_code
     * @property string|null $carrier_title
     * @property string|null $track_number
     * @property bool $email_sent
     * @property int|null $customer_id
     * @property string|null $customer_type
     * @property int $order_id
     * @property int|null $order_address_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property int|null $inventory_source_id
     * @property string|null $inventory_source_name
     * @property Model $customer
     * @method MorphTo customer()
     * @property InventorySource|null $inventory_source
     * @method BelongsTo|_IH_InventorySource_QB inventory_source()
     * @method static _IH_Shipment_QB onWriteConnection()
     * @method _IH_Shipment_QB newQuery()
     * @method static _IH_Shipment_QB on()
     * @method static _IH_Shipment_QB query()
     * @method static _IH_Shipment_QB with()
     * @method _IH_Shipment_QB newModelQuery()
     * @method static _IH_Shipment_C|Shipment[] all()
     * @mixin _IH_Shipment_QB
     * @method static ShipmentFactory factory(...$parameters)
     */
    class Shipment extends Model {}
    
    /**
     * @property int $id
     * @property string|null $name
     * @property string|null $description
     * @property string|null $sku
     * @property int|null $qty
     * @property int|null $weight
     * @property float|null $price
     * @property float|null $base_price
     * @property float|null $total
     * @property float|null $base_total
     * @property int|null $product_id
     * @property string|null $product_type
     * @property int|null $order_item_id
     * @property int $shipment_id
     * @property array|null $additional
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read $type
     * @property Model $product
     * @method MorphTo product()
     * @method static _IH_ShipmentItem_QB onWriteConnection()
     * @method _IH_ShipmentItem_QB newQuery()
     * @method static _IH_ShipmentItem_QB on()
     * @method static _IH_ShipmentItem_QB query()
     * @method static _IH_ShipmentItem_QB with()
     * @method _IH_ShipmentItem_QB newModelQuery()
     * @method static _IH_ShipmentItem_C|ShipmentItem[] all()
     * @mixin _IH_ShipmentItem_QB
     */
    class ShipmentItem extends Model {}
}

namespace Webkul\SocialLogin\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\SocialLogin\Models\_IH_CustomerSocialAccount_C;
    use LaravelIdea\Helper\Webkul\SocialLogin\Models\_IH_CustomerSocialAccount_QB;
    
    /**
     * @property int $id
     * @property string|null $provider_name
     * @property string|null $provider_id
     * @property int $customer_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_CustomerSocialAccount_QB onWriteConnection()
     * @method _IH_CustomerSocialAccount_QB newQuery()
     * @method static _IH_CustomerSocialAccount_QB on()
     * @method static _IH_CustomerSocialAccount_QB query()
     * @method static _IH_CustomerSocialAccount_QB with()
     * @method _IH_CustomerSocialAccount_QB newModelQuery()
     * @method static _IH_CustomerSocialAccount_C|CustomerSocialAccount[] all()
     * @mixin _IH_CustomerSocialAccount_QB
     */
    class CustomerSocialAccount extends Model {}
}

namespace Webkul\Tax\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Tax\Models\_IH_TaxCategory_C;
    use LaravelIdea\Helper\Webkul\Tax\Models\_IH_TaxCategory_QB;
    use LaravelIdea\Helper\Webkul\Tax\Models\_IH_TaxMap_C;
    use LaravelIdea\Helper\Webkul\Tax\Models\_IH_TaxMap_QB;
    use LaravelIdea\Helper\Webkul\Tax\Models\_IH_TaxRate_C;
    use LaravelIdea\Helper\Webkul\Tax\Models\_IH_TaxRate_QB;
    use Webkul\Tax\Database\Factories\TaxCategoryFactory;
    use Webkul\Tax\Database\Factories\TaxMapFactory;
    use Webkul\Tax\Database\Factories\TaxRateFactory;
    
    /**
     * @property int $id
     * @property string $code
     * @property string $name
     * @property string $description
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_TaxCategory_QB onWriteConnection()
     * @method _IH_TaxCategory_QB newQuery()
     * @method static _IH_TaxCategory_QB on()
     * @method static _IH_TaxCategory_QB query()
     * @method static _IH_TaxCategory_QB with()
     * @method _IH_TaxCategory_QB newModelQuery()
     * @method static _IH_TaxCategory_C|TaxCategory[] all()
     * @mixin _IH_TaxCategory_QB
     * @method static TaxCategoryFactory factory(...$parameters)
     */
    class TaxCategory extends Model {}
    
    /**
     * @property int $id
     * @property int $tax_category_id
     * @property int $tax_rate_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_TaxMap_QB onWriteConnection()
     * @method _IH_TaxMap_QB newQuery()
     * @method static _IH_TaxMap_QB on()
     * @method static _IH_TaxMap_QB query()
     * @method static _IH_TaxMap_QB with()
     * @method _IH_TaxMap_QB newModelQuery()
     * @method static _IH_TaxMap_C|TaxMap[] all()
     * @mixin _IH_TaxMap_QB
     * @method static TaxMapFactory factory(...$parameters)
     */
    class TaxMap extends Model {}
    
    /**
     * @property int $id
     * @property string $identifier
     * @property bool $is_zip
     * @property string|null $zip_code
     * @property string|null $zip_from
     * @property string|null $zip_to
     * @property string $state
     * @property string $country
     * @property float $tax_rate
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_TaxRate_QB onWriteConnection()
     * @method _IH_TaxRate_QB newQuery()
     * @method static _IH_TaxRate_QB on()
     * @method static _IH_TaxRate_QB query()
     * @method static _IH_TaxRate_QB with()
     * @method _IH_TaxRate_QB newModelQuery()
     * @method static _IH_TaxRate_C|TaxRate[] all()
     * @mixin _IH_TaxRate_QB
     * @method static TaxRateFactory factory(...$parameters)
     */
    class TaxRate extends Model {}
}

namespace Webkul\User\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\MorphToMany;
    use Illuminate\Notifications\DatabaseNotification;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Illuminate\Notifications\_IH_DatabaseNotification_C;
    use LaravelIdea\Helper\Illuminate\Notifications\_IH_DatabaseNotification_QB;
    use LaravelIdea\Helper\Webkul\User\Models\_IH_Admin_C;
    use LaravelIdea\Helper\Webkul\User\Models\_IH_Admin_QB;
    use LaravelIdea\Helper\Webkul\User\Models\_IH_Role_C;
    use LaravelIdea\Helper\Webkul\User\Models\_IH_Role_QB;
    use Webkul\User\Database\Factories\AdminFactory;
    
    /**
     * @property int $id
     * @property string $name
     * @property string $email
     * @property string|null $password
     * @property string|null $api_token
     * @property bool $status
     * @property int $role_id
     * @property string|null $remember_token
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _IH_DatabaseNotification_C|DatabaseNotification[] $notifications
     * @property-read int $notifications_count
     * @method MorphToMany|_IH_DatabaseNotification_QB notifications()
     * @method static _IH_Admin_QB onWriteConnection()
     * @method _IH_Admin_QB newQuery()
     * @method static _IH_Admin_QB on()
     * @method static _IH_Admin_QB query()
     * @method static _IH_Admin_QB with()
     * @method _IH_Admin_QB newModelQuery()
     * @method static _IH_Admin_C|Admin[] all()
     * @mixin _IH_Admin_QB
     * @method static AdminFactory factory(...$parameters)
     */
    class Admin extends Model {}
    
    /**
     * @property int $id
     * @property string $name
     * @property string|null $description
     * @property string $permission_type
     * @property array|null $permissions
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_Role_QB onWriteConnection()
     * @method _IH_Role_QB newQuery()
     * @method static _IH_Role_QB on()
     * @method static _IH_Role_QB query()
     * @method static _IH_Role_QB with()
     * @method _IH_Role_QB newModelQuery()
     * @method static _IH_Role_C|Role[] all()
     * @mixin _IH_Role_QB
     */
    class Role extends Model {}
}

namespace Webkul\Velocity\Models {

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_Category_C;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_Category_QB;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_ContentTranslation_C;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_ContentTranslation_QB;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_Content_C;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_Content_QB;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_OrderBrand_C;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_OrderBrand_QB;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_VelocityCustomerCompareProduct_C;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_VelocityCustomerCompareProduct_QB;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_VelocityMetadata_C;
    use LaravelIdea\Helper\Webkul\Velocity\Models\_IH_VelocityMetadata_QB;
    
    /**
     * @method static _IH_Category_QB onWriteConnection()
     * @method _IH_Category_QB newQuery()
     * @method static _IH_Category_QB on()
     * @method static _IH_Category_QB query()
     * @method static _IH_Category_QB with()
     * @method _IH_Category_QB newModelQuery()
     * @method static _IH_Category_C|Category[] all()
     * @mixin _IH_Category_QB
     */
    class Category extends Model {}
    
    /**
     * @property int $id
     * @property string|null $content_type
     * @property int|null $position
     * @property bool $status
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_Content_QB onWriteConnection()
     * @method _IH_Content_QB newQuery()
     * @method static _IH_Content_QB on()
     * @method static _IH_Content_QB query()
     * @method static _IH_Content_QB with()
     * @method _IH_Content_QB newModelQuery()
     * @method static _IH_Content_C|Content[] all()
     * @mixin _IH_Content_QB
     */
    class Content extends Model {}
    
    /**
     * @property int $id
     * @property int|null $content_id
     * @property string|null $title
     * @property string|null $custom_title
     * @property string|null $custom_heading
     * @property string|null $page_link
     * @property bool $link_target
     * @property string|null $catalog_type
     * @property string|null $products
     * @property string|null $description
     * @property string|null $locale
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_ContentTranslation_QB onWriteConnection()
     * @method _IH_ContentTranslation_QB newQuery()
     * @method static _IH_ContentTranslation_QB on()
     * @method static _IH_ContentTranslation_QB query()
     * @method static _IH_ContentTranslation_QB with()
     * @method _IH_ContentTranslation_QB newModelQuery()
     * @method static _IH_ContentTranslation_C|ContentTranslation[] all()
     * @mixin _IH_ContentTranslation_QB
     */
    class ContentTranslation extends Model {}
    
    /**
     * @property int $id
     * @property int|null $order_id
     * @property int|null $order_item_id
     * @property int|null $product_id
     * @property int|null $brand
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_OrderBrand_QB onWriteConnection()
     * @method _IH_OrderBrand_QB newQuery()
     * @method static _IH_OrderBrand_QB on()
     * @method static _IH_OrderBrand_QB query()
     * @method static _IH_OrderBrand_QB with()
     * @method _IH_OrderBrand_QB newModelQuery()
     * @method static _IH_OrderBrand_C|OrderBrand[] all()
     * @mixin _IH_OrderBrand_QB
     */
    class OrderBrand extends Model {}
    
    /**
     * @property int $id
     * @property int $product_flat_id
     * @property int $customer_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method static _IH_VelocityCustomerCompareProduct_QB onWriteConnection()
     * @method _IH_VelocityCustomerCompareProduct_QB newQuery()
     * @method static _IH_VelocityCustomerCompareProduct_QB on()
     * @method static _IH_VelocityCustomerCompareProduct_QB query()
     * @method static _IH_VelocityCustomerCompareProduct_QB with()
     * @method _IH_VelocityCustomerCompareProduct_QB newModelQuery()
     * @method static _IH_VelocityCustomerCompareProduct_C|VelocityCustomerCompareProduct[] all()
     * @mixin _IH_VelocityCustomerCompareProduct_QB
     */
    class VelocityCustomerCompareProduct extends Model {}
    
    /**
     * @property int $id
     * @property string $home_page_content
     * @property string $footer_left_content
     * @property string $footer_middle_content
     * @property bool $slider
     * @property string|null $advertisement
     * @property int $sidebar_category_count
     * @property int $featured_product_count
     * @property int $new_products_count
     * @property string|null $subscription_bar_content
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $product_view_images
     * @property string|null $product_policy
     * @property string $locale
     * @property string $channel
     * @property string $header_content_count
     * @method static _IH_VelocityMetadata_QB onWriteConnection()
     * @method _IH_VelocityMetadata_QB newQuery()
     * @method static _IH_VelocityMetadata_QB on()
     * @method static _IH_VelocityMetadata_QB query()
     * @method static _IH_VelocityMetadata_QB with()
     * @method _IH_VelocityMetadata_QB newModelQuery()
     * @method static _IH_VelocityMetadata_C|VelocityMetadata[] all()
     * @mixin _IH_VelocityMetadata_QB
     */
    class VelocityMetadata extends Model {}
}